<?php
namespace QuestPC\Dbg;

function appendLog( $fname, $fdata ) {
	if ( ($f = fopen( $fname, 'ab+' )) === false ) {
		return;
	}
	fputs( $f, "{$fdata}\n\n" );
	fclose( $f );
}

/**
 * Conditional logging.
 */
function logCond(/* $varname, $var, $debug = false, $fname = 'sdv.out' */) {
	global $appContext;
	$defaultLog = ( isset( $appContext ) ) ?
		$appContext->sdvDebugLog : 'sdv.out';
	$args = func_get_args();
	$debug = array_key_exists( 2, $args ) ? $args[2] : false;
	$fname = array_key_exists( 3, $args ) ? $args[3] : $defaultLog;
	$fname = str_replace( '\\', '/', $fname );
	if ( $appContext->debugging === false || $debug !== true ) {
		return;
	}
	if ( array_key_exists( 1, $args ) ) {
/*
		# variant that is non-problematic with external ob handler
		$fdata = "\${$args[0]}=" . var_export( $args[1], true );
*/
		# variant that is non-problematic with recursion
		ob_start();
		var_dump( $args[1] );
		$fdata = $args[0] . ' = ' . ob_get_clean();
	} else {
		$fdata = $args[0];
	}
	appendLog( $fname, $fdata );
}

function backtrace( $debug = false ) {
	$stack = debug_backtrace( false );
	foreach ( $stack as &$element ) {
		unset( $element['args'] );
	}
	logCond( "backtrace", $stack, $debug );
}

function log(/* $varname, $var */) {
	$args = func_get_args();
	if ( count( $args ) > 1 ) {
		logCond( $args[0], $args[1], true );
	} else {
		global $appContext;
		$defaultLog = ( isset( $appContext ) ) ?
			$appContext->sdvDebugLog : 'sdv.out';
		appendLog( $defaultLog, $args[0] );
	}
}

function except( \Exception $e ) {
	global $appContext;
	$appContext->debugging = true;
	if ( $e instanceof \QuestPC\SdvException ) {
		log(__METHOD__.':message:'.$e->getMessage(), serialize( $e->getData() ) );
	} else {
		log(__METHOD__.':message',$e->getMessage());
	}
	if ( $e instanceof \QuestPC\SdvException ) {
		log(__METHOD__.':extendedCode',$e->getExtendedCode());
	}
}

function except_die( \Exception $e ) {
	except( $e );
	dump_request_time();
	die( 'Oops.. see log' );
}

function dump_sql_time( $requestTime ) {
	global $appContext;
	$mysqlTime = property_exists( $appContext, 'dbwTimer' ) ?
		$appContext->dbwTimer->getTotal() : 0;
	$sphinxTime = property_exists( $appContext, 'sphinxTimer' ) ?
		$appContext->sphinxTimer->getTotal() : 0;
	$curlTime = $appContext->profiler->getTotal('curl');
	if ( $mysqlTime > 0 ) {
		log('### mysql time ###',round( $mysqlTime, 7 ));
	}
	if ( $sphinxTime > 0 ) {
		log('### sphinx time ###',round( $sphinxTime, 7 ));
	}
	if ( $curlTime > 0 ) {
		log('### curl time ###',round( $curlTime, 7 ));
	}
	log('### internal time ###',
		round( $requestTime - $mysqlTime - $sphinxTime - $curlTime, 7 )
	);
}

/**
 * @return boolean
 *   true: serving time was too slow
 *   false: serving time is not too slow
 */
function dump_request_time() {
	global $appContext;
	if ( !property_exists( $appContext, 'profiler' ) ) {
		# Most probably it's a cli run where Init.php was not included.
		return false;
	}
	$result = false;
	$requestTime = $appContext->profiler->stop( 'request' );
	if ( $requestTime > 4 ) {
		# Always log slow requests.
		log('*** serving time is slow ***', $requestTime);
		dump_sql_time($requestTime);
		$result = true;
	} elseif ( $appContext->debugging ) {
		log('### serving time ###', $requestTime);
		dump_sql_time($requestTime);
	}
	if ( !property_exists( $appContext, 'router' ) ) {
		# Most probably it's a cli run.
		return $result;
	}
	if ( $result ) {
		log( '*** slow request uri ***', $appContext->router->getRequestUri() );
	} else {
		if ( $appContext->debugging ) {
			log( '### request uri ###', $appContext->router->getRequestUri() );
		}
	}
	return $result;
}
