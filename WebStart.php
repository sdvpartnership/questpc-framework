<?php
namespace QuestPC;

if ( !isset( $appContext ) ) {
	die( 'Undefined application context.' );
}

if ( php_sapi_name() === 'cli' ) {
	die( 'Not designed to run in cli mode.' );
}

try {
	// The order is important, because Router uses Request.
	if ( !property_exists( $appContext, 'request' ) ) {
		$appContext->request = WebRequest::singleton();
	}
	if ( !property_exists( $appContext, 'response' ) ) {
		$appContext->response = WebResponse::singleton();
	}
	$appContext->router = new Router();
	$appContext->router->setRequestUri();
	$route = $appContext->router->getRoute( $appContext->routeList );
	if ( $route === false ) {
		ShutdownException::terminate( 'Termination due to unknown route', __METHOD__, $appContext->router->getRequestUri() );
	} else {
		$ctrl = $appContext->router->doRoute( $route );
		$appContext->response->add( strval( $ctrl ) );
	}
} catch ( \Exception $e ) {
	Dbg\except($e);
	Gl::checkShutdown( $e );
}
$appContext->response->output();

Dbg\dump_request_time();
