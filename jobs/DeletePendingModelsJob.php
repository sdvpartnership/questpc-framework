<?php
namespace QuestPC;

abstract class DeletePendingModelsJob extends BaseJob {

	protected $pagerQuery;
	protected $collectionClassName;

	public function execute() {
		$pager = new DbPager();
		call_user_func_array( array( $pager, 'setPagerQuery' ), $this->pagerQuery );
		$reflect = new \ReflectionClass(
			AutoLoader::getFqnClassName( $this->collectionClassName )
		);
		$collection = $reflect->newInstance();
		do {
			$pager->setOffset( 0 );
			$rows = $pager->getCurrPage();
			$collection->clear();
			foreach ( $rows as $row ) {
				$model = $collection->newSeparateModel();
				$model->loadPrimaryRow( $row );
				if ( $model->isPendingDelete() ) {
					$collection->addModel( $model );
				}
			}
			if ( count( $collection ) < 1 ) {
				# Everything that is not worth to keep is deleted.
				break;
			}
			# Dbg\log('collection',$collection);
			$collection->delete();
		} while ( true );
	}

} /* end of DeletePendingModelsJob class */
