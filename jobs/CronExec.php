<?php
namespace QuestPC;

/**
 * Stores and executes one cron job.
 */
class CronExecJob extends FeaturesObject {

	# unique id of job
	protected $jobId;

	# Amount of minutes in one tick.
	protected $tickLen;
	# Current (awaiting) job tick number (per day since 00:00).
	protected $currTick = -1;
	# Last executed job tick number (per day since 00:00).
	protected $lastTick = null;
	# CronTimeParts instance optionally used
	# to override current time for:
	#  1. debugging;
	#  2. running virtual jobs.
	protected $overrideTime;
	# Whether the job is disabled via .ctrl file.
	protected $active = true;
	# Minutes left till tick should fire.
	protected $minutesLeft = null;
	# Current tick job class names selected via periods
	# from ->cronPeriods instances.
	protected $currentJobClassNames = array();

	# Array of CronPeriod instances.
	protected $cronPeriods = array();

	protected $scriptPath;
	# Place 'stop' string at the very begin of control file to stop the job execution.
	protected $ctrlFilePath;
	# stdout / stderr of job will be redirected to that file.
	# Job is run forked in separate process.
	protected $outputFilePath;

	/**
	 * Adds new job
	 * @param $jobId  string
	 * @param $tickLen int
	 *   a length of one tick in minutes
	 * @param $overrideTime
	 *   used to override current time with specified time
	 */
	function __construct( $jobId, CronTimeParts $overrideTime ) {
		global $appContext;
		$this->jobId = strval( $jobId );
		$this->tickLen = $overrideTime->getTickLen();
		$this->overrideTime = $overrideTime;
		$this->scriptPath = "{$appContext->IP}/maintenance/runJobs.php";
		$this->ctrlFilePath = "{$appContext->IP}/maintenance/{$jobId}.ctrl";
		$this->outputFilePath = "{$appContext->IP}/maintenance/logs/{$jobId}.out";
	}

	/**
	 * Adds period definition and class names to execute when
	 * current CronTimeParts values will match the period definition.
	 * @param $periodDef string
	 *   period definition (see CronPeriod class)
	 * @param $classNames array
	 *   names of BaseJob descendant classes to execute when period matches.
	 */
	function addCronPeriod( $periodDef, array $classNames ) {
		$cronPeriod = new CronPeriod(
			$this->overrideTime->isVirtual(),
			$periodDef
		);
		if ( $cronPeriod->addClassNames( $classNames ) > 0 ) {
			$this->cronPeriods[] = $cronPeriod;
		}
	}

	/**
	 * Checks, whether control file forces job to stop
	 */
	function isActive() {
		if ( $this->active ) {
				@touch( $this->ctrlFilePath );
			$this->active = !preg_match( '/^stop/', @file_get_contents( $this->ctrlFilePath ) );
		}
		return $this->active;
	}

	/**
	 * Execute scheduled classes for time specified.
	 */
	function executeInterval( CronTimeParts $time ) {
		# only real time is allowed
		$time->tryIsReal();
		$this->lastTick = $time->tick;
		# check periods and gather class names
		$this->currentJobClassNames = array();
		foreach ( $this->cronPeriods as $cronPeriod ) {
			if ( $cronPeriod->intervalMatches( $time ) ) {
				$this->currentJobClassNames = array_merge( $this->currentJobClassNames, $cronPeriod->getProp( 'classNames' ) );
			}
		}
		if ( count( $this->currentJobClassNames ) > 0 ) {
			# there are classes pending to instantiate via runJobs.php
			$shellArgs = array_merge(
				array( 'php', $this->scriptPath, $this->jobId, $time->dow, $time->tick ),
				$this->currentJobClassNames
			);
			$cmd = Gl::escapeShellParameters( $shellArgs );
			# Dbg\log(__METHOD__,$cmd);
			Gl::execBackground( $cmd, $this->outputFilePath );
		}
		# Virtual run should not be overridden.
		if ( !$this->overrideTime->isVirtual() ) {
			# We're overriding just once. Stop overriding if there's any.
			$this->overrideTime->stopOverriding();
		}
		$this->minutesLeft = 0;
	}

	/**
	 * @param $dow int
	 *   day of the week (1 - monday, 7 - sunday)
	 * @param @minutesInDay int
	 *   number of minutes in day passed since 00:00
	 */
	function startIfNeeded( $dow, $minutesInDay ) {
		# Either use overriden tick or current tick from $minutesInDay.
		$this->currTick = is_int( $this->overrideTime->tick ) ?
			$this->overrideTime->tick : intval( floor( $minutesInDay / $this->tickLen ) );
		# Virtual run should repeat the same tick again and again.
		# Note that virtual run is always overriden one.
		# Normal overriden run should happen only once.
		if ( $this->overrideTime->isVirtual() ||
				!$this->overrideTime->isOverriden() ) {
			# Check whether a time matches tick by rounding to 1 (not to zero)
			# to make less probability of skipping a job under load.
			$this->minutesLeft = $this->tickLen - ($minutesInDay % $this->tickLen) - 1;
			# Uncomment next line for debugging:
			# echo "minutesleft:{$this->minutesLeft}\ntickLen:{$this->tickLen}\n\n";
			if ( ($this->minutesLeft === $this->tickLen - 1) &&
				$this->overrideTime->isVirtual() ) {
				/**
				 * For virtual run, reset ->lastTick value just after last execution
				 * (if there was any) otherwise the job will stay in "already executed"
				 * state forever.
				 */
				$this->lastTick = null;
			}
			if ( $this->minutesLeft > 0 ) {
				# Not a begin of tick.
				return;
			}
		}
		if ( $this->lastTick === $this->currTick ) {
			# We have already checked that tick.
			$this->minutesLeft = -1;
			return;
		}
		$execTime = new CronTimeParts();
		$execTime->tick = $this->currTick;
		# Either use overriden dow or passed real dow.
		$execTime->dow = is_int( $this->overrideTime->dow ) ? $this->overrideTime->dow : $dow;
		$this->executeInterval( $execTime );
	}

} /* end of CronExecJob class */

/**
 * Pseudo-cron compatible both to win / linux.
 * Can create multiple jobs for different tick intervals and job class names.
 */
class CronExec {

	protected $jobs = array();

	/**
	 * @param $jobId string
	 * @param $tickLen int
	 *   amount of minutes in one tick
	 * @param $jobClassDefs array
	 *   key: period definition string
	 *   value: mixed
	 *     string class name
	 *     array of string class names
	 */
	function addJob( $jobId, CronTimeParts $overrideTime, array $jobClassDefs ) {
		# ->overrideTime has to be cloned, otherwise single instance of ->overrideTime
		# will be shared by multiple jobs, resulting in nasty bugs.
		$job = new CronExecJob( $jobId, clone $overrideTime );
		foreach ( $jobClassDefs as $periodDef => $jobClassNames ) {
			if ( !is_array( $jobClassNames ) ) {
				$jobClassNames = array( $jobClassNames );
			}
			$job->addCronPeriod( $periodDef, $jobClassNames );
		}
		$this->jobs[$job->getProp( 'jobId' )] = $job;
	}

	/**
	 * Poll jobs, so the CPU time won't be wasted too much.
	 */
	function poll() {
		do {
			$totalJobsCount = 0;
			$inactiveJobsCount = 0;
			foreach ( $this->jobs as $jobId => $job ) {
				$totalJobsCount++;
				$wasActive = $job->getProp( 'active' );
				if ( !$job->isActive() ) {
					if ( $wasActive ) {
						echo "Job {$jobId} become inactive\n";
					}
					$inactiveJobsCount++;
					continue;
				}
				$oldMinutesLeft = $job->getProp( 'minutesLeft' );
				$job->startIfNeeded( intval( date( 'N' ) ), Gl::getMinutesInDay() );
				if ( ($minutesLeft = $job->getProp( 'minutesLeft' )) > 0 ) {
					$currTick = $job->getProp( 'currTick' );
					if ( $minutesLeft !== $oldMinutesLeft ) {
						echo "{$minutesLeft} minutes till execution of {$jobId} tick {$currTick}\n";
					}
				} elseif ( $minutesLeft === 0 ) {
					$lastJobClassNames = $job->getProp( 'currentJobClassNames' );
					$lastTick = $job->getProp( 'lastTick' );
					if ( count( $lastJobClassNames ) > 0 ) {
						echo "Executing {$jobId} with tick number {$lastTick}\nwith classNames " . implode( ',', $lastJobClassNames ) . "\n";
					} else {
						if ( !isset( $currTick ) ) {
							echo "No more active jobs\n";
							break;
						}
						echo "Job {$jobId} tick number {$lastTick} has no active classNames for tick {$currTick}\n";
					}
				} else {
					echo "Already checked {$jobId}\n";
				}
			}
			if ( $inactiveJobsCount === $totalJobsCount ) {
				echo "No more active jobs\n";
				break;
			}
			# Sleep for 1/3 + 1 of a minute: we do not want to miss a job.
			# However we also do not want to have "Already executing" state
			# more than twice.
			sleep( 21 );
		} while ( true );
	}

} /* end of CronExec class */
