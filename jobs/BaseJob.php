<?php
namespace QuestPC;

/**
 * Abstract regular job.
 */
abstract class BaseJob {

	protected $jobGroup;
	protected $time;

	function __construct( $jobGroup, CronTimeParts $time ) {
		$this->jobGroup = $jobGroup;
		$this->time = $time;
	}

	function setDebugLog( $logPrefix ) {
		global $appContext;
		# daily rotate for default Dbg\*() log file path
		# note: do not forget that runJobs.php initially runs with default Dbg\*() log file path!
		$appContext->sdvDebugLog = "{$appContext->IP}/maintenance/logs/{$logPrefix}." . Gl::timestamp( 'Y-m-d' ) . ".dbg";
	}

	function execute() {
		/* noop */
	}

} /* end of BaseJob class */


/**
 * Virtual jobs should be inherited from this class to distinguish these
 * from regular jobs.
 */
abstract class VirtualJob extends BaseJob {
} /* end of VirtualJob class */
