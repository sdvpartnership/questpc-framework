<?php
namespace QuestPC;

/**
 * Stores and validates cron time parts:
 * day of week, daily tick number.
 */
class CronTimeParts {

	# 24 * 60
	const MINUTES_IN_DAY = 1440;

	/**
	 * CronTimeParts magic __get/__set property values:
	 * false : use actual dow/tick number with poll delay;
	 * true  : use actual dow/tick number without poll delay;
	 * int   : use interger dow/tick value without poll delay
	 *         (overrides current time).
	 */
	protected $parts = array();
	protected $tickLen;

	function __construct() {
		# day of the week number
		$this->parts['dow']  = false;
		# daily tick number (interval number)
		$this->parts['tick'] = false;
	}

	/**
	 * Please note that tick length is optional and thus is not set for
	 * all instances.
	 * It is used by CronExec schedule;
	 * Unused when passing CronTimeParts instance to jobs.
	 */
	public function setTickLen( $tickLen ) {
		if ( !is_int( $tickLen ) || $tickLen < 1 ) {
			SdvException::throwError( 'tick length (in minutes) must be positive int',
				__METHOD__, $tickLen );
		}
		$this->tickLen = $tickLen;
	}

	public function getTickLen() {
		return $this->tickLen;
	}

	public function __set( $name, $value ) {
		if ( !is_bool( $value ) && ( !is_int( $value ) || $value < 0 ) ) {
			# Dbg\log(__METHOD__,array($name,$value));
			SdvException::throwError( "value of property must be boolean or positive int", __METHOD__, array( $name, $value ) );
		}
		$this->parts[$name] = $value;
	}

	public function __get( $name ) {
		if ( !array_key_exists( $name, $this->parts ) ) {
			SdvException::throwError( "specified property name does not exists", __METHOD__, $name );
		}
		return $this->parts[$name];
	}

	public function isOverriden() {
		return ( $this->parts['dow'] !== false || $this->parts['tick'] !== false );
	}

	public function isVirtual() {
		if ( !is_int( $this->parts['tick'] ) ) {
			return false;
		}
		return $this->parts['tick'] * $this->tickLen > self::MINUTES_IN_DAY;
	}

	public function stopOverriding() {
		$this->parts['dow'] =
		$this->parts['tick'] = false;
	}

	public function isReal() {
		return is_int( $this->parts['dow'] ) && is_int( $this->parts['tick'] );
	}

	public function tryIsReal() {
		if ( !$this->isReal() ) {
			SdvException::throwError( 'Instance has boolean (virtual) time. Real (integer) time is required.', __METHOD__, $this );
		}
	}

} /* end of CronTimeParts class */

/**
 * Parses and matches one cron period definition.
 */
class CronPeriod extends FeaturesObject {

	# Class names to instantiate when current time matches mode.
	protected $classNames = array();
	# default mode of daily execution 'at' / 'each'
	protected $weekdayMode = 'each';
	# default value for mode of execution (each day)
	protected $weekdayValue = 1;
	# default mode of tick execution 'at' / 'each'
	protected $tickMode = 'each';
	# default value for mode of execution (each tick)
	protected $tickValue = 1;
	# Whether current tick number is in length of the day or
	# above the length of the day;
	protected $isVirtualRun;

	/**
	 * @param $isVirtualRun boolean
	 *   whether current tick number is in length of the day or
	 *   above the length of the day;
	 * @param $periodDef string
	 *   '*' - execute each tick (the same as 'each_tick 1');
	 *   'at_tick num' - execute only at specified tick once per day;
	 *   'each_tick num' - execute at each nth tick per day;
	 *   'at_weekday num' - execute only at specified day of the week;
	 */
	function __construct( $isVirtualRun, $periodDef ) {
		$this->isVirtualRun = $isVirtualRun;
		# Dbg\log(__METHOD__.':isVirtualRun',$this->isVirtualRun);
		# parse period definition into ->mode and ->value
		preg_match_all( '/(?:(at_tick|each_tick|at_weekday)\s*(\d+)|(\*))/', $periodDef, $matches, PREG_SET_ORDER );
		# Dbg\log(__METHOD__.':matches',$matches);
		foreach ( $matches as $match ) {
			$this->addMatch( $match );
		}
	}

	/**
	 * @param $classNames array
	 * @return int
	 *   count of added job class names
	 */
	function addClassNames( array $classNames ) {
		foreach ( $classNames as $className ) {
			# Dbg\log(__METHOD__.':className',$className);
			$reflect = new \ReflectionClass( AutoLoader::getFqnClassName( $className ) );
			# safety check for class names
			if ( !$reflect->isSubclassOf( __NAMESPACE__ . '\\BaseJob' ) ) {
				SdvException::throwError( 'job class is not descendant of BaseJob', basename( __FILE__ ), $className );
			}
			# Run virtual jobs only when tickValue is virtual:
			# "at" is longer than day.
			if ( $this->isVirtualRun ) {
				if ( $reflect->isSubclassOf( __NAMESPACE__ . '\\VirtualJob' ) ) {
					# Dbg\log(__METHOD__.':added VirtualJob',$className);
					$this->classNames[] = $className;
				}
			} else {
				# Dbg\log(__METHOD__.':added BaseJob',$className);
				$this->classNames[] = $className;
			}
		}
		return count( $this->classNames );
	}

	/**
	 * fixme: currently only single mode / value pair is supported.
	 */
	protected function addMatch( $match ) {
		if ( count( $match ) === 4 ) {
			/* '*' case */
			$this->weekdayMode = $this->tickMode = 'each';
			$this->weekdayValue = $this->tickValue = 1;
		} elseif ( count( $match ) === 3 ) {
			/* 'at/each' case */
			switch ( $match[1] ) {
			case 'at_tick' :
				$this->tickMode = 'at';
				$this->tickValue = intval( $match[2] );
				break;
			case 'each_tick' :
				$this->tickMode = 'each';
				$this->tickValue = intval( $match[2] );
				break;
			case 'at_weekday' :
				$this->weekdayMode = 'at';
				$this->weekdayValue = intval( $match[2] );
				break;
			default :
				SdvException::throwError( 'invalid definition of execution period', __METHOD__, $match[1] );
			}
		} else {
			SdvException::throwError( 'invalid definition of execution period', __METHOD__, $match[1] );
		}
	}

	function intervalMatches( CronTimeParts $time ) {
		$tickMatches = false;
		if ( $this->tickMode === 'at' ) {
			$tickMatches = ($this->tickValue === $time->tick);
		} else { /* $this->mode === 'each' */
			$tickMatches = (($time->tick % $this->tickValue) === 0);
		}
		if ( $this->weekdayMode === 'at' ) {
			return ($this->weekdayValue === $time->dow) && $tickMatches;
		} else { /* $this->mode === 'each' */
			return (($time->dow % $this->weekdayValue) === 0) && $tickMatches;
		}
	}

} /* end of CronPeriod */
