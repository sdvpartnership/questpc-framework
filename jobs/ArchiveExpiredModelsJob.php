<?php
namespace QuestPC;

abstract class ArchiveExpiredModelsJob extends BaseJob {

	protected $pagerQuery;
	# Non-archived (actual) collection.
	protected $srcCollectionClassName;
	# Collection with archived items.
	# Usually has only different primary table name with the same fields and keys.
	protected $dstCollectionClassName;

	public function execute() {
		$pager = new DbPager();
		call_user_func_array( array( $pager, 'setPagerQuery' ), $this->pagerQuery );
		$reflect = new \ReflectionClass(
			AutoLoader::getFqnClassName( $this->srcCollectionClassName )
		);
		$srcCollection = $reflect->newInstance();
		$reflect = new \ReflectionClass(
			AutoLoader::getFqnClassName( $this->dstCollectionClassName )
		);
		$dstCollection = $reflect->newInstance();
		do {
			$pager->setOffset( 0 );
			$rows = $pager->getCurrPage();
			$srcCollection->clear();
			$dstCollection->clear();
			foreach ( $rows as $row ) {
				$srcModel = $srcCollection->newSeparateModel();
				$srcModel->loadPrimaryRow( $row );
				if ( $srcModel->isExpiredByTime() ) {
					$srcCollection->addModel( $srcModel );
				}
			}
			if ( count( $srcCollection ) < 1 ) {
				# Everything that is not worth to keep is deleted.
				break;
			}
			# Dbg\log('srcCollection',$srcCollection);
			$srcCollection->loadAllByPkTable();
			foreach ( $srcCollection as $srcModel ) {
					$dstModel = $dstCollection->newSeparateModel();
					$srcModel->copyTo( $dstModel );
					# Dbg\log(__METHOD__.':srcModel',$srcModel);
					# Dbg\log(__METHOD__.':dstModel',$dstModel);
					$dstCollection->addModel( $dstModel );
					/**
					 * @note: Optionally suppress or control ->preload() and ->destroy()
					 *        when external ->fs is used in models.
					 */
					$srcModel->setPendingDelete( true );
			}
			$srcCollection->delete();
			$dstCollection->update();
		} while ( true );
	}

} /* end of ArchiveExpiredModelsJob class */
