<?php
namespace QuestPC;

abstract class AbstractChoiceEdit {

	protected $field;
	protected $rowNum;
	protected $userChoice;
	protected $result;

	function __construct( AbstractField $field, $rowNum = null ) {
		$this->field = $field;
		$this->rowNum = $rowNum;
		$this->userChoice = $field->source->getChoice();
		$this->generate();
	}

	public function getTagArray() {
		$fieldError = $this->field->source->getError( $this->rowNum );
		if ( $fieldError !== false ) {
			XmlTree::addClass( $this->result, 'field-error' );
			$this->result['title'] = $fieldError->getMessage();
		}
		return $this->result;
	}

} /* end of AbstractChoiceEdit class */

class RadioChoiceEdit extends AbstractChoiceEdit {

	protected function generate() {
		$inputs = array( '@len' => 0 );
		$len = &$inputs['@len'];
		# @todo: use AccordeonFormView for such fields?
		# @note: actual only for non-JS form UI
		if ( $this->field->source->getEmptyTo() !== false ) {
			# Make it possible to select empty value (clear data) for
			# ->emptyTo enabled field source.
			$inputs[$len++] = array(
				'@tag' => 'option',
				'value' => '',
				''
			);
		}
		foreach ( $this->field->defaultValues as $text => $value ) {
			$inputs[$len] = array(
				'@tag' => 'option',
				'value' => $value,
				$text
			);
			if ( $this->userChoice !== null && $value == $this->userChoice ) {
				$inputs[$len]['selected'] = 'selected';
			}
			if ( $this->field->optional ) {
				$inputs[$len]['disabled'] = 'disabled';
			}
			$len++;
		}
		$this->result = array(
			'@tag' => 'select',
			'autocomplete' => $this->field->autocomplete,
			'name' => $this->field->source->getInputNames(),
			$inputs
		);
	}

} /* end of RadioChoiceEdit class */

class CheckboxChoiceEdit extends AbstractChoiceEdit {

	protected function generate() {
		$this->result = array( '@len' => 0 );
		$len = &$this->result['@len'];
		$inputName = $this->field->source->getInputNames();
		if ( !is_array( $this->field->defaultValues ) ) {
			SdvException::throwError(
				'You forgot to set field defaultValues to array during creation via ::mvc() or at later stage via ::setDefaultValues()',
				__METHOD__,
				$this->field
			);
		}
		foreach ( $this->field->defaultValues as $text => $value ) {
			$input = array(
				'@tag' => 'input',
				'autocomplete' => $this->field->autocomplete,
				'type' => 'checkbox',
				'id' => "{$inputName}_{$len}",
				'name' => "{$inputName}[]",
				'value' => $value
			);
			if ( is_array( $this->userChoice ) && in_array( $value, $this->userChoice ) ) {
				$input['checked'] = 'checked';
			}
			if ( $this->field->optional ) {
				$input['disabled'] = 'disabled';
			}
			$this->result[$len] = array(
				'@tag' => 'div',
				$input,
				array(
					'@tag' => 'label',
					'for' => "{$inputName}_{$len}",
					$text
				)
			);
			$len++;
		}
	}

} /* end of CheckboxChoiceEdit class */

class ScalarChoiceEdit extends AbstractChoiceEdit {

	protected static $inputTpl = array(
		'@tag' => 'input',
		'type' => 'text',
		'class' => 'input-scalar',
	);
	# Place scalar choice into tagarray 'value' attribute.
	protected static $valueKey = 'value';
	# Attribute key for width of field, if size was specified.
	protected static $widthKey = 'size';
	# Attribute key for height of field, if rownum was specified.
	protected static $heightKey = null;

	protected function generate() {
		# Dbg\log(__METHOD__.':field',$this->field);
		/**
		 * Translate null-value of userChoice which indicates
		 * non-set value usually loaded from model into
		 * default value.
		 * Usually null is converted to '' (see ScalarField::setDefaultValues()),
		 * but arbitrary scalar value may be specified as well.
		 */
		$value = ($this->userChoice === null) ? $this->field->defaultValues : $this->userChoice;
		$this->result = static::$inputTpl;
		$this->result += array(
			'autocomplete' => $this->field->autocomplete,
			'name' => $this->field->source->getInputNames(),
			static::$valueKey => $value
		);
		if ( isset( $this->field->size ) ) {
			/**
			 * this->result[@size] "overrides" input css width,
			 * when defined for particular field.
			 */
			$this->result[static::$widthKey] = $this->field->size;
		}
		if ( isset( $this->field->rownum ) ) {
			if ( static::$heightKey === null ) {
				SdvException::throwError( 'Unknown input height key', __METHOD__, $field );
			}
			$this->result[static::$heightKey] = $this->field->rownum;
		}
		if ( $this->field->optional ) {
			$this->result['disabled'] = 'disabled';
		}
		# Dbg\log(__METHOD__.':tagarray',$this->result);
	}

} /* end of ScalarChoiceEdit class */

class HiddenChoiceEdit extends ScalarChoiceEdit {

	protected static $inputTpl = array(
		'@tag' => 'input',
		'type' => 'hidden',
	);

} /* end of HiddenChoiceEdit class */

class TextChoiceEdit extends ScalarChoiceEdit {

	protected static $inputTpl = array(
		'@tag' => 'textarea',
	);
	# Place scalar choice into tagarray first inner text node.
	protected static $valueKey = 0;
	# Attribute key for width of field, if size was specified.
	protected static $widthKey = 'cols';
	# Attribute key for height of field, if rownum was specified.
	protected static $heightKey = 'rows';

} /* end of TextChoiceEdit class */

class RangeChoiceEdit extends AbstractChoiceEdit {

	protected function generate() {
		$inputNames = $this->field->source->getInputNames();
		$min = $this->field->defaultValues['min'];
		$max = $this->field->defaultValues['max'];
		if ( $this->userChoice !== null ) {
			if ( $this->userChoice['min'] !== null ) {
				$min = $this->userChoice['min'];
			}
			if ( $this->userChoice['max'] !== null ) {
				$max = $this->userChoice['max'];
			}
		}
		$from = array(
			'@tag' => 'input',
			'autocomplete' => $this->field->autocomplete,
			'type' => 'text',
			'name' => $inputNames['min'],
			'value' => $min
		);
		if ( isset( $this->field->size ) ) {
			$from['size'] = $this->field->size;
		}
		$to = array(
			'@tag' => 'input',
			'autocomplete' => $this->field->autocomplete,
			'type' => 'text',
			'name' => $inputNames['max'],
			'value' => $max
		);
		if ( isset( $this->field->size ) ) {
			$to['size'] = $this->field->size;
		}
		if ( $this->field->optional ) {
			$from['disabled'] = $to['disabled'] = 'disabled';
		}
		$this->result = array( '@tag' => 'span',
			'от ',
			$from,
			' до ',
			$to
		);
	}

} /* end of RangeChoiceEdit class */

class CompoundFieldEdit extends AbstractChoiceEdit {

	/**
	 * @param $rowNum mixed
	 *   null: render empty row;
	 *   integer: render selected row number from $this->userChoice;
	 * @return array
	 *   tagarray of rendered child fields for one compound row;
	 */
	protected function generateRow( $rowNum = null ) {
		$row = array( '@tag' => 'tr', 'class' => 'compound-field-row' );
		foreach ( $this->field->source->childs as $fieldName => $child ) {
			if ( $rowNum === null ) {
				$child->source->setChoice( null );
			} else {
				$child->source->setChoice( $this->userChoice[$fieldName][$rowNum] );
			}
			$row[] = array(
				'@tag' => 'td',
				array( '@tag'=> 'div',
					'class' => 'compound-subfield-title',
					$child->title
				),
				array( '@tag' => 'div',
					'class' => 'compound-subfield-container',
					$child->view( 'edit', $rowNum )
				),
			);
		}
		$row[] = array( '@tag' => 'td', 'class' => 'compound-field-ctrl',
			array( '@tag' => 'span',
				'class' => 'compound-field-remove',
				'Load compound-field.js'
			)
		);
		return $row;
	}

	protected function generate() {
		# Dbg\log(__METHOD__.':field',$this->field);
		# Dbg\log(__METHOD__.':userChoice',$this->userChoice);
		$table = array( '@tag' => 'table', 'class' => 'compound-field' );
		$rowsCount = $this->field->source->getChoiceRowsCount();
		if ( $rowsCount === 0 ) {
			# No ->choice (empty model).
			# Just output one row with child fields default values.
			# $table[] = $this->generateRow();
		} else {
			for ( $i = 0; $i < $rowsCount; $i++ ) {
				# ->choice is populated (loaded form or model data).
				$table[] = $this->generateRow( $i );
			}
		}
		$table['data-compound-field-new-row'] = Gl::hsc( XmlTree::renderTagArray( $this->generateRow() ) );
		$addButton = array( '@tag' => 'div', 'class' => 'compound-field-ctrl',
			array( '@tag' => 'span',
				'class' => 'compound-field-add',
				'Load compound-field.js'
			)
		);
		$this->result = array( '@tag' => 'div', 'class' => 'compound-field-container',
			$table,
			$addButton
		);
	}

	public function getTagArray() {
		# No need to check ->getError() because that was already done for childs.
		return $this->result;
	}

} /* end of CompoundFieldEdit class */
