<?php
namespace QuestPC;

/**
 * @todo:
 * This class probably should be converted to trait when moving to php 5.4.
 */
class TabularItemsViewMix extends FeaturesObject {

	/**
	 * Table row rendering state.
	 * One item occupies only one column, however can occupy more than one row.
	 */
	# Table total number of columns.
	# Equals to count of items rendered per table row (trivial case is 1)
	protected $columnsPerRow;
	# How many table rows the item takes (trivial case is 1)
	protected $itemTakesRows;
	# Internal counter of _complete_ rows (one row has ->itemTakesRows physical TR's)
	protected $completeRowsCount = 1;
	# Current column number to fill with view data.
	protected $columnIdx = 0;
	/**
	 * Block of ->itemTakesRows TR's of actual view data.
	 * array( '@tag' => tr',
	 *   array( '@tag'=> 'td', 'model0_row0 ),
	 *   array( '@tag'=> 'td', 'model1_row0 ),
	 *   ...
	 *   array( '@tag'=> 'td', 'modelN_row0 ),
	 * ),
	 * array( '@tag' => 'tr',
	 *   array( '@tag'=> 'td', 'model0_row1 ),
	 *   array( '@tag'=> 'td', 'model1_row1 ),
	 *   ...
	 *   array( '@tag'=> 'td', 'modelN_row1 ),
	 * )
	 */
	
	protected $itemsBlock;

	/**
	 * @param $cell array
	 *   When cell '@tag' key is 'td' / 'th', cell is added unmodified;
	 *   otherwise cell content will be wrapped into 'td'.
	 */
	protected function appendCell( $internalRow, array &$cell ) {
		if ( !array_key_exists( '@tag', $cell ) ) {
			$cell['@tag'] = 'td';
		} elseif ( !in_array( $cell['@tag'], array( 'td', 'th' ) ) ) {
			$cell = array( '@tag' => 'td', $cell );
		}
		$this->itemsBlock[$internalRow][$this->columnIdx] = $cell;
	}

	/**
	 * Appends table cells (tagarrays) of one model view.
	 * @param $args variadic
	 *   list of arrays to add;
	 *   Each array is table cell of the same model view.
	 *   There can be 0..->itemTakesRows amount of cells.
	 *   Missing cells will be filled by empty TD tagarrays.
	 */
	public function appendCells( /* args */ ) {
		$cells = func_get_args();
		$deltaCells = $this->itemTakesRows - count( $cells );
		if ( $deltaCells < 0 ) {
			SdvException::throwError( 'Number of view cells must be equal to ->itemTakesRows',
				__METHOD__,
				array(
					'itemTakesRows' => $this->itemTakesRows,
					'cells' => $cells
				)
			);
		} elseif ( $deltaCells > 0 ) {
			for ( $i = 0; $i < $deltaCells; $i++ ) {
				$cells[] = array( '@tag' => 'td' );
			}
		}
		if ( $this->columnIdx === 0 ) {
			$this->itemsBlock = array_fill( 0, $this->itemTakesRows, array( '@tag' => 'tr' ) );
		}
		foreach ( $cells as $internalRow => $cell ) {
			$this->appendCell( $internalRow, $cell );
		}
		$this->columnIdx++;
		if ( $this->columnIdx >= $this->columnsPerRow ) {
			$this->tree->appendArray( $this->itemsBlock );
			$this->columnIdx = 0;
			$this->completeRowsCount++;
		}
	}

	/**
	 * Should be called to ensure the last incomplete row of model cell tagarrays
	 * is added to ->tree.
	 */
	public function flushCells() {
		if ( $this->columnIdx === 0 ) {
			return;
		}
		# Add bufferized elements of last row, if any
		$restEmptyRowsCount = $this->columnsPerRow - $this->columnIdx;
		/**
		 * Adjust incomplete row content to center only if there were
		 * more than one row added.
		 */
		$alignToCenter = $this->completeRowsCount > 1;
		if ( ($restEmptyRowsCount % 2) !== 0 ) {
			/**
			 * Do not align even single incomplete row when it cannot be
			 * properly centered.
			 */
			 $alignToCenter = false;
		}
		# Dbg\log(__METHOD__.':itemsBlock unaligned',$this->itemsBlock);
		for ( $i = 0; $i < $restEmptyRowsCount; $i++ ) {
			if ( ($i % 2) === 0 || !$alignToCenter ) {
				array_push( $this->itemsBlock[0], array( '@tag' => 'td', 'rowspan' => $this->itemTakesRows ) );
			} else {
				array_unshift( $this->itemsBlock[0], array( '@tag' => 'td', 'rowspan' => $this->itemTakesRows ) );
			}
		}
		# Dbg\log(__METHOD__.':itemsBlock aligned',$this->itemsBlock);
		$this->tree->appendArray( $this->itemsBlock );
	}

	/**
	 * @param $columnsPerRow int
	 *   how many models should be displayed per table row;
	 * @param $itemTakesRows int
	 *   how many of adjascent physical TR/TD of the table belong to one model;
	 */
	function __construct( $columnsPerRow, $itemTakesRows ) {
		$this->columnsPerRow = intval( $columnsPerRow );
		$this->itemTakesRows = intval( $itemTakesRows );
	}

} /* end of TabularItemsViewMix class */
