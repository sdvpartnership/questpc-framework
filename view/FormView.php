<?php
namespace QuestPC;

abstract class FormView extends HtmlPageView {

	/**
	 * If one wants to detect whether form was submitted, define
	 * unique custom input[@name] then use $appContext->request->getGPval().
	 */
	protected $inputSubmitTags = array(
		'@tag' => 'input',
		'type' => 'submit',
		'value' => 'Поиск'
	);

	protected function getInputBlock( AbstractField $field ) {
		if ( $field instanceof HiddenField ) {
			return $field->view();
		}
		$result = array();
		if ( $field->title !== '' ) {
			$result[] = array( '@tag' => 'div', 'class' => 'form-field-title', $field->title );
		}
		$result[] = array( '@tag' => 'div', 'class' => 'form-field', $field->view() );
		return $result;
	}

	protected function getSubmitButton( AbstractForm $form ) {
		if ( !isset( $this->inputSubmitTags['name'] ) ) {
			$this->inputSubmitTags['name'] = $form->getFormId();
		}
		return array( '@tag' => 'div', $this->inputSubmitTags );
	}

	public function getFieldsTagArray( AbstractForm $form ) {
		$this->form = $form;
		$inputs = array();
		# "Unnamed" access to form ->fields.
		foreach ( $form as $field ) {
			$inputs[] = $this->getInputBlock( $field );
		}
		$inputs[] = $this->getSubmitButton( $form );
		return $inputs;
	}

} /* end of FormView class */
