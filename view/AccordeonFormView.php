<?php
namespace QuestPC;

/**
 * Renders specified form inputs as AccordeonFormView suitable for
 * jquery.accordeon-form.
 */
abstract class AccordeonFormView extends FormView {

	protected function getInputBlock( AbstractField $field ) {
		if ( $field instanceof HiddenField ) {
			return $field->view();
		}
		$inputName = $field->source->getInputBaseName();
		$userChoice = $field->source->getChoice();
		# Dbg\log('inputName',$inputName);
		# Dbg\log(__METHOD__.':field',$field);
		$blockClass = 'inputblock';
		$inputClass = 'inputblock-input';
		$blockHeader = array(
			'@tag' => 'div',
			'class' => 'inputblock-title',
			'@len' => 0
		);
		$bhLen = &$blockHeader['@len'];
		if ( $field->optional ) {
			$blockClass .= ' inputblock-optional';
			$blockHeader[$bhLen] = array(
				'@tag' => 'input',
				'id' => "{$inputName}_optional",
				'type' => 'checkbox',
			);
			if ( $userChoice === null ) {
				$inputClass .= ' inputblock-disabled';
			} else {
				$blockHeader[$bhLen]['checked'] = 'checked';
			}
			$bhLen++;
		}
		$blockHeader[$bhLen] = array(
			'@tag' => 'label',
			'for' => "{$inputName}_optional",
			$field->title
		);
		$targetField = clone $field;
		if ( $userChoice !== null ) {
			$targetField->optional = false;
		}
		return array(
			'@tag' => 'div',
			'class' => $blockClass,
			$blockHeader,
			array(
				'@tag' => 'div',
				'class' => $inputClass,
				$targetField->view()
			)
		);
	}

	protected function getSubmitButton( AbstractForm $form ) {
		return array(
			'@tag' => 'div',
			'class' => 'inputblock',
			array(
				'@tag' => 'div',
				'class' => 'inputblock-input',
				$this->inputSubmitTags
			)
		);
	}

} /* end of AccordeonFormView class */
