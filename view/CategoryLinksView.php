<?php
namespace QuestPC;

class CategoryLinksView extends HtmlPageView {

	# instanceof CategoryTree used to generate links
	protected $categoryTree;
	protected $categoryTreeStack = array();

	/**
	 * Use in page controllers.
	 */
	public function setCategoryTree( CategoryTree $categoryTree ) {
		if ( count( $this->categoryTreeStack ) !== 0 ) {
			SdvException::throwError( '->categoryTreeStack is not empty, ' .
				'use ->pushCategoryTree() / ->popCategoryTree() instead.',
				__METHOD__, $this->categoryTreeStack
			);
		}
		$this->categoryTree = $categoryTree;
	}

	/**
	 * Use in page views, because one controller may use different views.
	 */
	public function pushCategoryTree( CategoryTree $categoryTree ) {
		if ( isset( $this->categoryTree ) ) {
			array_push( $this->categoryTreeStack, $this->categoryTree );
		}
		$this->categoryTree = $categoryTree;
	}

	/**
	 * Use in page views, because one controller may use different views.
	 */
	public function popCategoryTree() {
		if ( count( $this->categoryTreeStack ) === 0 ) {
			if ( $this->categoryTree === null ) {
				SdvException::throwError( '->categoryTreeStack is already empty', __METHOD__ );
			}
			$this->categoryTree = null;
		} else {
			$this->categoryTree = array_pop( $this->categoryTreeStack );
		}
	}
	
	/**
	 * Generate link to top node of specified category of current ->categoryTree
	 * @param $categoryId int
	 *   CategoryTree category id
	 * @param $content mixed
	 *   null: use top node name as link content;
	 *   string / tagarray: use value as link content;
	 * @param $urlTemplate mixed
	 *   string:
	 *     $1 will be replaced by generated urlencoded category path
	 *     for example: '/publication$1/page1.html';
	 *   null:
	 *     use default template from ->categoryTree instance;
	 * @return tagarray
	 */
	public function getCategoryLink(
			$categoryId,
			$content = null,
			$urlTemplate = null
	) {
		$path = $this->categoryTree->getPathById( $categoryId );
		if ( $urlTemplate === null ) {
			$urlTemplate = $this->categoryTree->getProp( 'defaultUrlTemplate' );
		}
		return
			$this->getLocalLink(
				($content === null) ?
					$this->categoryTree->getTopNodeName( $path, CategoryTree::STRIP_BASE64_HASH ) :
					$content,
				str_replace( '$1', CategoryTree::encodeUri( $path ), $urlTemplate )
			);
	}

	/**
	 * Generate full-path list of links to specified category of current ->categoryTree.
	 * @param $categoryId int
	 *   CategoryTree category id
	 * @param $urlTemplate mixed
	 *   string
	 *     $1 will be replaced by generated urlencoded category path
	 *   null
	 *     use default url template value from current ->categoryTree
	 * @param $removeBasePath array
	 *   value: string
	 *     names of base categories that should not be displayed
	 * @return tagarray
	 *   with one or more links
	 */
	public function getPathLinks(
			$categoryId,
			$urlTemplate = null,
			array $removeBasePath = array()
	) {
		if ( $urlTemplate === null ) {
			$urlTemplate = $this->categoryTree->getProp( 'defaultUrlTemplate' );
		}
		list( $path, $categoryIds ) = $this->categoryTree->getPathById(
			$categoryId,
			CategoryTree::GET_PATH_IDS |
			CategoryTree::GET_PATH_VROOT
		);
		$view = array();
		$i = 0;
		foreach ( $categoryIds as $selfId ) {
			if ( !array_key_exists( $i, $removeBasePath ) ||
					$removeBasePath[$i] !== $path[$i]['name'] ) {
				if ( count( $view ) !== 0 ) {
					$view[] = ' / ';
				}
				$view[] = $this->getCategoryLink( $selfId, null, $urlTemplate );
			}
			$i++;
		}
		return $view;
	}

	protected static $treeUrlTemplate;
	protected function getCategoryLevelTags( array &$tags, array &$treeCatsLevel ) {
		if ( count( $treeCatsLevel ) === 0 ) {
			return;
		}
		$ul = array( '@tag' => 'ul' );
		foreach ( $treeCatsLevel as $catId => &$subCatsLevel ) {
			$itemProp = null;
			# Compare top nodes only pathes to mapped itemprop pathes.
			if ( count( $subCatsLevel ) === 0
					&& count( $this->itemPropMapping ) > 0 ) {
				$currPath = CategoryTree::stripPath(
					$this->categoryTree->getPathById( $catId )
				);
				# Pop out author, publisher, manufactorer value,
				# which is not needed for comparsion.
				$itemPropValue = array_pop( $currPath );
				$currPath = serialize( $currPath );
				# Dbg\log(__METHOD__.':currPath',$currPath);
				foreach ( $this->itemPropMapping as $itemPropName => $itemPath ) {
					# Dbg\log(__METHOD__.':itemPath',$itemPath);
					if ( $currPath === $itemPath ) {
						$itemProp = $itemPropName;
						break;
					}
				}
			}
			$catLink = $this->getCategoryLink( $catId, null, static::$treeUrlTemplate );
			if ( $itemProp !== null &&
					is_array( $catLink ) &&
					array_key_exists( '@tag', $catLink ) ) {
				$catLink['itemprop'] = $itemPropName;
			}
			$ul[] = array( '@tag' => 'li', $catLink );
			$this->getCategoryLevelTags( $ul, $subCatsLevel );
		}
		$tags[] = $ul;
	}

	/**
	 * Renders specified list of categories in nested ul/li tree format.
	 * @param $catIds array
	 *   value: int
	 *     category_id from current ->categoryTree
	 * @param $urlTemplate mixed
	 *   string
	 *     $1 will be replaced by generated urlencoded category path
	 *   null
	 *     use default url template value from current ->categoryTree
	 * @param $removeBasePath array
	 *   value: string
	 *     names of base categories that should not be displayed
	 * @param $itemPropMapping array
	 *   key: value of schema.org itemprop attribute;
	 *   value: array stripped path which matches this itemprop attribute;
	 */
	public function getCategoryTreeTags(
			array $catIds,
			$urlTemplate = null,
			array $removeBasePath = array(),
			array $itemPropMapping = array()
	) {
		static::$treeUrlTemplate = ( $urlTemplate === null ) ?
			 $this->categoryTree->getProp( 'defaultUrlTemplate' ) : $urlTemplate;
		$this->itemPropMapping = array_map( 'serialize', $itemPropMapping );
		$treeCats = array();
		foreach ( $catIds as $catId ) {
			list( $path, $categoryIds ) = $this->categoryTree->getPathById(
				$catId,
				CategoryTree::GET_PATH_IDS |
				CategoryTree::GET_PATH_VROOT
			);
			# Dbg\log(__METHOD__,$path);
			# Dbg\log(__METHOD__,$categoryIds);
			$treeCatsLevel = &$treeCats;
			foreach ( $categoryIds as $level => $selfId ) {
				if ( !array_key_exists( $level, $removeBasePath ) ||
						$removeBasePath[$level] !== $path[$level]['name'] ) {
					if ( !array_key_exists( $selfId, $treeCatsLevel ) ) {
						$treeCatsLevel[$selfId] = array();
					}
					$treeCatsLevel = &$treeCatsLevel[$selfId];
				}
			}
		}
		# Dbg\log(__METHOD__,$treeCats);
		$tags = array();
		$this->getCategoryLevelTags( $tags, $treeCats );
		# Dbg\log(__METHOD__,$tags);
		return $tags;
	}

	public function addPathLinks( $categoryId, $urlTemplate = null ) {
		if ( $urlTemplate === null ) {
			$urlTemplate = $this->categoryTree->getProp( 'defaultUrlTemplate' );
		}
		$this->tree->appendArray( $this->getPathLinks( $categoryId, $urlTemplate ) );
	}

	/**
	 * An example of custom linkTemplate usage for ->getCategoryLink()
	 * used to generate local link to ItemsHistoryPage routed
	 * first pages of pager.
	 * @param $categoryId int
	 * @return tagarray
	 */
	public function addCategoryLink( $categoryId, $content = null, $urlTemplate ) {
		$this->tree->appendArray(
			$this->getCategoryLink(
				$categoryId,
				$content,
				$urlTemplate
			)
		);
	}

} /* end of CategoryLinksView class */
