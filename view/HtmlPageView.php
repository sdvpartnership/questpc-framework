<?php
namespace QuestPC;

/**
 * Base class of all html pages.
 * Supports manipulation of page head tags and anchor creation.
 * Supports instantiation of mixins: views with shared ->tree and ->pagePath,
 * that can be used to manipulate single DOM tree via different HtmlPageView
 * descendant classes.
 */
class HtmlPageView extends FeaturesObject {

	# (Unique) path (or url) of current page.
	# (usually just a path without protocol/host prefix).
	# Shared between view "mixins".
	protected $pagePath;
	# Instance of XmlTree.
	# Shared between view "mixins".
	protected $tree;

	# FeaturesObject setter setup
	protected static $propTypes = array(
		'pagePath' => 'string'
	);

	# FeaturesObject shared properties setup
	protected static $sharedPropNames = array(
		'pagePath',
		'tree'
	);

	public function __construct() {
	}

	/**
	 * Called during instantiating via ::baseForShared().
	 * @param $pagePath string
	 *   ->pagePath value (uri part of page for linker to match current page);
	 *
	 * Note: Shared views are instantiated w/o calling this method and have their
	 *   ::$sharedPropNames keys named properties pointing to the base view properties.
	 */
	public function constructBase( $pagePath ) {
		$this->pagePath = $pagePath;
		$this->tree = new XmlTree();
	}

	public function appendArray( array $tagArray ) {
		$this->tree->appendArray( $tagArray );
	}

	/**
	 * @note: Descendants may have different arguments.
	 */
	public function loadTemplate( /* args */ ) {
		global $appContext;
		list( $tplPath ) = func_get_args();
		$this->tree = XmlTree::newFromFile( 'html', "{$appContext->IP}{$tplPath}" );
	}

	function setTitle( $title ) {
		$this->tree->rootXpath( '/html/head/title' )->append( '#text', $title );
	}

	function setHeadMeta( $where = 'before', array $metaData ) {
		$tree = $this->tree;
		if ( $where !== 'before' ) {
			$where = 'after';
		}
		$metaData['@tag'] = 'meta';
		$tree->rootXpath( '/html/head/meta' );
		# Dbg\log(__METHOD__.':1',$tree->count());
		if ( $tree->count() < 1 ) {
			$tree->rootXpath( '/html/head/*' );
			# Dbg\log(__METHOD__.':2',$tree->count());
			if ( $tree->count() < 1 ) {
				$tree->rootXpath( '/html/head' )->appendArray( $metaData );
			} else {
				# There is no another 'meta' tags.
				# Insert new 'meta' tag before all of another tags in /html/head .
				$tree->first()->insertArray( 'before', $metaData );
			}
		} else {
			if ( $where === 'before' ) {
				$tree->first();
			} else { /* after */
				$tree->last();
			}
			$tree->insertArray( $where, $metaData );
		}
	}

	function setHeadStyles( $where = 'before', array $styles ) {
		$tree = $this->tree;
		if ( count( $styles ) < 1 ) {
			return;
		}
		if ( $where !== 'before' ) {
			$where = 'after';
		}
		$a = array();
		foreach ( $styles as $styleDef ) {
			# Dbg\log(__METHOD__,$styleDef);
			$styleDef['@tag'] = 'link';
			if ( !array_key_exists( 'rel', $styleDef ) ) {
				# default rel is a stylesheet link
				$styleDef['rel'] = 'stylesheet';
				if ( !array_key_exists( 'type', $styleDef ) ) {
					$styleDef['type'] = 'text/css';
				}
			}
			$a[] = $styleDef;
		}
		$tree->rootXpath( '/html/head/link[@rel="stylesheet"]' );
		# Dbg\log(__METHOD__,$tree->count());
		if ( $tree->count() < 1 ) {
			$tree->rootXpath( '/html/head/script' );
			if ( $tree->count() < 1 ) {
				$tree->rootXpath( '/html/head' )->appendArray( $a );
			} else {
				$tree->first()->insertArray( 'before', $a );
			}
		} else {
			if ( $where === 'before' ) {
				$tree->first();
			} else { /* after */
				$tree->last();
			}
			$tree->insertArray( $where, $a );
		}
	}

	function setHeadScripts( $where = 'before', array $scripts ) {
		$tree = $this->tree;
		if ( count( $scripts ) < 1 ) {
			return;
		}
		if ( $where !== 'before' ) {
			$where = 'after';
		}
		$a = array();
		foreach ( $scripts as $scriptUri ) {
			$a[] =
				array( '@tag' => 'script', 'src' => $scriptUri, 'type' => 'text/javascript',
					array( '#text' => '' )
				);
		}
		$tree->rootXpath( '/html/head/script' );
		# Dbg\log(__METHOD__,$tree->count());
		if ( $tree->count() < 1 ) {
			$tree->rootXpath( '/html/head/link[@rel="stylesheet"]' );
			if ( $tree->count() < 1 ) {
				$tree->up()->appendArray( $a );
			} else {
				$tree->last()->insertArray( 'after', $a );
			}
		} else {
			if ( $where === 'before' ) {
				$tree->first();
			} else { /* after */
				$tree->last();
			}
			$tree->insertArray( $where, $a );
		}
	}

	/**
	 * @return array
	 *   tagarray for link with schema and host name
	 */
	function getLink( $innerContent, $urlInfo ) {
		global $appContext;
		$url = is_array( $urlInfo ) ? $urlInfo[0] : $urlInfo;
		$parts = parse_url( $url );
		if ( is_array( $parts ) && array_key_exists( 'scheme', $parts ) &&
				array_key_exists( $parts['scheme'], $appContext->validUrlSchemes ) ) {
			$link = array( '@tag' => 'a', 'href' => $url, $innerContent );
			if ( is_array( $urlInfo ) ) {
				foreach ( $urlInfo as $attr => $val ) {
					if ( is_string( $attr ) ) {
						$link[$attr] = $val;
					}
				}
			}
			return $link;
		}
		SdvException::throwError( 'Invalid full url (no scheme or not allowed scheme)',
			__METHOD__, $url
		);
	}

	/**
	 * @return array
	 *   tagarray for local link (w/o schema and host name)
	 */
	function getLocalLink( $innerContent, $relUrl ) {
		global $appContext;
		if ( !property_exists( $appContext, 'hostPart' ) ) {
			SdvException::throwFatal( 'appContext->hostPart is not defined', __METHOD__ );
		}
		return ( $relUrl === $this->pagePath ) ?
			array( '@tag' => 'span', 'class' => 'curr-link', $innerContent ) :
			$this->getLink( $innerContent, $appContext->hostPart . $relUrl );
	}

	/**
	 * Replaces each element of xpath result with specified html.
	 */
	public function patch_replace( $xpath, $html ) {
		$tagArray = XmlTree::htmlToTagArray( $html );
		$this->tree
		->rootXpath( $xpath )
		->replaceArray( $tagArray );
	}

	/**
	 * Replaces childrens of each element of xpath result with specified html.
	 */
	public function patch_set( $xpath, $html ) {
		$tagArray = XmlTree::htmlToTagArray( $html );
		$this->tree
		->rootXpath( $xpath )
		->setArray( $tagArray );
	}

	/**
	 * Appends specified html as new childrens of each element of xpath result.
	 */
	public function patch_append( $xpath, $html ) {
		$tagArray = XmlTree::htmlToTagArray( $html );
		$this->tree
		->rootXpath( $xpath )
		->appendArray( $tagArray );
	}

	/**
	 * Inserts specified html before each element of xpath result.
	 */
	public function patch_insertBefore( $xpath, $html ) {
		$tagArray = XmlTree::htmlToTagArray( $html );
		$this->tree
		->rootXpath( $xpath )
		->insertArray( 'before', $tagArray );
	}

	/**
	 * Inserts specified html after each element of xpath result.
	 */
	public function patch_insertAfter( $xpath, $html ) {
		$tagArray = XmlTree::htmlToTagArray( $html );
		$this->tree
		->rootXpath( $xpath )
		->insertArray( 'after', $tagArray );
	}

	function __toString() {
		return $this->tree->byVid( null )->getString();
	}

} /* end of HtmlPageView class */
