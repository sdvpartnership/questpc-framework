<?php
namespace QuestPC;

/**
 * Implements pager back / forward links generation.
 */
class PagerPageView extends HtmlPageView {

	protected $pagerQueryString;

	# basePath template for pager back / forward urls generation
	# $1 will be replaced by pageNum
	# $2 will be replaced by pagerQueryString
	protected $basePathTpl;

	/**
	 * Set pager base path template.
	 */
	public function setBasePathTpl( $basePathTpl ) {
		$this->basePathTpl = $basePathTpl;
	}

	public function addPageNumber( DbPager $pager ) {
		if ( $pager->getPageNum() === 1 && !$pager->hasNextPage() ) {
			$header = ' '; // nbsp
		} else {
			$header = 'Страница ' . $pager->getPageNum();
		}
		$this->baseInstance->addHeader( $header );
	}

	protected function getPagerLink( $pageNum ) {
		return str_replace(
			array(
				'$1',
				'$2'
			),
			array(
				$pageNum,
				$this->pagerQueryString
			),
			$this->basePathTpl
		);
	}

	public function addPagerLinks( $pagerQueryString, DbPager $pager ) {
		$this->pagerQueryString = $pagerQueryString;
		$pageNum = $pager->getPageNum();
		$view = array(
			0 => array( '@tag' => 'div', 'class' => 'search_pager_back', '' ),
			1 => array( '@tag' => 'div', 'class' => 'search_pager_forward', '' ),
		);
		if ( $pageNum > 1 ) {
			$view[0][] = $this->getLocalLink( '<<', $this->getPagerLink( $pageNum - 1 ) );
		}
		if ( $pager->hasNextPage() ) {
			$view[1][] = $this->getLocalLink( '>>', $this->getPagerLink( $pageNum + 1 ) );
		}
		$view = array(
			'@tag' => 'div',
			'class' => 'search_pager_links',
			$view
		);
		$this->tree->appendArray( $view );
		# chaining
		return $this;
	}

} /* end of PagerPageView class */
