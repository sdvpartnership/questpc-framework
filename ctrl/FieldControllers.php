<?php
namespace QuestPC;

/**
 * Connects field data source instance and static field view generator
 * together.
 */
abstract class AbstractField {

	# Currently used data source model for this controller.
	public $dataSourceClassName;
	# List of view tagarray generators for this model.
	public $viewClassNames;
	# Instance of data source associated to current field.
	public $source;

	# Short description of the field.
	public $title;

	/**
	 * Initialize data source (model) of field.
	 */
	public function setSource( \stdClass $dataDef ) {
		if ( property_exists( $dataDef, 'source' ) ) {
			$this->dataSourceClassName = $dataDef->source;
			unset( $dataDef->source );
		}
		$dataSourceClassName = AutoLoader::getFqnClassName(
			$this->dataSourceClassName,
			__NAMESPACE__
		);
		$this->source = new $dataSourceClassName();
		$this->source->setDataDef( $dataDef );
	}
	
	/**
	 * It is possible to override view attributes for already existing field,
	 * that hasn't been viewed yet; however be very careful about this.
	 */
	public function setView( $viewAttributes = null ) {
		if ( !is_array( $viewAttributes ) ) {
			$viewAttributes = array();
		}
		if ( array_key_exists( 'title', $viewAttributes ) ) {
			$this->title = $viewAttributes['title'];
		} elseif ( !isset( $this->title ) ) {
			$this->title = '';
		}
		if ( array_key_exists( 'view', $viewAttributes ) ) {
			foreach ( $viewAttributes['view'] as $key => $className ) {
				$this->viewClassNames[$key] = AutoLoader::getFqnClassName(
					$className, __NAMESPACE__
				);
			}
		}
	}

	/**
	 * @param $type string
	 *   key of ->viewClassNames (type of view to create)
	 * @param $rowNum
	 *   null: single row (not a child of CompoundField);
	 *   int:  number of row of child of CompoundField;
	 */
	public function view( $type = 'edit', $rowNum = null ) {
		if ( !array_key_exists( $type, $this->viewClassNames ) ) {
			SdvException::throwError(
				'Unknown type of view',
				__METHOD__,
				$type
			);
		}
		$viewClassName = $this->viewClassNames[$type];
		$fieldView = new $viewClassName( $this, $rowNum );
		return $fieldView->getTagArray();
	}

} /* end of AbstractField class */

/**
 * Implements common logic for single input / textarea mvc field.
 */
abstract class SingleField extends AbstractField {

	# Size of the field input.
	public $size;
	# Is the field mandatory or optional?
	# Currently is used by AccordeonFormView to auto-collapse long lists of checkboxes / radiobuttons.
	public $optional;
	# Does this field requires browser autocompletion? Might be used by view generator.
	public $autocomplete;
	# List of possible value(s) for this field.
	# Used by field view.
	public $defaultValues;

	public function setDefaultValues( $defaultValues ) {
		$this->defaultValues = $defaultValues;
	}

	public function getDefaultValues() {
		return $this->defaultValues;
	}

	/**
	 * @param $dataDef array
	 *   fields for field data source constructor
	 *   (see description of AbstractFieldDataSource::__construct());
	 *   optional property ->source specifies value of
	 *     ->dataSourceClassName;
	 * @param $viewAttributes array
	 *   attributes used by associated static view tagarray generator;
	 *   key:   'title'
	 *   value: string, title of html field
	 *   key:   'optional'
	 *   value: boolean, whether the field wrapper should be rendered
	 *   disabled by default
	 * @param $defaultValues mixed
	 *   default values for field, used by static view tagarray generator;
	 */
	public static function mvc( array $dataDef, $viewAttributes = null, $defaultValues = null ) {
		$self = new static;
		$self->setSource( (object) $dataDef );
		$self->setView( $viewAttributes );
		$self->setDefaultValues( $defaultValues );
		return $self;
	}

	/**
	 * Also add attributes that are meaningful for single field
	 */
	public function setView( $viewAttributes = null ) {
		if ( !is_array( $viewAttributes ) ) {
			$viewAttributes = array();
		}
		parent::setView( $viewAttributes );
		if ( array_key_exists( 'optional', $viewAttributes ) ) {
			$this->optional = (bool) $viewAttributes['optional'];
		} elseif ( !isset( $this->optional ) ) {
			$this->optional = false;
		}
		if ( array_key_exists( 'autocomplete', $viewAttributes ) ) {
			$this->autocomplete = $viewAttributes['autocomplete'] ? 'on' : 'off';
		} elseif ( !isset( $this->autocomplete ) ) {
			$this->autocomplete = 'off';
		}
		if ( array_key_exists( 'size', $viewAttributes ) ) {
			$this->size = intval( $viewAttributes['size'] );
		}
	}

} /* end of SingleField class */

class RadioField extends SingleField {

	public $dataSourceClassName = 'ScalarFieldDataSource';
	public $viewClassNames = array( 'edit' => 'QuestPC\\RadioChoiceEdit' );

	public function setDefaultValues( $defaultValues ) {
		if ( !is_array( $defaultValues ) && $defaultValues !== null ) {
			SdvException::throwError(
				'defaultValues must be null or an array',
				__METHOD__,
				$defaultValues
			);
		}
		$this->defaultValues = $defaultValues;
	}

	public function view( $type = 'edit', $rowNum = null ) {
		if ( !isset( $this->defaultValues ) ) {
			SdvException::throwError(
				'defaultValues is not set for field',
				__METHOD__,
				$this
			);
		}
		return parent::view( $type, $rowNum );
	}

} /* end of RadioField class */

class CheckboxField extends SingleField {

	public $dataSourceClassName = 'CheckboxFieldDataSource';
	public $viewClassNames = array( 'edit' => 'QuestPC\\CheckboxChoiceEdit' );

	public function setDefaultValues( $defaultValues ) {
		if ( !is_array( $defaultValues ) && $defaultValues !== null ) {
			SdvException::throwError(
				'defaultValues must be null or an array',
				__METHOD__,
				$defaultValues
			);
		}
		$this->defaultValues = $defaultValues;
	}

	public function view( $type = 'edit', $rowNum = null ) {
		if ( $rowNum !== null ) {
			SdvException::throwError(
				'This type of field cannot be the part of CompoundField because two-level input nesting [][] is currently unsupported',
				__METHOD__,
				$this
			);
		}
		if ( !isset( $this->defaultValues ) ) {
			SdvException::throwError(
				'defaultValues is not set for field',
				__METHOD__,
				$this
			);
		}
		return parent::view( $type, $rowNum );
	}

} /* end of CheckboxField class */

class ScalarField extends SingleField {

	public $dataSourceClassName = 'ScalarFieldDataSource';
	public $viewClassNames = array( 'edit' => 'QuestPC\\ScalarChoiceEdit' );

	/**
	 * For scalar field defaultValues is not the list of possible values,
	 * but a default value of field.
	 */
	public function setDefaultValues( $defaultValues ) {
		if ( $defaultValues === null ) {
			$defaultValues = '';
		}
		if ( !is_scalar( $defaultValues ) ) {
			SdvException::throwError(
				'Default value of field must be scalar',
				__METHOD__,
				$defaultValues
			);
		}
		$this->defaultValues = $defaultValues;
	}

} /* end of ScalarField class */

class TextField extends ScalarField {

	# Number of rows for user input.
	public $rownum;

	public $viewClassNames = array( 'edit' => 'QuestPC\\TextChoiceEdit' );

	public function setView( $viewAttributes = null ) {
		parent::setView( $viewAttributes );
		if ( is_array( $viewAttributes ) && array_key_exists( 'rownum', $viewAttributes ) ) {
			$this->rownum = intval( $viewAttributes['rownum'] );
		}
	}

} /* end of TextField class */

class HiddenField extends ScalarField {

	public $viewClassNames = array( 'edit' => 'QuestPC\\HiddenChoiceEdit' );

} /* end of HiddenField class */

/**
 * Range of choices for single field.
 */
class RangeField extends SingleField {

	public $dataSourceClassName = 'RangeFieldDataSource';
	public $viewClassNames = array( 'edit' => 'QuestPC\\RangeChoiceEdit' );

	public function setDefaultValues( $defaultValues ) {
		if ( !is_array( $defaultValues) ||
				count( $defaultValues ) !== 2 ||
				!array_key_exists( 'min', $defaultValues ) ||
				!array_key_exists( 'max', $defaultValues ) 
		) {
			SdvException::throwError(
				'Invalid value of defaultValues',
				__METHOD__,
				$defaultValues
			);
		}
		$this->defaultValues = $defaultValues;
	}

	public function view( $type = 'edit', $rowNum = null ) {
		if ( $rowNum !== null ) {
			SdvException::throwError(
				'This type of field is meaningless as child of CompoundField',
				__METHOD__,
				$this
			);
		}
		if ( !isset( $this->defaultValues ) ) {
			SdvException::throwError(
				'defaultValues is not set for field',
				__METHOD__,
				$this
			);
		}
		return parent::view( $type, $rowNum );
	}

} /* end of RangeField class */

class FileField extends SingleField {

	public $dataSourceClassName = 'FileFieldDataSource';
	public $viewClassNames = array( 'edit' => 'QuestPC\\FileChoiceEdit' );

	/**
	 * File fields cannot have default values by their design.
	 */
	public function setDefaultValues( $defaultValues ) {
		if ( $defaultValues !== null ) {
			SdvException::throwError(
				'This type of field cannot have default values',
				__METHOD__,
				$defaultValues
			);
		}
		$this->defaultValues = $defaultValues;
	}

} /* end of FileField class */

/**
 * A "field" which holds multiple rows of multiple columns.
 * @todo: Add jQuery module to add / remove rows.
 */
class CompoundField extends AbstractField {

	# Currently used data source model for this controller.
	public $dataSourceClassName = 'CompoundFieldDataSource';
	# List of view tagarray generators for this model.
	public $viewClassNames = array( 'edit' => 'QuestPC\\CompoundFieldEdit' );

	public static function multi( array $fields, array $dataDef = array(), $viewAttributes = null ) {
		$self = new static();
		$self->setSource( (object) $dataDef );
		$self->setView( $viewAttributes );
		foreach ( $fields as $field ) {
			$self->source->addChild( $field );
		}
		# Dbg\log(__METHOD__,$self);
		return $self;
	}

	public function view( $type = 'edit', $rowNum = null ) {
		if ( $rowNum !== null ) {
			SdvException::throwError(
				'Nested CompoundField are not supported',
				__METHOD__,
				$this
			);
		}
		return parent::view( $type, $rowNum );
	}

} /* end of CompoundField class */
