<?php
namespace QuestPC;

class AbstractPage {

	// model, view
	protected $model, $view;
	// shortcut to shared views, if any
	protected $shView;

	# view constructor parameter of descendant ->setView()
	protected $pagePath;

	# list of styles inserted before already existing html/head/link[@rel="stylesheet"]
	# order is important.
	protected $stylesBefore = array();
		# example: array( 'href' => '/test2/my2.css', 'media' => 'projection,screen' ),
	# list of styles inserted after already existing html/head/link[@rel="stylesheet"]
	# order is important.
	protected $stylesAfter = array();
	# list of scripts inserted before already existing html/head/scripts
	# order is important.
	protected $scriptsBefore = array();
		# example: '//ajax.googleapis.com/ajax/libs/jquery/1.8.0/jquery.min.js'
	# list of scripts inserted after already existing html/head/scripts
	# order is important.
	protected $scriptsAfter = array();

	/**
	 * Currently class is instantiated by Router without agruments.
	 */
	function __construct() {
	}

	function getPagePath() {
		return $this->pagePath;
	}

	/**
	 * Set controller action(s).
	 */
	public function setActions( Route $route ) {
		/* noop */
	}

	/**
	 * These model queries are supposed to run before view is called.
	 * Of course one may have additional model queries at later stage as well.
	 */
	public function initialModelQueries() {
		/* noop */
	}

	public function setView() {
		/* noop */
	}

	public function initSharedView() {
		/* noop */
	}

	function getTitle() {
		return '';
	}

	function setHead() {
		global $appContext;
		$this->view->setTitle( $this->getTitle() );
		if ( !property_exists( $appContext, 'hostPart' ) ) {
			SdvException::throwFatal( 'appContext->hostPart is not defined', __METHOD__ );
		}
		if ( !isset( $this->pagePath ) ) {
			SdvException::throwFatal( '->pagePath is not set', __METHOD__ );
		}
		$this->view->setHeadStyles( 'before', array( array(
			'rel' => 'canonical', 'href' => $appContext->hostPart . $this->pagePath
		) ) );
		$this->view->setHeadStyles( 'before', $this->stylesBefore );
		$this->view->setHeadStyles( 'after', $this->stylesAfter );
		$this->view->setHeadScripts( 'before', $this->scriptsBefore );
		$this->view->setHeadScripts( 'after', $this->scriptsAfter );
	}

	/**
	 * Called by Router.
	 * @param $route Route
	 *   matching route;
	 * @param $viewArgs mixed
	 *   null - no args for ->setView() call;
	 *   array - arguments of ->setView() call;
	 *   example: Used by StaticParticipantsListJob to override
	 *   SearchPage::basePathTpl value.
	 */
	public function main( Route $route, $viewArgs = null ) {
		$this->setActions( $route );
		$this->initialModelQueries();
		if ( $viewArgs === null ) {
			$this->setView();
		} else {
			call_user_func_array( array( $this, 'setView' ), $viewArgs );
		}
		# Create "shortcut" property for shared views, if any.
		if ( isset( $this->view ) && property_exists( $this->view, 'shared' ) ) {
			$this->shView = $this->view->shared;
			$this->initSharedView();
		}
		$this->setHead();
		$this->setBody();
		$this->viewDomPatcher();
	}

	function setBody() {
	}

	protected function getRequestId() {
		# Use canonical page path as request id.
		return ltrim( urldecode( $this->getPagePath() ), '/' );
	}

	protected function viewDomPatcher() {
		/* noop */
	}

	function __toString() {
		return strval( $this->view );
	}

} /* end of AbstractPage class */
