<?php
namespace QuestPC;

abstract class AbstractFieldDataSource {

	/**
	 * Field name. Used as base name for all SingleFieldDataSource descendants.
	 *
	 * There might be more than one actual field names for RangeFieldDataSource.
	 *
	 * For CompoundFieldDataSource child field input names are used for actual fields instead.
	 * However this one is required for AbstractForm::addField() to work correctly.
	 */
	protected $inputBaseName;
	/**
	 * Index of model in collection, if multiple models are included into one form.
	 * It will be prepended to each field name when defined.
	 *
	 */
	protected $idx;
	# Optional method name / closure used to validate GET/POST input values.
	protected $valueValidator;
	# Optional: associated model instance. Set to null to diassociate.
	protected $model;
	# Currently set / get choice (value of field).
	# Populated in the following ways:
	# 1. Set directly via ->setChoice().
	# 2. Populated with user-submitted data by ->setUserChoice().
	# 3. Loaded with associated model data via ->setModelChoice().
	protected $choice;

	public function setDataDef( \stdClass $data ) {
		if ( property_exists( $data, 'validator' ) ) {
			$this->valueValidator = $data->validator;
		}
		if ( property_exists( $data, 'idx' ) ) {
			$this->setFieldIdx( $data->idx );
		}
		if ( property_exists( $data, 'model' ) ) {
			$this->setModel( $model );
		}
	}

	public function getFieldIdx() {
		return $this->idx;
	}

	public function setFieldIdx( $idx ) {
		$this->idx = $idx;
	}

	/**
	 * @return string
	 *   Unique ID of field used as html input name prefix and request variable prefix.
	 */
	public function getInputBaseName() {
		if ( !isset( $this->inputBaseName ) ) {
			SdvException::throwError(
				'Neither ->name nor ->fqnField were specified in ->setDataDef()',
				__METHOD__,
				$data
			);
		}
		$inputName = $this->inputBaseName;
		if ( isset( $this->idx ) ) {
			$inputName .= "{$this->idx}";
		}
		return $inputName;
	}

	/**
	 * @return mixed
	 *   instance of model currently associated with field data source;
	 *   null : no association;
	 */
	public function getModel() {
		return isset( $this->model ) ? $this->model : null;
	}

	/**
	 * @param choice mixed
	 *   Set to null to emulate non-existing input value;
	 * @todo: Validate programmatically set values.
	 *   Currently only user-submitted values are validated via call stack
	 *   ->setUserChoice() ->getUserInputs() ->getValidatedInput().
	 */
	public function setChoice( $choice ) {
		$this->choice = $choice;
	}

	public function getChoice() {
		return $this->choice;
	}

	public function setUserChoice() {
		$this->setChoice( $this->getUserInputs() );
	}

	public function setModelChoice() {
		$modelVal = $this->getModelVal();
		if ( !$modelVal instanceof NoValue ) {
			# Set model's choice only when model returned non-null field value.
			# Otherwise default values will be displayed instead of erroneous ones.
			$this->setChoice( $modelVal );
		}
	}

} /* end of AbstractFieldDataSource class */

/**
 * Provides GET / POST html field data access and optional
 * SQL subquery builder for associated DB field.
 */
abstract class SingleFieldDataSource extends AbstractFieldDataSource {

	# Is a part of set of fields with the same name (used by CompoundField)
	# Such fields will have [] characters prepended to their names.
	protected $isMulti = false;
	# Compare operator for ->getMatchingInClause() used by query builder.
	protected $cmp = '=';
	/**
	 * Translation of empty string input ->choice:
	 * 1. false : Pass empty string "as is".
	 * 2. scalar / null / array : pass value instead of empty string.
	 * 3. instanceof NoValue : do not call model field handler at all.
	 */
	protected $emptyTo = false;
	# When current field choice throwed recoverable SdvException during
	# importing ->toModel(), stores exception instance.
	# Can store multiple values, because one SingleField instance is used to
	# access / generate the whole row (column of child fields) of CompoundField.
	protected $errors = false;

	/**
	 * UI field is optionally connected to table field by:
	 *   1. Specifying 'fqnField' key in SingleField::mvc() $dataDef parameter.
	 *   2. Specifying 'fqnField' key in $field->source->setDataDef() $data parameter.
	 *   3. Binding via $field->source->bindToTableField().
	 * 1 & 3 are preferrable ways.
	 */
	# Optional: schema table which this field belongs to.
	protected $tableName;
	# Optional: schema table field which this input field belongs to.
	protected $fieldName;

	/**
	 * @param $data stdClass
	 *   ->idx : optional index tail of input name(s);
	 *   ->cmp : optional SQL comparsion operator for query builder;
	 *   ->validator: optional common validator for each input of field;
	 *     string: this instance method name, use to avoid closure duplication;
	 *     closure: "external" validator (mixin), use for non-standard inputs;
	 *   ->name: set $this->inputBaseName to that value;
	 *   ->fqnField: associate html field to fqnField of DB schema;
	 *     ->inputBaseName will be generated automatically in such case;
	 *     necessary field names of associated ->model will be automatically used
	 *     in such case;
	 *   ->model: associate field with specified AbstractModel descendant instance
	 *     (note this allows to mix different models in one form,
	 *      also allows to have some fields not being connected to model);
	 *   ->emptyTo: translation value for empty string choice;
	 */
	public function setDataDef( \stdClass $data ) {
		parent::setDataDef( $data );
		if ( property_exists( $data, 'cmp' ) ) {
			$this->cmp = $data->cmp;
		}
		if ( property_exists( $data, 'multi' ) ) {
			$this->setMulti( $data->multi );
		}
		if ( property_exists( $data, 'emptyTo' ) ) {
			$this->setEmptyTo( $data->emptyTo );
		}
		if ( property_exists( $data, 'name' ) ) {
			$this->inputBaseName = $data->name;
		} elseif ( property_exists( $data, 'fqnField' ) ) {
			list( $tableName, $fieldName ) = explode( '.', $data->fqnField, 2 );
			$this->bindToTableField( $tableName, $fieldName );
		}
	}

	/**
	 * Whether the current field is child of CompoundField (multi-row field).
	 * When ->isMulti is true, field input base name will be prepended with [] suffix.
	 */
	public function setMulti( $isMulti ) {
		$this->isMulti = (bool) $isMulti;
	}

	public function isMulti() {
		return $this->isMulti;
	}

	/**
	 * @param $emptyTo mixed
	 *   Translation of empty string input ->choice:
	 *   1. false : Pass empty string "as is".
	 *   2. scalar / null / array : pass value instead of empty string.
	 *   3. instanceof NoValue : do not call model field handler at all.
	 */
	public function setEmptyTo( $emptyTo ) {
		$this->emptyTo = $emptyTo;
	}

	public function getEmptyTo() {
		return $this->emptyTo;
	}

	/**
	 * Get exception instance linked to current field, if any.
	 * @param $rowNum mixed
	 *   null: not a child of CompoundField or get a first error of CompoundField child;
	 *   int:  get error of CompoundField child at specified row number;
	 * @return mixed
	 *   false: no error
	 *   SdvException: instance of recoverable exception occured
	 *     (usually during storing of form data into model).
	 */
	public function getError( $rowNum = null ) {
		if ( is_array( $this->errors ) ) {
			if ( $rowNum === null ) {
				return reset( $this->errors );
			} else {
				return array_key_exists( $rowNum, $this->errors ) ? $this->errors[$rowNum] : false;
			}
		} else {
			return $this->errors;
		}
	}

	public function setError( SdvException $e, $rowNum = null ) {
		if ( $rowNum === null ) {
			$this->errors = $e;
		} else {
			if ( !is_array( $this->errors ) ) {
				$this->errors = array();
			}
			# Is meaningful when ->isMulti = true.
			# See CompoundFieldDataSource::toModel() for details.
			$this->errors[$rowNum] = $e;
		}
	}

	public function isBoundToTableField() {
		return isset( $this->tableName ) && isset( $this->fieldName );
	}

	/**
	 * Binds UI field to specified schema table / field.
	 */
	public function bindToTableField( $tableName, $fieldName = null ) {
		if ( $fieldName !== null ) {
			$this->fieldName = $fieldName;
		} elseif ( !isset( $this->fieldName ) ) {
			# Used by CompoundFieldDataSource::addChild()
			# @note: For CompoundFieldDataSource child ->inputBaseName equals fieldName of "parent" tableName.
			$this->fieldName = $this->inputBaseName;
		}
		$this->tableName = $tableName;
		/**
		 * @note: Table names are separated via double underscores,
		 * because single underscores are common in table names.
		 */
		$this->inputBaseName = "{$this->tableName}__{$this->fieldName}";
	}

	/**
	 * Associate field data source with model.
	 */
	public function setModel( $model = null ) {
		if ( $model !== null && !$this->isBoundToTableField() ) {
			SdvException::throwError(
				'Specifying ->model requires ->fqnField to be set',
				__METHOD__,
				array(
					'source' => $this,
					'model' => $model
				)
			);
		}
		$this->model = $model;
	}

	/**
	 * @return mixed
	 *   value of associated model table field;
	 */
	public function getModelVal() {
		return $this->model->getFieldVals( $this->tableName, $this->fieldName );
	}

	/**
	 * Get variable names (GET / POST).
	 * @note: Use via appContext->request.
	 * By default assumes that element defines one input.
	 * @return mixed
	 *   string | array(string)
	 *     value: GET / POST variable name of associated field;
	 */
	public function getVarNames() {
		return $this->getInputBaseName();
	}

	/**
	 * Get html input names. Matches variable names with the exception that
	 * child fields of CompoundField has ->isMulti = true which generates
	 * [] postfix.
	 * @note: Use by field view.
	 * @return mixed
	 *   string | array(string)
	 *     value: html input element name of associated field;
	 */
	public function getInputNames() {
		$names = $this->getVarNames();
		if ( $this->isMulti() ) {
			if ( is_array( $names ) ) {
				SdvException::throwError(
					'Multi-value field data source cannot be a part of CompoundField',
					__METHOD__,
					$this
				);
			} else {
				return "{$names}[]";
			}
		} else {
			return $names;
		}
	}

	public function isNonEmptyStr( $val ) {
		$val = trim( $val );
		return ($val === '') ? null : $val;
	}

	public function isUint( $val ) {
		$val = trim( $val );
		if ( !is_numeric( $val ) ) {
			return null;
		}
		$val = intval( $val );
		return $val < 0 ? null : $val;
	}

	protected function requestValue( $inputName ) {
		global $appContext;
		return $appContext->request->getGPval( $inputName );
	}

	public function getValidatedInput( $inputName ) {
		$inputValue = $this->requestValue( $inputName );
		if ( isset( $this->valueValidator ) ) {
			if ( is_string( $this->valueValidator ) ) {
				return $this->{$this->valueValidator}( $inputValue );
			} elseif ( $this->valueValidator instanceof Closure ) {
				return $this->valueValidator->__invoke( $inputValue );
			} elseif ( is_callable( $this->valueValidator ) ) {
				return call_user_func( $this->valueValidator, $inputValue );
			} else {
				SdvException::throwError(
					'Invalid value of ->valueValidator',
					__METHOD__,
					$this->valueValidator
				);
			}
		} else {
			return $inputValue;
		}
	}

	/**
	 * @return mixed
	 *   scalar / array
	 *     val: value(s) of each html input with name(s) got from ->getVarNames();
	 */
	public function getUserInputs() {
		$inputNames = $this->getVarNames();
		if ( is_array( $inputNames ) ) {
			$userChoice = array();
			foreach ( $inputNames as $inputKey => $inputName ) {
				$userChoice[$inputKey] = $this->getValidatedInput( $inputName );
			}
		} else {
			$userChoice = $this->getValidatedInput( $inputNames );
		}
		return $userChoice;
	}

	public function getFQNfieldName() {
		return "{$this->tableName}.{$this->fieldName}";
	}

	public function getTableName() {
		return $this->tableName;
	}

	public function getFieldName() {
		return $this->fieldName;
	}

	/**
	 * @return array
	 *   key / value for http_build_string()
	 */
	public function getQuery() {
		if ( $this->choice === null ) {
			return null;
		}
		$inputName = $this->getVarNames();
		if ( is_array( $inputName ) ) {
			SdvException::throwError(
				'Unimplemented ->getQuery() to build query array from multiple input names',
				__METHOD__,
				$inputName
			);
		}
		return array( $inputName => $this->choice );
	}

	protected function setModelField( $fieldMethod, $val ) {
		Dbg\log(__METHOD__.':fieldMethod',$fieldMethod);
		Dbg\log(__METHOD__.':val',$val);
		try {
			$this->model->{$fieldMethod}( $val );
		} catch ( SdvException $e ) {
			if ( $e->getErrorCode() !== 'recoverable' ) {
				# Re-throw unrecoverable exception.
				throw $e;
			}
			$this->setError( $e );
		}
	}

	/**
	 * Stores current field choice into model.
	 * Empty string form input values will be translated via ->getEmptyTo() value.
	 *   false : do not translate
	 *     (will store empty string to model);
	 *   instanceof NoValue : do not store empty string into model
	 *     (used for PK's of newly created models);
	 *   otherwise : use ->getEmptyTo() value instead of empty string;
	 */
	public function toModel() {
		$tableName = $this->getTableName();
		$fieldName = $this->getFieldName();
		list( $fieldMethod, $fieldDef ) = $this->model->getFieldHandler( $tableName, $fieldName );
		$choice = $this->getChoice();
		# Dbg\log(__METHOD__.':tableName',$tableName);
		# Dbg\log(__METHOD__.':fieldName',$fieldName);
		# Dbg\log(__METHOD__.':choice',$choice);
		if ( $choice !== null ) {
			if ( is_array( $choice ) ) {
				foreach ( $choice as $scalarChoice ) {
					$this->setModelField( $fieldMethod, $scalarChoice );
				}
			} else {
				if ( trim( $choice ) === '' ) {
					$emptyTo = $this->getEmptyTo();
					if ( !$emptyTo instanceof NoValue ) {
						if ( $emptyTo !== false ) {
							$choice = $emptyTo;
						}
						$this->setModelField( $fieldMethod, $choice );
					}
				} else {
					$this->setModelField( $fieldMethod, $choice );
				}
			}
		}
	}

} /* end of SingleFieldDataSource */

class ScalarFieldDataSource extends SingleFieldDataSource {

	public function setChoice( $choice ) {
		# Scalar value.
		if ( is_array( $choice ) ) {
			$choice = array_pop( $choice );
		}
		parent::setChoice( $choice );
	}

	public function getMatchingClause() {
		global $appContext;
		return $appContext->dbw->prepareQuery(
			array( $this->tableName, $this->fieldName ), $this->cmp, "?choice",
			new DbwParams( array( 'choice' => $this->choice ) )
		);
	}

} /* end of ScalarFieldDataSource class */

class CheckboxFieldDataSource extends SingleFieldDataSource {

	public function setChoice( $choice ) {
		if ( $choice !== null && !is_array( $choice ) ) {
			$choice = array( $choice );
		}
		parent::setChoice( $choice );
	}

	public function getMatchingClause() {
		global $appContext;
		return $appContext->dbw->getSingleRowInClause(
			array( $this->tableName, $this->fieldName ),
			$this->choice
		);
	}

} /* end of CheckboxFieldDataSource class */

class RangeFieldDataSource extends SingleFieldDataSource {

	public function getVarNames() {
		$inputName = parent::getVarNames();
		return array(
			'min' => "{$inputName}__min",
			'max' => "{$inputName}__max"
		);
	}

	public function setUserChoice() {
		$inputs = $this->getUserInputs();
		$userChoice = null;
		# Range of numeric values.
		$inputs['min'] = is_numeric( $inputs['min'] ) ?
			intval( $inputs['min'] ) : null;
		$maxVal = is_numeric( $inputs['max'] ) ?
			intval( $inputs['max'] ) : null;
		if ( $inputs['min'] !== null || $inputs['max'] !== null ) {
			$userChoice = $inputs;
		}
		$this->setChoice( $userChoice );
	}

	public function getModelVal() {
		SdvException::throwError( 'This type of field cannot be connected to model', __METHOD__ );
	}

	public function getMatchingClause() {
		global $appContext;
		$result = '';
		if ( $this->choice['min'] !== null ) {
			$result .= $appContext->dbw->prepareQuery(
				array( $this->tableName, $this->fieldName ), ">=", "?choice.min",
				new DbwParams( array( 'choice.min' => $this->choice['min'] ) )
			);
		}
		if ( $this->choice['max'] !== null ) {
			if ( $result !== '' ) {
				$result .= ' AND ';
			}
			$result .= $appContext->dbw->prepareQuery(
				array( $this->tableName, $this->fieldName ), "<=", "?choice.max",
				new DbwParams( array( 'choice.max' => $this->choice['max'] ) )
			);
		}
		return $result;
	}

	public function getQuery() {
		if ( $this->choice === null ) {
			return null;
		}
		$query = array();
		foreach ( $this->getVarNames() as $inputKey => $inputName ) {
			if ( $this->choice[$inputKey] !== null ) {
				$query[$inputName] = $this->choice[$inputKey];
			}
		}
		return $query;
	}

} /* end of RangeFieldDataSource class */

class FileFieldDataSource extends SingleFieldDataSource {

	# Base directory for file storage
	protected $baseDir;

	protected function requestValue( $inputName ) {
		global $appContext;
		return $appContext->request->getFile( $inputName );
	}

	public function getBaseDir() {
		return $this->baseDir;
	}

	/**
	 * The directory or subdirectory where succesfully uploaded file
	 * should be placed to.
	 * Files that validator will not place into such directory will be
	 * rejected and not displayed as links.
	 */
	public function setBaseDir( $baseDir ) {
		$fullDir = realpath( $baseDir );
		if ( $fullDir === false ) {
			SdvException::throwError(
				'Specified baseDir does not exists',
				__METHOD__,
				$baseDir
			);
		}
		$this->baseDir = $fullDir;
	}

	public function setDataDef( \stdClass $data ) {
		# @note: Validator's responsibility is to move temporary file
		# into destination directory, in case of success, or throw an
		# error in case of failure.
		if ( !isset( $data->validator ) ) {
			SdvException::throwError(
				'This type of field requires validator to be set',
				__METHOD__,
				$data
			);
		}
		parent::setDataDef( $data );
		if ( !isset( $data->baseDir ) ) {
			SdvException::throwError(
				'This type of field requires baseDir to be set',
				__METHOD__,
				$data
			);
		}
		$this->setBaseDir( $data->baseDir );
	}

} /* end of FileFieldDataSource class */

class CompoundFieldDataSource extends AbstractFieldDataSource {

	public $childs = array();

	public function addChild( SingleField $field ) {
		# @note: For CompoundFieldDataSource ->inputBaseName equals child field tableName.
		$field->source->bindToTableField( $this->inputBaseName );
		$fieldName = $field->source->getFieldName();
		# Will submit multiple child fields as array.
		$field->source->setMulti( true );
		# Childs should have the same index as CompoundField.
		$field->source->setFieldIdx( $this->getFieldIdx() );
		$this->childs[$fieldName] = $field;
	}

	public function setDataDef( \stdClass $data ) {
		parent::setDataDef( $data );
		# @note: For CompoundFieldDataSource ->inputBaseName equals child field tableName.
		$this->inputBaseName = $data->name;
	}

	public function isBoundToTableField() {
		return true;
	}

	/**
	 * Associate field data source with model.
	 */
	public function setModel( $model = null ) {
		if ( !($model instanceof AbstractModel ) ) {
			SdvException::throwError( __CLASS__ . ' requires real model to be bound', __METHOD__,
				$model
			);
		}
		$this->model = $model;
	}

	public function getChildFieldNames() {
		return array_keys( $this->childs );
	}

	public function getUserInputs() {
		$result = array();
		foreach ( $this->childs as $fieldName => $child ) {
			$result[$fieldName] = $child->source->getUserInputs();
		}
		Dbg\log(__METHOD__,$result);
		return $result;
	}

	/**
	 * @return mixed
	 *   value of associated model table field;
	 */
	public function getModelVal() {
		$result = $this->model->getFieldsetVals( $this->inputBaseName, $this->getChildFieldNames() );
		Dbg\log(__METHOD__,$result);
		return $result;
	}

	public function getError( $rowNum = null ) {
		if ( $rowNum !== null ) {
			SdvException::throwError(
				'Nesting CompoundField is unsupported',
				__METHOD__,
				$this
			);
		}
		foreach ( $this->childs as $child ) {
			$childError = $child->source->getError();
			if ( $childError !== false ) {
				return $childError;
			}
		}
		return false;
	}

	/**
	 * @return integer
	 *   total number of rows for current choice;
	 */
	public function getChoiceRowsCount() {
		foreach ( $this->childs as $fieldName => $child ) {
			# Number of rows is always the same for all child fields,
			# so we are just returning number of first child field rows.
			if ( $fieldName === 'pub_category_id' ) {
				Dbg\log(__METHOD__.':choice',$this->choice);
			}
			return array_key_exists( $fieldName, $this->choice ) ?
				count( $this->choice[$fieldName] ) : 0;
		}
	}

	/**
	 * Stores current UI field choice into model.
	 */
	public function toModel() {
		# CompoundField name is a linked schema table name.
		# It's child field names are schema table field names.
		$tableName = $this->inputBaseName;
		$childFieldNames = $this->getChildFieldNames();
		# Get field definition from model's field(s) handler.
		list( $rowMethod, $fieldDef ) = $this->model->getFieldHandler( $tableName, $childFieldNames[0] );
		# Make single-field definition to be compatible to multi-field definition.
		$fieldDef = (array) $fieldDef;
		# Field definition should exactly match child fieldnames.
		if ( serialize( $fieldDef ) !== serialize( $childFieldNames ) ) {
			SdvException::throwError(
				'table handler fields do not match compound field child fields',
				__METHOD__,
				array( 'handler' => $fieldDef, 'compound' => $childFieldNames )
			);
		}
		$choice = $this->getChoice();
		$rowCount = $this->getChoiceRowsCount();
		# Dbg\log(__METHOD__.':rowCount',$rowCount);
		for ( $rowNum = 0; $rowNum < $rowCount; $rowNum++ ) {
			$args = array();
			foreach ( $childFieldNames as $fieldName ) {
				$args[] = $choice[$fieldName][$rowNum];
			}
			Dbg\log(__METHOD__.':rowMethod',$rowMethod);
			Dbg\log(__METHOD__.':args',$args);
			try {
				call_user_func_array( array( $this->model, $rowMethod ), $args );
			} catch ( SdvException $e ) {
				if ( $e->getErrorCode() !== 'recoverable' ) {
					# Re-throw unrecoverable exception.
					throw $e;
				}
				$exData = $e->getData();
				if ( !is_array( $exData ) || !array_key_exists( 'fieldName', $exData ) ) {
					# Re-throw exception which has no offending fieldName specified.
					throw $e;
				}
				# Turn on error for offending fieldName at current row number.
				$this->childs[$exData['fieldName']]->source->setError( $e, $rowNum );
			}
		}
	}

} /* end of CompoundFieldDataSource class */
