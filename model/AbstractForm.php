<?php
namespace QuestPC;

class AbstractForm
	extends FeaturesObject
	implements \Countable, \Iterator {

	/**
	 * id of form, usually used as form[@id] and submit input[@name]
	 */
	protected $formId;
	public function getFormId() {
		return $this->formId;
	}

	/**
	 * Named fields collection (for SQL query building and form view).
	 *   key: string
	 *     unique inputBaseName;
	 *     might be used for non-linear access to fields in form views;
	 *   value: AbstractField
	 *     field controller, which connects html input(s),
	 *     subquery builder and field view.
	 */
	protected $fields = array();

	/*** begin of iterator methods ***/
	public function count() {
		return count( $this->fields );
	}

	function rewind() {
		reset( $this->fields );
	}

	function current() {
		return current( $this->fields );
	}

	function key() {
		return key( $this->fields );
	}

	function next() {
		next( $this->fields );
	}

	function valid() {
		return key( $this->fields ) !== null;
	}
	/*** end of iterator methods ***/

	public function addField( AbstractField $field ) {
		$inputBaseName = $field->source->getInputBaseName();
		if ( array_key_exists( $inputBaseName, $this->fields ) ) {
			SdvException::throwError(
				'Non-unique input base name',
				__METHOD__,
				array(
					'inputBaseName' => $inputBaseName,
					'input1' => $this->fields[$inputBaseName],
					'input2' => $field,
				)
			);
		}
		$this->fields[$inputBaseName] = $field;
	}

	public function addFields( /* $fields */ ) {
		$fields = func_get_args();
		foreach ( $fields as $field ) {
			$this->addField( $field );
		}
	}

	/**
	 * A safer shortcut to ->fields[]
	 */
	public function getField( $inputBaseName ) {
		if ( !array_key_exists( $inputBaseName, $this->fields ) ) {
			SdvException::throwError(
				'Unknown input base name',
				__METHOD__,
				$inputBaseName
			);
		}
		return $this->fields[$inputBaseName];
	}

	public function hasField( $inputBaseName ) {
		return array_key_exists( $inputBaseName, $this->fields );
	}

	/**
	 * In case one wants to replace field, just removeField() it,
	 * then addField() with the same inputBaseName.
	 */
	public function removeField( $inputBaseName ) {
		if ( !array_key_exists( $inputBaseName, $this->fields ) ) {
			SdvException::throwError(
				'Unknown input base name',
				__METHOD__,
				$inputBaseName
			);
		}
		unset( $this->fields[$inputBaseName] );
	}

	/**
	 * Get current field choice by it's inputBaseName.
	 */
	public function getChoice( $inputBaseName ) {
		if ( !array_key_exists( $inputBaseName, $this->fields ) ) {
			SdvException::throwError(
				'Unknown input base name',
				__METHOD__,
				$inputBaseName
			);
		}
		return $this->fields[$inputBaseName]->source->getChoice();
	}

	/**
	 * Set current field choice by it's inputBaseName.
	 */
	public function setChoice( $inputBaseName, $choice ) {
		if ( !array_key_exists( $inputBaseName, $this->fields ) ) {
			SdvException::throwError(
				'Unknown input base name',
				__METHOD__,
				$inputBaseName
			);
		}
		return $this->fields[$inputBaseName]->source->setChoice( $choice );
	}

	/**
	 * A shortcut to ->fields[]->view()
	 */
	public function view( $inputBaseName, $mode ) {
		if ( !array_key_exists( $inputBaseName, $this->fields ) ) {
			SdvException::throwError(
				'Unknown input base name',
				__METHOD__,
				$inputBaseName
			);
		}
		return $this->fields[$inputBaseName]->view( $mode );
	}

	/**
	 * A shortcut to ->fields[]->view( 'edit' )
	 */
	public function edit( $inputBaseName ) {
		if ( !array_key_exists( $inputBaseName, $this->fields ) ) {
			SdvException::throwError(
				'Unknown input base name',
				__METHOD__,
				$inputBaseName
			);
		}
		return $this->fields[$inputBaseName]->view();
	}

	/**
	 * Get form query string (usually for AbstractSearchForm) like if it was submitted
	 * via GET method.
	 */
	public function getQueryString() {
		$query = array();
		foreach ( $this->fields as $field ) {
			$fieldQuery = $field->source->getQuery();
			if ( $fieldQuery !== null ) {
				$query = array_merge( $query, $fieldQuery );
			}
		}
		# this query string matches form-submitted query string, except for:
		# 1. 'search' key of submit button is not passed
		# 2. array keys are passed in form [0], [1], [2] instead of [], [], []
		# Dbg\log(__METHOD__.':query',$query);
		$result = http_build_query( $query, '', '&' );
		if ( $result !== '' ) {
			return "?{$result}";
		}
		return '';
	}

	/**
	 * Reset form to default values.
	 */
	public function setEmptyChoices() {
		foreach ( $this->fields as $field ) {
			# null is translated into default value.
			$field->source->setChoice( null );
		}
		# Dbg\log(__METHOD__,$this->fields);
	}

	/**
	 * @note: User choices will be validated by field data source validators, when
	 * these are passed to field data source constructor.
	 */
	public function setUserChoices() {
		foreach ( $this->fields as $field ) {
			$field->source->setUserChoice();
		}
		# Dbg\log(__METHOD__,$this->fields);
	}

	public function getChoices() {
		$result = array();
		foreach ( $this->fields as $field ) {
			$result[$field->source->getInputBaseName()] = $field->source->getChoice();
		}
		return $result;
	}

} /* end of AbstractForm class */
