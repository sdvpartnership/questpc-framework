<?php
namespace QuestPC;

/**
 * Mapping of source tree pathes to destination tree pathes.
 *
 * The trees are usually stripped pathes of CategoryTree instances,
 * however are not limited to (there's not much of CategoryTree binding).
 *
 * Currently maps one source path to one destination path or to nothing (false).
 * @todo: Support multiple destination pathes per one source path, when needed.
 */
class AbstractTreeMap {

	# Intentionally made non-zero to catch ->map definition errors.
	/**
	 * Optional ->map node key which value is base path of sublevel's destination path values
	 * that begin with '/' character.
	 */
	const BASE_PATH = 1;
	/**
	 * Optional ->map node key which is used to match current tree level when
	 * no matches are found at it's sublevels.
	 */
	const MATCH_ALL = 2;

	protected $debug = false;

	/**
	 * A small sample map of source to destination tree pathes.
	 * Do not forget to override in descendant class.
	 */
	protected $map = array(
		/*
		'Books of our shop' => array(
			self::BASE_PATH => 'Books',
			self::MATCH_ALL => 'Books/Another',
			'Fiction' => array(
				self::BASE_PATH => '/Fiction', // 'Books/Fiction' (from parent BASE_PATH)
				self::MATCH_ALL => '/Another', // 'Books/Fiction/Another' (from self::BASE_PATH)
				'Fantasy' => '/', // 'Books/Fiction/Fantasy'
				// will not map to destination (no destination), even skip self::MATCH_ALL;
				'Bad jokes' => false,
				'Sci-fi' => '/Scientific', // 'Books/Fiction/Scientific'
			),
			'History' => array(
				self::BASE_PATH => 'Books/History', // 'Books/History' (copied complete path)
				self::MATCH_ALL => '', // 'Books/History', (from self::BASE_PATH)
				'Fiction' => '/Fantasy and fiction', // 'Books/History/Fantasy and fiction',
				'Chronicles' => '/Ancient writings', // 'Books/History/Ancient writings'
				'Asian' => '/', // 'Books/History/Asian'
			),
		)
		*/
	);

	/**
	 * A stack of self::BASE_PATH key node values. It's rolled only at levels
	 * which have self::BASE_PATH key defined.
	 */
	protected $basePathStack = array();
	/**
	 * Current source path array. Rolled during recursion.
	 */
	protected $srcPath = array();
	/**
	 * Which level of source tree path is minimally available for mapping.
	 * Used as performance optimization in ::match().
	 */
	protected $minSrcPathLen;
	/**
	 * ->map that is converted from source notation (efficient text definition) into
	 * "working" notation (efficient tree path matching).
	 *
	 * The source nested keys are replaced to one-level serialized source pathes keys.
	 * The destination string values are replaced to path arrays (arrays of nodes).
	 * self::BASE_PATH substitution of first destination node is pefrormed,
	 * when applicable.
	 */
	protected $pathMap = array();

	/**
	 * Try to substitute default left / right values into specified path.
	 *
	 *   Left substitution is peformed for first empty element of $dstPath
	 *     by adding top value from ->basePathStack, if there's any.
	 *   Right substitution is performed for last empty element of $dstPath
	 *     by adding $topNodeNameTpl, if defined.
	 *
	 * @param $dstPath array
	 *   Destination path which will be tried to substitute.
	 * @param $topNodeNameTpl mixed
	 *   string: right substitution value;
	 *   null: unavailable (impossible to use);
	 */
	protected function substitutePath( array &$dstPath, $topNodeNameTpl = null ) {
		/* if ( $this->debug ) {
			Dbg\log(__METHOD__.':dstPath before',$dstPath);
			Dbg\log(__METHOD__.':basePathStack',$this->basePathStack);
			Dbg\log(__METHOD__.':topNodeNameTpl',$topNodeNameTpl);
		} */
		# Substitution should be detected before $dstPath is actually modified.
		$hasLeftSubstitution = count( $dstPath ) === 0 ||
			$dstPath[0] === '';
		$hasRightSubstitution = count( $dstPath ) > 0 &&
			$dstPath[count( $dstPath ) - 1] === '';
		if ( $hasLeftSubstitution ) {
			# Check whether there is basePath in ->basePathStack.
			if ( count( $this->basePathStack ) === 0 ) {
				SdvException::throwError( 'Cannot substitute empty base path', __METHOD__ );
			}
			# Substitute '/path1/pathN' to 'basepath1/basepathN/path1/pathN'.
			array_shift( $dstPath );
			$dstPath = array_merge(
				$this->basePathStack[count( $this->basePathStack ) - 1],
				$dstPath
			);
		}
		if ( $hasRightSubstitution && $topNodeNameTpl !== null ) {
			# Substitute destination path empty top node name when possible.
			$topNode = array_pop( $dstPath );
			/*
			if ( $topNode !== null && $topNode !== '' ) {
				# Put top destination node back, because it's not empty.
				$dstPath[] = $topNode;
			}
			*/
			$dstPath[] = $topNodeNameTpl;
		}
		if ( count( $dstPath ) === 0 ) {
			SdvException::throwError( 'Empty path map', __METHOD__ );
		}
		# Check $dstPath for empty nodes (source code mistakes).
		foreach ( $dstPath as $nodeName ) {
			if ( $nodeName === '' ) {
				SdvException::throwError( 'Empty node in path map', __METHOD__ );
			}
		}
		if ( $this->debug ) {
			# Dbg\log(__METHOD__.':dstPath result',$dstPath);
		}
	}

	/**
	 * Adds source path to destination path mapping.
	 * Source path is current value of ->srcPath property,
	 * which value is updated recursively.
	 * @param $dstPathStr mixed
	 *   string: destination path string to match;
	 *   false: no destination: do not match source to destination (skip);
	 *     When destination path string begins with empty node name '/'
	 *     or is completely empty '', will try to use top element of ->basePathStack
	 *     as the value replacement.
	 * @param $topNodeNameTpl mixed
	 *   string: When last element of destination path string is empty '/',
	 *   it will be replaced by this value;
	 *   null: unavailable (impossible to use);
	 */
	protected function addMapping( $dstPathStr, $topNodeNameTpl = null ) {
		$srcPathKey = serialize( $this->srcPath );
		if ( $this->debug ) {
			# Dbg\log(__METHOD__.':dstPathStr',$dstPathStr);
			# Dbg\log(__METHOD__.':topNodeNameTpl',$topNodeNameTpl);
			Dbg\log(__METHOD__.':src',CategoryTree::encodePath( $this->srcPath ));
		}
		if ( $dstPathStr === false ) {
			$this->pathMap[$srcPathKey] = false;
			if ( $this->debug ) {
				Dbg\log(__METHOD__.':dst',false);
			}
			return;
		}
		if ( !is_string( $dstPathStr ) ) {
			SdvException::throwError( 'Path map should be a string', __METHOD__,
				array(
					'src' => $srcPathKey,
					'dst' => $dstPathStr
				)
			);
		}
		$dstPath = CategoryTree::extractPath( $dstPathStr );
		try {
			$this->substitutePath( $dstPath, $topNodeNameTpl );
		} catch ( SdvException $e ) {
			$e->setData( array(
				'src' => $srcPathKey,
				'dst' => $dstPathStr
			) );
			throw $e;
		}
		# Create pathInfo node for $srcPathKey.
		$this->pathMap[$srcPathKey] = (object) array(
			# Destination path array to match.
			'dst' => $dstPath,
			# Statistics of source pathes matching for current destination path.
			# Might be ->srcPath or _some_ of it's nested subpathes.
			'hits' => array(),
			# was:
			# 'hit' => false,
		);
		if ( $this->debug ) {
			Dbg\log(__METHOD__.':dst',CategoryTree::encodePath( $dstPath ));
		}
		# Find current ->minSrcPathLen used in ::match() performance optimization.
		if ( !isset( $this->minSrcPathLen ) ||
				$this->minSrcPathLen > count( $this->srcPath ) ) {
			$this->minSrcPathLen = count( $this->srcPath );
		}
	}

	/**
	 * Recursively converts tree map from source notation that is optimal
	 * for source definition to notation that is optimal to match pathes.
	 * @param $level array
	 *   current level of ->map;
	 * @param $topNodeNameTpl mixed
	 *   string: When last element of destination path string is empty '/',
	 *   it will be replaced by this value;
	 *   null: unavailable (impossible to use);
	 */
	protected function convertMap( array &$level, $topNodeNameTpl = null ) {
		# Whether _current_ level has self::BASE_PATH node?
		$basePathDefined = false;
		foreach ( $level as $nodeName => $leaf ) {
			if ( $nodeName === self::BASE_PATH ) {
				$basePathDefined = true;
				# Top element of ->basePathStack is basePath replacement value
				# for current subtree.
				$basePath = CategoryTree::extractPath( $leaf );
				try {
					$this->substitutePath( $basePath, $topNodeNameTpl );
				} catch ( SdvException $e ) {
					$e->setData( array(
						'srcPath' => $this->srcPath,
						'src' => $nodeName,
						'basePath' => $leaf,
					) );
					throw $e;
				}
				/* if ( $this->debug ) {
					Dbg\log(__METHOD__.':added basePath',CategoryTree::encodePath( $basePath ));
				} */
				$this->basePathStack[] = $basePath;
				continue;
			} elseif ( $nodeName === self::MATCH_ALL ) {
				# $leaf is dstPath string. Map ->srcPath to $leaf.
				# This mapping will match to base of subtree when no subpathes match.
				$matchAllPath = CategoryTree::extractPath( $leaf );
				try {
					$this->substitutePath( $matchAllPath, $topNodeNameTpl );
				} catch ( SdvException $e ) {
					$e->setData( array(
						'matchAll' => $leaf,
						'src' => $nodeName,
						'dst' => $dstPathStr,
					) );
					throw $e;
				}
				$this->addMapping( CategoryTree::encodePath( $matchAllPath ) );
				continue;
			}
			if ( !is_string( $nodeName ) ) {
				SdvException::throwError( 'Invalid value of node name', __METHOD__,
					array(
						'nodeName' => $nodeName,
						'srcPath' => $this->srcPath,
						'leaf' => $leaf,
					)
				);
			}
			# "Normal" nodeName, add it to ->srcPath.
			$this->srcPath[] = $nodeName;
			if ( is_array( $leaf ) ) {
				# Map pathes recursively.
				# Will use source $nodeName key as $topNodeNameTpl for
				# self::MATCH_ALL destination child subnodes.
				$this->convertMap( $leaf, $nodeName );
			} else {
				# $leaf is dstPath string or false. Map ->srcPath to $leaf.
				# Also will substitute destination path empty top node name
				# with current source $nodeName, when possible.
				$this->addMapping( $leaf, $nodeName );
			}
			# Current $nodeName matching is over.
			array_pop( $this->srcPath );
		}
		if ( $basePathDefined ) {
			# Current $basePath matching is over.
			array_pop( $this->basePathStack );
		}
	}

	/**
	 * @param $srcPath array
	 *   source CategoryTree path to check for possible matches
	 *   in current instance.
	 * @return mixed
	 *   array: matching mapped path to create;
	 *   false: no matches for $srcPath;
	 */
	public function match( array $srcPath ) {
		# Remove 'name' nodes from source path, if any.
		$srcMatchPath = CategoryTree::stripPath( $srcPath );
		# Save serialized key of original $srcPath supplied.
		$srcPathKey = serialize( $srcMatchPath );
		# Try to match source path and lower depths till minimal available depth.
		while ( count( $srcMatchPath ) >= $this->minSrcPathLen ) {
			$srcMatchKey = serialize( $srcMatchPath );
			if ( array_key_exists( $srcMatchKey, $this->pathMap ) ) {
				$pathInfo = $this->pathMap[$srcMatchKey];
				if ( $pathInfo === false ) {
					return false;
				}
				# Add _original_ $srcPath key to matching node statistics.
				# @todo: check RAM / CPU consumption
				$pathInfo->hits[$srcPathKey] = true;
				# @note: previous less informative but faster variant:
				# $pathInfo->hit = true;
				return $pathInfo->dst;
			}
			# $srcMatchPath is decreased because current level has no match
			# till match is found or ->minSrcPathLen is reached.
			array_pop( $srcMatchPath );
		}
		return false;
	}

	/**
	 * @return array
	 *   ->pathMap with currently collected hits - actual mappings in ::match();
	 */
	public function getPathHits() {
		return $this->pathMap;
	}

	/**
	 * @param $basePath array
	 *   Usually is a value of AbstractUrlDescription::path property
	 *   in descendant of AbstractUrlSource::iterate() callback
	 *   in actual job class.
	 */
	function __construct( array $basePath ) {
		global $appContext;
		$this->debug = property_exists( $appContext, 'debugTreeMap' ) &&
			$appContext->debugTreeMap;
		# Start source path from supplied base path level (usually a shop name).
		$this->srcPath = $basePath;
		# Dbg\log(__METHOD__.':basePath',$basePath);
		if ( count( $this->map ) === 0 ) {
			SdvException::throwError( 'Please define real ->map in descendant class', __METHOD__ );
		}
		# Convert ->map to ->pathMap.
		$this->convertMap( $this->map );
		# Dbg\log(__METHOD__.':pathMap',$this->pathMap);
		# Dbg\log(__METHOD__.':minSrcPathLen',$this->minSrcPathLen);
		# ->map is unneeded during match() runs.
		unset( $this->map );
	}

} /* end of AbstractTreeMap class */
