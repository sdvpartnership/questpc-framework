<?php
namespace QuestPC;

/**
 * Form that is used to build query for selected schema tables.
 */
class AbstractSearchForm extends AbstractForm {

	protected $mainTableName;
	protected $primaryKey;

	/**
	 * Builds model search query for specified DbPager.
	 * @param $orderField string
	 *   field name of ->mainTableName to sort the pager
	 * @param $orderWay string
	 *   'ASC' / 'DESC' - capitalized only
	 */
	public function bindQuery( $orderField, $orderWay, DbPager $pager ) {
		global $appContext;
		if ( $orderWay !== 'ASC' ) {
			$orderWay = 'DESC';
		}
		$dbw = $appContext->dbw;
		$select = $dbw->prepareQuery(
			"SELECT", array( $this->mainTableName, '*' ), "FROM", "^{$this->mainTableName}"
		);
		$where = '';
		$joins = array();
		foreach ( $this->fields as $field ) {
			$dataSource = $field->source;
			if ( $dataSource->getChoice() === null ) {
				continue;
			}
			if ( $where !== '' ) {
				$where .= ' AND ';
			} else {
				$where = "\nWHERE ";
			}
			$where .= $dataSource->getMatchingClause();
			$tableName = $dataSource->getTableName();
			if ( $tableName !== $this->mainTableName &&
					!array_key_exists( $tableName, $joins ) ) {
				$joins[$tableName] = $dbw->prepareQuery(
					"LEFT JOIN", "^{$tableName}", "ON",
					array( $this->mainTableName, $this->primaryKey ), "=",
					array( $tableName, $this->primaryKey ) );
			}
		}
		$pager->setPagerQuery(
			"#{$select} " . implode( ' ', $joins ) . " {$where} ",
			"GROUP BY", array( $this->mainTableName, $this->primaryKey ),
			"ORDER BY", array( $this->mainTableName, $orderField ), $orderWay
		);
	}

} /* end of AbstractSearchForm class */
