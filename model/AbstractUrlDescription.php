<?php
namespace QuestPC;

/**
 * Provides information on specified url source as well as
 * supports of local mirroring of banned sources, which are
 * unavailable to download at regular hosting.
 *
 * Descentant instance of AbstractUrlDescription should be returned
 * by descendant instance of AbstractUrlSource class.
 */
abstract class AbstractUrlDescription {

	# Might be optionally overriden in descendant's ->processFields() implementation.
	protected $curlExtraOpts = array();

	# instanceof WebDav class for mirroring of banned url sources;
	protected static $mirror;
	/**
	 * static::$mirror parameters
	 */
	protected static $host;
	protected static $login;
	protected static $password;
	# basepath for webdav mirrors
	protected static $baseCol;

	/**
	 * Description properties.
	 * Please do not modify, they are made public to make
	 * implementation lightweight.
	 */
	public $banned;
	public $channel_id;
	public $city_id;
	public $path;

	function __construct( $channelId, $cityId, $pathRoot ) {
		$this->banned = false;
		$this->channel_id = $channelId;
		$this->city_id = $cityId;
		$this->path = array( $pathRoot );
	}

	/**
	 * Called before AbstractUrlSource::getNextFeed() returns this instance.
	 * May be used in ancestor class to conditionally setup additional fields.
	 */
	public function processFields() {
		/* noop */
	}

	public static function mirrorSingleton() {
		if ( !isset( static::$mirror ) ) {
			static::$mirror = new WebDav();
			static::$mirror->setHost( static::$host, static::$login, static::$password );
		}
	}

	public static function mkMirrorBaseCol() {
		static::mirrorSingleton();
		static::$mirror->mkCol( static::$baseCol );
	}

	/**
	 * Encodes url so it becomes compatible to filesystem storage
	 * (no webdav disalloved characters).
	 */
	public static function urlFSencode( $url ) {
		return preg_replace(
			array(
				'/_/',
				'/%([\dA-F]{2})/'
			),
			array(
				'_5F',
				'_$1'
			),
			urlencode( $url )
		);
	}

	protected function getUrlFSencode() {
		return static::urlFSencode( $this->url );
	}

	protected function getMirrorPath() {
		return  static::$baseCol . '/' . $this->getUrlFSencode();
	}

	/**
	 * Direct download.
	 */
	protected function directDownload() {
		$retval = -1;
		$fileName = CurlExec::download( $this->url, $retval, $this->curlExtraOpts );
		if ( $retval !== 0 ) {
			SdvException::throwError( 'Error executing command', __METHOD__, Gl::$lastCmdLine );
		}
		return $fileName;
	}

	/**
	 * Download from mirror.
	 */
	protected function mirrorDownload() {
		static::mirrorSingleton();
		return static::$mirror->download( $this->getMirrorPath() );
	}

	public function uploadBanned() {
		if ( !$this->banned ) {
			return;
		}
		$fileName = '';
		try {
			$fileName = $this->directDownload();
			static::mirrorSingleton();
			static::$mirror->uploadFile( $this->getMirrorPath(), $fileName );
		} catch ( \Exception $e ) {
			if ( $fileName !== '' ) {
				@unlink( $fileName );
			}
			throw $e;
		}
		@unlink( $fileName );
	}

	public function download() {
		if ( $this->banned ) {
			return $this->mirrorDownload();
		} else {
			return $this->directDownload();
		}
	}

} /* end of AbstractUrlDescription class */
