<?php
namespace QuestPC;

/**
 * Remember that instance caches are valid only until remove / move operations
 * are implemented. In the last case, only APC / memcached storage may be used.
 * @todo: caches might duplicate some info. reduce cache size.
 */
class CategoryTreeCache {

	const purgePerNthWrite = 50;
	const purgeMinChilds = 5000;
	const purgeMinParents = 5000;
	protected $purgeCounter = 0;
	/**
	 * Caches serialized child nodes for each of queried parent.
	 * value: array
	 *   key: parent_id
	 *   value: array
	 *     key: child_id
	 *     value: serialized node
	 */
	protected $parentCache = array();

	/**
	 * Completely clears cache.
	 * Mostly for debugging purposes.
	 */
	public function clear() {
		$this->parentCache = array();
	}

	/**
	 * Checks, whether the node is already cached in ->parentCache
	 * @param $node array
	 *   db row of node table (w/o key)
	 * @param $parentId
	 *   category id of node's immediate parent
	 * @return mixed
	 *   int category_id of node;
	 *   false node is not in cache;
	 */
	public function lookupByChildNode( array $node, $parentId ) {
		ksort( $node );
		$parentId = intval( $parentId );
		$serializedNode = serialize( $node );
		if ( !array_key_exists( $parentId, $this->parentCache ) ) {
			return false;
		}
		foreach ( $this->parentCache[$parentId] as $childId => $serializedChild ) {
			if ( $serializedChild === $serializedNode ) {
				return $childId;
			}
		}
		return false;
	}

	public function getParentId( $categoryId ) {
		$categoryId = intval( $categoryId );
		foreach ( $this->parentCache as $parentId => &$parentChilds ) {
			if ( array_key_exists( $categoryId, $parentChilds ) ) {
				return $parentId;
			}
		}
		return false;
	}

	public function getLeafByCategoryId( $categoryId ) {
		$categoryId = intval( $categoryId );
		foreach ( $this->parentCache as $parentId => &$parentChilds ) {
			if ( array_key_exists( $categoryId, $parentChilds ) ) {
				$leaf = (object) unserialize( $parentChilds[$categoryId] );
				$leaf->parent_id = $parentId;
				return $leaf;
			}
		}
		return false;
	}

	/**
	 * Reduces the size of too large ->parentCache, which probably should prevent
	 * out of memory errors when running on very large / wide trees.
	 * @param $skipCatId int
	 *   id of parent just added child category
	 */
	protected function tryToPurge( $skipCatId ) {
		# check width of the tree
		foreach ( $this->parentCache as $parentId => &$parentChilds ) {
			if ( $parentId !== $skipCatId &&
					count( $parentChilds ) > self::purgeMinChilds ) {
				# @note: Non-optimal, but better than nothing.
				# Dbg\log(__METHOD__.':reducing childs of parent node',$parentId);
				# Dbg\log(__METHOD__.':before reducing',$parentChilds);
				$this->parentCache[$parentId] = array_slice( $parentChilds, -self::purgeMinChilds, self::purgeMinChilds, true );
				# Dbg\log(__METHOD__.':after reducing',$parentChilds);
			}
		}
		# check depth of the tree
		$parentsDelta = count( $this->parentCache ) - self::purgeMinParents;
		if ( $parentsDelta > 0 ) {
			# Dbg\log(__METHOD__.':reducing parent node',$skipCatId);
			# Dbg\log(__METHOD__.':before reducing',$this->parentCache);
			foreach ( array_keys( $this->parentCache ) as $parentId ) {
				if ( $parentId !== $skipCatId ) {
					unset( $this->parentCache[$parentId] );
					$parentsDelta--;
				}
				if ( $parentsDelta <= 0 ) {
					break;
				}
			}
			# Dbg\log(__METHOD__.':after reducing',$this->parentCache);
		}
	}

	/**
	 * Cache node in ->parentCache
	 * @param $node array
	 *   db row of node table (w/o key)
	 * @param $categoryId int
	 *   key of nodeTable
	 * @param $parentId
	 *   category id of node's immediate parent
	 */
	public function toParentCache( array $node, $categoryId, $parentId ) {
		ksort( $node );
		$categoryId = intval( $categoryId );
		$parentId = intval( $parentId );
		if ( !array_key_exists( $parentId, $this->parentCache ) ) {
			$this->parentCache[$parentId] = array();
		}
		$this->parentCache[$parentId][$categoryId] = serialize( $node );
		# do not try to purge cache too often, because it's a time consuming operation
		if ( ++$this->purgeCounter >= self::purgePerNthWrite ) {
			$this->purgeCounter = 0;
			$this->tryToPurge( $parentId );
		}
	}

	public function getPath( $categoryId ) {
		$dbres = array();
		$selfId = $categoryId;
		while ( ($leaf = $this->getLeafByCategoryId( $selfId )) !== false ) {
			switch ( count( $dbres ) ) {
			case 0 :
				$leaf->ancestor_level = CategoryTree::TREE_SELF;
				break;
			case 1 :
				$leaf->ancestor_level = CategoryTree::TREE_DIRECT_PARENT;
				break;
			}
			$leaf->category_id = $selfId;
			$selfId = $leaf->parent_id;
			unset( $leaf->parent_id );
			$dbres[] = $leaf;
		}
		if ( $selfId !== 0 ) {
			return false;
		}
		$dbres = array_reverse( $dbres );
		foreach ( $dbres as $level => $leaf ) {
			if ( !property_exists( $leaf, 'ancestor_level' ) ) {
				$leaf->ancestor_level = $level + 1;
			}
		}
		return $dbres;
	}

	public function setPath( array $dbres ) {
		# Dbg\log(__METHOD__.':dbres',$dbres);
		$parentId = 0;
		foreach ( $dbres as $leaf ) {
			$node = (array) $leaf;
			unset( $node['ancestor_level'] );
			unset( $node['category_id'] );
			$this->toParentCache( $node, $leaf->category_id, $parentId );
			$parentId = $leaf->category_id;
		}
	}

	/**
	 * Checks whether specified category exists in parent cache.
	 */
	public function inParentCache( $categoryId ) {
		$categoryId = intval( $categoryId );
		# check parents
		if ( array_key_exists( $categoryId, $this->parentCache ) ) {
			return true;
		}
		# check childs of parents
		foreach ( $this->parentCache as &$childsList ) {
			if ( array_key_exists( $categoryId, $childsList ) ) {
				return true;
			}
		}
		return false;
	}

} /* end of CategoryTreeCache class */

class CategoryTree extends FeaturesObject {

	# mb_strlen of ->nodeTable.`name` field in DB schema definition.
	const nodeNameMaxLen = 128;

	# Virtual root level which is stored in ->rootNode property. Not stored in DB.
	const TREE_ROOT = 0;
	# Ddirect parent of queried node. Stored in DB.
	const TREE_DIRECT_PARENT = 1000000;
	# Level of self, "forged" in UNION query. Not stored in DB.
	const TREE_SELF = 2000000;

	protected $cache;

	/**
	 * SQL table with node names.
	 * Name of DB table where row represents one node.
	 */
	protected $nodeTable;
	/**
	 * Uses dynamic schema with mandatory 'category_id' and 'name' fields
	 * and optional arbitrary extra fields. There are two keys:
	 * 1. 'category_id'
	 * 2. virtual key (enforced by code) 'name' and all arbitrary columns.
	 * Note that all columns besides 'category_id' must be present in
	 * $node array passed to method calls.
	 *
	 *	'category' => array(
	 *		array(
	 *			'category_id' => 'int unsigned NOT NULL AUTO_INCREMENT',
	 *			'name' => array( 'mb_length' => 128, 'varchar(!mb_length!) NOT NULL' )
	 *		),
	 *		array(
	 *			'PRIMARY KEY (`category_id`)',
	 *		)
	 *	),
	 */

	/**
	 * SQL table with tree structure.
	 * Name of DB table where hierarchial tree is stored.
	 */
	protected $treeTable;
	/**
	 * Uses fixed schema:
	 *	'category_level' => array(
	 *		array(
	 *			'category_id' => 'int unsigned NOT NULL',
	 *			# zero value for root entry
	 *			'ancestor_id' => 'int unsigned NOT NULL DEFAULT \'0\'',
	 *			# zero value for root entry, 100000 value for topmost entry
	 *			'ancestor_level' => 'int unsigned NOT NULL DEFAULT \'0\'',
	 *		),
	 *		array(
	 *			'PRIMARY KEY (`category_id`,`ancestor_id`,`ancestor_level`)',
	 *			'KEY `category_id` (`category_id`)',
	 *			'KEY `ancestor_id` (`ancestor_id`)',
	 *		)
	 *	),
	 */

	/**
	 * Virtual root node initialized by constructor.
	 * Has the same fields as DB table nodeTable, however is not stored in DB.
	 * It's 'name' column also a display name of the tree
	 * value: array() - virtual DB row; 'name' column is mandatory;
	 */
	protected $rootNode;

	# FeaturesObject setter definition.
	protected static $propTypes = array(
		'defaultUrlTemplate' => 'string',
	);
	# Default controller script path that will be associated with CategoryTree instance.
	protected $defaultUrlTemplate;

	function __construct() {
		$this->cache = new CategoryTreeCache();
	}

	/**
	 * Makes node row suitable for inserting or updating into database.
	 * @param $node mixed
	 *   string: node name;
	 *   array: node row with mandatory 'name' field;
	 * @modifies array
	 *   node row for DB;
	 * @note: Should be overloaded in case custom ->nodeTable schema
	 * has extra fields defined besides `category_id` and `name`,
	 * or `name` length is not default.
	 */
	protected function makeDBrow( &$node ) {
		if ( !is_array( $node ) ) {
			$node = array( 'name' => $node );
		} elseif ( !array_key_exists( 'name', $node ) ) {
			SdvException::throwError( 'Tree node does not have mandatory `name` field', 
				__METHOD__, $node
			);
		}
		$nameLen = mb_strlen( $node['name'], 'UTF-8' );
		if ( $nameLen > self::nodeNameMaxLen ) {
			# Try to minimize collisions for `name` field overflows.
			$cutSuffix = base64_encode( md5( $node['name'], true ) );
			$node['name'] = Gl::limitUtfStr( $node['name'], self::nodeNameMaxLen, $cutSuffix );
			# ymlLogAnalyzer.sh
			Dbg\log(__METHOD__.':node name was cut to fit into DB field',$node['name']);
		}
	}

	/**
	 * Completely clears cache.
	 * Mostly for debugging purposes.
	 */
	public function clearCache() {
		$this->cache->clear();
	}

	/**
	 * Return child category id of parent category that matches
	 * the specified node.
	 * @param $node array
	 *   child node: row of ->nodeTable w/o `category_id`;
	 * @param $parentId int
	 *   known id of node's parent
	 * @return mixed
	 *   int   - category id of specified child node;
	 *   false - specified parent has no such node;
	 */
	protected function getChildId( array $node, $parentId = 0 ) {
		global $appContext;
		if ( array_key_exists( 'category_id', $node ) ) {
			# we assume that wouldn't happen, but..
			return intval( $node['category_id'] );
		}
		if ( ($categoryId = $this->cache->lookupByChildNode( $node, $parentId )) !== false ) {
			return $categoryId;
		}
		$ancestor = array(
			'ancestor_id' => $parentId,
			'ancestor_level' => self::TREE_DIRECT_PARENT,
		);
		$dbres = $appContext->dbw->query(
			"SELECT", array( $this->nodeTable, 'category_id' ),
			"FROM", "^{$this->nodeTable}",
			"INNER JOIN", "^{$this->treeTable}",
			"ON", array( $this->treeTable, 'category_id' ), "=",
				array( $this->nodeTable, 'category_id' ),
			"WHERE", "~treeFieldSet", "AND", "~nodeFieldSet",
			new DbwParams( array(
				'treeFieldSet' => new DbwFieldSet( $ancestor, $this->treeTable ),
				'nodeFieldSet' => new DbwFieldSet( $node, $this->nodeTable )
			) )
		);
		if ( count( $dbres ) === 0 ) {
			return false;
		}
		foreach ( $dbres as $row ) {
			$categoryId = intval( $row->category_id );
			break;
		}
		# update tree cache
		$this->cache->toParentCache( $node, $categoryId, $parentId );
		return $categoryId;
	}

	const GET_ID_DEFAULT = 0;
	/* return only top node id by default */
	const GET_ID_ALL_NODES = 1;

	/**
	 * Get category id of fully-qualified tree path (no vroot).
	 * @param $path array
	 *   value: node (string or array);
	 * @param $mode int
	 *   return category_id of path top node (& self::GET_ID_ALL_NODES = 0);
	 *   return all category ids of path nodes (& self::GET_ID_ALL_NODES = 1);
	 * @return mixed
	 *   int   - category_id of path top node;
	 *   array - all category ids of path nodes;
	 *   false - path is not found;
	 */
	public function getIdByPath( array $path, $mode = self::GET_ID_DEFAULT ) {
		$parentId = 0;
		$catIds = array();
		foreach ( $path as $node ) {
			if ( !is_array( $node ) ) {
				# included name only, convert to complete row
				$node = array( 'name' => $node );
			}
			if ( ($mode & self::GET_ID_ALL_NODES) && $parentId !== 0 ) {
				$catIds[] = $parentId;
			}
			if ( ( $parentId = $this->getChildId( $node, $parentId ) ) === false ) {
				return false;
			}
		}
		if ( $mode & self::GET_ID_ALL_NODES ) {
			$catIds[] = $parentId;
			return $catIds;
		}
		return $parentId;
	}

	/**
	 * Make full tree path
	 * @param $path array
	 *   each element is node of the corresponding level
	 *   mixed
	 *     array - node db row
	 *   string
	 *     node db row 'name' column
	 * @return int
	 *   category id of path's top node (path id)
	 */
	public function mkPath( array $path ) {
		$parentId = 0;
		foreach ( $path as $node ) {
			if ( !is_array( $node ) ) {
				# included name only, convert to complete row
				$node = array( 'name' => $node );
			}
			$parentId = $this->mkNode( $node, $parentId );
		}
		return $parentId;
	}

	public function categoryExists( $categoryId ) {
		$categoryId = intval( $categoryId );
		if ( $this->cache->inParentCache( $categoryId ) ) {
			return true;
		}
		return $this->getPathById( $categoryId ) !== false;
	}

	// Do not include path vroot (->rootNode) by default.
	// Do not remove overflow name hashes by default.
	const GET_PATH_DEFAULT = 0;
	// Return array( $path, $catIds ) instead of path only.
	const GET_PATH_VROOT = 1;
	const GET_PATH_IDS = 2;
	/**
	 * Get full tree path
	 * @param $categoryId int
	 *   category id of path's top node (path id)
	 * @param $withRoot bool
	 *   true to return virtual root element, false otherwise
	 * @return mixed
	 *   false - path does not exists;
	 *   array
	 *     key 0: mixed
	 *       false - path does not exists;
	 *       array
	 *         each element is node of the corresponding level;
	 *     key 1: mixed
	 *       false - path does not exists;
	 *       array
	 *         each element is category id of the node of the corresponding level;
	 */
	public function getPathById( $categoryId, $mode = self::GET_PATH_DEFAULT ) {
		global $appContext;
		$categoryId = intval( $categoryId );
		if ( !is_array( $dbres = $this->cache->getPath( $categoryId ) ) ) {
			# Dbg\log(__METHOD__.':categoryId',$categoryId);
			$dbres = $appContext->dbw->query(
				"SELECT", array( $this->nodeTable, '*' ),
					array( $this->treeTable, 'ancestor_level' ),
				"FROM", "^{$this->treeTable}",
				"INNER JOIN", "^{$this->nodeTable}",
				"ON", array( $this->nodeTable, 'category_id' ), "=",
					array( $this->treeTable, 'ancestor_id' ),
				"WHERE", array( $this->treeTable, 'category_id' ), "=", "?categoryId",
				"UNION",
				"SELECT", array( $this->nodeTable, '*' ), ',', '!selfLevel',
				"FROM", "^{$this->nodeTable}",
				"WHERE", array( $this->nodeTable, 'category_id' ), "=", "?categoryId",
				"ORDER BY", "`ancestor_level", "ASC",
				new DbwParams( array(
					'categoryId' => $categoryId,
					# @note: It is important to use non-quoted integer value for selfLevel,
					# otherwise ORDER BY result order will be incorrect. MySQL bug?
					# MW developers said "quoting integer does not hurt" it seems not true
					# in UNION by forged integer column case.
					'selfLevel' => self::TREE_SELF,
				) )
			);
			if ( count( $dbres ) === 0 ) {
				return ( $mode & self::GET_PATH_IDS ) ?
					array( false, false ) : false ;
			}
			foreach ( $dbres as $leaf ) {
				$leaf->category_id = intval( $leaf->category_id );
				$leaf->ancestor_level = intval( $leaf->ancestor_level );
			}
			# Dbg\log(__METHOD__.':dbres from db',$dbres);
			$this->cache->setPath( $dbres );
		} else {
			# Dbg\log(__METHOD__.':dbres reconstructed',$dbres);
		}
		if ( $mode & self::GET_PATH_VROOT ) {
			# add virtual root entry
			$rootNode = (object) $this->rootNode;
			$rootNode->category_id = 0;
			$rootNode->ancestor_level = self::TREE_ROOT;
			array_unshift( $dbres, $rootNode );
			# Dbg\log(__METHOD__.':with root node',$dbres);
		}
		$categoryIds = array();
		$nodes = array();
		foreach ( $dbres as $leaf ) {
			$node = (array) $leaf;
			$categoryIds[] = $node['category_id'];
			unset( $node['category_id'] );
			unset( $node['ancestor_level'] );
			$nodes[] = $node;
		}
		return ( $mode & self::GET_PATH_IDS ) ?
			array( $nodes, $categoryIds ) : $nodes;
	}
/*
SELECT * FROM (
SELECT `category`.* , `category_level`.`ancestor_level`
FROM `category_level` 
INNER JOIN `category` ON `category`.`category_id` = `category_level`.`ancestor_id`
WHERE `category_level`.`category_id` = '275'
UNION SELECT `category`.* , 2000000 FROM `category`
WHERE `category`.`category_id` = '275'
) `path` ORDER BY `ancestor_level` ASC
*/

	/**
	 * Get category id of parent's child node
	 * @param $node array
	 *   db row of node table (w/o category_id)
	 * @param $parentId int
	 *   category_id of node's immediate parent
	 * @return mixed
	 *   int   - category_id of $node;
	 *   false - such child of parent's node does not exist;
	 */
	public function getCategoryId( array $node, $parentId ) {
		global $appContext;
		$parentId = intval( $parentId );
		if ( ($categoryId = $this->cache->lookupByChildNode( $node, $parentId )) !== false ) {
			return $categoryId;
		}
		# check whether target category already has children with matching $node
		$result = $appContext->dbw->query(
			"SELECT", array( $this->treeTable, '*' ),
			"FROM", "^{$this->treeTable}",
			"INNER JOIN", "^{$this->nodeTable}",
			"ON", array( $this->nodeTable, 'category_id' ), "=",
				array( $this->treeTable, 'category_id' ),
			"WHERE", array( $this->treeTable, 'ancestor_id' ), "=", "?parentId",
				"AND", array( $this->treeTable, 'ancestor_level' ), "=", "?maxLevel",
				# match columns of node in joined ->nodeTable
				"AND", "~nodeFieldSet",
			new DbwParams( array(
				'parentId' => $parentId,
				'maxLevel' => self::TREE_DIRECT_PARENT,
				# match columns of node in joined ->nodeTable
				'nodeFieldSet' => new DbwFieldSet( $node, $this->nodeTable ),
			) )
		);
		# Dbg\log(__METHOD__.':find result',$result);
		if ( count( $result ) > 0 ) {
			# children already exists;
			$categoryId = intval( $result[0]->category_id );
			# update the cache
			$this->cache->toParentCache( $node, $categoryId, $parentId );
			# return children category_id
			return $categoryId;
		}
		return false;
	}

	/**
	 * Creates pager that can page all nodes of the tree.
	 */
	public function allNodesPagerFactory() {
		$pager = new DbPager();
		$pager->setPagerQuery(
			"SELECT", '*',
			'FROM', "^{$this->nodeTable}",
			"ORDER BY", "`category_id", "ASC"
		);
		return $pager;
	}

	/**
	 * Applies SQL expression that can page all immediate childs of parent's node.
	 * @param $parentId int
	 *   category_id of parent node
	 * @param $pager DbPager
	 *   DBPager to apply
	 */
	public function immediateChildsBindQuery( $parentId = 0, DbPager $pager ) {
		$pager->setPagerQuery(
			"SELECT", array( $this->nodeTable, '*' ),
			"FROM", "^{$this->treeTable}",
			# Queries for large trees were taking +2sec w/o this index hint.
			# Taking less than 0.5sec with this index hint.
			"USE INDEX", "(", "`ancestor_id", ")",
			"INNER JOIN", "^{$this->nodeTable}",
			"ON", array( $this->nodeTable, 'category_id' ), "=",
				array( $this->treeTable, 'category_id' ),
			"WHERE", array( $this->treeTable, 'ancestor_id' ), "=", "?parentId",
				"AND", array( $this->treeTable, 'ancestor_level' ), "=", "?maxLevel",
			"ORDER BY", "`name", "ASC",
			new DbwParams( array(
				'parentId' => $parentId,
				'maxLevel' => self::TREE_DIRECT_PARENT,
			) )
		);
	}

/*
# @note: SELECT * to see how JOINS work.
SELECT `publication_category`.*
FROM `publication_category`
INNER JOIN `publication_category_level`
ON `publication_category_level`.`category_id` = `publication_category`.`category_id`
INNER JOIN `publication_category` AS `self_category`
ON `self_category`.`category_id` = `publication_category_level`.`ancestor_id`
WHERE `publication_category`.`category_id` != '36'
AND `publication_category`.`name` = 'BMW'
AND `ancestor_level` = 1000000
ORDER BY `self_category`.`name`
LIMIT 0 , 13
*/
	/**
	 * Applies SQL expression that can page all nodes with the same name as
	 * specified category top node except that node.
	 * @param $categorySelector mixed
	 *   int: category_id, which top node name will be used as matching node name
	 *        Pager will skip the node with this id.
	 *   string: matching node name
	 *        Pager will include all nodes. Controller / view is responsible to
	 *        skip self-node, if desired.
	 * @param $pager DbPager
	 */
	public function matchingNameBindQuery( $categorySelector, DbPager $pager ) {
		if ( is_int( $categorySelector ) ) {
			$categoryId = $categorySelector;
			if ( ($path = $this->getPathById( $categoryId )) === false ) {
				SdvException::throwError( 'Unknown category id', __METHOD__, $categoryId );
			}
			$topNode = array_pop( $path );
			array_push( $path, $topNode );
			$nodeName = $topNode['name'];
		} else {
			$nodeName = $categorySelector;
			$categoryId = null;
		}
		$query = array(
			"SELECT", array( $this->nodeTable, "*" ),
			"FROM", "^{$this->nodeTable}",
			"INNER JOIN", "^{$this->treeTable}",
			# [3] = get all (ancestor_id,ancestor_level) of [2]
			"ON", array( $this->treeTable, 'category_id' ), "=", array( $this->nodeTable, 'category_id' ),
			"INNER JOIN", new DbTableAlias( $this->nodeTable, 'self_category' ),
			# [5] = get name of [4]
			"ON", array( 'self_category', 'category_id' ), "=", array( $this->treeTable, 'ancestor_id' ),
			# [1] = get all category_id which name is value of 'nodeName'
			"WHERE", array( $this->nodeTable, 'name' ), "=", "?nodeName",
			# [4] = reduce [3] to only top ancestors
			"AND", "`ancestor_level", "=", '?maxLevel',
		);
		if ( $categoryId !== null ) {
			# [2] = [1] except "self" category_id
			array_push( $query,
				"AND", array( $this->nodeTable, 'category_id' ), "!=", "?categoryId"
			);
		}
		array_push( $query,
			# order by [5]
			"ORDER BY", array( 'self_category', 'name' ),
			new DbwParams( array(
				'categoryId' => $categoryId,
				'nodeName' => $nodeName,
				'maxLevel' => self::TREE_DIRECT_PARENT,
			) )
		);
		call_user_func_array( array( $pager, 'setPagerQuery' ), $query );
	}

	protected $mkNodeCreated;
	/**
	 * Add a node as last child of target node
	 * @param $node array
	 *   DB row of node to add (usually includes 'name' key for node name)
	 * @param $parentId mixed
	 *   null create root node
	 *   int id of target node
	 * @modifies ->mkNodeCreated
	 *   true:  newly created node;
	 *   false: node already exists;
	 * @return int
	 *   id of created / already_existing node
	 */
	protected function mkNode( array $node, $parentId = 0 ) {
		global $appContext;
		$this->mkNodeCreated = true;
		$parentId = intval( $parentId );
		# Dbg\log(__METHOD__.':node',$node);
		# Dbg\log(__METHOD__.':parentId',$parentId);
		# Next call is required to avoid empty transaction in case
		# category was already cached.
		if ( ($categoryId = $this->cache->lookupByChildNode( $node, $parentId )) !== false ) {
			$this->mkNodeCreated = false;
			return $categoryId;
		}
		try {
			$appContext->dbw->begin();
			if ( ($categoryId = $this->getCategoryId( $node, $parentId ) ) !== false ) {
				$this->mkNodeCreated = false;
				throw SdvException::throwSuccess( '', '', $categoryId );
			}
			# create a new node
			$this->makeDBrow( $node );
			$nodeRows = array( $node );
			$appContext->dbw->insert( $this->nodeTable, $nodeRows, 0 );
			$categoryId = intval( $appContext->dbw->insert_id() );
			# Dbg\log(__METHOD__.':category insert_id',$categoryId);
			# get levels list of parent's node
			$parentLevels = $appContext->dbw->query(
				"SELECT", "`ancestor_id",
				"FROM", "^{$this->treeTable}",
				"WHERE", "`category_id", "=", "?parentId",
				"ORDER BY", "`ancestor_level", "ASC",
				new DbwParams( array( 'parentId' => $parentId ) )
			);
			# Dbg\log(__METHOD__.':parentLevels',$parentLevels);
			# update ->treeTable for newly created node
			$childLevels = array();
			$lastLevel = self::TREE_ROOT;
			foreach ( $parentLevels as $parentLevel ) {
				$childLevels[] = array(
					'category_id' => $categoryId,
					'ancestor_id' => intval( $parentLevel->ancestor_id ),
					'ancestor_level' => $lastLevel++,
				);
			}
			$childLevels[] = array(
				'category_id' => $categoryId,
				'ancestor_id' => $parentId,
				'ancestor_level' => self::TREE_DIRECT_PARENT,
			);
			# Dbg\log(__METHOD__.':childLevels',$childLevels);
			$appContext->dbw->insert( $this->treeTable, $childLevels, 0 );
			# update tree cache
			$this->cache->toParentCache( $node, $categoryId, $parentId );
		} catch ( \Exception $e ) {
			if ( ($e instanceof SdvException) && $e->getErrorCode() === 'success' ) {
				$categoryId = $e->getData();
			} else {
				$appContext->dbw->rollback();
				throw $e;
			}
		}
		$appContext->dbw->commit();
		return $categoryId;
	}

	/**
	 * Renames already existing node of tree.
	 * @param $categoryId int
	 *   value of existing category Id
	 * @param $node mixed
	 *   array / string node name with _optional_ extra columns
	 */
	public function rename( $categoryId, $node ) {
		global $appContext;
		$categoryId = intval( $categoryId );
		$this->makeDBrow( $node );
		# Rename a node.
		$appContext->dbw->query(
			"UPDATE", "^{$this->nodeTable}",
			"SET", "~fieldSet",
			"WHERE", "`category_id", "=", "?categoryId",
			new DbwParams( array(
				'fieldSet' => new UpdateFieldSet( $node ),
				'categoryId' => $categoryId
			) )
		);
		# Cache coherency check.
		$parentId = $this->cache->getParentId( $categoryId );
		if ( $parentId === false ) {
			# The node is not in the cache.
			return;
		}
		# Rename node in cache.
		$this->cache->toParentCache( $node, $categoryId, $parentId );
	}

	/**
	 * @note: Passed by reference as performance optimization.
	 */
	protected static function equalNodes( &$node1, &$node2 ) {
		if ( !is_array( $node1 ) ) {
			$node1 = array( 'name' => $node1 );
		}
		ksort( $node1 );
		if ( !is_array( $node2 ) ) {
			$node2 = array( 'name' => $node2 );
		}
		ksort( $node2 );
		return serialize( $node1 ) === serialize( $node2 );
	}

	const PATHES_NOT_EQUAL = 1;
	const PATHES_EQUAL = 2;
	const PATH1_IS_NOT_SUBPATH_OF_PATH2 = 3;
	const PATH2_IS_NOT_SUBPATH_OF_PATH1 = 4;
	const PATH1_IS_SUBPATH_OF_PATH2 = 5;
	const PATH2_IS_SUBPATH_OF_PATH1 = 6;

	public static function comparePathes( array $path1, array $path2 ) {
		if ( count( $path1 ) === count( $path2 ) ) {
			foreach ( $path1 as $key => &$val1 ) {
				if ( !static::equalNodes( $val1, $path2[$key] ) ) {
					return self::PATHES_NOT_EQUAL;
				}
			}
			return self::PATHES_EQUAL;
		}
		$path1IsLonger = count( $path1 ) > count( $path2 );
		if ( $path1IsLonger ) {
			$longPath = &$path1;
			$smallPath = &$path2;
		} else {
			$longPath = &$path2;
			$smallPath = &$path1;
		}
		foreach ( $smallPath as $key => &$smallVal ) {
			if ( !static::equalNodes( $smallVal, $longPath[$key] ) ) {
				return $path1IsLonger ?
					self::PATH1_IS_NOT_SUBPATH_OF_PATH2 : self::PATH2_IS_NOT_SUBPATH_OF_PATH1;
			}
		}
		return $path1IsLonger ?
			self::PATH1_IS_SUBPATH_OF_PATH2 : self::PATH2_IS_SUBPATH_OF_PATH1;
	}

	public static function stripBase64Hash( $nodeName ) {
		if ( mb_strlen( $nodeName, 'UTF-8' ) === self::nodeNameMaxLen &&
				($base64md5 = substr( $nodeName, -24 )) !== false ) {
			# Dbg\log(__METHOD__.':base64md5',$base64md5);
			return (base64_decode( $base64md5, true ) === false) ?
				$nodeName :
				substr( $nodeName, 0, strlen( $nodeName ) - 24 ) . '...';
		} else {
			return $nodeName;
		}
	}

	const STRIP_BASE64_HASH = 1;
	public static function stripPath( array $path, $mode = 0 ) {
		$result = array();
		foreach ( $path as $node ) {
			$nodeName = is_array( $node ) ? $node['name'] : $node;
			$result[] = ($mode & self::STRIP_BASE64_HASH) ?
				static::stripBase64Hash( $nodeName ) :
				$nodeName;
		}
		return $result;
	}

	/**
	 * Parses uri part containing CategoryTree path (no vroot).
	 * @param $uriPart string
	 *   complete full path string;
	 * @return array
	 *   complete full path;
	 */
	public static function decodeUri( $uriPart ) {
		# remove leading '/'
		$uriPart = ltrim( $uriPart, '/' );
		if ( $uriPart === '' ) {
			# http://stackoverflow.com/questions/9470300/explode-on-empty-string-returns-array-count-as-1
			return array();
		}
		$path = array_map( 'urldecode', array_map( 'trim', explode( '/', $uriPart ) ) );
		return $path;
	}

	/**
	 * Encodes CategoryTree path into uri part.
	 * @param $path array
	 *   CategoryTree path (no vroot)
	 * @return string
	 *   uri-encoded path string
	 */
	public static function encodeUri( array $path ) {
		foreach ( $path as &$node ) {
			if ( is_array( $node ) ) {
				$node = $node['name'];
			}
		}
		$pathStr = implode( '/', array_map( 'urlencode', $path ) );
		if ( $pathStr !== '' ) {
			# add leading slash only for non 0-length paths
			$pathStr = "/{$pathStr}";
		}
		return $pathStr;
	}

	public function getDefaultScriptPath( array $path ) {
		return str_replace( '$1',
			static::encodeUri( $path ),
			$this->defaultUrlTemplate
		);
	}

	/**
	 * Get top node name of non-vroot (normal) path.
	 * @param $path array
	 *   path array of nodes
	 * @return string
	 *   top node name
	 */
	public function getTopNodeName( array $path, $mode = 0 ) {
		if ( count( $path ) === 0 ) {
			return $this->rootNode['name'];
		}
		$topNode = array_pop( $path );
		array_push( $path, $topNode );
		$topNodeName = is_array( $topNode ) ? $topNode['name'] : $topNode;
		return ( $mode & self::STRIP_BASE64_HASH ) ?
			static::stripBase64Hash( $topNodeName ) :
			$topNodeName;
	}

	/**
	 * Extracts path from string separated via '/' character.
	 * To have '/' to be a part of node name, encode it as '%2F'.
	 * @param $str string
	 * @return array
	 *   "stripped" path (no 'name' subarrays)
	 */
	public static function extractPath( $str ) {
		return array_map( 'trim', array_map( 'urldecode', explode( '/', $str ) ) );
	}

	public static function encodeNodeName( $nodeName ) {
		return str_replace( '/', '%2F', $nodeName );
	}

	public static function encodePath( array $strippedPath ) {
		return implode( '/', array_map( array( __CLASS__, 'encodeNodeName' ), $strippedPath ) );
	}

} /* end of CategoryTree class */
