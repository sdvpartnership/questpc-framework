<?php
namespace QuestPC;

/**
 * A form which fields are linked to AbstractModel descendant fields.
 * Usually the fields are bound to the same model instance.
 * However it also supports multiple models (untested yet).
 */
class ModelEditForm extends AbstractForm {

	protected $fieldIndex;

	public function setModel( AbstractModel $model ) {
		foreach ( $this->fields as $field ) {
			if ( $field->source->isBoundToTableField() ) {
				$field->source->setModel( $model );
			}
		}
	}

	public function buildFieldIndex() {
		$this->fieldIndex = array();
		foreach ( $this->fields as $field ) {
			$fieldIdx = $field->source->getFieldIdx();
			if ( $fieldIdx !== null ) {
				if ( !array_key_exists( $fieldIdx, $this->fieldIndex ) ) {
					$this->fieldIndex[$fieldIdx] = array();
				}
				$this->fieldIndex[$fieldIdx][] = $field;
			}
		}
	}

	/**
	 * @note: Experimental. Untested yet.
	 */
	public function setModels( AbstractModelCollection $collection ) {
		$this->buildFieldIndex();
		$modelIdx = 0;
		foreach ( $collection as $key => $model ) {
			foreach ( $this->fieldIndex[$modelIdx] as $field ) {
				$field->source->setModel( $model );
			}
			$modelIdx++;
		}
	}

	/**
	 * Transfers data from model(s) into form fields.
	 */
	public function fromModel() {
		foreach ( $this->fields as $field ) {
			if ( $field->source->isBoundToTableField() ) {
				$field->source->setModelChoice();
			}
		}
	}

	/**
	 * Transfers data from form fields into model(s).
	 * @return bool
	 *   true: no errors, model is assumed to be valid;
	 *   false: there were errors, do not store the model;
	 */
	public function toModel() {
		$success = true;
		foreach ( $this->fields as $field ) {
			if ( $field->source->isBoundToTableField() ) {
				$field->source->toModel();
				if ( $field->source->getError() !== false ) {
					$success = false;
				}
			}
		}
		return $success;
	}

} /* end of ModelEditForm class */
