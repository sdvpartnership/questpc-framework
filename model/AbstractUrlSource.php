<?php
namespace QuestPC;

/**
 * Defines url sources and iterates through them.
 */
abstract class AbstractUrlSource extends FeaturesObject {

	protected static $isSingleton = true;

	protected static $cityModel;

	protected static $propTypes = array(
		'feedIdx' => 'integer'
	);

	# Total number of source urls to download complete set.
	# This number is required for FeedsImportJob.
	protected $numberOfSources;
	# Total number of banned source urls.
	# This number is required for FeedsMirrorJob.
	protected $numberOfBanned;

	/**
	 * flipped ->channelMap
	 * key: int
	 *   channelId; value: channel (interes/activity) string
	 */
	protected $channelFlip;
	/**
	 * array_keys of ->channelSources
	 * key: int
	 *   consequtive channelKey
	 * value: int
	 *   non-consequtive channelId
	 */
	protected $channelKeys;

	# global AbstractUrlDescription counter
	protected $feedIdx = 0;
	# used to iterate through ->channelSources / ->localFeed
	protected $feedCounter;

	/**
	 * key: ->channelMap key
	 * value: template for specified ->channelmap key
	 * @note: optional;
	 */
	protected $linkTemplates;

	/**
	 * key: ->channelSources[$channelId][$key] where $key is string;
	 * value: array
	 *   key: lpsearch city_id
	 *   value: url template
	 * @note: optional;
	 */
	protected $localFeed;

	/**
	 * [channel_name => channel_id] or [shop_name => shop_id] map.
	 */
	protected $channelMap;

	/**
	 * key: int
	 *   channelId
	 *     used to address another structures and to find channel name.
	 *   Channel name becomes level0_category.
	 * values: array
	 *   array feed definition
	 *     country-wide or set of local feeds for each city
	 *
	 * example:
	 *
	 *	->channelMap value => [
	 *   [
	 *     0 => level1_category,.. N => level(N+1)_category,
	 *     'url' => string, 'expiration' => const, 'banned' => boolean (optional)
	 *   ],...
	 *   ->localFeed template key =>
	 *   [
	 *     0 => level1_category,.. N => level(N+1)_category, ('#' value will be replaced by city name),
	 *     '$1' => string, , 'expiration' => const,
	 *     'localFeedKey' => 'overriden_value' (optional), 'banned' => boolean (optional)
	 *   ]
	 * ]
	 * @note: required;
	 */
	protected $channelSources;

	/**
	 * @return string
	 *   pcre regex split expression for non-lpsearch interes channel names.
	 */
	public function getChannelSubstRegex() {
		$regex = '%';
		foreach ( $this->channelMap as $channelName => $channelId ) {
			if ( $regex !== '%' ) {
				$regex .= '|';
			}
			if ( $channelId >= 1000000 ) {
				$regex .= '(^|\p{^L})(' . preg_quote( $channelName, '%' ) . ')($|\p{^L})';
			}
		}
		$regex .= '%ui';
		return $regex;
	}

	/**
	 * Descentant instance of AbstractUrlDescription should be returned
	 * by descendant instance of AbstractUrlSource class.
	 */
	protected function urlDescriptionFactory( $channelId, $cityId, $pathRoot ) {
		return new AbstractUrlDescription( $channelId, $cityId, $pathRoot );
	}

	/**
	 * Get country-wide feed description.
	 * @param $sourceDef
	 *   channel source array
	 * @return AbstractUrlDescription descendant instance
	 */
	protected function getCountryFeed( array $sourceDef ) {
		$result = $this->urlDescriptionFactory(
			$this->feedCounter->channelId,
			0,
			$this->channelFlip[$this->feedCounter->channelId]
		);
		$search = array();
		$replace = array();
		foreach ( $sourceDef as $key => $val ) {
			if ( is_int( $key ) ) {
				$result->path[] = $val;
			} elseif ( strpos( $key, '$' ) === 0 ) {
				$search[] = $key;
				$replace[] = $val; 
			} else {
				$result->{$key} = $val;
			}
		}
		if ( !property_exists( $result, 'url' ) ) {
			# try to build url from ->linkTemplates
			if ( count( $search ) === 0 ) {
				SdvException::throwError( 'Source definition has neither url nor replacements',
					__METHOD__, $sourceDef
				);
			}
			$channelName = $this->channelFlip[$result->channel_id];
			if ( !is_array( $this->linkTemplates ) ||
					!array_key_exists( $channelName, $this->linkTemplates ) ) {
				SdvException::throwError( 'No link template is defined for specified channel name',
					__METHOD__, $channelName
				);
			}
			$result->url = str_replace(
				$search,
				$replace,
				$this->linkTemplates[$channelName]
			);
		}
		return $result;
	}

	/**
	 * Get city targeted feed description.
	 * @param $perCityGroupName
	 *    string key of channel source array (used as key of ->localFeed)
	 * @param $perCityDef
	 *    channel source array
	 * @return mixed
	 *   AbstractUrlDescription descendant instance;
	 *   false when current ->feedCounter is moved on to try next element;
	 */
	protected function getPerCityFeed( $perCityGroupName, array $perCityDef ) {
		# Get local feed array for current group name.
		$localFeedArray = &$this->localFeed[$perCityGroupName];
		if ( $this->feedCounter->cityIdx < count( $localFeedArray ) ) {
			$cityIds = array_keys( $localFeedArray );
			$result = $this->urlDescriptionFactory(
				$this->feedCounter->channelId,
				$cityIds[$this->feedCounter->cityIdx++],
				$this->channelFlip[$this->feedCounter->channelId]
			);
			$cityName = static::$cityModel->getName( $result->city_id );
			if ( $cityName !== false ) {
				$search = array();
				$replace = array();
				foreach ( $perCityDef as $key => $val ) {
					if ( is_int( $key ) ) {
						$result->path[] = ($val === '#') ? $cityName : $val;
					} elseif ( strpos( $key, '$' ) === 0 ) {
						$search[] = $key;
						$replace[] = $val;
					} else {
						$result->{$key} = $val;
					}
				}
				# Get local feed description element for current city.
				$localFeedDesc = $localFeedArray[$result->city_id];
				if ( !is_array( $localFeedDesc ) ) {
					$localFeedDesc = array( $localFeedDesc );
				}
				# Substitute feed url parameters.
				$result->url = str_replace(
					$search,
					$replace,
					$localFeedDesc[0]
				);
				# If there are string keys in local feed description,
				# add these to result.
				foreach ( $localFeedDesc as $key => $val ) {
					if ( is_string( $key ) ) {
						$result->{$key} = $val;
					}
				}
				return $result;
			}
		}
		$this->feedCounter->cityIdx = 0;
		return false;
	}

	/**
	 * Iterates through the available feed sources.
	 * @return array
	 *   key 'channel_id' value int
	 *     channel_id (for val < 1000000 lpsearch interes_id);
	 *   key 'city_id' value int
	 *     lpsearch city_id or 0 to match any city;
	 *   key 'url' value string
	 *     corresponding url to download, parse and import into DB;
	 *   key 'path' complete path of CategoryTree to create, which
	 *     categoryId will be used to store feed item;
	 */
	public function getNextFeed() {
		do {
			$this->feedCounter->channelId = $this->channelKeys[$this->feedCounter->channelKey];
			$channelSources = &$this->channelSources[$this->feedCounter->channelId];
			if ( $channelSources !== null ) {
				$sourceKeys = array_keys( $channelSources );
				while ( $this->feedCounter->sourceIdx < count( $sourceKeys ) ) {
					$sourceKey = $sourceKeys[$this->feedCounter->sourceIdx];
					$sourceDef = $channelSources[$sourceKey];
					if ( is_int( $sourceKey ) ) {
						$this->feedCounter->sourceIdx++;
						$result = $this->getCountryFeed( $sourceDef );
						$result->processFields();
						return $result;
					} else { /* is_string( $sourceKey ) */
						if ( array_key_exists( 'localFeedKey', $sourceDef ) ) {
							# Override per city source key.
							$sourceKey = $sourceDef['localFeedKey'];
							unset( $sourceDef['localFeedKey'] );
						}
						$result = $this->getPerCityFeed( $sourceKey, $sourceDef );
						if ( $result !== false ) {
							$result->processFields();
							return $result;
						}
						$this->feedCounter->sourceIdx++;
					}
				}
				$this->feedCounter->sourceIdx = 0;
			}
			$this->feedCounter->channelKey++;
			$this->feedCounter->hasOverflow = false;
			if ( $this->feedCounter->channelKey >= count( $this->channelKeys) ) {
				$this->feedCounter->hasOverflow = true;
				$this->feedCounter->channelKey = 0;
			}
		} while ( true );
	}

	public function isFirstElem() {
		return $this->feedCounter->channelKey === 0 &&
			$this->feedCounter->sourceIdx === 0;
	}

	public function resetCounter() {
		# reset global AbstractUrlDescription counter
		$this->feedIdx = 0;
		# reset nested structures counter
		$this->feedCounter = (object) array(
			# whether the counter just vent through the last element
			'hasOverflow' => false,
			# key of ->channelKeys
			'channelKey' => 0,
			# current channelId
			'channelId' => null,
			# key of ->channelSources[]
			'sourceIdx' => 0,
			# key of ->localFeed[->channelSources[]['perCity'][]]
			'cityIdx' => 0,
		);
	}

	protected $feedCounterStack = array();
	/**
	 * Push internal ->feedCounter state into stack.
	 */
	public function saveCounter() {
		array_push( $this->feedCounterStack, clone $this->feedCounter );
	}

	/**
	 * Restore internal ->feedCounter state from stack.
	 */
	public function restoreCounter() {
		$this->feedCounter = array_pop( $this->feedCounterStack );
	}

	/**
	 * Iterates through ->channelSources / ->localFeed definitions.
	 * @param $class object
	 *   class to call
	 * @param $method string
	 *   method of class to call
	 * @note: AbstractUrlDescription instance will be passed as the only argument
	 *   of the callback
	 * @note: callback should return:
	 *   true:  in "num mode", count this AbstractUrlDescription
	 *   false: in "num mode", do not count this AbstractUrlDescription
	 *   null:  leave the iterator (immediate break)
	 * @note: When running in "num mode" (see below), the callback should
	 *   return true for "counted" AbstractUrlDescription instance. That could be
	 *   each instance or only ->banned === true instances to count only
	 *   banned feeds.
	 * @param: $num mixed
	 *   null: iterate from 0 AbstractUrlDescription to the last AbstractUrlDescription
	 *   int:  "num mode"
	 *         iterate from ->feedIdx AbstractUrlDescription $num times but only
	 *         these AbstractUrlDescription instances are counted for which callback
	 *         returned true.
	 * @return mixed
	 *   true: iterator completed successfully
	 *   int:  number of processed AbstractUrlDescription instances before
	 *         immediate break occured.
	 */
	public function iterate( $class, $method, $num = null ) {
		if ( $num === null ) {
			$this->resetCounter();
		} else {
			# Dbg\log('num mode feedidx',$this->feedIdx);
			# go to current ->feedIdx position
			for ( $i = 0; $i < $this->feedIdx; $i++ ) {
				# Dbg\log('num mode i',$i);
				$feedDesc = $this->getNextFeed();
				# Dbg\log(__METHOD__.':feedDesc',$feedDesc);
				# Dbg\log(__METHOD__.':feedCounter',$this->feedCounter);
			}
		}
		# iterate
		$j = 0;
		while ( true ) {
			$feedDesc = $this->getNextFeed();
			# Dbg\log(__METHOD__.':feedDesc',$feedDesc);
			# Dbg\log(__METHOD__.':feedCounter',$this->feedCounter);
			if ( $num === null && $this->feedCounter->hasOverflow ) {
				$this->resetCounter();
				break;
			} else {
				$this->feedIdx++;
			}
			if ( is_object( $class ) ) {
				$cbResult = $class->{$method}( $feedDesc );
			} else {
				$class = AutoLoader::getFqnClassName( $class );
				$cbResult = $class::$method( $feedDesc );
			}
			if ( $cbResult === null ) {
				return $j;
			}
			# @note: order is important, increment should stay last
			if ( $num !== null && $cbResult === true && ++$j >= $num ) {
				break;
			}
		}
		return true;
	}

	protected function urlDescriptionStats( AbstractUrlDescription $urlDescription ) {
		$this->numberOfSources++;
		if ( $urlDescription->banned ) {
			$this->numberOfBanned++;
		}
		# calculate statistics for all sources available
		return true;
	}

	/**
	 * Calculates total number of AbstractUrlDescription instances available.
	 * Calculates number of banned AbstractUrlDescription instances available.
	 */
	protected function calculateStatistics() {
		$this->numberOfSources = 0;
		$this->numberOfBanned = 0;
		$this->iterate( $this, 'urlDescriptionStats' );
	}

	protected $comparePath;
	protected function comparePathes( AbstractUrlDescription $urlDescription ) {
		$cmpResult = CategoryTree::comparePathes( $this->comparePath, $feedDesc->path );
		if ( $cmpResult === CategoryTree::PATH2_IS_SUBPATH_OF_PATH1 ||
				$cmpResult === CategoryTree::PATHES_EQUAL ) {
			# immediate break
			return null;
		}
		# continue iteration until last AbstractUrlDescription is received
		return true;
	}

	/**
	 * Checks whether the specified path is a FeedSource (sub)path.
	 * @note: Do not use! Buggy!
	 * @param array
	 *   CategoryTree path to check
	 * @return boolean
	 *   true  - path belongs to one of FeedSource (sub)pathes;
	 *   false - path is not among FeedSource (sub)pathes;
	 */
	public function isSourceSubpath( array $path ) {
		$result = false;
		$this->saveCounter();
		$this->comparePath = $path;
		$result = $this->iterate( $this, 'comparePathes' );
		$this->restoreCounter();
		return is_int( $result );
	}

	/**
	 * @note: Set to real ->cityModel instance in descendant class,
	 * if required.
	 */
	protected function setCityModel() {
		static::$cityModel = null;
	}

	function __construct() {
		$this->setCityModel();
		$this->channelFlip = array_flip( $this->channelMap );
		$this->channelKeys = array_keys( $this->channelSources );
		$this->calculateStatistics();
		# @note: next lines are the bugcheck
		# ->numberOfSources and ->numberOfBanned should match
		# after each run, iterated AbstractUrlDescription instances also must match.
		# @todo: convert into proper test.
		# Dbg\log('before 2nd pass',$this);
		# $this->calculateStatistics();
		# Dbg\log('before reset',$this);
		# $this->resetCounter();
		# Dbg\log('after reset',$this);
	}

} /* end of AbstractUrlSource class */
