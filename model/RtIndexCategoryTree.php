<?php
namespace QuestPC;

/**
 * Adds SphinxSearch rtindex on top of usual MySQL CategoryTree schema.
 * Index fields must be like this:
 *   type = rt
 *   rt_field = name
 *   rt_field = parents
 *   morphology = stem_en, stem_ru, soundex
 *   min_stemming_len = 2
 *   charset_type = utf-8
 *   inplace_enable = 1
 *   index_exact_words = 1
 */
class RtIndexCategoryTree extends CategoryTree {

	# Sphinx index name to use.
	protected $sphinxIndexName;

	protected $mkPathCreated;
	protected $mkPathIds;

	/**
	 * @return array
	 *   all category_ids of last ->mkPath() path created except
	 *   for vroot (0) and self (top node).
	 */
	public function getMkPathIds() {
		return $this->mkPathIds;
	}

	public function mkPath( array $path ) {
		global $appContext;
		$this->mkPathCreated = false;
		$this->mkPathIds = array();
		$categoryId = parent::mkPath( $path );
		if ( $this->mkPathCreated ) {
			# Update sphinx index.
			array_pop( $this->mkPathIds );
			$rows = array(
				array(
					'id' => $categoryId,
					'name' => $this->topNodeName,
					'parents' => implode( ' ', $this->mkPathIds )
				)
			);
			$appContext->sphinx->replace( $this->sphinxIndexName, $rows );
		}
		return $categoryId;
	}

	protected $topNodeName;
	protected function mkNode( array $node, $parentId = 0 ) {
		# Get non-truncated category name for sphinx index.
		$this->topNodeName = is_array( $node ) ? $node['name'] : $node;
		$categoryId = parent::mkNode( $node, $parentId );
		$this->mkPathIds[] = $categoryId;
		if ( $this->mkNodeCreated ) {
			$this->mkPathCreated = true;
		}
		return $categoryId;
	}

	public function rename( $categoryId, $node ) {
		global $appContext;
		# Get non-truncated category name for sphinx index.
		$longName = is_array( $node ) ? $node['name'] : $node;
		parent::rename( $categoryId, $node );
		list( $path, $categoryIds ) = $this->getPathById( $categoryId, self::GET_PATH_IDS );
		array_pop( $categoryIds );
		# Update sphinx index.
		$rows = array(
			array(
				'id' => $categoryId,
				'name' => $longName,
				'parents' => implode( ' ', $categoryIds )
			)
		);
		$appContext->sphinx->replace( $this->sphinxIndexName, $rows );
	}

	/**
	 * DbPager::postprocessRowsHook that virtually joins category_id received by
	 *   ->searchChildsBindQuery() to ->nodeTable.
	 */
	public function postprocessChilds( array $rows ) {
		global $appContext;
		if ( count( $rows ) === 0 ) {
			return $rows;
		}
		$catIds = array();
		foreach ( $rows as $row ) {
			$catIds[] = $row->id;
		}
		return $appContext->dbw->query(
			"SELECT", "*",
			"FROM", "^{$this->nodeTable}",
			"WHERE", '$category_id',
			new DbwParams( array(
				'category_id' => $catIds
			) )
		);
	}

	/**
	 * Searches for subcategory name substrings in specified category.
	 */
	public function searchChildsBindQuery( $categoryId, DbPager $pager, $subcatNameSearch ) {
		global $appContext;
		$categoryId = intval( $categoryId );
		$sphinxFieldValues = array(
			'name' => $subcatNameSearch,
		);
		if ( $categoryId !== 0 ) {
			$sphinxFieldValues['parents'] = $categoryId;
		}
		$pager->setLazyDb( $appContext->sphinx );
		$pager->setPagerQuery(
			"SELECT", "*",
			"FROM", "^{$this->sphinxIndexName}",
			"WHERE", ":MATCH(", "?sphinxMatch" , ")",
			new DbwParams( array(
				'sphinxMatch' => Dbw::getSphinxMatch( $sphinxFieldValues )
			) )
		);
		$pager->postprocessRowsHook = array( $this, 'postprocessChilds' );
	}

} /* end of RtIndexCategoryTree class */
