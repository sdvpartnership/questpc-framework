<?php
namespace QuestPC;

/**
 * A generic model which loads and validates data from DB results
 * without direct DB access.
 */
class AbstractModel extends FeaturesObject {

	protected static $pkTable;
	protected static $pkFieldName;
	protected static $tableHandlers = array();
	protected static $uniqFieldNames = array();

	/**
	 * Set to true when model is outdated.
	 * Usually should be notified in model's view or used in ->isFinallyPendingDelete() as
	 * additional pending to delete condition.
	 */
	protected $expiredByTime = false;

	/**
	 * Set to true when model should be deleted from DB.
	 */
	protected $pendingDelete = false;

	/**
	 * Enforces default value on uninitialized properties.
	 */
	protected function setUndefPropValues( \stdClass $row, array $colNames, $defVal ) {
		foreach ( $colNames as $colName ) {
			if ( !property_exists( $row, $colName ) ) {
				$row->{$colName} = $defVal;
			}
		}
	}

	/**
	 * Deletes $row properties, when all of it's properties have null value.
	 * Usually is performed when complete NULL-value row is meaningless to store into DB.
	 * @param $requiredFields array
	 *   values are names of $row properties (DB field names)
	 * @param $row stdClass
	 *   row of DB data (keys are field names, values are data)
	 */
	protected function checkNullRow( array $requiredFields, \stdClass $row ) {
		foreach ( $requiredFields as $subProp ) {
			if ( !Gl::isNullProp( $row, $subProp ) ) {
				# table row is not completely null
				return;
			}
		}
		# clear full null-valued table row
		# Dbg\log(__METHOD__,$row);
		$row = new \stdClass();
	}

	public static function getTableHandlers() {
		return static::$tableHandlers;
	}

	/**
	 * @return array
	 *   list of tables from Schema used by current model.
	 *   note: theoretically, table sets for different models may overlap,
	 *   although it is not used throughly yet.
	 */
	public static function getTables() {
		return array_keys( static::$tableHandlers );
	}

	public function getPkTable() {
		return static::$pkTable;
	}

	public function getPkFieldName() {
		return static::$pkFieldName;
	}

	/**
	 * @param $toBeExpired bool
	 *   true, when model is expired by the time of it's last creation / modification.
	 */
	public function setExpiredByTime( $toBeExpired ) {
		if ( !is_bool( $toBeExpired ) ) {
			Q\SdvException::throwRecoverable( 'Invalid value of expired by time', __METHOD__, $toBeExpired );
		}
		$this->expiredByTime = $toBeExpired;
	}

	public function isExpiredByTime() {
		return $this->expiredByTime;
	}

	public function setPendingDeleteDebug( $toBeDeleted, $caller, $reason ) {
		if ( $toBeDeleted === true ) {
			# Q\Dbg\log(__METHOD__.':login',$this->person->login);
			# Q\Dbg\log(__METHOD__.':caller',$caller);
			# Q\Dbg\log(__METHOD__.':reason',$reason);
		}
		$this->setPendingDelete( $toBeDeleted );
	}

	public function isPendingDelete() {
		return $this->pendingDelete;
	}

	/**
	 * @param $toBeDeleted bool
	 *   true, when model has to be deleted (incomplete or invalid model).
	 */
	public function setPendingDelete( $toBeDeleted ) {
		if ( !is_bool( $toBeDeleted ) ) {
			Q\SdvException::throwRecoverable( 'Invalid value of pending delete', __METHOD__, $toBeDeleted );
		}
		$this->pendingDelete = $toBeDeleted;
	}

	/**
	 * Used by AbstractModelCollection to set final conditional
	 * pending deletion of model before collection ->update().
	 * @return bool
	 *   model's pending delete state
	 */
	public function isFinallyPendingDelete() {
		/* no extra pending deletion conditions yet */
		return $this->isPendingDelete();
	}

	/**
	 * Called by AbstractModelCollection after all tableHandlers rows were loaded.
	 */
	public function afterLoadAll() {
		/* noop */
	}

	/**
	 * @result \stdClass
	 *   key / value of each field currently set in model instance;
	 */
	public function getAllFieldsVals() {
		$fields = new \stdClass();
		foreach ( static::$tableHandlers as $tableName => $tableDef ) {
			foreach ( $tableDef as $fieldName => $fieldDef ) {
				if ( $fieldDef === null ) {
					# skip this field; Used in next cases:
					# 1."duplicate" PK already populated via previous call.
					# 2. Skip field already populated by multi-field (usually a table row) handler.
					continue;
				}
				# Dbg\log('fieldName',$fieldName);
				# Dbg\log('fieldDef',$fieldDef);
				if ( is_array( $fieldDef ) ) {
					$fieldMethod = array_pop( $fieldDef );
					$fieldDef = array_unshift( $fieldDef, $fieldName );
					$fieldsetVals = $this->getFieldsetVals( $tableName, $fieldDef );
					foreach ( $fieldsetVals as $key => $val ) {
						if ( property_exists( $fields, $key ) ) {
							Q\SdvException::throwError( 'Ambiguous field name', __METOD__, $key );
						}
						$fields->{$key} = $val;
					}
				} else {
					if ( property_exists( $fields, $fieldName ) ) {
						Q\SdvException::throwError( 'Ambiguous field name', __METOD__, $fieldName );
					}
					$fields->{$fieldName} = $this->getFieldVals( $tableName, $fieldName );
				}
			}
		}
		return $fields;
	}

	protected function tryHandlerExists( $tableName, $fieldName ) {
		if ( !array_key_exists( $tableName, static::$tableHandlers ) ) {
			SdvException::throwError(
				'No such tableName for model defined',
				__METHOD__,
				$tableName
			);
		}
		if ( !array_key_exists( $fieldName, static::$tableHandlers[$tableName] ) ) {
			SdvException::throwError(
				'No such tableName for model defined',
				__METHOD__,
				array( 'tableName' => $tableName, 'fieldName' => $fieldName )
			);
		}
	}

	/**
	 * @param $tableName string
	 *   table member of ->tableHandlers;
	 * @param $fieldName string
	 *   field member of ->tableHandlers[$tableName];
	 * @return mixed
	 *   null / array / scalar value(s) of specified $tableName / $fieldName;
	 *   NoValue : field was not set (for example due to exception throwed in field handler);
	 */
	public function getFieldVals( $tableName, $fieldName ) {
		if ( !$this->hasProp( $tableName ) ) {
			# current optional table has no properties set in current model
			return null;
		}
		# Dbg\log(__METHOD__.':tableName',$tableName);
		# Dbg\log(__METHOD__.':fieldName',$fieldName);
		$this->tryHandlerExists( $tableName, $fieldName );
		if ( is_array( static::$tableHandlers[$tableName][$fieldName] ) ) {
			SdvException::throwError(
				'Multi-field handlers should be accessed via ::getFieldsetVals()',
				__METHOD__,
				array( 'tableName' => $tableName, 'fieldName' => $fieldName )
			);
		}
		if ( $tableName === static::$pkTable && $fieldName === static::$pkFieldName ) {
			# Dbg\log(__METHOD__.':pk',$this->getPK());
			return $this->getPK();
		}
		if ( is_object( $this->{$tableName} ) ) {
			if ( property_exists( $this->$tableName, $fieldName ) ) {
				# Dbg\log(__METHOD__.':object',$this->{$tableName}->{$fieldName});
				return $this->{$tableName}->{$fieldName};
			} else {
				return new NoValue();
			}
		} elseif ( is_array( $this->{$tableName} ) ) {
			$result = array();
			foreach ( $this->{$tableName} as $row ) {
				$result[] = $row[$fieldName];
			}
			# Dbg\log(__METHOD__.':array',$result);
			return $result;
		} else {
			SdvException::throwError(
				'Unsupported type of field value (bug in descendant?)',
				__METHOD__,
				array( 'tableName' => $tableName, 'fieldName' => $fieldName, 'value' => $this->{$tableName} )
			);
		}
	}

	/**
	 * @param $tableName string
	 *   table member of ->tableHandlers;
	 * @param $fieldNames array
	 *   field members of ->tableHandlers[$tableName] (usually CompoundField childs field names);
	 * @return array
	 *   values of specified $tableName / $fieldName;
	 */
	public function getFieldsetVals( $tableName, array $fieldNames ) {
		if ( count( $fieldNames ) === 1 ) {
			$fieldName = $fieldNames[0];
			$vals = $this->getFieldVals( $tableName, $fieldName );
			# Dbg\log(__METHOD__.':vals scalar',$vals);
			return array( $fieldName => $vals );
		}
		if ( !$this->hasProp( $tableName ) ) {
			# current optional table has no properties set in current model
			return null;
		}
		$fieldName = $fieldNames[0];
		$this->tryHandlerExists( $tableName, $fieldName );
		if ( !is_array( static::$tableHandlers[$tableName][$fieldName] ) ) {
			SdvException::throwError(
				'Single field handlers should be accessed via ::getFieldVals()',
				__METHOD__,
				array( 'tableName' => $tableName, 'fieldName' => $fieldName )
			);
		}
		$handlerFields = static::$tableHandlers[$tableName][$fieldName];
		$method = array_pop( $handlerFields );
		array_unshift( $handlerFields, $fieldName );
		if ( serialize( $fieldNames ) !== serialize( $handlerFields ) ) {
			SdvException::throwError(
				'Field names and / or field order of CompoundFieldForm childs and model multi-field handler do not match',
				__METHOD__,
				array( 'compound' => $fieldNames, 'handler' => $handlerFields )
			);
		}
		$result = array_flip( $fieldNames );
		foreach ( $fieldNames as $fieldName ) {
			$result[$fieldName] = array();
			if ( is_array( $this->{$tableName} ) ) {
				foreach ( $this->{$tableName} as $row ) {
					$result[$fieldName][] = $row[$fieldName];
				}
			} else {
				SdvException::throwError(
					'Unsupported type of field value (bug in descendant?)',
					__METHOD__,
					array( 'tableName' => $tableName, 'fieldName' => $fieldName, 'value' => $this->{$tableName} )
				);
			}
		}
		# Dbg\log(__METHOD__.':result',$result);
		return $result;
	}

	/**
	 * @return array
	 *   key
	 *     int 0
	 *   value
	 *     string: fieldMethod (handler) for selected $tableName / $fieldName;
	 *     null: Skip this field (no handler);
	 *   key
	 *     int 1
	 *   value
	 *     fieldMethod's (handler's) table fields whose values
	 *     should be passed to $this->{$fieldMethod}() :
	 *       scalar: single field handler;
	 *       array: names of fields in multi-field (row) handler;
	 */
	public function getFieldHandler( $tableName, $fieldName ) {
		if ( !array_key_exists( $tableName, static::$tableHandlers ) ) {
			SdvException::throwError( 'Undefined table name', __METHOD__, $tableName );
		}
		if ( !array_key_exists( $fieldName, static::$tableHandlers[$tableName] ) ) {
			SdvException::throwError(
				'Undefined field name of table',
				__METHOD__,
				array( 'tableName' => $tableName, 'fieldName' => $fieldName )
			);
		}
		$fieldDef = static::$tableHandlers[$tableName][$fieldName];
		if ( is_array( $fieldDef ) ) {
			# Extract model's multi-field $fieldMethod.
			$fieldMethod = array_pop( $fieldDef );
			# $fieldDef will contain all handler fields in proper order.
			array_unshift( $fieldDef, $fieldName );
			return array( $fieldMethod, $fieldDef );
		} else {
			return array( $fieldDef, $fieldName );
		}
	}

	public function getUniqFieldNames() {
		return static::$uniqFieldNames;
	}

	/**
	 * AbstractModelCollection uses this method to get UNIQUE KEY of ->pkTable
	 * which is available from source data (when surrogate key is unavailable yet).
	 * @return array
	 *   source UNIQ
	 * @note
	 *   source UNIQ is always associative array,
	 *   encoded UNIQ is always a string.
	 */
	public function getUniq() {
		$sourceUniq = array();
		$pkTableFields = $this->getProp( static::$pkTable );
		foreach ( static::$uniqFieldNames as $uniqFieldName ) {
			if ( !property_exists( $pkTableFields, $uniqFieldName ) ) {
				SdvException::throwError(
					'UNIQ field value must be defined',
					__METHOD__,
					array(
						'pkTable' => static::$pkTable,
						'uniqFieldName' => $uniqFieldName,
						'pkTableFields' => $pkTableFields
					)
				);
			}
			$fieldVal = $pkTableFields->{$uniqFieldName};
			/**
			 * Non-null values has to be converted into strings,
			 * otherwise generated source UNIQ value will not typematch
			 * source UNIQ value generated by
			 * AbstractModelCollection::getModelUniqByRow().
			 */
			$sourceUniq[$uniqFieldName] = ( $fieldVal !== null ) ?
				strval( $fieldVal ) : $fieldVal;
		}
		return $sourceUniq;
	}

	/**
	 * Copies current instance values into supplied destination instance.
	 * Destination instance is not necessarily has to be derived from current class.
	 * However table handlers (not table names) must have matches.
	 * @param $dstModel AbstractModel
	 */
	public function copyTo( AbstractModel $dstModel ) {
		foreach ( static::$tableHandlers as $tableName => $fieldHandlers ) {
			foreach ( $fieldHandlers as $fieldName => $fieldDef ) {
				if ( $fieldDef === null ) {
					# skip this field; Used in next cases:
					# 1."duplicate" PK already populated via previous call.
					# 2. Skip field already populated by multi-field (usually a table row) handler.
					continue;
				}
				if ( is_array( $fieldDef ) ) {
					$fieldMethod = array_pop( $fieldDef );
					$fieldNames = array_merge( array( $fieldName ), $fieldDef );
					$valSet = $this->getFieldsetVals( $tableName, $fieldNames );
					# Dbg\log(__METHOD__.':valSet',$valSet);
					foreach ( $valSet as $fieldName => $vals ) {
						$rowCount = count( $vals );
						break;
					}
					# Build multi-field callback (one or many $fieldMethod calls with multiple arguments).
					for ( $i = 0; $i < $rowCount; $i++ ) {
						$args = array();
						# $fieldKey is integer 0..n index.
						foreach ( $fieldNames as $fieldKey => $fieldName ) {
							$val = $valSet[$fieldName][$i];
							if ( $val instanceof NoValue ) {
								SdvException::throwError(
									'Unable to handle partially initialized list of values',
									__METHOD__,
									$valSet
								);
							}
							$args[$fieldKey] = $val;
						}
						# Dbg\log(__METHOD__.':args',$args);
						call_user_func_array( array( $dstModel, $fieldMethod ), $args );
					}
				} else {
					# Build single-field callback (one or many $fieldMethod calls with single argument).
					$val = $this->getFieldVals( $tableName, $fieldName );
					# Dbg\log(__METHOD__.':val', $val );
					if ( is_array( $val ) ) {
						foreach ( $val as $scalarVal ) {
							if ( $scalarVal instanceof NoValue ) {
								SdvException::throwError(
									'List of values should not include NoValue. NoValue has to be scalar.',
									__METHOD__,
									$val
								);
							}
							$dstModel->{$fieldDef}( $scalarVal );
						}
					} else {
						if ( !($val instanceof NoValue) ) {
							$dstModel->{$fieldDef}( $val );
						}
					}
				}
			}
		}
	}

	/**
	 * Loads primary table from data row supplied.
	 * @param $data stdClass
	 *   primary table single row
	 * note: may raise exception if row data is incompatible in some way.
	 */
	public function loadPrimaryRow( \stdClass $row ) {
		$this->loadTableRow( static::$pkTable, $row );
	}

	/**
	 * Generic setter of model properties from DB table row supplied.
	 * @param $tableName string
	 *   key of static::$tableHandlers array; usually matches DB table name;
	 * @param $row stdClass
	 *   table single row; note: not all of fields required to be loaded.
	 */
	public function loadTableRow( $tableName, \stdClass $row ) {
		# Dbg\log(__METHOD__.':tableName',$tableName);
		# Dbg\log(__METHOD__.':row',$row);
		if ( !array_key_exists( $tableName, static::$tableHandlers ) ) {
			SdvException::throwError(
				'Database table name is not registered in static::$tableHandlers',
				__METHOD__,
				array( 'tableName' => $tableName, 'tableHandlers' => static::$tableHandlers )
			);
		}
		foreach ( static::$tableHandlers[$tableName] as $fieldName => $fieldDef ) {
			if ( $fieldDef === null ) {
				# skip this field; Used in next cases:
				# 1."duplicate" PK already populated via previous call.
				# 2. Skip field already populated by multi-field (usually a table row) handler.
				continue;
			}
			# Dbg\log('fieldName',$fieldName);
			# Dbg\log('fieldDef',$fieldDef);
			if ( property_exists( $row, $fieldName ) ) {
				if ( is_array( $fieldDef ) ) {
					$args = array( $row->{$fieldName} );
					$fieldMethod = array_pop( $fieldDef );
					foreach ( $fieldDef as $extraFieldName ) {
						$args[] = $row->{$extraFieldName};
					}
					call_user_func_array( array( $this, $fieldMethod ), $args );
				} else {
					$this->{$fieldDef}( $row->{$fieldName} );
				}
			}
		}
	}

} /* end of AbstractModel class */
