<?php
namespace QuestPC;

if ( php_sapi_name() !== 'cli' ) {
	die();
}

require_once( rtrim( __DIR__, '/\\' ) . '/../Boot.php' );

$wikitext = file_get_contents( 'wiki1.txt' );

$outFile = "{$appContext->IP}/blog/wiki1.htm";
unlink( $outFile );

$wikiParser = new WikiText( array(
	'wpwiki_wiki_site' => 'po-kategoryam.ru',
	'wpwiki_toc_title' => 'Soderzhanie',
	'wpwiki_page_title' => 'wiki1.htm'
) );

$html = $wikiParser->transform( $wikitext );

file_put_contents( $outFile, $html );
