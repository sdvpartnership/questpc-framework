=== Bootstrap ===
This directory represents minimal file set for bootstrap of questpc framework.

* CLI bootstrap is performed via require_once() of 'Boot.php'.
* HTTP bootstrap is performed via 'index.php', which also uses 'Boot.php'.

This bootstrap is used for CLI-based maintenance\tests\ of framework.
HTTP bootstrap is provided only as example to copy into real project.
Real project setup is performed via 'LocalContext.php'.

'sdv_engine.txt' contains relative path to questpc 'Init.php', howerver in real development
and especially in production it is recommended to use absolute filesystem path.
