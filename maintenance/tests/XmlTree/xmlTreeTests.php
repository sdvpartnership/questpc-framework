<?php
namespace QuestPC;

if ( php_sapi_name() !== 'cli' ) {
	die();
}

require_once( rtrim( __DIR__, '/\\' ) . '/../Boot.php' );

function writeAllTypes( XmlTree $tree, $idx ) {
	foreach ( array( 'xml', 'html', 'json' ) as $type ) {
		$tree->save( $type, "output{$idx}.{$type}" );
	}
}

# note: Most of test do not have self-check. To ensure that XmlTree class
# behaves correctly, compare previous test result files to current ones.

# test 1: load complex html file.

$tree = XmlTree::newFromFile( 'html', 'xmlTreeTests.htm' );

$treeArray = $tree->getArray();
Dbg\log('tree array',$treeArray);

writeAllTypes( $tree, 1 );

$tree->replaceArray( $treeArray );

writeAllTypes( $tree, 2 );

# test 2: create tree from string and append nodes from array to each of xpath result node.

$tree = new XmlTree( 'xml', '<xml><log></log><log></log></xml>' );
$tree->xpath( '/xml/log' )->replaceArray(
	array(
		'test',
		array(
			'@tag' => 'ul',
			'type' => 'disc',
			'abra',
			array(
				'@tag' => 'li',
				'different worlds'
			),
			'hello!',
			array(
				'@tag' => 'li',
				'clear sky',
				array( '#comment' => 'I was here!' )
			),
			'new vision'
		),
		array( '#cdata' => 'test2' )
	)
);

# test3: get the whole tree as array.

$treeArray = $tree->byVid( null )->getArray();
Dbg\log('tree2 array',$treeArray);

$tree->byVid( null )->xpath( '/' );

writeAllTypes( $tree, 3 );

# test4: use replaceArray() to replace the whole tree with the same content from $treeArray.

$tree->replaceArray( $treeArray );

Dbg\log('tree2 pass2 array',$tree->getArray());

writeAllTypes( $tree, 4 );

if ( file_get_contents( 'output3.json' ) !== file_get_contents( 'output4.json' ) ) {
	throw new \Exception( 'test4 failed.' );
}

# test 4.1: innerHTML

$tree->rootXpath( '/xml/ul/*' )->innerHTML( '<a href="test.html">Hello!<br>new line</a>' );
$treeArray = $tree->byVid( null )->getArray();
# each ul child node should have anchor with br as child
Dbg\log('tree4.1 array',$treeArray);
writeAllTypes( $tree, 4.1 );


# test5: ->insert( 'before' ) test.

$tree->rootXpath( 'ul' )
	->addAttr( 'class', 'our_ul' )
	->insert( 'before', 'span', 'text before' )
	->addAttr( 'class', 'elem_before' );

writeAllTypes( $tree, 5 );

# test6: ->insert( 'after' ) test.

$tree->byVid( null )
	->xpath( 'ul' )
	->insert( 'after', 'div', 'text after' )
	->addAttr( 'class', 'elem_after' );

writeAllTypes( $tree, 6 );

# test7: ->insertArray( 'before' ) test.

$tree->rootXpath( 'ul' )
	->insertArray( 'before',
			array(
				array(
					'@tag' => 'center',
					'Hello!'
				),
				array(
					'@tag' => 'bold',
					'Warning!!!'
				)
			)
		);

writeAllTypes( $tree, 7 );

# test8: ->insert( 'after' ) test.

$tree->rootXpath( 'ul' )
	->insertArray( 'after',
			array(
				array(
					'@tag' => 'italic',
					'new age'
				),
				array(
					'@tag' => 'underline',
					'new wave'
				)
			)
		);

writeAllTypes( $tree, 8 );

# test9: comment/cdata special characters test. create tree from array test.

$tree = new XmlTree( 'xml',
	array(
		'@tag' => 'xml',
		array( '#comment' => 'Test --- just a test' ),
		array( '#cdata' => 'Random strings ]]> yet another string ]]> and once more.' )
	)
);

$xml = $tree->getString( 'xml' );
Dbg\log('comment and cdata before',$xml);

$tree = new XmlTree( 'xml', $xml );
Dbg\log('comment and cdata after',$tree->getString( 'xml' ));
Dbg\log('comment and cdata array after',$tree->getArray());
