<?php
namespace QuestPC;

if ( php_sapi_name() !== 'cli' ) {
	die();
}

$optionsList = '[new|drop|update]';

if ( count( $appContext->argv ) === 0 || count( $appContext->argv ) > 1 ) {
	Gl::dieUsage( $optionsList );
}

try {
	$sc = new SchemaCreator( $appContext->dbw );

	if ( in_array( 'drop', $appContext->argv, true ) ) {
		$sc->setDropTables( true );
	} elseif ( in_array( 'update', $appContext->argv, true ) ) {
		$sc->setUpdateSchema( true );
	} elseif ( !in_array( 'new', $appContext->argv, true ) ) {
		Gl::dieUsage( $optionsList );
	}
	echo 'Are you sure you want to update ' . $appContext->dbw->getDbName() . "?\n";
	if ( !preg_match('/^y/i', fgets( STDIN ) ) ) {
		echo "Cancelled\n\n";
		exit;
	}
	# create schema tables
	$sc->createTables( $appContext->schema->getProp( 'tableList' ) );
} catch ( \Exception $e ) {
	Dbg\except_die( $e );
}
