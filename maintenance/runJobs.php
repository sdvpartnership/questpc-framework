<?php
namespace QuestPC;

if ( php_sapi_name() !== 'cli' ) {
	die();
}

Dbg\log('runJobs argv',$appContext->argv);

if ( count( $appContext->argv ) < 4 ) {
	die( "Usage: {$appContext->scriptName} [logprefix.jobgroup] [dow_num] [tick_num] [className |...| className]\n" );
}

$split = explode( '.', $appContext->argv[0], 2 );
if ( count( $split ) !== 2 ) {
	die( "First argument is not logprefix.jobgroup\n" );
}
list( $logPrefix, $jobGroup ) = $split;
$time = new CronTimeParts();
$time->dow = intval( $appContext->argv[1] );
$time->tick = intval( $appContext->argv[2] );
@mkdir( "{$appContext->IP}/maintenance/logs" );

try {
	for ( $i = 3; $i < count( $appContext->argv ); $i++ ) {
		$className = AutoLoader::getFqnClassName( $appContext->argv[$i] );
		if ( !class_exists( $className ) ) {
			SdvException::throwError( 'job class does not exist', basename( __FILE__ ), $className );
		}
		$reflect = new \ReflectionClass( $className );
		if ( !$reflect->isSubclassOf( __NAMESPACE__ . '\\BaseJob' ) ) {
			SdvException::throwError( 'job class is not descendant of BaseJob', basename( __FILE__ ), $className );
		}
		# execute job for current interval number
		$job = $reflect->newInstance( $jobGroup, $time );
		$job->setDebugLog( "{$logPrefix}.{$jobGroup}." .
			AutoLoader::getBaseClassName( $className )
		);
		$job->execute();
	}
} catch ( \Exception $e ) {
	Dbg\except_die( $e );
}

$appContext->sdvDebugLog = "{$appContext->IP}/maintenance/logs/{$logPrefix}.{$jobGroup}.profiling." . Gl::timestamp( 'Y-m-d' ) . ".dbg";
Dbg\dump_request_time();
