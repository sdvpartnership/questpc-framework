<?php
namespace QuestPC;

class Dbw {

	/**
	 * Always log SQL queries which take more than one second to run.
	 *
	 * Note: When debugging, do not forget to restart MySQL server,
	 * because some slow queries are slow only at first run, before
	 * query cache is filled.
	 */
	const slowQueryTime = 3;

	# substatement tokens which does not require ':' prefix
	# todo: add new tokens, if needed
	protected static $subStmtTokens = array(
		'SELECT' => true,
		'CREATE TABLE' => true,
		'INSERT INTO' => true,
		'REPLACE INTO' => true,
		'DELETE FROM' => true,
		'UPDATE' => true,
		'DROP' => true,
		'SET' => true,
		'FROM' => true,
		'WHERE' => true,
		'LIMIT' => true,
		'LEFT JOIN' => true,
		'INNER JOIN' => true,
		'GROUP BY' => true,
		'ORDER BY' => true,
		'UNION' => true,
		'USE INDEX' => true,
		'ASC' => true,
		'DESC' => true,
		'ON' => true,
		'AND' => true,
		'OR' => true,
		'(' => true,
		')' => true,
		'*' => true,
		'.' => true,
		'=' => true,
		'!=' => true,
		'>' => true,
		'<' => true,
		'>=' => true,
		'<=' => true,
		',' => true,
		"\n" => true,
	);

	# whether adjascent tokens of the same type should be automatically
	# separated by commas;
	const dbtk_repetitive = 1;
	# whether token value has to be replaced (functional mapping) via call
	const dbtk_substituted = 2;
	# whether token can be non-string
	const dbtk_complex_value = 4;

	protected static $tokenProps;

	# an instance of mysqli object
	protected $db;
	# name of used db
	protected $dbName;
	# table prefix for shared DB
	protected $prefix = '';
	# current transaction level, in MySQL 0..1 are the possible values
	protected $transactionLevel = 0;
	# timer used to calculate current / total queries time
	protected $timer;

/* # uncomment when php 5.3 will become really widespread
	# warning! this method requires php 5.3+
	function __get( $name ) {
		return $this->db->$name;
	}
	# warning! this method requires php 5.3+
	function __call( $name, $args ) {
		return call_user_func_array( array( $this->db, $name ), $args );
	}
*/

	/**
	 * @param $args variadic
	 *   prefix string
	 *     tables prefix to run in shared database mode
	 *   params of mysqli::construct()
	 */
	function __construct() {
		$args = func_get_args();
		$this->prefix = array_shift( $args );
		if ( $this->prefix !== '' ) {
			$this->prefix .= '_';
		}
		if ( count( $args ) < 4 ) {
			SdvException::throwError( 'Invalid count of arguments',
				__METHOD__, $args
			);
		}
		$this->dbName = $args[3];
		$reflect = new \ReflectionClass( '\\mysqli' );
		$this->db = $reflect->newInstanceArgs( $args );
		if ( mysqli_connect_error() ) {
			SdvException::throwError( 'Connect Error', __METHOD__,
				array( 'errno' => mysqli_connect_errno(), 'error' => mysqli_connect_error() )
			);
		}
		/* change character set to utf8 */
		if ( !$this->db->set_charset( 'utf8' ) ) {
			SdvException::throwError( 'Error loading character set utf8', __METHOD__, $this->db->error );
		}
		if ( $this->db->query( 'SET NAMES utf8' ) === false ||
				$this->db->query( 'SET collation_connection=utf8_general_ci' ) === false ) {
			SdvException::throwError( 'Cannot switch into utf8 mode', __METHOD__ );
		}
		if ( !is_array( static::$tokenProps ) ) {
			static::$tokenProps = array(
				# statement substring (should not contain special characters)
				':' => 0,
				# table name; mostly the same as field name, however can contain db prefix
				'^' => self::dbtk_repetitive,
				# field name (string or array[table,field] pair)
				'`' => self::dbtk_repetitive | self::dbtk_complex_value,
				# substituted quoted scalar data value (string, string-like number, null)
				'?' => self::dbtk_repetitive | self::dbtk_substituted,
				# substituted non-quoted integer value (for example SELECT offset, limit)
				'!' => self::dbtk_repetitive | self::dbtk_substituted,
				# substituted "IN" clause
				'$' => self::dbtk_substituted,
				# substituted match for set of table fields (AND)
				# keys are column names, values are column values
				'~' => self::dbtk_substituted,
				# keys are ints (use default table) or strings (table names)
				# values are arrays
				#   keys are column names, values are column values
				'*' => self::dbtk_substituted,
				# prepared statement substring (non-checked for SQL XSS, bad)
				'#' => 0,
			);
		}
	}

	/**
	 * ->setTimer issued separately in $appContext->lazyLoad() closure,
	 * so we do not have to establish mysqli connection to read the timer
	 * value (see LocalSettings.php and sdv_dbg.php).
	 */
	public function setTimer( SdvTimer $timer ) {
		$this->timer = $timer;
	}

	public function getDbName() {
		return $this->dbName;
	}

	/**
	 * @return bool
	 *   true when table exists, false otherwise
	 */
	public function tableExists( $tableName ) {
		# Building raw query, because `information_schema`.`Table_Name`
		# should not be prefixed.
		$dbName = $this->quoteData( $this->dbName );
		$tableName = $this->quoteData( "{$this->prefix}{$tableName}" );
		$result = $this->rawQuery(
			"SELECT `Table_Name` " .
			"FROM `information_schema`.`TABLES` " .
			"WHERE `Table_Name` = {$tableName} AND " .
			"`TABLE_SCHEMA` = {$dbName}"
		);
		return $result->num_rows === 1;
	}

	/**
	 * @return string
	 *   statement used to create already existing table
	 */
	protected function showCreateTable( $tableName ) {
		$row = $this->querySingleRow( ":SHOW CREATE TABLE", "^{$tableName}" );
		return $row->{'Create Table'};
	}

	/**
	 * @param $schemaDef stdClass
	 *   properties from database schema:
	 *     ->tableName
	 *     ->columns
	 *     ->sql_indexes
	 *     ->engine
	 * @return string
	 *   statement that should be used to create new table
	 */
	protected function getCreateTableStmt( \stdClass $schemaDef ) {
		$this->tryValidFieldName( $schemaDef->tableName );
		$this->checkColumnsNames( array_keys( $schemaDef->columns ) );
		$indexes_stmt = implode( $schemaDef->sql_indexes, ",\n" );
		$s = $this->prepareQuery( "CREATE TABLE", "^{$schemaDef->tableName}", "#(\n" );
		foreach ( $schemaDef->columns as $colname => $coltype ) {
			$s .= "`" . $this->db->real_escape_string( $colname ) . "` ${coltype},\n";
		}
		$s .= "${indexes_stmt} \n) DEFAULT CHARSET=utf8";
		if ( $schemaDef->engine !== null ) {
			if ( !preg_match( '/^\w+$/', $schemaDef->engine ) ) {
				SdvException::throwFatal( 'An attempt to inject invalid ENGINE', __METHOD__,
					$schemaDef->engine );
			}
			$s .= " ENGINE={$schemaDef->engine}";
		}
		return $s;
	}

	/**
	 * Provides rough tokenizer of SQL queries.
	 * Used in ->compareQueries() method to compare two SQL queries.
	 * @param $stmt string
	 *   raw SQL statement
	 * @return array
	 *   token lines array
	 *     SQL tokens
	 */
	protected function getStmtTokens( $stmt ) {
		$raws = array_map( 'trim', preg_split( "/\n/", $stmt, -1, PREG_SPLIT_NO_EMPTY ) );
		$tokenLines = array();
		foreach ( $raws as $raw ) {
			if ( count( $raw ) > 0 ) {
				$tokens = array_map( 'trim',
					preg_split(
						# @note: parentheses are required for PREG_SPLIT_DELIM_CAPTURE to work
						'/(`)/',
						$raw,
						-1,
						PREG_SPLIT_NO_EMPTY | PREG_SPLIT_DELIM_CAPTURE
					)
				);
				foreach ( $tokens as &$token ) {
					if ( preg_match( '/^(\))( (?:DEFAULT CHARSET|ENGINE)=\w+)(?:.*?)( (?:DEFAULT CHARSET|ENGINE)=\w+)(?:.*?)/', $token, $matches ) ) {
						# remove all match
						unset( $matches[0] );
						# sort DEFAULT CHARSET / ENGINE entries
						if ( $matches[2] < $matches[3] ) {
							$tmp = $matches[2];
							$matches[2] = $matches[3];
							$matches[3] = $tmp;
						}
						# reassembly token
						$token = implode( '', $matches );
					}
					if ( preg_match( '/^(int|tinyint)(\(\d+\))(.*)/', $token, $matches ) ) {
						# remove all match
						unset( $matches[0] );
						# remove int digits size definition
						unset( $matches[2] );
						# reassembly token
						$token = implode( '', $matches );
					}
				}
				if ( count( $tokens ) > 0 ) {
					$tokenLines[] = $tokens;
				}
			}
		}
		return $tokenLines;
	}

	/**
	 * Compares two SQL statements.
	 * @note: Incomplete and limited, use at your own risk.
	 * Currently is used only to detect schema changes in ->updateTable().
	 * @return mixed
	 *   array of difference tokens
	 *   int 1 when statements match
	 */
	protected function compareQueries( $stmt1, $stmt2 ) {
		$tokenLines1 = $this->getStmtTokens( $stmt1 );
		$tokenLines2 = $this->getStmtTokens( $stmt2 );
		# ensure both arrays have the same number of lines
		$delta = count( $tokenLines1 ) - count( $tokenLines2 );
		if ( $delta !== 0 ) {
			$firstSmaller = ($delta < 0);
			$delta = abs( $delta );
			for ( $i = 0; $i < $delta; $i++ ) {
				if ( $firstSmaller ) {
					$tokenLines1[] = array();
				} else {
					$tokenLines2[] = array();
				}
			}
		}
		try {
			foreach ( $tokenLines1 as $key => $tokens1 ) {
				$tokens2 = &$tokenLines2[$key];
				if ( count( $tokens1 ) !== count( $tokens2 ) ) {
					SdvException::throwError( 'Token lines are not equal', __METHOD__ );
				}
				foreach ( $tokens1 as $key => $token1 ) {
					if ( $token1 !== $tokens2[$key] ) {
						SdvException::throwError( 'Tokens do not match', __METHOD__ );
					}
				}
			}
		} catch ( SdvException $e ) {
			return
				array(
					'stmt1' => $stmt1,
					'stmt2' => $stmt2,
					'line1' => implode( '', $tokens1 ),
					'line2' => implode( '', $tokens2 ),
				);
		}
		return 1;
	}

	/**
	 * Creates previousely non-existing table from $schemaDef provided.
	 * @throw SdvException
	 *   when query error (eg. table exists)
	 * @return boolean true
	 *   success
	 */
	function createTable( \stdClass $schemaDef ) {
		$this->rawQuery( $this->getCreateTableStmt( $schemaDef ) );
		# @note: will return true only if ->rawQuery() will not throw an exception
		return true;
	}

	/**
	 * Creates new table from $schemaDef provided only if
	 * table does not exists.
	 * If table exists, compares previous and current create
	 * statements.
	 * @param $schemaDef
	 *   schema table definition, see ->getCreateTableStmt() for
	 *   description of the properties
	 * @return mixed
	 *   true  - previousely non-existing table was created successfully;
	 *   int 1 - schema did not change;
	 *   array - schema change was detected. array contains
	 *   diagnostic report from ->compareQueries();
	 */
	function updateTable( \stdClass $schemaDef ) {
		$cts = $this->getCreateTableStmt( $schemaDef );
		if ( !$this->tableExists( $schemaDef->tableName ) ) {
			$this->rawQuery( $cts );
			# @note: will return 1 only if ->rawQuery() will not throw an exception
			return true;
		}
		return $this->compareQueries( $cts, $this->showCreateTable( $schemaDef->tableName ) );
	}

	function close() {
		if ( $this->db->kill( $this->db->thread_id ) === false ||
				$this->db->close() === false ) {
			SdvException::throwError( 'Database close error', __METHOD__, $this->db->error() );
		}
	}

	function getHostInfo() {
		return $this->db->host_info;
	}

	function getTableName( $name ) {
		if ( $name instanceof DbTableAlias ) {
			return $name->toString( $this );
		}
		return $this->quoteFieldName( "{$this->prefix}{$name}" );
	}

	function isValidFieldName( $fieldName ) {
		return preg_match( '/^[A-Z][A-Z_\d\.]*$/i', $fieldName );
	}

	function tryValidFieldName( $fieldName ) {
		if ( !$this->isValidFieldName( $fieldName ) ) {
			SdvException::throwFatal( 'Invalid SQL field name', __METHOD__, $fieldName );
		}
		return $this->quoteFieldName( $fieldName );
	}

	function checkColumnsNames( $columnsList ) {
		if ( $columnsList === null ) {
			return;
		}
		if ( !is_array( $columnsList ) ) {
			SdvException::throwFatal( 'columnsList must be an array or null', __METHOD__, $columnsList );
		}
		foreach ( $columnsList as $fieldName ) {
			$this->tryValidFieldName( $fieldName );
		}
	}

	function quoteMetaField( $fieldName ) {
		return ($fieldName === '*') ?
			'*' : $this->quoteFieldName( $fieldName );
	}
	
	function quoteFieldName( $arg1, $arg2 = null ) {
		# ->quoteFieldName( $tableName, $fieldName ) call
		if ( $arg2 !== null ) {
			return $this->getTableName( $arg1 ) . '.' . $this->quoteMetaField( $arg2 );
		}
		# ->quoteFieldName( array( $tableName, $fieldName ) ) call
		if ( is_array( $arg1 ) ) {
			return $this->getTableName( $arg1[0] ) . '.' . $this->quoteMetaField( $arg1[1] );
		}
		# ->quoteFieldName( $fieldName ) call
		return '`' . str_replace( '`', '``', $arg1 ) . '`';
	}

	/**
	 * SphinxQL fields search MATCH() query generator.
	 * @param $fieldValues array
	 *   key:   sphinx rtindex key name
	 *   value: substring to search in key
	 */
	public static function getSphinxMatch( array $fieldValues ) {
		$result = '';
		foreach ( $fieldValues as $field => $value ) {
			$value = trim( $value );
			if ( $value === '' ) {
				# Only non-empty values can be searched, otherwise
				# sphinx will generate syntax error.
				continue;
			}
			if ( $result !== '' ) {
				$result .= ' ';
			}
			$result .= "@{$field} " . self::escapeSphinxQL( $value );
		}
		return $result;
	}

	protected static $sphinxQLsearch = array(
			'\\',
			'(',
			')',
			'|',
			'-',
			'!',
			'@',
			'~',
			'"',
			'&',
			'/',
			'^',
			'$',
			'=',
			"'",
			"\x00",
			"\n",
			"\r",
			"\x1a",
	);
	protected static $sphinxQLreplace = array(
			'\\\\',
			'\\\(',
			'\\\)',
			'\\\|',
			'\\\-',
			'\\\!',
			'\\\@',
			'\\\~',
			'\\\"',
			'\\\&',
			'\\\/',
			'\\\^',
			'\\\$',
			'\\\=',
			"\\'",
			"\\x00",
			"\\n",
			"\\r",
			"\\x1a",
	);
	/**
	 * @todo: check against XSS.
	 */
	public static function escapeSphinxQL( $stmt ) {
		return str_replace( static::$sphinxQLsearch, static::$sphinxQLreplace, $stmt );
	}

	/**
	 * Quote data values.
	 */
	function quoteData( $s ) {
		if ( $s === null ) {
			return 'NULL';
		} elseif ( !is_scalar( $s ) ) {
			SdvException::throwError( 'Quoted data must be scalar', __METHOD__, $s );
		} else {
			# This will also quote numeric values. This should be harmless,
			# and protects against weird problems that occur when they really
			# _are_ strings such as article titles and string->number->string
			# conversion is not 1:1.
			# @note: Quoting numeric values turned out to be not so harmless
			# in some cases, eg. UNION SELECT of "virtual row value":
			# Quoting numeric values is also incompatible to SphinxQL.
/*
SELECT `category`.* , `category_level`.`ancestor_level` FROM `category_level` INNER JOIN `category` ON `category`.`category_id` = `category_level`.`ancestor_id` WHERE `category_level`.`category_id` = '166' UNION SELECT `category`.* , 2000000 FROM `category` WHERE `category`.`category_id` = '166' ORDER BY `ancestor_level` ASC
*/
			# UNION SELECT `category`.* , '2000000' produced wrong ancestor_level ORDER
			return is_int( $s ) ? $s : ("'" . $this->db->real_escape_string( $s ) . "'");
		}
	}

	protected function rawQuery( $stmt ) {
		global $appContext;
		if ( $appContext->debugSQL ) {
			Dbg\log("raw query stmt",$stmt);
		}
		$this->timer->start();
		if ( ($result = $this->db->query( $stmt ) ) === false ) {
			Dbg\log("erroneous query stmt",$stmt);
			SdvException::throwError( 'DB query error', __METHOD__,
				array( 'error' => mysqli_error( $this->db ), 'stmt' => $stmt )
			);
		}
		$queryTime = round( $this->timer->stop(), 7 );
		if ( $queryTime > self::slowQueryTime ) {
			if ( !$appContext->debugSQL ) {
				# Always log slow queries regardless of ->debugSQL value.
				Dbg\log("slow query stmt",$stmt);
			}
			Dbg\log("*** slow query time ***",$queryTime);
		} elseif ( $appContext->debugSQL ) {
			Dbg\log("last query time",$queryTime);
		}
		return $result;
	}

	public function raw( $stmt ) {
		$result = $this->rawQuery( $stmt );
		if ( !is_object( $result ) ) {
			return $result;
		}
		$rows = array();
		while ( $row = $result->fetch_object() ) {
			$rows[] = $row;
		}
		return $rows;
	}

	protected function getFieldSetStr( DbwFieldSet $fieldSet ) {
		$stmt = $fieldSet->openingBrace;
		$firstElem = true;
		foreach ( $fieldSet->assocKeyVal as $key => $val ) {
			if ( is_int( $key ) ) {
				SdvException::throwError( 'key must be field name', __METHOD__, $key );
			}
			if ( !is_scalar( $val ) ) {
				SdvException::throwError( 'value must be scalar', __METHOD__, $val );
			}
			if ( $firstElem ) {
				$firstElem = false;
			} else {
				$stmt .= $fieldSet->separator;
			}
			$stmt .= ( ( $fieldSet->tableName !== null ) ?
					$this->quoteFieldName( $fieldSet->tableName, $key ) : $this->quoteFieldName( $key ) ) .
				' = ' . $this->quoteData( $val );
		}
		$stmt .= $fieldSet->closingBrace;
		return $stmt;
	}

	protected function get2dFieldSetStr( Dbw2dFieldSet $params2d ) {
		$stmt = '';
		if ( count( $params2d->fieldSets ) > 1 ) {
			# "OR" must be quoted
			$stmt .= '( ';
		}
		$firstElem = true;
		foreach ( $params2d->fieldSets as $fieldSet ) {
			if ( $firstElem ) {
				$firstElem = false;
			} else {
				$stmt .= ' OR ';
			}
			$stmt .= $this->getFieldSetStr( $fieldSet );
		}
		if ( count( $params2d->fieldSets ) > 1 ) {
			# "OR" must be quoted
			$stmt .= ' )';
		}
		return $stmt;
	}

	protected function prepareQueryInternal( array &$dbtokens, DbwParams $replacements ) {
		$prevPrefix = null;
		# Dbg\log(__METHOD__.":original_dbtokens",$dbtokens);
		# Dbg\log(__METHOD__.":replacements",$replacements);
		foreach ( $dbtokens as &$token ) {
			$prefix = '';
			# *** check for complex token values ***
			if ( is_array( $token ) ) {
				# array token is a special version of field name with table name prefix
				$prefix = '`';
			} elseif ( $token instanceof DbTableAlias ) {
				# DbTableAlias is a special version of table name with table alias
				$prefix = '^';
			}
			# *** check for string token values ***
			if ( $prefix === '' ) {
				if ( !is_string( $token ) ) {
					SdvException::throwError( 'Token should be a string', __METHOD__, $token );
				}
				# check for known substatements (SELECT and so on...)
				if ( array_key_exists( $token, static::$subStmtTokens ) ) {
					# known statement substring
					$prefix = ':';
				} else {
					$prefix = substr( $token, 0, 1 );
					$token = substr( $token, 1 );
				}
				if ( !array_key_exists( $prefix, static::$tokenProps ) ) {
					SdvException::throwError( 'Invalid prefix character for token', __METHOD__, array( $prefix, $token ) );
				}
				if ( static::$tokenProps[$prefix] & self::dbtk_substituted ) {
					if ( !$replacements->offsetExists( $token ) ) {
						Dbg\log(__METHOD__.':missing offset',array($dbtokens, $prefix, $token));
						SdvException::throwError( 'Replacements has no selected offset', __METHOD__,
							array( $dbtokens, $prefix, $token )
						);
					}
				}
			}
			switch ( $prefix ) {
			# statement substring (should not contain special characters)
			case ':' :
				if ( $token !== $this->db->real_escape_string( $token ) ) {
					SdvException::throwError(
						'Statement substring should not contain characters escaped by ' .
						'http://php.net/manual/en/mysqli.real-escape-string.php',
						__METHOD__,
						array(
							'token' => $token,
							'escapedToken' => $this->db->real_escape_string( $token ),
						)
					);
				}
				break;
			case '#' :
				# note: use ->prepareQuery() to build substatements
				break;
			# table name; mostly the same as field name, however can contain db prefix
			case '^' :
				$token = $this->getTableName( $token );
				break;
			# field name (string or array[table,field] pair)
			case '`' :
				$token = $this->quoteFieldName( $token );
				break;
			# quoted scalar data value (string, string-like number, null)
			case '?' :
				$token = $this->quoteData( $replacements->offsetGet( $token ) );
				break;
			# non-quoted integer value (for example SELECT offset, limit)
			case '!' :
				$token = intval( $replacements->offsetGet( $token ) );
				break;
			# substituted "IN" clause
			case '$' :
				$token = $this->getSingleRowInClause( $token, $replacements->offsetGet( $token ) );
				break;
			# match set of table fields
			# keys are column names, values are column values
			case '~' :
				$fieldSet = $replacements->offsetGet( $token );
				$token = $this->getFieldSetStr( $fieldSet );
				break;
			# match set of sets of single or multiple table fields
			# keys are ints (use default table) or strings (table names)
			# values are arrays
			#   keys are column names, values are column values
			case '*' :
				$params2d = $replacements->offsetGet( $token );
				$token = $this->get2dFieldSetStr( $params2d );
				break;
			default :
				SdvException::throwError( 'Unknown statement token (switch)', __METHOD__, array( $prefix, $token ) );
			}
			if ( static::$tokenProps[$prefix] & self::dbtk_repetitive ) {
				if ( $prevPrefix === $prefix ) {
					# if previous token has the same type, separate both with comma (list of the same type tokens)
					$token = ", {$token}";
				}
			}
			$prevPrefix = $prefix;
		}
		# Dbg\log(__METHOD__,implode( ' ', $dbtokens ));
		return implode( ' ', $dbtokens );
	}

	protected function getReplacements( array &$dbtokens ) {
		# get replacements array (last arg), if any
		if ( !( ( $replacements = array_pop( $dbtokens ) ) instanceof DbwParams ) ) {
			# last element is not replacements, push it back
			array_push( $dbtokens, $replacements );
			# use empty replacements instance
			$replacements = new DbwParams();
		}
		return $replacements;
	}

	public function prepareQuery( /* dbargs */ ) {
		$dbtokens = func_get_args();
		$replacements = $this->getReplacements( $dbtokens );
		return $this->prepareQueryInternal( $dbtokens, $replacements );
	}

	public function prepareQueryArray( $dbtokens ) {
		$replacements = $this->getReplacements( $dbtokens );
		return $this->prepareQueryInternal( $dbtokens, $replacements );
	}

	public function query( /* dbargs */ ) {
		$dbtokens = func_get_args();
		$replacements = $this->getReplacements( $dbtokens );
		# does not call ->prepareQuery() for performance reasons:
		# return $this->raw( call_user_func_array( array( $this, 'prepareQuery', func_get_args() ) ) );
		return $this->raw( $this->prepareQueryInternal( $dbtokens, $replacements ) );
	}

	/**
	 * Querying by array supports named query tokens such as 'pkTableName' => "^product" :
	 * 
	 *	$this->query = array(
	 *		"SELECT", "*",
	 *		"FROM", 'pkTableName' => "^product",
	 *		"WHERE", "~uniqueKey",
	 *		new Q\DbwParams( array(
	 *			'uniqueKey' => new Q\DbwFieldSet( array(
	 *				'shop_id' => $this->shopId,
	 *				'offer_id' => $this->offerId
	 *			) )
	 *		) )
	 *	);
	 *	$rows = $appContext->dbw->queryArray( $this->query );
	 *	$this->query['pkTableName'] = "^product_archive";
	 *	$rows = $appContext->dbw->queryArray( $this->query );
	 */
	public function queryArray( array $dbtokens ) {
		$replacements = $this->getReplacements( $dbtokens );
		# does not call ->prepareQuery() for performance reasons:
		# return $this->raw( call_user_func_array( array( $this, 'prepareQuery', func_get_args() ) ) );
		return $this->raw( $this->prepareQueryInternal( $dbtokens, $replacements ) );
	}

	/**
	 * @return mixed
	 *   stdClass: query returned row
	 *   null: no such row
	 */
	public function querySingleRow( /* dbargs */ ) {
		$dbtokens = func_get_args();
		$replacements = $this->getReplacements( $dbtokens );
		$result = $this->rawQuery( $this->prepareQueryInternal( $dbtokens, $replacements ) );
		if ( !is_object( $result ) ) {
			return $result;
		}
		return $result->fetch_object();
	}

	/**
	 * @return mixed
	 *   stdClass: query returned row
	 *   null: no such row
	 */
	public function querySingleRowArray( $dbtokens ) {
		$replacements = $this->getReplacements( $dbtokens );
		$result = $this->rawQuery( $this->prepareQueryInternal( $dbtokens, $replacements ) );
		if ( !is_object( $result ) ) {
			return $result;
		}
		return $result->fetch_object();
	}

	/**
	 * Begin a transaction, committing previously open transaction, if any.
	 */
	function begin() {
		$this->rawQuery( 'BEGIN' );
		$this->transactionLevel = 1;
	}

	/**
	 * End transaction, committing changes.
	 */
	function commit() {
		if ( $this->transactionLevel > 0 ) {
			$this->rawQuery( 'COMMIT' );
			$this->transactionLevel = 0;
		}
	}

	/**
	 * Rollback transaction.
	 */
	function rollback() {
		if ( $this->transactionLevel > 0 ) {
			$this->rawQuery( 'ROLLBACK' );
			Dbg\log(__METHOD__,'rollback');
			$this->transactionLevel = 0;
		}
	}

	/**
	 * Get SQL multiple rows/columns data string, similar to:
	 * (('AD', 2010), ('AF', 2009), ('AG', 1992))
	 * suitable for multi-row / multi-column inserts and deletes
	 * @param $data array
	 *   array of arrays,
	 *     inner array keys are field names, values - field values;
	 * @param $template string
	 *   where to place resulting string in function result, depending
	 *   on '%' character, eg.:
	 *   "VALUES\n%" / "IN\n(%)"
	 *   note: template itself not checked for SQL injection, be careful
	 *     however, '%'-content is properly quoted against SQL injection.
	 * Note: please be very careful not allowing SQL injection in
	 * $template parameter
	 * @return param $fieldsList array
	 *   names of columns in $data
	 * @return string
	 *   SQL multiple rows/columns data string.
	 */
	function getMultiRowStr( array &$data, $template, &$fieldsList ) {
		# use first row of $data to detect named / unnamed columns
		$fieldsList = array_key_exists( 0, $data[0] ) ?
			null : array_keys( $data[0] );
		$this->checkColumnsNames( $fieldsList );
		$namedColumnsList = ( is_array( $fieldsList ) ) ?
			' (' . implode( ',', array_map( array( $this, 'quoteFieldName' ), $fieldsList ) ) .
			')' : '';
		$firstElem = true;
		$stmt = '';
		foreach ( $data as &$row ) {
			if ( $firstElem ) {
				$firstElem = false;
			} else {
				$stmt .= ',';
			}
			$s = '(';
			$i = 0;
			if ( is_array( $fieldsList ) ) {
				# named columns
				foreach ( $fieldsList as $field ) {
					if ( $s != '(' ) {
						$s .= ',';
					}
					$s .= $this->quoteData( $row[$field] );
				}
			} else {
				# unnamed columns
				foreach ( $row as &$val ) {
					if ( $s != '(' ) {
						$s .= ',';
					}
					$s .= $this->quoteData( $val );
				}
			}
			$stmt .= "$s)\n";
			$i++;
		}
		return "{$namedColumnsList} " . str_replace( '%', $stmt, $template );
	}

	function replace( $table, array &$data ) {
		$fieldsList = null;
		$stmt = $this->prepareQuery( "REPLACE INTO", "^{$table}" ) .
			$this->getMultiRowStr( $data, "VALUES\n%", $fieldsList );
		$this->rawQuery( $stmt );
	}

	/**
	 * Multiline insert with flush threshold.
	 * Please note than last_insert_id() for multiple rows insert is not useful,
	 * thus is not supported.
	 * todo: support last_insert_id() for single row?
	 */
	function insert( $table, array &$data, $flush = 0, $uniqueFields = null ) {
		$this->tryValidFieldName( $table );
		$this->checkColumnsNames( $uniqueFields );
		if ( count( $data ) === 0 ) {
			# nothing to insert
			return;
		}
		if ( count( $data ) < $flush ) {
			return;
		}
		$fieldsList = null;
		$stmt = $this->prepareQuery( "INSERT INTO", "^{$table}" ) .
			$this->getMultiRowStr( $data, "VALUES\n%", $fieldsList );
		if ( is_array( $fieldsList ) && is_array( $uniqueFields ) ) {
			$fields = array_diff( $fieldsList, $uniqueFields );
			if ( count( $fields ) > 0 ) {
				$stmt .= "ON DUPLICATE KEY UPDATE ";
				$firstElem = true;
				foreach ( $fields as $field ) {
					if (  $firstElem ) {
						$firstElem = false;
					} else {
						$stmt .= ',';
					}
					$qf = $this->quoteFieldName( $field );
					$stmt .=  "{$qf} = VALUES({$qf})";
				}
			}
		}
		$data = array();
		# Dbg\log("insert stmt",$stmt,true);
		$this->rawQuery( $stmt );
	}

	/**
	 * Single row value "WHERE IN" clause, optimized for integer row values.
	 * The $fieldValues row does not have field names,
	 * field name is taken from $fieldName.
	 * @return string
	 *   optimized WHERE IN clause for set of values
	 */
	public function getSingleRowInClause( $fieldName, array $fieldValues ) {
		if ( count( $fieldValues ) < 1 ) {
			SdvException::throwFatal( 'Cannot build WHERE clause for empty set given', __METHOD__ );
		}
		$qf = $this->quoteFieldName( $fieldName );
		if ( count( $fieldValues ) === 1 ) {
			return "{$qf} = " . $this->quoteData( $fieldValues[0] );
		}
		# check, whether there are non-integer values
		foreach ( $fieldValues as $field ) {
			if ( !is_int( $field ) ) {
				# string values
				return "{$qf} IN (" . implode( ',', array_map( array( $this, 'quoteData' ), $fieldValues ) ) . ')';
			}
		}
		# integer-only values
		sort( $fieldValues, SORT_NUMERIC );
		$prevVal = $fieldValues[0];
		foreach ( $fieldValues as $val ) {
			if ( ( $val - $prevVal ) > 1 ) {
				return "{$qf} IN (" . implode( ',', array_map( 'intval', $fieldValues ) ) . ')';
			}
			$prevVal = $val;
		}
		return "{$qf} BETWEEN {$fieldValues[0]} AND " . array_pop( $fieldValues );
	}

	function insert_id() {
		return $this->db->insert_id;
	}

	/**
	 * Assure that table has only $new_rows with selected key field name.
	 * Smart select / update / delete to minimize table fragmentation.
	 *
	 * note: Atomic integrity is built-in, however also has fall-back for
	 * integrity at callers-level.
	 *
	 * todo: currently it supports single column PK, extend for multi-column PK,
	 * when needed.
	 */
	function setUniqueDataForField( $tableName, array $new_rows, $keyFieldName ) {
		# Validation of $new_rows will be checked by getMultiRowStr(), when called.
		$this->checkColumnsNames( array( $tableName, $keyFieldName ) );
		# Should we use callers integrity or our own integrity?
		$noCallerTransaction = ($this->transactionLevel === 0);
		if ( $noCallerTransaction ) {
			$this->begin();
		}
		# get list of key field values;
		$keyFieldValues = array();
		# build serialized representation of $new_rows
		$new_serial_key = array();
		foreach ( $new_rows as $key => $row ) {
			# there might be multiple occurences of the same field value,
			# thus we are writing values to keys then getting them via array_keys();
			$keyFieldValues[$row[$keyFieldName]] = true;
			# make sure numeric values of new rows are represented as strings
			# and keys are sorted to have identical row serialize()
			ksort( $row, SORT_STRING );
			foreach ( $row as &$val ) {
				if ( $val !== null && !is_string( $val ) ) {
					$val = strval( $val );
				}
			}
			$new_serial_key[serialize( $row )] = $key;
		}
		if ( count( $keyFieldValues ) > 0 ) {
			$orig_rows = $this->query( ':SELECT * FROM', "^{$tableName}", 'WHERE', "\${$keyFieldName}",
				new DbwParams( array( $keyFieldName => array_keys( $keyFieldValues ) ) ) );
		} else {
			$orig_rows = array();
		}
		# build serialized representation of $orig_rows
		$orig_serial_key = array();
		foreach ( $orig_rows as $key => $row ) {
			$row = (array) $row;
			# numeric values of original rows are already represented as strings
			# make keys sorted to have identical row serialize()
			ksort( $row, SORT_STRING );
			$orig_serial_key[serialize( $row )] = $key;
		}
		# Dbg\log(__METHOD__.':new_serial_key',$new_serial_key);
		# Dbg\log(__METHOD__.':orig_serial_key',$orig_serial_key);
		# get difference between new and old rows
		$update_serial_key = array_diff_key( $new_serial_key, $orig_serial_key );
		$delete_serial_key = array_diff_key( $orig_serial_key, $new_serial_key );
		if ( count( $update_serial_key ) > 0 ) {
			# Dbg\log(__METHOD__.':update_serial_key',$update_serial_key);
			$update_rows = array();
			foreach ( $update_serial_key as $new_key ) {
				$update_rows[] = $new_rows[$new_key];
			}
			# Some row might be entirely new,
			# another might exists but have different field values.
			$this->insert( $tableName, $update_rows, 0, array( $keyFieldName ) );
		}
		if ( count( $delete_serial_key ) > 0 ) {
			# Dbg\log(__METHOD__.':delete_serial_key',$delete_serial_key);
			$deleteRows = array();
			foreach ( $delete_serial_key as $orig_key ) {
				$deleteRows[] = (array) $orig_rows[$orig_key];
			}
			$fieldsList = null;
			$stmt = $this->prepareQuery( "DELETE FROM", "^{$tableName}", "WHERE" ) . " " .
				$this->getMultiRowStr( $deleteRows, "IN\n(%)", $fieldsList );
			$this->rawQuery( $stmt );
		}
		if ( $noCallerTransaction ) {
			$this->commit();
		}
	}

} /* end of Dbw class */
