<?php
namespace QuestPC;

class DbTableAlias {

	protected $tableName;
	protected $alias;

	function __construct( $tableName, $alias ) {
		$this->tableName = $tableName;
		$this->alias = $alias;
	}

	function toString( Dbw $dbw ) {
		return $dbw->getTableName( $this->tableName ) .
			" AS " .
			$dbw->getTableName( $this->alias );
	}

} /* end of DbTableAlias class */

class DbwParams extends \ArrayObject {

	public function offsetSetNew( $index, $newval ) {
		if ( $this->offsetExists( $index ) ) {
			SdvException::throwError( 'Key already exists', __METHOD__, array( $index, $newval ) );
		}
		$this->offsetSet( $index, $newval );
	}

} /* end of DbwParams class */

/**
 * DbwParams value:
 * ['row1' => 'value1', ..., 'rowN' => 'valueN']
 * matches to SQL substatement:
 * (`row1` = 'value1' AND ... `rowN` = 'valueN')
 */
class DbwFieldSet {

	/**
	 * @note: Made public for faster access. Do not modify outside of class.
	 */
	public $assocKeyVal;
	public $tableName;
	public $openingBrace = '(';
	public $closingBrace = ')';
	public $separator = ' AND ';

	public function setBraces() {
		switch ( count( $this->assocKeyVal ) ) {
		case 0:
			SdvException::throwError( '->assocKeyVal should not be empty',
				__METHOD__, $this );
		case 1:
			$this->openingBrace =
			$this->closingBrace = '';
		}
	}
	
	function __construct( array $fieldSet, $tableName = null ) {
		$this->tableName = $tableName;
		$this->assocKeyVal = $fieldSet;
		$this->setBraces();
	}

} /* end of DbwFieldSet class */

class UpdateFieldSet extends DbwFieldSet {

	public $openingBrace = '';
	public $closingBrace = '';
	public $separator = ', ';

} /* end of UpdateFieldSet class */

/**
 * DbwParams value:
 *   [
 *     'table1' => ['t1row1' => 'value1', ..., 't1rowN' => 'valueN'],
 *     ...
 *     0 => ['row1' => 'valueA', ..., 'rowN' => 'valueB'],
 *     ...
 *     'table3' => ['t3row1' => 'valueX', ..., 't3rowN' => 'valueY']
 *   ]
 * matches to SQL substatement:
 *   (
 *     (`table1`.`t1row1` = 'value1' AND ... `table1`.`t1rowN` = 'valueN') OR
 *     ...
 *     (`row1` = 'valueA' AND ... `rowN` = 'valueB') OR
 *     ...
 *     (`table3`.`t3row1` = 'valueX' AND ... `table3`.`t3rowN` = 'valueY')
 *   )
 */
class Dbw2dFieldSet {

	/**
	 * @note: Made public for faster access. Do not modify outside of class.
	 */
	public $fieldSets;

	function __construct( array $fieldSets, $tableName = null ) {
		foreach ( $fieldSets as $tableName => $fieldSet ) {
			if ( is_int( $tableName ) ) {
				$tableName = null;
			}
			$this->fieldSets[] = new DbwFieldSet( $fieldSet, $tableName );
		}
	}

} /* end of Dbw2dFieldSet class */
