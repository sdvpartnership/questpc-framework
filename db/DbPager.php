<?php
namespace QuestPC;

/**
 * A pager that uses built-in \QuestPC\Dbw instance.
 */
class DbPager extends AbstractPager {

	protected $lazyLoadDb;

	protected $pagesCountQuery;
	protected $pagerArgs;
	protected $pagerArgsReplacements;

	function __construct(
		$collection = null,
		$loadAllTables = false,
		$limit = null,
		$offset = 0
	) {
		global $appContext;
		# php 5.3 closures ugliness.
		$self = $this;
		$this->lazyLoad( 'db', function() use ( $self ) {
			return $self->getLazyDb();
		} );
		parent::__construct( $collection, $loadAllTables, $limit, $offset );
	}

	public function setLazyDb( $dbw = null ) {
		if ( property_exists( $this, 'db' ) ) {
			SdvException::throwError(
				'Cannot override already loaded ->db',
				__METHOD__,
				$this->db
			);
		}
		$this->lazyLoadDb = $dbw;
	}

	public function getLazyDb() {
		global $appContext;
		return $this->lazyLoadDb instanceof Dbw ?
			$this->lazyLoadDb :
			$appContext->dbw;
	}

	/**
	 * note: optional; required only if one wants to calculate number of pages in the UI
	 */
	protected function setPagesCountQuery( $stmt ) {
		$this->pagesCountQuery = $stmt;
	}

	/**
	 * @param $dbargs
	 *   array of dbtokens with optional DbwParams instance as last element
	 */
	public function setPagerQuery( /* dbargs */ ) {
		$dbargs = func_get_args();
		if ( !( ( $replacements = array_pop( $dbargs ) ) instanceof DbwParams ) ) {
			# last element is not replacements, push it back
			array_push( $dbargs, $replacements );
			# use empty replacements instance
			$replacements = new DbwParams();
		}
		$replacements->offsetSetNew( 'offset', $this->offset );
		# +1 to detect whether there is next page or not
		$replacements->offsetSetNew( 'limit', $this->limit + 1 );
		$this->pagerArgs = array_merge( $dbargs, array(
			'LIMIT', '!offset', '!limit', $replacements ) );
		$this->pagerArgsReplacements = $replacements;
	}

	/**
	 * @return int
	 *   count of pages in current product pager for the current pager's limit.
	 * todo: create maintenance tests, currently is unchecked.
	 */
	public function getPagesCount() {
		$result = $this->db->raw( $this->pagesCountQuery );
		# Dbg\log(__METHOD__,$result);
		return ceil( $result->count_id / $this->limit );
	}

	protected function getCurrRows() {
		if ( !isset( $this->pagerArgsReplacements ) ) {
			SdvException::throwError(
				'::setPagerQuery() was not called on supplied instance or the instance was improperly overwritten',
				__METHOD__,
				$this
			);
		}
		$this->pagerArgsReplacements->offsetSet( 'offset', $this->offset );
		$this->pagerArgsReplacements->offsetSet( 'limit', $this->limit + 1 );
		# Dbg\log(__METHOD__,$this->pagerArgs);
		$this->rows = call_user_func_array( array( $this->db, 'query' ), $this->pagerArgs );
	}

} /* end of DbPager class */
