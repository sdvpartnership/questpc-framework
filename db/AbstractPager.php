<?php
namespace QuestPC;

/**
 * A pager whose descendants can implement arbitrary paging
 * (eg. via Dbw class, but not limited to).
 */
abstract class AbstractPager extends FeaturesObject {

	# default count of rows per page (LIMIT)
	const DEFAULT_LIMIT = 12;

	protected $offset = 0;
	protected $limit = self::DEFAULT_LIMIT;
	protected $count;
	protected $hasNextPage = false;

	// warning: is not updated after query, has to be set manually
	protected $pageNum = 1;

	// resulting rows
	public $rows;
	// rows postprocessing hook
	public $postprocessRowsHook;
	// resulting model collection (optional, used only when set)
	protected $collection;
	// whether all tables should be loaded after pkTable data populated
	// into ->collection
	protected $loadAllTables = false;

	function __construct(
		$collection = null,
		$loadAllTables = false,
		$limit = null,
		$offset = 0
	) {
		if ( $collection instanceof AbstractModelCollection ) {
			$this->collection = $collection;
		}
		$this->loadAllTables = $loadAllTables === 'loadAllTables';
		$this->setOffset( $offset );
		if ( $limit !== null ) {
			$this->setLimit( $limit );
		}
	}

	public function setOffset( $offset ) {
		$this->offset = $offset;
	}

	public function setLimit( $limit ) {
		$this->limit = $limit;
	}

	public function getPageNum() {
		return $this->pageNum;
	}

	/**
	 * Set current page number but do not fetch the page.
	 * @param $pageNum int
	 *   page number (1..n)
	 */
	public function setPageNum( $pageNum ) {
		$pageNum = intval( $pageNum );
		if ( $pageNum < 1 ) {
			SdvException::throwRecoverable( 'Pages should be counted from 1', __METHOD__, $pageNum );
		}
		$this->pageNum = $pageNum;
		$this->setOffset( ( $pageNum - 1 ) * $this->limit );
	}

	public function getCount() {
		return count( $this->rows );
	}

	public function getRows() {
		return $this->rows;
	}

	public function getCollection() {
		return $this->collection;
	}

	/**
	 * Fetch arbitrary pager page.
	 * @param $pageNum int
	 *   page number (1..n)
	 * @return array
	 *   keys: product_id
	 *   values: stdClass with product decription (see above)
	 */
	public function getPage( $pageNum ) {
		$this->setPageNum( $pageNum );
		return $this->getCurrPage();
	}

	protected function getCurrRows() {
		$this->rows = array();
	}

	/**
	 * Fetch current page. Moves DB cursor to next page or to the end of dataset.
	 * @return mixed
	 *   array next rows of data for ->pagerQuery
	 *   instanceof AbstractModelCollection when ->collection was set
	 */
	public function getCurrPage() {
		$this->getCurrRows();
		$this->hasNextPage = (count( $this->rows ) > $this->limit);
		if ( $this->hasNextPage ) {
			array_pop( $this->rows );
		}
		# Optional arbitrary transformation of rows. Please keep number of rows intact,
		# change fields only. Maybe used to query more than one DBMS at once.
		# For example, Agrippa uses ->postprocessRowsHook to query MySql after
		# SphinxSearch query.
		if ( isset( $this->postprocessRowsHook ) ) {
			$this->rows = call_user_func( $this->postprocessRowsHook, $this->rows );
		}
		$this->count = count( $this->rows );
		$this->offset += $this->count;
		if ( isset( $this->collection ) ) {
			$this->collection->clear();
			$this->collection->loadPkTableRows( $this->rows );
			if ( $this->loadAllTables ) {
				$this->collection->loadAllByPkTable();
			}
			return $this->collection;
		}
		return $this->rows;
	}

	/**
	 * Fetch previous page.
	 * @return array
	 *   previous rows of data for ->pagerQuery
	 */
	public function getPrevPage() {
		# delta to begin of last offset
		$delta = ( $this->count === null ) ?
			$this->limit : $this->count;
		# go to last offset - limit: that is offset to previous page
		if ( ($this->offset -= ($delta + $this->limit) ) < 0 ) {
			$this->offset = 0;
			return array();
		}
		$this->hasNextPage = true;
		return $this->getCurrPage();
	}

	public function hasNextPage() {
		return $this->hasNextPage;
	}

} /* end of AbstractPager class */
