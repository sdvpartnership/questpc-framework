<?php
namespace QuestPC;

/**
 * DbPager which results are associated with specified CategoryTree instance.
 */
class CategoryPager extends DbPager {

	protected $categoryTree;
	protected $limit = 40;

	public function setCategoryTree( CategoryTree $categoryTree ) {
		$this->categoryTree = $categoryTree;
	}

	public function getCategoryTree() {
		return $this->categoryTree;
	}

} /* end of CategoryPager class */
