<?php
namespace QuestPC;

class SchemaCreator extends FeaturesObject {

	protected $db;	
	protected $dropTables = false;
	protected $updateSchema = false;

	function __construct( Dbw $dbw ) {
		$this->db = $dbw;
	}

	function setDropTables( $dropTables ) {
		$this->dropTables = (bool) $dropTables;
	}

	function setUpdateSchema( $updateSchema ) {
		$this->updateSchema = (bool) $updateSchema;
	}

	private $queryArgs = array(); // part of escapeArgCB
	private function escapeArgCB( $matches ) {
		$matches = $matches[0];
		// match quote as SQL data (replace with quoted query arg)
		if ( preg_match( '`^\?(.+?)\?$`', $matches, $varname ) ) {
			$varname = $varname[1];
			if ( !array_key_exists( $varname, $this->queryArgs ) ) {
				return $matches;
			}
			return $this->db->quoteData( $this->queryArgs[$varname] );
		}
		// match quote as SQL table name (no query args)
		if ( preg_match( '`^\^(.+?)\^$`', $matches, $varname ) ) {
			$varname = $varname[1];
			return $this->db->getTableName( $varname );
		}
		// match quote as field length (replace with integer query arg)
		if ( preg_match( '`^\!(.+?)\!$`', $matches, $varname ) ) {
			$varname = $varname[1];
			if ( !array_key_exists( $varname, $this->queryArgs ) ) {
				SdvException::throwFatal( 'No parametric value for definition',
					__METHOD__,
					array(
						'varname' => $varname,
						'queryArgs' => $this->queryArgs,
					)
				);
			}
			return intval( $this->queryArgs[$varname] );
		}
		// match quote as IN clause (replace with list of values from array)
		if ( preg_match( '`^\$(.+?)\$$`', $matches, $varname ) ) {
			$varname = $varname[1];
			return $this->db->getSingleRowInClause( $varname, $this->queryArgs[$varname] );
		}
		SdvException::throwFatal( 'Unhandled match', __METHOD__, $matches );
	}

	/**
	 * todo: rewrite the Schema to use new Dbw::prepareQuery() instead.
	 * note: also will require to check Schema methods and access to Schema
	 * properties all through the code, eg. AbstractModelCollection class.
	 */
	protected function oldPrepareQuery( $stmt, $args = null ) {
		if ( $args === null ) {
			$this->queryArgs = array();
		} else {
			if ( !is_array( $args ) ) {
				SdvException::throwFatal( 'args is not an array', __METHOD__, $args );
			}
			$this->queryArgs = $args;
		}
		try {
			$stmt = preg_replace_callback(
				'`((?:\?|\^|\!|\$).+?(?:\?|\^|\!|\$))`',
				array( $this, 'escapeArgCB' ),
				$stmt
			);
		} catch ( SdvException $e ) {
			$data = $e->getData();
			$data['stmt'] = $stmt;
			$e->setData( $data );
			throw $e;
		}
		return $stmt;
	}

	/**
	 * Replace arguments in schema field definition, if there are any.
	 */
	protected function replaceFieldArgs( array &$fieldDef ) {
		$replace = '';
		$replacements = array();
		foreach ( $fieldDef as $key => $def ) {
			if ( is_int( $key ) ) {
				# field definition
				if ( $replace !== '' ) {
					SdvException::throwFatal( 'Multiple field definitions (multiple integer keys) are not allowed', __METHOD__, $fieldDef );
				}
				$replace = $def;
			} else {
				# query parameter for the current field definition
				$replacements[$key] = $def;
			}
		}
		$fieldDef = $this->oldPrepareQuery( $replace, $replacements );
	}

	function createTables( $tableList ) {
		$hasWarnings = false;
		if ( $this->dropTables ) {
			$dropList = implode( ',', array_map( array( $this->db, 'getTableName' ), array_keys( $tableList ) ) );
			$this->db->raw( "DROP TABLE IF EXISTS {$dropList}" );
			# Dbg\log(__METHOD__,"DROP TABLE IF EXISTS {$dropList}");
		}
		$dbMethod = $this->updateSchema ? 'updateTable' : 'createTable';
		foreach( $tableList as $tableName => $tableData ) {
			$schemaDef = new \stdClass();
			foreach ( $tableData[Schema::FIELD_DEF] as &$fieldDef ) {
				if ( is_array( $fieldDef ) ) {
					$this->replaceFieldArgs( $fieldDef );
				}
			}
			# Add table name to Dbw::createTable() arguments
			$schemaDef->tableName = $tableName;
			$schemaDef->columns = $tableData[0];
			$schemaDef->sql_indexes = $tableData[1];
			$schemaDef->engine = $tableData[2];
			$result = call_user_func( array( $this->db, $dbMethod ), $schemaDef );
			if ( $result === true ) {
				echo "Created {$schemaDef->tableName}\n";
			} elseif ( is_array( $result ) ) {
				$hasWarnings = true;
				echo "Existing and provided create statements do not match.\n";
				echo "Please review and update schema manually first.\n";
				foreach ( $result as $key => $val ) {
					echo "{$key}: {$val}\n";
				}
				echo "\n";
			}
		}
		if ( !$hasWarnings ) {
			echo "Success.\n\n";
		}
	}

} /* end of SchemaCreator class */
