<?php
namespace QuestPC;

/**
 * Sample ->tableList definition:
	array(
 		'article_info' => array(
			array(
				'article_id' => 'int unsigned NOT NULL AUTO_INCREMENT',
				# uri part (latin-only)
				'pagename' => array( 'mb_min_len' => 1, 'mb_max_len' => 255, 'varbinary(!mb_max_len!) NOT NULL' ),
				# html/title/text() (may be non-latin)
				'title' => array( 'mb_min_len' => 1, 'mb_max_len' => 255, 'varchar(!mb_max_len!) NOT NULL' ),
				'creation' => 'datetime NOT NULL',
				'excerpt' => array( 'mb_min_len' => 1, 'mb_max_len' => 255, 'varchar(!mb_max_len!) NOT NULL' ),
			),
			array(
				'PRIMARY KEY (`article_id`)',
				'UNIQUE KEY `pagename` (`pagename`)',
				'KEY `title` (`title`)',
				'KEY `creation` (`creation`)'
			)
		),
		'article_text' => array(
			array(
				'article_id' => 'int unsigned NOT NULL',
				'content' => 'mediumtext NOT NULL',
			),
			array(
				'PRIMARY KEY (`article_id`)',
			)
		),
		# publication category_id linked to article
		'article_category' => array(
			array(
				'article_id' => 'int unsigned NOT NULL',
				'pub_category_id' => 'int unsigned NOT NULL',
			),
			array(
				'PRIMARY KEY (`article_id`,`pub_category_id`)',
			)
		),
	)
 */

class Schema extends FeaturesObject {

	const DEFAULT_DB_ENGINE = 'InnoDB';

	# keys in schema table array;
	# may be used to dynamically generate table fields / indexes
	const FIELD_DEF = 0;
	const INDEX_DEF = 1;
	const ENGINE_DEF = 2;

	protected static $isSingleton = true;

	protected $tableList;
	# Quick access to table fields properties.
	protected $fieldProp = array();

	function __construct( $tableList ) {
		$this->tableList = $tableList;
		foreach ( $this->tableList as $tableName => &$table ) {
			foreach ( $table[self::FIELD_DEF] as $fieldName => &$fieldDef ) {
				if ( is_array( $fieldDef ) ) {
					# Store field props which are often requested by models and / or parsers.
					foreach ( $fieldDef as $key => $val ) {
						if ( is_string( $key ) ) {
							$this->fieldProp["{$tableName}.{$fieldName}.{$key}"] = $val;
						}
					}
				}
			}
			# Set default DB engine for every table.
			if ( !array_key_exists( self::ENGINE_DEF, $table ) ) {
				$table[self::ENGINE_DEF] = self::DEFAULT_DB_ENGINE;
			}
		}
		# Dbg\log(__METHOD__.':fieldProp',$this->fieldProp);
	}

	/**
	 * Find out, whether the specified tableName.fieldName is a primary key.
	 * todo: expand to support set of field names in array as a primary key.
	 * @param $tableName string
	 * @param $fieldName string
	 * @return boolean
	 *   true: specified field is PK of the table, false otherwise
	 */
	public function fieldIsPK( $tableName, $fieldName ) {
		if ( !array_key_exists( $tableName, $this->tableList ) ) {
			SdvException::throwError( 'Specified table is not the part of current schema',
				__METHOD__,
				$tableName
			);
		}
		# Find out, whether the $fieldName field of the current table is PK or not.
		foreach ( $this->tableList[$tableName][Schema::INDEX_DEF] as $indexDef ) {
			if ( preg_match(
					'/^\s*PRIMARY\s+KEY.+?\(\s*(?:`|)' .
				preg_quote( $fieldName, '/' ) .
				'(?:`|)\s*\)\s*$/i',
				$indexDef ) ) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Get number of fields in table.
	 * @param $tableName
	 *   table name that is a part of schema;
	 * @return int
	 *   count of fields for specified table name;
	 *   0, when table name does not belong to schema;
	 */
	public function fieldsCount( $tableName ) {
		return array_key_exists( $tableName, $this->tableList ) ?
			count( $this->tableList[$tableName][Schema::FIELD_DEF] ) : 0;
	}

	/**
	 * @param $fieldPropName
	 *   string name of schema table field property (eg. 'mb_length', 'mb_min_len', 'length');
	 * @throws SdvException when property does not exists;
	 */
	public function getFieldProp( $fieldPropName ) {
		if ( array_key_exists( $fieldPropName, $this->fieldProp ) ) {
			return $this->fieldProp[$fieldPropName];
		}
		SdvException::throwError( 'Unknown field property name', __METHOD__, $fieldPropName );
	}

	public function getFieldProps( /* $fieldPropNames */ ) {
		$result = array();
		$fieldPropNames = func_get_args();
		foreach ( $fieldPropNames as $fieldPropName ) {
			if ( array_key_exists( $fieldPropName, $this->fieldProp ) ) {
				$result[] = $this->fieldProp[$fieldPropName];
			} else {
				SdvException::throwError( 'Unknown field property name', __METHOD__, $fieldPropName );
			}
		}
		return $result;
	}
	
} /* end of Schema class */
