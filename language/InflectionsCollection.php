<?php
namespace QuestPC;

class InflectionsCollection {

	protected $currentInstance;
	protected $instances;

	function __construct( /* args */ ) {
		$args = func_get_args();
		$this->instances = new FeaturesObject();
		foreach ( $args as $className ) {
			$this->addClass( $className );
		}
	}

	public function addClass( $className ) {
		if ( !$this->instances->hasLazy( $className ) ) {
			$fqnClassName = AutoLoader::getFqnClassName( $className );
			$this->instances->lazyLoad( $className, function() use( $fqnClassName ) {
				$reflect = new \ReflectionClass( $fqnClassName );
				$instance = $reflect->newInstance();
				return ($instance instanceof Inflections) ? $instance : new \stdClass();
			} );
		}
	}

	public function removeClass( $className ) {
		if ( $this->instances->hasLazy( $className ) ) {
			$this->instances->deleteLazy( $className );
		}
	}

	public function removeAllClasses() {
		foreach ( $this->instances->getLazyList() as $className ) {
			$this->instances->deleteLazy( $className );
		}
	}

	public function useClass( $className = null ) {
		$this->currentInstance = ( $className === null ) ?
			null : $this->instances->{$className};
	}

	public function findInflection( $word, $inflectionType ) {
		if ( $this->currentInstance === null ) {
			foreach ( $this->instances->getLazyList() as $className ) {
				$inflection = $this->instances
					->{$className}
					->findInflection( $word, $inflectionType );
				if ( $inflection !== false ) {
					return $inflection;
				}
			}
		} else {
			$inflection = $this->currentInstance->findInflection( $word, $inflectionType );
			if ( $inflection !== false ) {
				return $inflection;
			}
		}
		return false;
	}

	public function getInflection( $word, $inflectionType ) {
		$inflection = $this->findInflection( $word, $inflectionType );
		return ($inflection === false) ? $word : $inflection;
	}

	public function getUcInflection( $word, $inflectionType ) {
		$inflection = $this->findInflection( $word, $inflectionType );
		return ( $inflection === false ) ? $word : Gl::ucfirst( $inflection );
	}

} /* end of InflectionsCollection class */
