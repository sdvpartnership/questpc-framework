<?php
namespace QuestPC;

/**
 * Used for local language inflections as well as for translations from
 * system language (usually from English).
 */
abstract class Inflections {

	/**
	 * Defined inflections types.
	 *
	 * Declensions (склонения)
	 */
	/* именительный падеж (кто? что?) */
	const NOMINATIVE = 1;
	/* родительный падеж (кого? чего?) */
	const GENITIVE = 2;
	/* дательный падеж (кому? чему?) */
	const DATIVE = 3;
	/* винительный падеж (кого? что?) */
	const ACCUSATIVE = 4;
	/* творительный падеж (кем? чем?) */
	const INSTRUMENTAL = 5;
	/* предложный падеж (о ком? о чём?) */
	const PREPOSITIONAL = 6;
	/**
	 * Declension translations. (перевод со склонениями)
	 */
	const LOCAL_NOMINATIVE = 7;
	const LOCAL_GENITIVE = 8;
	const LOCAL_DATIVE = 9;
	const LOCAL_ACCUSATIVE = 10;
	const LOCAL_INSTRUMENTAL = 11;
	const LOCAL_PREPOSITIONAL = 12;
	/**
	 * Conjugation (спряжение)
	 * @todo: define when needed.
	 */

	/**
	 * Must be defined as:
	 *	'Москва' => array(
	 *		self::PREPOSITIONAL => 'Москве',
	 *		self::ACCUSATIVE => 'Москву',
	 *	),
	 * @note: First char must be uppercased only for names / city names etc.
	 * @note: To uppercase, use InflectionsCollection::getUcInflection().
	 */
	protected $words;

	public function findInflection( $word, $inflectionType ) {
		if ( !array_key_exists( $word, $this->words ) ) {
			return false;
		}
		return array_key_exists( $inflectionType, $this->words[$word] ) ?
			$this->words[$word][$inflectionType] : false;
	}

} /* end of Inflections class */
