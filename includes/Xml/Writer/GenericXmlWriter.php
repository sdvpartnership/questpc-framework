<?php
namespace QuestPC;

class GenericXmlWriter extends \XMLWriter {

	public static function openDocument( $uri, $indent = true ) {
		$self = new static();
		$self->openURI( $uri );
		$self->setIndent( $indent );
		$self->startDocument( '1.0', 'UTF-8' );
		return $self;
	}

	public function writeArray( $subtree ) {
		$this->_writeArray( $subtree );
	}

	public function startTagArray( array $subtree ) {
		$this->_startTagArray( $subtree );
	}

	protected function _startTagArray( array &$subtree ) {
		# element with possible nested subtree
		$this->startElement( $subtree['@tag'] );
		# first pass: write element attributes, if any
		foreach ( $subtree as $key => &$val ) {
			# check for non-special attribute
			if ( !is_int( $key ) && substr( $key, 0, 1 ) !== '@' ) {
				$this->writeAttribute( $key, $val );
			}
		}
	}

	protected function _writeArray( &$subtree ) {
		if ( is_scalar( $subtree ) ) {
			# text node
			$this->text( $subtree );
		} elseif ( array_key_exists( '@tag', $subtree ) ) {
			$this->_startTagArray( $subtree );
			# second pass: write subelements, if any
			$hasSubelements = false;
			foreach ( $subtree as $key => &$val ) {
				if ( is_int( $key ) ) {
					$hasSubelements = true;
					$this->_writeArray( $subtree[$key] );
				}
			}
			if ( $hasSubelements ) {
				# Force end element even for empty inner text.
				$this->fullEndElement( /* $subtree['@tag'] */ );
			} else {
				# Self-close tag when there is no empty inner text at all.
				$this->endElement( /* $subtree['@tag'] */ );
			}
		} elseif ( array_key_exists( '#comment', $subtree ) ) {
			$this->writeComment( $subtree['#comment'] );
		} elseif ( array_key_exists( '#cdata', $subtree ) ) {
			$this->writeCData( $subtree['#cdata'] );
		} else {
			# unnamed list of nodes
			foreach ( $subtree as $key => &$val ) {
				# Only integer keys are belonging to the list of nodes,
				# non-integer keys may be only special attribute:
				# (eg. optional '@len').
				if ( is_int( $key ) ) {
					$this->_writeArray( $val );
				}
			}
		}
	}

	public function flushDocument() {
		$this->endDocument();
		$this->flush( true );
	}

} /* end of GenericXmlWriter class */
