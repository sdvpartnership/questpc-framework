<?php
namespace QuestPC;

abstract class XmlCollectionWriter extends GenericXmlWriter {

	/**
	 * @param $strings array
	 *   key is variable name, text is value
	 *   note: this is NOT tagarray, but is supposed to be converted into tagarray
	 */
	public function writeHeader( array $strings ) {
		/* noop */
	}

	public function writeCollection( AbstractModelCollection $collection ) {
		foreach ( $collection as $model ) {
			$this->writeItemTags( $model );
		}
	}

} /* end of XmlCollectionWriter class */
