<?php
namespace QuestPC;

class RssCollectionWriter extends XmlCollectionWriter {

	public static function openDocument( $uri, $indent = true ) {
		$self = parent::openDocument( $uri, $indent );
		$self->startElement( 'rss' );
		$self->writeAttribute( 'version', '2.0' );
		$self->startElement( 'channel' );
		return $self;
	}

	/**
	 * @param $strings array
	 *   key is variable name, text is value
	 *   note: this is NOT tagarray, but is supposed to be converted into tagarray
	 */
	public function writeHeader( array $strings ) {
		$subtree = array();
		foreach ( $strings as $key => $val ) {
			$subtree[] = array( '@tag' => $key, $val );
		}
		$subtree[] = array( '@tag' => 'lastBuildDate', date( 'r' ) );
		$this->writeArray( $subtree );
	}
	
	public function writeItemTags( AbstractModel $model ) {
		$this->writeArray( $model->getRssItemArray() );
	}

	public function flushDocument() {
		$this->endElement( /* 'channel' */ );
		$this->endElement( /* 'rss' */ );
		parent::flushDocument();
	}

} /* end of RssCollectionWriter class */
