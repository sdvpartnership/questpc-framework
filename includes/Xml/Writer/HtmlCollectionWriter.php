<?php
namespace QuestPC;

class HtmlCollectionWriter extends XmlCollectionWriter {

	public static function openDocument( $uri, $indent = true ) {
		$self = parent::openDocument( $uri, $indent );
		$self->writeDTD( 'html', '-//W3C//DTD XHTML 1.0 Transitional//EN', 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd' );
		$self->startTagArray( array(
			'@tag' => 'html',
			'xmlns' => 'http://www.w3.org/1999/xhtml',
			'lang' => 'ru',
			'dir' => 'ltr',
			'xmlns' => 'http://www.w3.org/1999/xhtml',
			'xml:lang' => 'ru'
		) );
		return $self;
	}

	/**
	 * @param $strings array
	 *   key is variable name, text is value
	 *   note: this is NOT tagarray, but is supposed to be converted into tagarray
	 */
	public function writeHeader( array $strings ) {
		$subtree = array(
			array( '@tag' => 'meta',
				'http-equiv' => 'content-type',
				'content' => 'text/html; charset=utf-8'
			),
			array( '@tag' => 'title',
				$strings['title'] . ' - ' . date( 'd.m.Y' )
			)
		);
		$this->writeArray( $subtree );
		$this->startElement( 'body' );
		$this->writeArray( array(
			'@tag' => 'h1', $strings['description']
		) );
	}
	
	public function writeItemTags( AbstractModel $model ) {
		$this->writeArray( $model->getHtmlViewArray() );
	}

	public function flushDocument() {
		$this->endElement( /* 'body' */ );
		$this->endElement( /* 'html' */ );
		parent::flushDocument();
	}

} /* end of HtmlCollectionWriter class */
