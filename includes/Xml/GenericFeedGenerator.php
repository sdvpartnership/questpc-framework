<?php
namespace QuestPC;

class GenericFeedGenerator {

	protected $pager;
	protected $xmlw;
	protected $stopOnPendingDelete;
	protected $sampleCollection;

	public function __construct( 
			DbPager $pager,
			XmlCollectionWriter $xmlw,
			$mode = true ) {
		$this->pager = $pager;
		$this->xmlw = $xmlw;
		$this->stopOnPendingDelete = (boolean) $mode;
	}

	public function getSampleCollection() {
		return $this->sampleCollection;
	}

	public function execute( $maxItemCount, $getItemsCount = 0 ) {
		$this->pager->setOffset( 0 );
		$itemCount = 0;
		$reflect = new \ReflectionClass( get_class( $this->pager->getCollection() ) );
		$dstCollection = $reflect->newInstance();
		$this->sampleCollection = $reflect->newInstance();
		$staying = true;
		do {
			$srcCollection = $this->pager->getCurrPage();
			if ( count( $srcCollection ) === 0 ) {
				break;
			}
			foreach ( $srcCollection as $model ) {
				if ( $itemCount >= $maxItemCount ) {
					break;
				}
				if ( $this->stopOnPendingDelete && $model->isPendingDelete() ) {
					Dbg\log(__METHOD__.':stopped due to ->isPendingDelete()',$model);
					$staying = false;
					break;
				}
				$dstCollection->addModel( $model );
				if ( $getItemsCount-- > 0 ) {
					$this->sampleCollection->addModel( $model );
				}
				$itemCount++;
			}
			$this->xmlw->writeCollection( $dstCollection );
			$dstCollection->clear();
		} while ( $staying && $itemCount < $maxItemCount );
		$this->xmlw->flushDocument();
		return $itemCount;
	}

} /* end of GenericFeedGenerator class */
