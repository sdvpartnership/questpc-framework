<?php
namespace QuestPC;

/**
 * DOMNodeList with the possibility of adding new nodes.
 */
class XmlNodeList extends \SplDoublyLinkedList {

	/**
	 * Creates new XmlNodeList from subpart of already existing XmlNodeList.
	 */
	static function newFromSplit( XmlNodeList $list, $startOffset, $endOffset ) {
		$newList = new static();
		$i = 0;
		foreach ( $list as $elem ) {
			if ( $i >= $endOffset ) {
				break;
			}
			if ( $i >= $startOffset ) {
				$newList->pushNode( $elem );
			}
			$i++;
		}
		return $newList;
	}

	static function newFromNode( \DOMNode $node ) {
		$instance = new static();
		$instance->pushNode( $node );
		return $instance;
	}

	function pushNode( \DOMNode $node ) {
		parent::push( $node );
	}

	/**
	 * Slower push() with typecheck.
	 * Required because push() typehinting causes E_STRICT warning.
	 * When $node is guaranteed to be valid, use parent::push() instead.
	 */
	function push( $node ) {
		if ( $node instanceof \DOMNode ) {
			parent::push( $node );
		} else {
			SdvException::throwError( 'node should be instanceof \\DOMNode', __METHOD__, $node );
		}
	}

	function pushDOMNodeList( \DOMNodeList $list ) {
		foreach ( $list as $elem ) {
			parent::push( $elem );
		}
	}

	function pushXmlNodeList( XmlNodeList $list ) {
		foreach ( $list as $elem ) {
			parent::push( $elem );
		}
	}

} /* end of XmlNodeList class */

/**
 * DOM wrapper with extensive support of list chaining (where it is possible).
 *   DOMDocument tree of DOMElement nodes.
 *   DOM-like _virtual_ id's (vid) for the lists of DOM nodes
 *     (not just single node).
 *   Optional possibility to load and to store tree into strings and files in
 *     various formats: xml, html, json.
 * Note: libxml_clear_errors() call is performed after load / save operations
 * to minimize memory leaks.
 */
class XmlTree {

	protected $allContentTypes = array(
		'xml',
		'html',
		'json'
	);

	protected $xmlContentTypes = array(
		'xml',
		'html'
	);

	# root DOMDocument element
	protected $tree;
	# root XmlNodeList with single element: ->tree
	protected $treeList;

	# current xml tree filename (if any)
	protected $fileName;

	# stdClass
	#   Each property name is vid
	#   Each value is reference to XmlNodeList
	# Similar to DOM element.id, used to quickly select previouselty built list of nodes
	# not having to traverse the whole tree.
	# The vid's are virtual because they aren't written into resulting XML.
	protected $vidRefs;

	# Name of current vid; either string id or null (root of the tree).
	# String '' means that current list is unnamed (available only as
	# the "last selected one" by ->currList until next ->setId() call).
	protected $currVid = null;

	# Refererence to last ->setId() XmlNodeList instance in ->vidRefs.
	# Element manipulation methods are using this reference to
	# add attributes / child nodes etc.
	#
	# ->currVid is corresponding vid name.
	# Default value null is _invalid_.
	protected $currList = null;

	/**
	 * @param $type string
	 *   'xml' / 'html' / 'json'
	 * @param $defaultContent mixed
	 *   xml/html/json string / DOMDocument / array
	 */
	public function __construct( $type = 'xml', $defaultContent = null ) {
		$this->init( $type, $defaultContent );
	}

	protected function tryCheckContentType( $type, $typeList ) {
		if ( !in_array( $type, $this->{$typeList}, true ) ) {
			SdvException::throwFatal( 'Invalid type of content', __METHOD__, $type );
		}
	}

	/**
	 * Init the tree with the default content
	 * @param $defaultContent mixed
	 *   null / DOMDocument / string (xml/html/encoded json) / array (decoded json)
	 * @param $type string
	 *   type of $defaultContent for string type of content
	 */
	public function init( $type = 'xml', $defaultContent = null ) {
		$this->tryCheckContentType( $type, 'allContentTypes' );
		$this->reset();
		if ( $defaultContent instanceof \DOMDocument ) {
			$this->tree = $defaultContent;
		} elseif ( is_array( $defaultContent ) ) {
			$this->replaceArray( $defaultContent );
		} elseif ( is_string( $defaultContent ) ) {
			# load content depending on $type
			if ( $type === 'xml' ) {
				# note: if you are getting errors, make sure you're not overwritting
				# built-in dom library with external one;
				# there should be no extension=php_domxml.dll in php.ini
				# strip pretty formatting (SIGNIFICANT_WHITESPACE nodes) on load
				# Dbg\log('this->tree->loadXML',$defaultContent);
				$this->tree->loadXML( $defaultContent, LIBXML_NOBLANKS );
				libxml_clear_errors();
				# Dbg\log('result of loadXML',$this->getArray());
			} elseif ( $type === 'html' ) {
				@$this->tree->loadHTML( $defaultContent );
				libxml_clear_errors();
			} elseif ( $type === 'json' ) {
				$this->loadJSON( $defaultContent );
			}
		} else {
			$this->reset();
		}
		# chaining
		return $this;
	}

	/**
	 * Create an instance from file.
	 * @param $fileName string
	 *   name of xml file to load into this tree
	 * @param $defaultContent mixed null / string / SimpleXMLElement
	 *   optional default content to put into tree when the file does not exists
	 */
	public static function newFromFile( $type = 'xml', $fileName, $defaultContent = null ) {
		$instance = new static();
		if ( $type === 'xml' || $type === 'html' ) {
			$instance->loadXMLHTML( $type, $fileName, $defaultContent );
		} elseif ( $type === 'json' ) {
			$instance->loadJSON( $fileName, $defaultContent );
		}
		return $instance;
	}

	/**
	 * Get default save filename.
	 */
	public function getFileName() {
		return $this->fileName;
	}

	/**
	 * Set default save filename.
	 */
	public function setFileName( $fileName ) {
		$this->fileName = $fileName;
	}

	/**
	 * Load xml or html from file.
	 * @param $type string
	 *   'xml' / 'html'
	 * @param $fileName string
	 *   name of xml file to load into this tree
	 * @param $defaultContent mixed null / string / SimpleXMLElement
	 *   optional default xml content to put into tree when the file does not exists
	 * @param $forceDefault boolean, default false
	 *   true: use $xml content for invalid non-xml files instead of throwing an exception
	 */
	public function loadXMLHTML( $type = 'xml', $fileName, $defaultContent = null, $forceDefault = false ) {
		$this->tryCheckContentType( $type, 'xmlContentTypes' );
		$this->reset();
		$loadResult = false;
		if ( $fExists = file_exists( $fileName ) ) {
			# strip pretty formatting (SIGNIFICANT_WHITESPACE nodes) on load
			$loadResult = ($type === 'html') ?
				@$this->tree->loadHTMLFile( $fileName ) :
				$this->tree->load( $fileName, LIBXML_NOBLANKS );
			libxml_clear_errors();
		}
		if ( $loadResult === false ) {
			# failed to load xml from file
			if ( $fExists ) {
				# non-xml file
				if ( $forceDefault ) {
					# force to load from $defaultContent
					if ( $defaultContent !== null ) { $this->init( $type, $defaultContent ); }
				} else {
					# bail out
					SdvException::throwFatal( 'An attempt to load non-xml file', __METHOD__, $fileName );
				}
			} else {
				# file does not exists, try to load from $defaultContent
				if ( $defaultContent !== null ) { $this->init( $type, $defaultContent ); }
			}
			if ( $defaultContent === null ) {
				SdvException::throwFatal( 'Neither file exists nor default xml content specified', __METHOD__,
					$fileName
				);
			}
		}
		$this->setFileName( $fileName );
		# chaining
		return $this;
	}

	protected function reset() {
		$this->tree = new \DOMDocument( '1.0', 'UTF-8' );
		$this->tree->preserveWhiteSpace = false;
		# Make sure that default ->documentElement exists,
		# otherwise ->map_replacearray() may fail on tree root.
		$this->tree->appendChild( $this->tree->createElement( 'xml' ) );
		$this->vidRefs = new \stdClass();
		$this->currVid = null;
		$this->treeList = XmlNodeList::newFromNode( $this->tree );
		$this->setCurrList( $this->treeList );
		$this->mmParams = new \stdClass();
	}

	/**
	 * Set ->currList.
	 * todo: implement a stack of previous ->currList similar to jQuery.
	 */
	protected function setCurrList( XmlNodeList $list ) {
		$this->currList = $list;
	}

	/**
	 * Add already created list to ->vidRefs with new vid.
	 * Also selects this vid as last used one.
	 * note: There is no check, whether elements of list whose vid is set,
	 * actually are a childrens of current tree. Be careful.
	 */
	protected function addVid( XmlNodeList $list, $vid = '' ) {
		if ( is_string( $vid ) ) {
			$this->currVid = $vid;
			$this->setCurrList( $list );
			if ( $vid !== '' ) {
				$this->vidRefs->{$vid} = $list;
			}
		} else {
			SdvException::throwFatal( 'Invalid vid', __METHOD__, $vid );
		}
	}

	/**
	 * Get amount of elements in ->currList
	 */
	public function count() {
		return $this->currList->count();
	}

	/**
	 * Change vid of current list (eg. make nameless list named one).
	 */
	public function setVid( $vid = '' ) {
		$this->addVid( $this->currList, $vid );
		# Chaining
		return $this;
	}

	/**
	 * Select sublist of current list as new current list.
	 * Something like jQuery gt / lt.
	 */
	public function lim( $vid = '', $count = 1, $offset = 0 ) {
		$newList = XmlNodeList::newFromSplit( $this->currList, $offset, $offset + $count );
		$this->addVid( $newList, $vid );
	}

	/**
	 * Select parents of current list elements as new current list.
	 * @param $vid string
	 *   named / anonymous ('') vid for new current list
	 */
	public function up( $vid = '' ) {
		$newList = new XmlNodeList();
		foreach ( $this->currList as $elem ) {
			if ( $elem instanceof \DOMDocument ) {
				# skipping: has no parent
				continue;
			}
			$newList->pushNode( $elem->parentNode );
		}
		$this->addVid( $newList, $vid );
		# Chaining
		return $this;
	}

	/**
	 * Clear previouseley collected list by it's vid.
	 * note: cannot clear tree vid and / or current vid.
	 */
	public function clearVid( $vid ) {
		if ( $this->currVid !== $vid &&
				is_string( $vid ) &&
				$vid !== '' &&
				property_exists( $this->vidRefs, $vid ) ) {
			unset( $this->vidRefs->{$vid} );
		}
	}

	/**
	 * Select new current element by already existing vid.
	 * @param $vid mixed string / null
	 *   null to set root;
	 *   ''   to make current element nameless.
	 */
	public function byVid( $vid = null ) {
		# set currList to corresponding vid (if any)
		if ( $vid === null ) {
			# root of the tree
			$this->setCurrList( $this->treeList );
		} elseif ( $vid === '' ) {
			/* noop: will clear currVid */
		} elseif ( property_exists( $this->vidRefs, $vid ) ) {
			# existing vid
			$this->setCurrList( $this->vidRefs->{$vid} );
		} else {
			# non-existant vid: empty list
			$this->setCurrList( new XmlNodeList() );
		}
		$this->currVid = $vid;
		# Chaining
		return $this;
	}

	/**
	 * Get DOM node from the current list by $idx. No chaining.
	 @ @param $idx mixed
	 *   integer idx of XmlNodeList;
	 *   null to return the whole set;
	 * @return mixed
	 *   DOMDocument / DOMElement / XmlNodeList / null (element not found)
	 */
	public function get( $idx = 0 ) {
		if ( $idx !== null ) {
			if ( $idx < 0 || $idx >= $this->currList->count() ) {
				return null;
			}
			return $this->currList->offsetGet( $idx );
		}
		return $this->currList;
	}

	/**
	 * Select single element of current list as current element.
	 * Also may be used to change vid of already existing single current element.
	 */
	public function item( $idx, $vid = '' ) {
		$this->addVid( XmlNodeList::newFromNode( $this->get( $idx ) ) , $vid );
		# Chaining
		return $this;
	}

	public function first( $vid = '' ) {
		# Chaining
		return $this->item( 0, $vid );
	}

	public function last( $vid = '' ) {
		# Chaining
		return $this->item( $this->currList->count() - 1, $vid );
	}

	/**
	 * Select single element of current tree by DOM id (_not_ vid).
	 */
	public function byId( $id, $vid = '' ) {
		$newList = new XmlNodeList();
		if ( ($element = $this->tree->getElementById( $id )) !== null ) {
			$newList->pushNode( $element );
		}
		$this->addVid( $newList, $vid );
		# Chaining
		return $this;
	}

	/**
	 * Run xpath query for each of current list elements, generating new current list.
	 * @param $path string
	 *   xpath query
	 * @param $vid string / null
	 *   optionally set new vid for the result set
	 */
	public function xpath( $path, $vid = '' ) {
		# note: xpath instance has to be recreated on every tree change,
		# otherwise further queries may fail.
		$xpath = new \DOMXPath( $this->tree );
		# collect all xpath query results into new XmlNodeList
		$newList = new XmlNodeList();
		foreach ( $this->currList as $element ) {
			$newList->pushDOMNodeList( $element instanceof \DOMDocument ?
				$xpath->query( $path ) : $xpath->query( $path, $element )
			);
		}
		if ( $vid === null ) {
			# return "raw" result
			return $newList;
		}
		$this->addVid( $newList, $vid );
		# Chaining
		return $this;
	}

	/**
	 * Run xpath query against the whole DOM tree, generating new current list.
	 * @param $path string
	 *   xpath query
	 * @param $vid string / null
	 *   optionally set new vid for the result set
	 */
	public function rootXpath( $path, $vid = '' ) {
		# select root of the tree as ->currList
		$this->byVid( null );
		# note: xpath instance has to be recreated on every tree change,
		# otherwise further queries may fail.
		$xpath = new \DOMXPath( $this->tree );
		# collect all xpath query results into new XmlNodeList
		$newList = new XmlNodeList();
		$newList->pushDOMNodeList( $xpath->query( $path ) );
		if ( $vid === null ) {
			# return "raw" result
			return $newList;
		}
		$this->addVid( $newList, $vid );
		# Chaining
		return $this;
	}

	/**
	 * Applies method of element to each element in ->currList.
	 * @param $fn string
	 *   method name of DOMNode to call
	 * @param $arg1 mixed
	 *   1st argument of $fn method
	 * @return XmlNodeList of applied methods results
	 */
	protected function mapElement1( $fn, $arg1 ) {
		# list of results
		$result = new XmlNodeList();
		foreach ( $this->currList as $elem ) {
			$result->pushNode( $elem->$fn( $arg1 ) );
		}
		return $result;
	}

	/**
	 * Applies method of element to each element in ->currList.
	 * @param $fn string
	 *   method name of DOMNode to call
	 * @param $arg1 mixed
	 *   1st argument of $fn method
	 * @param $arg2 mixed
	 *   2nd argument of $fn method
	 * @return XmlNodeList of applied methods results
	 */
	protected function mapElement2( $fn, $arg1, $arg2 ) {
		# list of results
		$result = new XmlNodeList();
		foreach ( $this->currList as $elem ) {
			$result->pushNode( $elem->$fn( $arg1, $arg2 ) );
		}
		return $result;
	}

	/**
	 * Applies method of current instance to each element in ->currList.
	 * Set the result of applying as new ->currList.
	 * @param $fn string
	 *   method name of this to call
	 */
	protected function mapMethod( $fn ) {
		# Dbg\log('mapMethod currList',$this->currList);
		# convert source XmlNodeList into resulting XmlNodeList
		$newList = new XmlNodeList();
		# Dbg\log('mapMethod number of nodes',count($this->currList));
		foreach ( $this->currList as $elem ) {
			# Dbg\log( 'mapMethod elem',$elem);
			$result = $this->$fn( $elem );
			if ( $result instanceof \DOMNode ) {
				$newList->pushNode( $result );
			} else {
				$newList->pushXmlNodeList( $result );
			}
		}
		$this->setCurrList( $newList );
		$this->mmParams = new \stdClass();
	}

	protected function createAttribute( $key, $val ) {
		$attr = $this->tree->createAttribute( $key );
		if ( $val !== null ) {
			if ( !is_scalar( $val ) ) {
				SdvException::throwError( 'Non-scalar value of XML attribute', __METHOD__, $val );
			}
			if ( !is_bool( $val ) ) {
				# str_replace() is a workaround for
				# "unterminated entity reference" error
				$val = str_replace( '&', '&amp;', strval( $val ) );
			}
		}
		# @todo: boolean values do not produce boolean attributes :(
		# See PersonsPageView for example.
		# As a workaround, use null-values attributes.
		$attr->value = $val;
		return $attr;
	}

	protected function createComment( $value ) {
		# Protection against "DOMDocument::load(): Comment must not contain '--' (double-hyphen)"
		# error.
		# http://msdn.microsoft.com/en-us/library/ff460727%28v=vs.85%29.aspx
		return $this->tree->createComment( str_replace( '--', '&#45;&#45;', $value ) );
	}

	protected function createCDATASection( $value ) {
		# http://en.wikipedia.org/wiki/CDATA#Nesting
		# It seems that libxml correctly escapes ']]>' in CDATA nodes, while not
		# escaping '--' in COMMENT nodes.
		# return $this->tree->createCDATASection( str_replace( ']]>', ']]]]><![CDATA[>', $value ) );
		return $this->tree->createCDATASection( $value );
	}

	protected function createElement1( $tagName ) {
		return new \DOMElement( $tagName );
	}

	protected function createElement2( $tagName, $value = null ) {
		switch ( $tagName ) {
		case '#text' :
			return $this->tree->createTextNode( $value );
		case '#comment' :
			return $this->createComment( $value );
		case '#cdata' :
			return $this->createCDATASection( $value );
		default :
			return ($value === null) ?
				new \DOMElement( $tagName ) : new \DOMElement( $tagName, $value );
		}
	}

	protected function createElement3( $tagName, $value = null, $namespaceURI = null ) {
		switch ( $tagName ) {
		case '#text' :
			return $this->tree->createTextNode( $value );
		case '#comment' :
			return $this->createComment( $value );
		case '#cdata' :
			return $this->createCDATASection( $value );
		default :
			if ( $namespaceURI !== null ) {
				if ( $value === null ) {
					$value = '';
				}
				return new \DOMElement( $tagName, $value, $namespaceURI );
			}
			return ($value === null) ?
				new \DOMElement( $tagName ) : new \DOMElement( $tagName, $value );
		}
	}

	/**
	 * Creates child element of each current list element(s) with optional vid.
	 * @param $tagName string
	 *   XML tag name or
	 *   "magic" names '#text'/'#comment'/'#cdata' to create text node
	 * @param $value string
	 *   optional inner text of element; null for empty tag
	 * @param $vid string / null
	 *   null: do not make added child new current element;
	 *   empty string: add child as nameless current element;
	 *   non-empty string: also set vid of added child;
	 * @param $namespaceURI string
	 *   optional XML namespace of element, when not null
	 * Note that vid will be set only when ->currList refers to single element.
	 */
	public function append( $tagName, $value = null, $vid = null, $namespaceURI = null ) {
		$mapElementResult = $this->mapElement1(
			'appendChild',
			$this->createElement3( $tagName, $value, $namespaceURI )
		);
		if ( $vid === null ) {
			# Chaining
			return $this;
		}
		# note: For 'appendChild' $mapElementResult is the list of nodes;
		# however applying another methods may produce different results.
		$this->addVid( $mapElementResult, $vid );
		# Chaining
		return $this;
	}

	# $this->mapMethod() callback "params"
	protected $mmParams;

	protected function map_innerHTML( $node ) {
		$targetNode = $this->getElement( $node );
		$this->r_removeChildren( $targetNode );
		foreach ( $this->mmParams->bodyChilds as $bodyChild ) {
			# Dbg\log(__METHOD__.':bodyChild',$bodyChild->tagName);
			$targetNode->appendChild(
				$this->tree->importNode( $bodyChild, true )
			);
		}
		return $node;
	}

	public function innerHTML( $html, $vid = null ) {
		$htmlTree = new \DOMDocument( '1.0', 'UTF-8' );
		$htmlTree->preserveWhiteSpace = false;
		# Dbg\log(__METHOD__.':addHtmlBody',static::addHtmlBody( $html ));
		$htmlTree->loadHTML( static::addHtmlBody( $html ) );
		libxml_clear_errors();
		# Dbg\log(__METHOD__.':htmlTree',$htmlTree);
		$xpath = new \DOMXPath( $htmlTree );
		$this->mmParams->bodyChilds = $xpath->query( '/html/body/node()' );
		$this->mapMethod( 'map_innerHTML' );
		if ( $vid !== null ) {
			$this->setVid( $vid );
		}
		# Chaining
		return $this;
	}

	/**
	 * ->mapMethod() callback to add new node before each nodes of the current list
	 */
	protected function map_insertbefore( $node ) {
		$refNode = $this->getElement( $node );
		# newElement has to be re-created for every insert op
		$newElement = $this->createElement3(
			$this->mmParams->createElementTagName,
			$this->mmParams->createElementValue,
			$this->mmParams->createElementNamespaceURI
		);
		# place inserted node into element of resulting XmlNodeList
		return $refNode->parentNode->insertBefore( $newElement, $refNode );
	}
	/**
	 * ->mapMethod() callback to add new node after each nodes of the current list
	 */
	protected function map_insertafter( $node ) {
		# newElement has to be re-created for every insert op
		$newElement = $this->createElement3(
			$this->mmParams->createElementTagName,
			$this->mmParams->createElementValue,
			$this->mmParams->createElementNamespaceURI
		);
		$refNode = $this->getElement( $node );
		# place inserted node into element of resulting XmlNodeList
		if( $refNode->nextSibling === null ) {
			return $refNode->parentNode->appendChild( $newElement );
		} else {
			return $refNode->parentNode->insertBefore( $newElement, $refNode->nextSibling );
		}
	}
	/**
	 * Inserts new element before / after each of current list element(s)
	 * optionally assigning vid to collection of created elements.
	 * @param $tagName string
	 *   XML tag name or
	 *   "magic" names '#text'/'#comment'/'#cdata' to create text node
	 * @param $value string
	 *   optional inner text of element; null for empty tag
	 * @param $vid string / null
	 *   null: do not make added child new current element;
	 *   empty string: add child as nameless current element;
	 *   non-empty string: also set vid of added child;
	 * @param $namespaceURI string
	 *   optional XML namespace of element, when not null
	 * Note that vid will be set only when ->currList refers to single element.
	 */
	public function insert( $where = 'before', $tagName, $value = null, $vid = null, $namespaceURI = null ) {
		if ( $where !== 'before' ) {
			$where = 'after';
		}
		# setup extra data for ->mapMethod()
		$this->mmParams->createElementTagName = $tagName;
		$this->mmParams->createElementValue = $value;
		$this->mmParams->createElementNamespaceURI = $namespaceURI;
		$this->mapMethod( "map_insert{$where}" );
		if ( $vid !== null ) {
			$this->setVid( $vid );
		}
		# Chaining
		return $this;
	}

	/**
	 * Add attribute to elements of current list.
	 */
	public function addAttr( $name, $value = null ) {
		$this->mapElement2( 'setAttribute', $name, $value );
		# Chaining
		return $this;
	}

	/**
	 * Get XML string representation of current node.
	 * note: without pretty formatting (no SIGNIFICANT_WHITESPACE nodes)
	 */
	protected function getXMLHTML( $type = 'xml' ) {
		# protected method: skip check
		# $this->tryCheckContentType( $type, 'xmlContentTypes' );
		if ( $this->currList->count() !== 1 ) {
			SdvException::throwFatal( 'Cannot export zero or multiple nodes; use ->lim() first', __METHOD__ );
		}
		if ( $this->currVid === null ) {
			# export root of the tree
			$result = ($type === 'html') ?
				$this->tree->saveHTML() :
				$this->tree->saveXML();
		} else {
			# export currently selected element inside the tree
			$result = ($type === 'html') ?
				$this->tree->saveHTML( $this->currList->offsetGet( 0 ) ) :
				$this->tree->saveXML( $this->currList->offsetGet( 0 ) );
		}
		libxml_clear_errors();
		return $result;
	}

	public function getString( $type = 'xml' ) {
		$this->tryCheckContentType( $type, 'allContentTypes' );
		if ( $type === 'xml' || $type === 'html' ) {
			return $this->getXMLHTML( $type );
		}
		# json string
		$treeArray = array();
		# check for empty tree
		if ( $this->tree->documentElement !== null ) {
			static::r_getArray( $this->tree->documentElement, $treeArray );
		}
		if ( ( $result = Gl::json_encode( $treeArray ) ) === false ) {
			SdvException::throwError( 'Invalid data cannot be decoded', __METHOD__, $treeArray );
		}
		return $result;
	}

	public function __toString() {
		return $this->tree->saveXML();
		libxml_clear_errors();
	}

	/**
	 * Save XML string representation of the whole tree into file.
	 */
	protected function saveXMLHTML( $type, $fileName ) {
		# protected method: skip check
		# $this->tryCheckContentType( $type, 'xmlContentTypes' );
		# save XML tree with pretty formatting
		# restore pretty formatting (SIGNIFICANT_WHITESPACE nodes) on save
		$this->tree->formatOutput = true;
		# Dbg\log(__METHOD__,strval($this));
		$result = ( $type === 'html' ) ?
			$this->tree->saveHTMLFile( $fileName ) :
			$this->tree->save( $fileName );
		libxml_clear_errors();
		if ( !$result ) {
			$this->tree->formatOutput = false;
			SdvException::throwError( 'Error writing file', __METHOD__, $fileName );
		}
		$this->tree->formatOutput = false;
	}

	/**
	 * Recursively builds array for node given.
	 * @param $node DOMNode
	 * @param
	 * @modified $result array
	 */
	protected static function r_getArray( \DOMNode $node, &$result ) {
		if ( $node instanceof \DOMText ) {
			# #text
			# incomplete but safe emulation of LIBXML_NOBLANKS;
			# we store empty text nodes because we do not want to convert
			# <tag></tag> into <tag />
			# if ( trim( $node->nodeValue ) !== '' ) {
			$result = trim( $node->nodeValue );
			# }
		} elseif ( $node instanceof \DOMComment ) {
			# #comment
			$result = array( '#comment' => $node->nodeValue );
		} elseif ( $node instanceof \DOMCdataSection ) {
			# #cdata
			$result = array( '#cdata' => $node->nodeValue );
		} else {
			$result = array( '@len' => 0 );
			# DOMElement
			# Dbg\log('node class',get_class( $node ));
			$result['@tag'] = $node->nodeName;
			# $result['@val'] = $node->nodeValue;
			if ( $node->attributes->length > 0 ) {
				foreach ( $node->attributes as $attrObj ) {
					$result[$attrObj->name] = $attrObj->value;
				}
			}
			if ( $node->childNodes->length > 0 ) {
				foreach ( $node->childNodes as $childNode ) {
					$result[$result['@len']] = null;
					static::r_getArray( $childNode, $result[$result['@len']++] );
				}
			}
		}
		# Dbg\log('level tag/attrs',$result);
	}

	protected function getElement( $node ) {
		return ( $node instanceof \DOMDocument ) ? $node->documentElement : $node;
	}

	/**
	 * Get array representation of current element
	 */
	public function getArray() {
		$result = array();
		$count = $this->currList->count();
		if ( $count > 1 ) {
			# multiple elements
			# Dbg\log(__METHOD__,$this->currList);
			foreach ( $this->currList as $node ) {
				$arrayResult = array();
				static::r_getArray( $node, $arrayResult );
				$result[] = $arrayResult;
			}
		} elseif ( $count === 1 ) {
			# single element
			$result = array();
			# null check because document may be completely empty sometimes.
			if ( ( $node = $this->getElement( $this->currList->offsetGet( 0 ) ) ) !== null ) {
				static::r_getArray( $node, $result );
			}
		}
		return $result;
	}

	/**
	 * Removes all childrens of specified DOMNode.
	 * @param $node DOMNode
	 */
	protected function r_removeChildren( \DOMNode $node ) {
		while ( $node->firstChild !== null ) {
			$this->r_removeChildren( $node->firstChild );
			$node->removeChild( $node->firstChild );
		}
	}

	/**
	 * ->mapMethod() callback to remove all childrens of all nodes of current list.
	 */
	protected function map_removeAllChildren( $node ) {
		$this->r_removeChildren( $this->getElement( $node ) );
		# place the same node into element of resulting XmlNodeList
		return $node;
	}

	/**
	 * ->mapMethod() callback to remove all childrens and self for
	 * all nodes of current list.
	 */
	protected function map_removeSelf( $node ) {
		$this->r_removeChildren( $this->getElement( $node ) );
		$result = $node->parentNode;
		$result->removeChild( $node );
		# place the parent node into element of resulting XmlNodeList
		# @note: May well result in duplicate nodes, is that ok? Not.
		return $result;
	}

	/**
	 * Remove all children nodes and self for all nodes in current collection.
	 * New collection will contain node parents (might be duplicates).
	 */
	public function removeSelf() {
		# no extra "parameter" is required
		$this->mapMethod( 'map_removeSelf' );
		# Chaining
		return $this;
	}

	/**
	 * Remove all children nodes of all nodes in current collection,
	 * leaving collection intact.
	 */
	public function removeChildrens() {
		# no extra "parameter" is required
		$this->mapMethod( 'map_removeAllChildren' );
		# Chaining
		return $this;
	}

	/**
	 * Recursively appends array to DOM node.
	 * @param $node DOMNode
	 * @param $subtree mixed
	 *   array / string of subtree matching DOM elements
	 *   note: passed by reference for optimization; do not modify!
	 */
	protected function r_appendArray( \DOMNode $node, &$subtree ) {
		if ( $subtree === null ) {
			SdvException::throwError( 'null-value in subtree', __METHOD__ );
		}
		if ( is_scalar( $subtree ) ) {
			# text node
			$child = $this->tree->createTextNode( $subtree );
		} elseif ( array_key_exists( '@tag', $subtree ) ) {
			# element with possible nested subtree
			$child = $this->tree->createElement( $subtree['@tag'] );
			foreach ( $subtree as $key => &$val ) {
				if ( is_int( $key ) ) {
					$this->r_appendArray( $child, $subtree[$key] );
				} else {
					# check for non-special attribute
					if ( substr( $key, 0, 1 ) !== '@' ) {
						$child->appendChild( $this->createAttribute( $key, $val ) );
					}
				}
			}
		} elseif ( array_key_exists( '#comment', $subtree ) ) {
			$child = $this->createComment( $subtree['#comment'] );
		} elseif ( array_key_exists( '#cdata', $subtree ) ) {
			$child = $this->createCDATASection( $subtree['#cdata'] );
		} else {
			# unnamed list of nodes
			foreach ( $subtree as $key => &$val ) {
				# Only integer keys are belonging to the list of nodes,
				# non-integer keys may be only special attribute:
				# (eg. optional '@len').
				if ( is_int( $key ) ) {
					$this->r_appendArray( $node, $val );
				}
			}
			return;
		}
		$node->appendChild( $child );
	}

	/**
	 * ->mapMethod() callback to insert each node of mapped list by specified array ->lastSubtree.
	 */
	protected function map_insertArray( $node ) {
		$targetNode = $this->getElement( $node );
		# insert a dummy "placeholder" node before / after $node
		$placeholder = $this->{"map_insert{$this->mmParams->insertWhere}"}( $node );
		# append placeholder node to array in ->mmParams->lastSubtree
		$this->map_appendArray( $placeholder );
		$result = new XmlNodeList();
		$level1Node = $placeholder->firstChild;
		while ( $level1Node !== null ) {
			$result->pushNode( $level1Node );
			$level1Node = $level1Node->nextSibling;
		}
		# http://stackoverflow.com/questions/170004/how-to-remove-only-the-parent-element-and-not-its-child-elements-in-javascript
		$fragment = $this->tree->createDocumentFragment();
		while ( $placeholder->firstChild !== null ) {
			$fragment->appendChild( $placeholder->firstChild );
		}
		$parent = $placeholder->parentNode;
		$parent->insertBefore( $fragment, $placeholder );
		$parent->removeChild( $placeholder );
		return $result;
	}
	/**
	 * ->mapMethod() callback to replace each node of mapped list by specified array ->lastSubtree.
	 */
	protected function map_replaceArray( $node ) {
		$result = $this->map_insertArray( $node );
		$targetNode = $this->getElement( $node );
		$parent = $targetNode->parentNode;
		$parent->removeChild( $targetNode );
		return $result;
	}
	/**
	 * ->mapMethod() callback to set array as childrens of each node of mapped list.
	 */
	protected function map_setArray( $node ) {
		$targetNode = $this->getElement( $node );
		$this->r_removeChildren( $targetNode );
		$this->r_appendArray( $targetNode, $this->mmParams->lastSubtree );
		return $node;
	}
	/**
	 * ->mapMethod() callback to append array as childrens of each node of mapped list.
	 */
	protected function map_appendArray( $node ) {
		$targetNode = $this->getElement( $node );
		$this->r_appendArray( $targetNode, $this->mmParams->lastSubtree );
		return $node;
	}

	/**
	 * Replaces each element of current list with subtree from specified array.
	 * @param $subtree array
	 */
	public function replaceArray( array $subtree ) {
		# ->lastSubtree is extra "parameter" of mapMethod
		$this->mmParams->lastSubtree = $subtree;
		$this->mmParams->insertWhere = 'after';
		$this->mmParams->createElementTagName = 'span';
		$this->mmParams->createElementValue = '';
		$this->mmParams->createElementNamespaceURI = null;
		$this->mapMethod( 'map_replaceArray' );
	}

	/**
	 * Replaces childrens of each element of current list with subtree from specified array.
	 * @param $subtree array
	 */
	public function setArray( array $subtree ) {
		# ->lastSubtree is extra "parameter" of mapMethod
		$this->mmParams->lastSubtree = $subtree;
		$this->mapMethod( 'map_setArray' );
	}

	/**
	 * Appends subtree from specified array as new childrens of each element of current list.
	 * @param $subtree array
	 */
	public function appendArray( array $subtree ) {
		# ->lastSubtree is extra "parameter" of mapMethod
		$this->mmParams->lastSubtree = $subtree;
		$this->mapMethod( 'map_appendArray' );
	}

	/**
	 * Inserts subtree from specified array before / after each element of current list.
	 * @param $subtree array
	 */
	public function insertArray( $where = 'before', array $subtree ) {
		# generate "parameters" of ->mapMethod()
		$this->mmParams->insertWhere = ( $where === 'before' ) ? $where : 'after';
		$this->mmParams->createElementTagName = 'span';
		$this->mmParams->createElementValue = '';
		$this->mmParams->createElementNamespaceURI = null;
		$this->mmParams->lastSubtree = $subtree;
		$this->mapMethod( 'map_insertArray' );
	}

	protected function saveJSON( $fileName ) {
		if ( ( $f = Gl::fopenChmod( $fileName, "wb" ) ) === false ) {
			SdvException::throwError( 'Cannot create file', __METHOD__, $fileName );
		}
		$content = $this->getString( 'json' );
		if ( fwrite( $f, $content ) !== strlen( $content ) ) {
			SdvException::throwError( 'Unable to write file', __METHOD__, $fileName );
		}
		fclose( $f );
	}

	/**
	 * Save current xml tree into file.
	 * @param $fileName mixed
	 *   null - use current $this->fileName; string - override current $this->fileName.
	 */
	public function save( $type = 'xml', $fileName = null ) {
		$this->tryCheckContentType( $type, 'allContentTypes' );
		if ( $fileName === null ) {
			$fileName = $this->getFileName();
		}
		if ( $fileName === null ) {
			SdvException::throwFatal( 'Uninitialized xml file name', __METHOD__ );
		}
		if ( $type === 'xml' || $type === 'html' ) {
			$this->saveXMLHTML( $type, $fileName );
		} elseif ( $type === 'json' ) {
			$this->saveJSON( $fileName );
		}
		# chaining
		return $this;
	}

	public static function addHtmlBody( $html ) {
		return '<html><head><meta http-equiv="Content-Type" content="text/html; charset=utf-8" /></head><body>' . $html . '</body></html>';
	}

	public static function removeHtmlBody( $html ) {
		# non-mb versions are safe because we are searching for ASCII codes < 128
		if ( !preg_match_all( '/<body.*?>/', $html, $matches, PREG_OFFSET_CAPTURE ) ||
				($pos2 = strrpos( $html, '</body>' )) === false ) {
			return false;
		}
		$pos1 = $matches[0][0][1] + strlen( $matches[0][0][0] );
		if ( $pos1 < $pos2 ) {
			return trim( substr( $html, $pos1, $pos2 - $pos1 ) );
		}
		return false;
	}

	public static function replaceHtmlBody( $html ) {
		return static::addHtmlBody( static::removeHtmlBody( $html ) );
	}

	protected static $safeTagAttributes = array(
		'html' => null,
		'head' => null,
		'meta' => null,
		'a' => array( 'href' => true ),
		'img' => array( 'alt' => true, 'src' => true ),
	);
	public static function sanitize( $html ) {
		$tree = new \DOMDocument();
		libxml_use_internal_errors( true );
		$loadResult = @$tree->loadHTML( static::addHtmlBody( $html ) );
		libxml_clear_errors();
		if ( !$loadResult ) {
			SdvException::throwRecoverable( 'Cannot load html content',
				__METHOD__, $html );
		}
		$xpath = new \DOMXPath( $tree );
		# do not allow unsafe tags
		if ( $xpath->query( "//script | //frame | //iframe | //object | //embed" )
				->length > 0 ) {
			SdvException::throwRecoverable( 'Insecure tags are not allowed',
				__METHOD__, $html );
		}
		# remove unsafe attributes
		$tags = $xpath->query( "//*" );
		foreach ( $tags as $tag ) {
			$safeAttrNames = array_key_exists( $tag->tagName, static::$safeTagAttributes ) ?
				 static::$safeTagAttributes[$tag->tagName] : array();
			if ( $safeAttrNames === null || !$tag->hasAttributes() ) {
				continue;
			}
			$badAttrNames = array();
			foreach ( $tag->attributes as $attrName => $attrNode ) {
				if ( !array_key_exists( $attrName, $safeAttrNames ) ) {
					$badAttrNames[] = $attrName;
				}
			}
			foreach ( $badAttrNames as $badAttr ) {
				$tag->removeAttribute( $badAttr );
			}
		}
		# get "href matches text" links
		$matchingAnchors = array();
		$anchors = $xpath->query( "//a" );
		foreach ( $anchors as $anchor ) {
			if ( $anchor->getAttribute( 'href' ) === $anchor->nodeValue ) {
				$matchingAnchors[] = $anchor;
			}
		}
		# shorten "href matches text" links
		if ( count( $matchingAnchors ) === 1 ) {
			$matchingAnchors[0]->nodeValue = '[[...]]';
		} elseif ( count( $matchingAnchors ) > 1 ) {
			foreach ( $matchingAnchors as $idx => $anchor ) {
				$anchor->nodeValue = '[[' . ($idx + 1) . ']]';
			}
		}
		if ( ($result = static::removeHtmlBody( $tree->saveHTML() )) === false ) {
			SdvException::throwRecoverable( 'Cannot remove html head',
				__METHOD__, $html );
		}
		return $result;
	}

	/**
	 * add one or more CSS class name to tag class attribute
	 */
	static function addClass( array &$tag, $className ) {
		if ( !isset( $tag['class'] ) ) {
			$tag['class'] = $className;
			return;
		}
		if ( !self::hasClassName( $tag['class'], $className ) ) {
			$tag['class'] .= " $className";
		}
	}

	/**
	 * Finds whether the class name already exists in class attribute
	 */
	static function hasClassName( $classAttr, $className ) {
		return preg_match( '/(\s|^)' . preg_quote( $className ). '(\s|$)/', $classAttr );
	}

	static protected function r_renderTagArray( &$tag ) {
		$tag_open = '';
		$tag_close = '';
		$tag_val = null;
		if ( is_array( $tag ) ) {
			ksort( $tag );
			if ( array_key_exists( '@tag', $tag ) ) {
				# List of properties inside tag.
				$hscTag = Gl::hsc( $tag['@tag'] );
				$tag_open .= '<' . $hscTag;
				foreach ( $tag as $attr_key => &$attr_val ) {
					if ( is_int( $attr_key ) ) {
						if ( $tag_val === null ) {
							$tag_val = '';
						}
						if ( is_array( $attr_val ) ) {
							# Recursive tags.
							$tag_val .= static::r_renderTagArray( $attr_val );
						} else {
							# Text.
							$tag_val .= Gl::hsc( $attr_val );
						}
					} else {
						# String keys are for tag attributes.
						if ( is_array( $attr_val ) || is_object( $attr_val ) || is_bool( $attr_val ) ) {
							SdvException::throwError( 'Tagged list attribute should have scalar value',
								__METHOD__,
								array(
									'attr_key' => $attr_key,
									'attr_val' => $attr_val,
									'tag' => $tag
								)
							);
						}
						if ( substr( $attr_key, 0, 1 ) !== '@' ) {
							# Include only non-reserved attributes.
							$hscKey = Gl::hsc( $attr_key );
							if ( $attr_val !== null ) {
								$tag_open .= ' ' . $hscKey . '="' . Gl::hsc( $attr_val ) . '"';
							} else {
								$tag_open .= ' ' . $hscKey . '="' . $hscKey . '"';
							}
						}
					}
				}
				if ( $tag_val !== null ) {
					$tag_open .= '>';
					$tag_close .= '</' . $hscTag . '>';
				} else {
					$tag_open .= ' />';
				}
			} elseif ( array_key_exists( '#comment', $tag ) ) {
				# COMMENT node.
				if ( strpos( $tag['#comment'], '-->' ) !== false ) {
					SdvException::throwError( 'Comment text cannot contain "-->" characters',
						__METHOD__, $tag
					);
				}
				$tag_val = '<!-- ' . $tag['#comment'] . ' -->';
			} elseif ( array_key_exists( '#cdata', $tag ) ) {
				# CDATA node.
				if ( strpos( $tag['#cdata'], ']]>' ) !== false ) {
					SdvException::throwError( 'Cdata text cannot contain "]]>" characters',
						__METHOD__, $tag
					);
				}
				$tag_val = "//<![CDATA[\n" . $tag['#cdata'] . "\n//]]>";
			} else {
				# Tagless list.
				$tag_val = '';
				foreach ( $tag as $attr_key => &$attr_val ) {
					if ( is_int( $attr_key ) ) {
						if ( is_array( $attr_val ) ) {
							# Recursive tags.
							$tag_val .= static::r_renderTagArray( $attr_val );
						} else {
							# Text.
							$tag_val .= Gl::hsc( $attr_val );
						}
					} elseif ( substr( $attr_key, 0, 1 ) !== "@" ) {
						SdvException::throwError( 'Tagless list cannot have tag attribute values',
							__METHOD__,
							array(
								'attr_key' => $attr_key,
								'attr_val' => $attr_val,
								'tag' => $tag
							)
						);
					}
				}
			}
		} else {
			# Just a text.
			$tag_val = Gl::hsc( $tag );
		}
		return $tag_open . $tag_val . $tag_close;
	}

	/**
	 * Renders nested tag array into string.
	 * @param   $tag  multidimensional array of xml/html tags
	 * @return  string  representation of xml/html
	 *
	 * the stucture of $tag is like this:
	 * array( "@tag"=>"td", "class"=>"myclass", 0=>"text before li", 1=>array( "@tag"=>"li", 0=>"text inside li" ), 2=>"text after li" )
	 *
	 * both tagged and tagless lists are supported
	 */
	static function renderTagArray( $tag ) {
		return static::r_renderTagArray( $tag );
	}

	/**
	 * Converts plain html into tag array.
	 * @param $html string
	 * @return array
	 *   multidimensional array of xml/html tags
	 */
	public static function htmlToTagArray( $html ) {
		$htmlTree = new \DOMDocument( '1.0', 'UTF-8' );
		$htmlTree->preserveWhiteSpace = false;
		$returnFullDocument = static::removeHtmlBody( $html ) !== false;
		# Dbg\log('returnFullDocument',$returnFullDocument);
		if ( !$returnFullDocument ) {
			$html = static::addHtmlBody( $html );
		}
		# Dbg\log(__METHOD__.':html with body',$html);
		$htmlTree->loadHTML( $html );
		libxml_clear_errors();
		# Dbg\log(__METHOD__.':htmlTree',$htmlTree);
		$xpath = new \DOMXPath( $htmlTree );
		$bodyChilds = $xpath->query(
			$returnFullDocument ? '//node()' : '/html/body/node()'
		);
		# Dbg\log(__METHOD__.':bodyChilds',$bodyChilds);
		$treeArray = array();
		foreach ( $bodyChilds as $bodyChild ) {
			$childTree = array();
			static::r_getArray( $bodyChild, $childTree );
			$treeArray[] = $childTree;
		}
		# Dbg\log(__METHOD__.':treeArray',$treeArray);
		return $treeArray;
	}

} /* end of XmlTree class */
