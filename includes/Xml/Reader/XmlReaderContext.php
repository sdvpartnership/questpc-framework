<?php
namespace QuestPC;

/**
 * note: Place all the reader state dependent properties into instance of this class.
 */
class XmlReaderContext {
	protected $reader = null;
	# current node nodeType / name / value for $this->reader
	protected $node;
	# DOM path array for current reader node, assuming top by default
	protected $nodePath = array();
	# DOM path string for current reader node, assuming top by default
	protected $nodePathStr = '';
	# indicates, whether previous node was empty ELEMENT (self-closing one)
	protected $isEmptyElement = false;

	function __construct( \XmlReader $reader ) {
		$this->reader = $reader;
		$this->node = new XmlNode( $reader );
		$this->node->generateConstantsNames();
	}

	function dumpNode() {
		return strval( $this->node );
	}

	function checkEmptyElement() {
		if ( $this->isEmptyElement ) {
			array_pop( $this->nodePath );
			$this->nodePathStr = implode( '/', $this->nodePath );
			# Dbg\log('nodePathStr',$this->nodePathStr);
			$this->isEmptyElement = false;
		}
	}

	function getNodeType() {
		return $this->node->getNodeType();
	}

	function getNodeTypeStr() {
		return $this->node->getNodeTypeStr();
	}

	function getElementName() {
		if ( ( $pathCount = count( $this->nodePath ) ) === 0 ) {
			return '';
		}
		return $this->nodePath[$pathCount - 1];
	}

	function getValue() {
		return $this->reader->value;
	}

	function getAttribute( $attrKey ) {
		return $this->reader->getAttribute( $attrKey );
	}

	function getAllAttributes() {
		$result = new \stdClass();
		while ( $this->reader->moveToNextAttribute() ) {
			$result->{$this->reader->name} = $this->reader->value;
		}
		return $result;
	}

	function tryGetAttribute( $attrKey ) {
		if ( ( $attrVal = $this->getAttribute( $attrKey ) ) === null ) {
			SdvException::throwError( 'Node without required attribute encountered', __METHOD__,
				array( 'path' => $this->nodePathStr, 'attrKey' => $attrKey )
			);
		}
		return $attrVal;
	}

	/**
	 * Builds current nodePath and nodePathStr for ELEMENT nodes.
	 *
	 * Please note that our node pathes have different syntax from xpath:
	 *   xpath: '/tag1/tag2/tag3'
	 *   XmlReaderContent: 'tag1/tag2/tag3'
	 * xpath functions like text(), attributes match and partial path matches
	 * are unsupported.
	 * Use regexp with nodePathStr for ELEMENT partial path matches,
	 *   XmlReaderContext::match( new XmlNode( 'TEXT', '#text' ) ) to match text() nodes,
	 *   XmlReaderContext::getAttribute() to match element attributes.
	 */
	function setNodePath() {
		if ( $this->reader->nodeType === \XmlReader::ELEMENT ) {
			$this->isEmptyElement = $this->reader->isEmptyElement;
			# opening element but not self-closing element
			array_push( $this->nodePath, $this->reader->name );
			if ( $this->nodePathStr === '' ) {
				$this->nodePathStr = $this->reader->name;
			} else {
				$this->nodePathStr .= "/{$this->reader->name}";
			}
			# Dbg\log('nodePathStr',$this->nodePathStr);
		} elseif ( $this->reader->nodeType === \XmlReader::END_ELEMENT ) {
			# closing element
			$lastNode = array_pop( $this->nodePath );
			if ( $this->reader->name !== $lastNode ) {
				SdvException::throwError( 'Closing XML element does not match opening XML element', __METHOD__,
					array( 'closing' => $this->reader->name, 'opening' => $lastNode )
				);
			}
			$this->nodePathStr = implode( '/', $this->nodePath );
			# Dbg\log('nodePathStr',$this->nodePathStr);
		}
	}

	function getNodePathDepth() {
		return count( $this->nodePath );
	}

	function getNodePathStr() {
		return $this->nodePathStr;
	}

	/**
	 * Whether current \XmlReader node ($this->node) matches the node passed as parameter.
	 * @param  $node XmlNode
	 *   a node to match
	 */
	function match( XmlNode $node ) {
		return $node->match( $this->node );
	}

	/**
	 * Can be used to process node pathes via PCRE, to imitate a
	 * limited subset of "pseudo-xpath".
	 */
	function matchPath( $pcre, array &$matches ) {
		preg_match( $pcre, $this->nodePathStr, $matches );
	}

	protected $singleSkipRead = false;
	/**
	 * XmlReader does not provide "move to previous node" function,
	 * but it is required sometimes.
	 * We are emulating rewind to one position back via single skipping
	 * of actual $this->reader->read() call in $this->trimRead().
	 * note: You cannot call this method twice expecting to rewind back
	 * further.
	 */
	function reReadCurrentNode() {
		$this->singleSkipRead = true;
	}

	/**
	 * Reads every node except SIGNIFICANT_WHITESPACE,
	 * which has to be trmmed (skipped). Also builds current ELEMENT path.
	 */
	function trimRead() {
		if ( $this->singleSkipRead ) {
			$this->singleSkipRead = false;
			return true;
		}
		$this->checkEmptyElement();
		# Suppress the errors; ErrorException is too non-informative;
		# Instead we'll throw SdvException in caller (XmlReaderParser).
		while ( ($readresult = @$this->reader->read()) &&
			$this->reader->nodeType === \XmlReader::SIGNIFICANT_WHITESPACE ) {
			# Dbg\log( 'while', strval( $this->node ) );
			/* noop */
		}
		# Dbg\log( 'trimRead', strval( $this->node ) );
		if ( $readresult ) {
			$this->setNodePath();
		}
		return $readresult;
	}

} /* end of XmlReaderContext class */
