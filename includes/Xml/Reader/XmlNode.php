<?php
namespace QuestPC;

/**
 * XmlNode, based either on XmlReader or fake stdClass "reader" object.
 * XmlReader itself should have separate node object instance, but it does not.
 * Todo: Is there any need to support attributes for fake stdClass node as well?
 */
class XmlNode {

	protected static $xmlReaderConstants = array(
		'NONE',
		'ELEMENT',
		'ATTRIBUTE',
		'TEXT',
		'CDATA',
		'ENTITY_REF',
		'ENTITY',
		'PI',
		'COMMENT',
		'DOC',
		'DOC_TYPE',
		'DOC_FRAGMENT',
		'NOTATION',
		'WHITESPACE',
		'SIGNIFICANT_WHITESPACE',
		'END_ELEMENT',
		'END_ENTITY',
		'XML_DECLARATION',
		null, // indicates that array is unprepared
	);

	protected $reader = null;

	public static function generateConstantsNames() {
		if ( in_array( null, self::$xmlReaderConstants, true ) ) {
			# prepare XmlReader constants used by self::s_getNodeTypeStr
			array_pop( self::$xmlReaderConstants );
			self::$xmlReaderConstants = array_flip( self::$xmlReaderConstants );
			foreach ( self::$xmlReaderConstants as $name => &$val ) {
				$val = constant( "\XmlReader::$name" );
			}
			self::$xmlReaderConstants = array_flip( self::$xmlReaderConstants );
		}
	}

	/**
	 * @param variadic
	 *   instanceof XmlReader (real reader) or
	 *   nodeType, name, value
	 */
	function __construct() {
		$args = func_get_args();
		if ( count( $args ) > 1 ) {
			# Create a fake stdClass reader.
			$this->reader = new \stdClass();
			# nodeType argument might be either real constant or string with constant name
			$this->reader->nodeType = is_string( $args[0] ) ?
				array_search( $args[0], self::$xmlReaderConstants, true ) :
				$args[0];
			if ( !array_key_exists( $this->reader->nodeType, self::$xmlReaderConstants ) ) {
				SdvException::throwError( 'Unknown nodeType', __METHOD__, $args[0] );
			}
			$this->reader->name = $args[1];
			# Fake stdClass $reader might do not have value property defined.
			$this->reader->value = ( count( $args ) > 2 ) ? $args[2] : null;
		} else {
			# A node with real reader.
			if ( !($args[0] instanceof \XmlReader ) ) {
				SdvException::throwError( 'First argument is not instanceof XmlReader', __METHOD__ );
			}
			$this->reader = $args[0];
		}
	}

	function match( XmlNode $anotherNode ) {
		if ( $anotherNode->reader->nodeType !== $this->reader->nodeType ||
				$anotherNode->reader->name !== $this->reader->name ) {
			return false;
		}
		if ( $this->reader->value !== null && $anotherNode->reader->value !== null &&
				$this->reader->value !== $anotherNode->reader->value ) {
			return false;
		}
		return true;
	}

	public static function s_getNodeTypeStr( $nodeType ) {
		return self::$xmlReaderConstants[$nodeType];
	}

	public function getNodeType() {
		return $this->reader->nodeType;
	}

	public function getNodeTypeStr() {
		return self::s_getNodeTypeStr( $this->reader->nodeType );
	}

	function __toString() {
		$result = 'nodeType: ' . $this->getNodeTypeStr() . ", nodeName: {$this->reader->name}";
		if ( $this->reader->value !== null ) {
			$result .= ", value: {$this->reader->value}";
		}
		return $result;
	}

} /* end of XmlNode class */
