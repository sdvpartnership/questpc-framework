<?php
namespace QuestPC;

/**
 * Two-pass parser of one level XML nodes (no nesting)
 * into specified descendant of AbstractModelCollection.
 *
 * PASS 1: ->processCurrentSubtree() gathers ->tagVal
 * PASS 2: ->parse() calls static::$tagHandlers hooks with tagVal values as parameters
 *
 */
class XmlOneLevelParser extends XmlReaderParser {

	# stdClass. keys are tagNames, values are tag text values
	# gathered by ->logTextVal();
	protected $tagVal;

	# execute always
	const TAG_MANDATORY = 1;
	# execute only when model is not pending deletion
	const TAG_SKIP_DELETE = 2;
	# execute only when tag was found
	const TAG_OPTIONAL = 3;

	# model instance
	protected $model;

	protected static $levelBasePath;

	/**
	 * One level hooks definitions.
	 * Used at 2nd pass of parser, after all XML values have been gathered
	 * into $this->tagVal at 1st pass (after ->processCurrentSubtree());
	 *
	 * key:   string tag name in ->levelBasePath subtree;
	 * value: array
	 *   tag definition
	 *   key:   string
	 *     tag name
	 *     (subpath 'NODE1/NODE2' probably should work as well, untested yet)
	 *   value: array
	 *     tag hook of special type
	 *     key:   int
	 *       self::TAG_* constant
	 *     value: mixed
	 *       tag hook (non-standard, not like XmlReaderParser)
	 *       null   - call parser's method: array( $this, "parseTag_{$key}" );
	 *         such properties require additional parsing before storage
	 *       string - call model's method:  array( $this->model, $val )
	 *         such properties may be directly stored into model
	 */
	protected static $tagHandlers;

	# list of cached ->hookMatch values for all derived classes
	protected static $hookMatches = array();
	# using PCRE instead of xpath for custom XML path matching
	protected $hookMatch;

	/**
	 * Tries to read text node (value) of current tag node.
	 * Assumes that tag value is never empty (is never self-closed).
	 * Throws an exception when tag value is empty.
	 * Logs the value as ->tagVal-> subproperty.
	 * @return string
	 *   value of text node
	 */
	protected function logTextValEx( $nodeSubPath ) {
		$this->tryMatchNode( self::$textNode );
		$this->tagVal->{$nodeSubPath} = $this->context->getValue();
	}

	/**
	 * The same as ->logTextValEx(), however does not throw an
	 * exception at self-closing (empty) tags.
	 */
	protected function logTextVal( $nodeSubPath ) {
		$this->tagVal->{$nodeSubPath} = $this->getTextVal();
	}

	protected function logNameAttrVal( $nodeSubPath ) {
		# Create "object val" for XmlOneLevelParser::dumpTagVals()
		$tagVal = new \stdClass();
		$tagVal->attrs = $this->context->getAllAttributes();
		$tagVal->text = $this->getTextVal();
		$this->tagVal->$nodeSubPath = $tagVal;
	}

	protected function logCdataOrEntitiesText( $nodeSubPath ) {
		if ( $this->readAndCmp( self::$textNode ) ) {
			# entities text
			$this->tagVal->{$nodeSubPath} = Gl::hed( $this->context->getValue() );
			return;
		}
		if ( $this->context->getNodeType() === \XmlReader::CDATA ) {
			# cdata-wrapped text
			$this->tagVal->{$nodeSubPath} = $this->context->getValue();
			return;
		}
		SdvException::throwRecoverable( '<description> content is neither TEXT nor CDATA',
			__METHOD__, $this->context->getNodeTypeStr() );
	}

	protected function logMultipleTextVal( $nodeSubPath ) {
		if ( !property_exists( $this->tagVal, $nodeSubPath ) ) {
			$this->tagVal->{$nodeSubPath} = array();
		}
		$this->tagVal->{$nodeSubPath}[] = $this->getTextVal();
	}

	protected function logMultipleNameAttrVal( $nodeSubPath ) {
		# Create "object val" for XmlOneLevelParser::dumpTagVals()
		$tagVal = new \stdClass();
		$tagVal->attrs = $this->context->getAllAttributes();
		$tagVal->text = $this->getTextVal();
		if ( !property_exists( $this->tagVal, $nodeSubPath ) ) {
			$this->tagVal->{$nodeSubPath} = array();
		}
		$this->tagVal->{$nodeSubPath}[] = $tagVal;
	}

	/**
	 * Get all tag names gathered by ->logTextVal() & ->logTextValEx()
	 * @return array
	 *   values - tag names
	 */
	function getTagNames() {
		return array_keys( (array) $this->tagVal );
	}

	function hasTagName( $tagName ) {
		return property_exists( $this->tagVal, $tagName );
	}

	/**
	 * Get inner text value (text node) of specified gathered tag name.
	 * @param $tagName string
	 * @return string
	 *   tag value (inner text)
	 */
	function getTagVal( $tagName ) {
		return property_exists( $this->tagVal, $tagName ) ?
			$this->tagVal->{$tagName} : null;
	}

	/**
	 * Mostly for logging and debugging purposes
	 */
	function dumpTagVals() {
		$result = '';
		foreach ( $this->getTagNames() as $tagName ) {
			$vals = $this->getTagVal( $tagName );
			if ( !is_array( $vals ) ) {
				$vals = array( $vals );
			}
			foreach ( $vals as $val ) {
				$hscTagName = Gl::hsc( $tagName );
				$result .= "<{$hscTagName}"  ;
				if ( $val instanceof \stdClass ) {
					# dump logged tag attributes
					foreach ( $val->attrs as $attrKey => $attrVal ) {
						$result .= ' ' . Gl::hsc( $attrKey ) . '="' .
							Gl::hsc( $attrVal ) . '"';
					}
					$text = $val->text;
				} else {
					# tag has no attributes
					$text = $val;
				}
				$result .= '>' . Gl::hsc( $text ) . "</{$hscTagName}>\n";
			}
		}
		return $result;
	}

	/**
	 * Clear all tag names gathered by ->logTextVal() & ->logTextValEx()
	 */
	function clearTagVals() {
		$this->tagVal = new \stdClass();
	}

	protected function preg_quote_percent( $s ) {
		return preg_quote( $s, '%' );
	}

	function __construct() {
		parent::__construct();
		$this->clearTagVals();
		/**
		 * PHP late static binding is screwed up when self::$hookMatch was not
		 * defined in derived classes, but it's value was set in these classes.
		 *
		 * Thus $this->hookMatch and table of derived classes is used instead.
		 */
		if ( !array_key_exists( get_called_class(), self::$hookMatches ) ) {
			# very first initialization
			foreach ( static::$tagHandlers as &$descriptor ) {
				if ( !array_key_exists( 2, $descriptor ) ) {
					# set default tag logging method
					$descriptor[2] = 'logTextVal';
				}
				if ( !array_key_exists( 1, $descriptor ) ) {
					# set default tag handler
					$descriptor[1] = null;
				}
			}
			# generate pcre hook match used by ->checkHook()
			self::$hookMatches[get_called_class()] =
				'%^' . $this->preg_quote_percent( static::$levelBasePath ) .
				'/(' .
				implode( '|',
					array_map(
						array( $this, 'preg_quote_percent' ),
						array_keys( static::$tagHandlers )
					)
				) .
				')$%';
		}
		$this->hookMatch = self::$hookMatches[get_called_class()];
	}
	
	/* overloaded */ protected function setPathHooks( array $pathHooks, $pathPrefix = '' ) {
		SdvException::throwError( 'setPathHooks is not supported. Use static::$tagHandlers instead', __METHOD__ );
	}

	/* overloaded */ protected function checkHook() {
		/*
		if ( $this->context->getNodeType() === \XmlReader::ELEMENT ) {
			Dbg\log(__METHOD__.':nodePathStr',$this->context->getNodePathStr());
			Dbg\log(__METHOD__.':hookMatch',$this->hookMatch);
		}
		*/
		if ( $this->context->getNodeType() === \XmlReader::ELEMENT &&
				preg_match( $this->hookMatch, $this->context->getNodePathStr(), $matches ) ) {
			# Dbg\log(__METHOD__.':tagSubpath',$matches[1]);
			# Dbg\log(__METHOD__.':call',static::$tagHandlers[$matches[1]][2]);
			$this->{static::$tagHandlers[$matches[1]][2]}( $matches[1] );
		}
	}

	/**
	 * Helper method.
	 * Convert pre-defined string value to it's unique key.
	 * In UI that matches radiobutton.
	 */
	protected function parseValueKey( $val, $valueKeys ) {
		return ( array_key_exists( $val, $valueKeys ) ) ?
			$valueKeys[$val] : null;
	}

	/**
	 * Helper method.
	 * Convert string with semicolon-separated values into
	 * array of checkbox keys with optional non-pre-defined
	 * "another" string fields.
	 */
	protected function parseList( $separator, $val, $valueKeys, $modelMethod ) {
		if ( $val === '' ) {
			# empty set
			return;
		}
		$multi = explode( $separator, $val );
		foreach ( $multi as $val ) {
			$val = trim( $val );
			if ( array_key_exists( $val, $valueKeys ) ) {
				# add known key
				$this->model->$modelMethod( intval( $valueKeys[$val] ) );
			} else {
				# no known key, add value string
				$this->model->$modelMethod( strval( $val ) );
			}
		}
	}

	/**
	 * Parses current XML subtree into one arbitrary model
	 * (not just descendant of AbstractModel).
	 * @param $model object
	 * @return boolean
	 *   true :  parsed model is valid;
	 *   false : parsed model is invalid;
	 */
	protected function _parseModel( $model ) {
		# Set local reference to model so it can be used in parseTag_* callbacks.
		$this->model = $model;
		try {
			foreach ( static::$tagHandlers as $tagName => $descriptor ) {
				list( $hookType, $callback ) = $descriptor;
				$hasTagName = $this->hasTagName( $tagName );
				if ( $hookType === self::TAG_MANDATORY && !$hasTagName ) {
					SdvException::throwRecoverable( 'Mandatory tag is not found',
						__METHOD__, $tagName );
				}
				if ( $hasTagName ) {
					if ( $model instanceof AbstractModel &&
							$model->isPendingDelete() &&
							$hookType === self::TAG_SKIP_DELETE ) {
						continue;
					}
					if ( $callback === null ) {
						# default callback
						$callback = array( $this, "parseTag_{$tagName}" );
					} elseif ( is_string( $callback ) ) {
						# model callback
						$callback = array( $model, $callback );
					} else {
						# override of default callback
						$callback = array( $this, $callback[0] );
					}
					call_user_func( $callback, $this->getTagVal( $tagName ) );
				}
			}
			# Dbg\log('model',$model);
		} catch ( SdvException $e ) {
			$this->logException( $e );
			if ( $e->getErrorCode() === 'recoverable' ) {
				Dbg\log(__METHOD__.':recovered');
				# do not add current invalid model,
				# however continue to process next models;
				return false;
			}
			Dbg\log(__METHOD__.':re-throwed');
			# re-throw unrecoverable exception
			throw $e;
		}
		return true;
	}

	public function parseModel( $model ) {
		# *** 1st pass: gather $this->tagVal ***
		$this->processCurrentSubtree();
		return $this->_parseModel( $model );
	}

	/**
	 * Parse current XML subtree into new member of AbstractModelCollection.
	 * When count of models in collection reaches threshold,
	 * flushes the collection.
	 */
	public function parse( AbstractModelCollection $collection ) {
		# *** 1st pass: gather $this->tagVal ***
		$this->processCurrentSubtree();
		# *** 2nd pass: parse $this->tagVal into model ***
		$model = $collection->newSeparateModel();
		if ( $this->_parseModel( $model ) ) {
			# there were no exceptions, add presumably valid model
			$collection->addModel( $model );
			# do not store too many models (save RAM)
			$collection->bufferizedUpdate();
		}
	}

} /* end of XmlOneLevelParser class */
