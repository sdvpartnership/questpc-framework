<?php
namespace QuestPC;

/**
 * Wrapper for XmlReader
 */
class XmlReaderParser {

	protected static $textNode;

	# note: top is available only when instantiating via newFromTop()
	protected $top = null;
	# note: uri is available only when instantiating via newFromUri()
	protected $uri = null;
	# reader context, used to pass the state of reader between child and top
	protected $context;
	# todo: Is there any need to support multiple hooks per single path?
	public $pathHooks = array();

	function __construct() {
		if ( !isset( self::$textNode ) ) {
			self::$textNode = new XmlNode( 'TEXT', '#text' );
		}
	}

	/**
	 * Creates XmlReaderParser from \XmlReader with _new_ context.
	 * Usually that is a "root" (top) reader with the stream being already opened.
	 */
	public static function newFromReader( \XMLReader $reader ) {
		$parser = new static();
		$parser->context = new XmlReaderContext( $reader );
		return $parser;
	}

	/**
	 * Creates XmlReaderUarser with specified uri.
	 * That is a root (top) XmlReaderParser.
	 */
	public static function newFromUri( $uri ) {
		$reader = new \XmlReader();
		if ( defined( 'LIBXML_PARSEHUGE' ) ) {
			$reader->open( $uri, null, LIBXML_PARSEHUGE );
		} else {
			$reader->open( $uri );
		}
		$parser = static::newFromReader( $reader );
		$parser->uri = $uri;
		return $parser;
	}

	/**
	 * Create XmlReaderParser with shared context from $top XmlReaderParser.
	 * Used to separately process hooks for the current subtree, then
	 * return to top parser.
	 */
	public static function newFromTop( XMLReaderParser $top ) {
		$parser = new static();
		$parser->top = $top;
		# current context is a reference to top's reader context
		$parser->context = $top->context;
		return $parser;
	}

	public function getUri() {
		if ( $this->uri !== null ) {
			return $this->uri;
		}
		if ( $this->top !== null ) {
			return $this->top->getUri();
		}
		return null;
	}

	/**
	 * Used in YmlDownloader to overcome bug when local temporary file name was
	 * used as shop.yml_link table field instead of real remote uri.
	 *
	 * note: this works because XmlReaderParser uses initial uri only to pass it to
	 * XmlReader and nowhere else.
	 */
	public function setUri( $uri ) {
		$this->uri = $uri;
	}

	protected function dumpNode() {
		return $this->context->dumpNode();
	}

	protected function matchNodeTypeStr( $nodeTypeStr ) {
		return $this->context->getNodeTypeStr() === $nodeTypeStr;
	}

	protected function matchElement( $nodeName ) {
		return $this->context->match( new XmlNode( \XmlReader::ELEMENT, $nodeName ) );
	}

	protected function matchEndElement( $nodeName ) {
		return $this->context->match( new XmlNode( \XmlReader::END_ELEMENT, $nodeName ) );
	}

	protected function readAndCmp( XmlNode $node ) {
		if ( !$this->context->trimRead() ) {
			$exData = array(
				'uri' => $this->getUri(),
				'expected' => strval( $node ),
				'got' => null,
				'lastNode' => $this->context->dumpNode(),
				'nodepath' => $this->context->getNodePathStr()
			);
			SdvException::throwRecoverable( 'Unexpected end of file. Expected XML node.', __METHOD__, $exData );
		}
		return $this->context->match( $node );
	}

	protected function tryMatchNode( XmlNode $node ) {
		if ( !$this->readAndCmp( $node ) ) {
			# note: one may want to re-read current node in catch() block of exception handler
			#   $this->context->reReadCurrentNode();
			SdvException::throwError( 'Unexpected node while reading XML stream', __METHOD__,
				array(
					'uri' => $this->getUri(),
					'expected' => strval( $node ),
					'got' => $this->context->dumpNode(),
					'nodepath' => $this->context->getNodePathStr()
				)
			);
		}
	}

	/**
	 * Safely reads text node of current tag without throwing an exception.
	 * Note: no logging!
	 * @return string
	 *   '' for empty node, non-empty otherwise
	 */
	protected function getTextVal() {
		if ( $this->readAndCmp( self::$textNode ) ) {
			return $this->context->getValue();
		} else {
			if ( !$this->matchNodeTypeStr( 'END_ELEMENT' ) ) {
				# init case: <element />
				# At the begin of this method there was empty (self-closing) tag.
				# Then we read next element via $this->readAndCmp(),
				# which _might_ be next required tag.
				# Rewind to that element, so the current tag will not be missed by
				# $this->processCurrentSubtree()
				$this->context->reReadCurrentNode();
			} /* else {
				# init case: <element></element>
			} */
		}
		return '';
	}

	/**
	 * Checks and initializes ->pathHooks.
	 * Must be called before ->processCurrentSubtree()
	 * @param pathHooks array
	 *   keys - partial nodePathStr, values - PHP callbacks:
	 *   methods either of this class or another class
	 * @param pathPrefix string
	 *   sting prepended to each key from $pathHooks parameter to ->pathHooks
	 *
	 * Note: Descendants might implement different checkHook() logic,
	 *   then calling this method will not be required.
	 */
	protected function setPathHooks( array $pathHooks, $pathPrefix = '' ) {
		$this->pathHooks = array();
		# store pathHooks callbacks with current class instance and path prefix
		if ( $pathPrefix !== '' ) {
			$pathPrefix .= '/';
		}
		foreach ( $pathHooks as $path => $hook ) {
			$fullPath = "{$pathPrefix}{$path}";
			$this->pathHooks[$fullPath] = is_array( $hook ) ?
				$hook : array( $this, $hook );
			if ( !is_object( $this->pathHooks[$fullPath][0] ) ) {
				SdvException::throwFatal( 'Non-callable path hook class', __METHOD__, $this->pathHooks[$fullPath][0] );
			}
			if ( !is_string( $this->pathHooks[$fullPath][1] ) ) {
				SdvException::throwFatal( 'Non-callable path hook method', __METHOD__, $this->pathHooks[$fullPath][1] );
			}
			if ( !is_callable( $this->pathHooks[$fullPath] ) ) {
				SdvException::throwFatal( 'Non-callable path hook', __METHOD__,
					array(
						'className' => get_class( $this->pathHooks[$fullPath][0] ),
						'methodName' => $this->pathHooks[$fullPath][1]
					)
				);
			}
		}
	}

	/**
	 * Used to detect whether current node matches the hook key in ->pathHooks.
	 * One may overload this method in descendant class to implement different hook logic.
	 */
	protected function checkHook() {
		/*
		# uncomment to debug
		if ( $this->context->getNodeType() === \XmlReader::ELEMENT ) {
			Dbg\log(__METHOD__.':nodePathStr',$this->context->getNodePathStr());
		}
		*/
		if ( $this->context->getNodeType() === \XmlReader::ELEMENT &&
			array_key_exists( $this->context->getNodePathStr(), $this->pathHooks ) ) {
				call_user_func( $this->pathHooks[$this->context->getNodePathStr()] );
		}
	}

	/**
	 * Assuming that initial reader position is right _after_ the beginning
	 * of current path, process nodes until the end of path is found.
	 * On return, places reader position right _at_ (not _after_) the end of path.
	 */
	protected function processCurrentSubtree() {
		$endPathCount = $this->context->getNodePathDepth();
		$endPathStr = $this->context->getNodePathStr();
		do {
			if ( !$this->context->trimRead() ) {
				if ( $endPathCount === 0 ) {
					# it is valid to have nothing to read after the end of root
					break;
				}
				SdvException::throwError( 'Unexpected end of stream, expected end of nodePath', __METHOD__, $endPathStr );
			}
			$this->checkHook();
		} while ( $this->context->getNodePathDepth() >= $endPathCount );
	}

	function __toString() {
		return get_called_class();
	}

} /* end of XmlReaderParser class */
