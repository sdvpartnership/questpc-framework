<?php
namespace QuestPC;

/**
 * An object with some basic feature methods.
 * todo: convert to traits when switching to PHP 5.4.
 */
class FeaturesObject {

	# FeaturesObject singleton instances table.
	protected static $fo_instances;
	# string: base class name for singleton.
	# true: singleton of the instance of single class only.
	protected static $isSingleton;

	/**
	 * Convert stdClass object into FeaturesObject.
	 */
	public static function fromStdClass( \stdClass $src ) {
		$dst = new static();
		foreach ( $src as $propName => $propVal ) {
			if ( property_exists( $dst, $propName ) ) {
				SdvException::throwError( 'stdClass property cannot overwrite already defined property of ' . __CLASS__,
					__METHOD__, $propName );
			}
			$dst->{$propName} = $propVal;
		}
		return $dst;
	}

	public static function singleton( /* args */ ) {
		$className = get_called_class();
		$args = func_get_args();
		# Dbg\log(__METHOD__.':className',$className);
		# Dbg\log(__METHOD__.':args',$args);
		if ( !isset( static::$isSingleton ) ||
				static::$isSingleton === false ) {
			SdvException::throwFatal( 'Class is not a singleton',
				__METHOD__, $className );
		}
		# Dbg\log(__METHOD__.':isSingleton',static::$isSingleton);
		if ( !isset( static::$fo_instances ) ) {
			static::$fo_instances = array();
		}
		# Dbg\log(__METHOD__.':fo_instances',self::$fo_instances);
		if ( static::$isSingleton === true ) {
			# singleton of this class only
			if ( array_key_exists( $className, self::$fo_instances ) ) {
				# Dbg\log(__METHOD__.':this class only, already exists',$className);
				return self::$fo_instances[$className];
			}
			# create new instance
			$reflect = new \ReflectionClass( $className );
			# Dbg\log(__METHOD__.':this class only, new instance',$className);
			$instance = $reflect->newInstanceArgs( $args );
			if ( method_exists( $instance, 'singletonHook' ) ) {
				$instance->singletonHook();
			}
			self::$fo_instances[$className] = $instance;
			return $instance;
		} else {
			# singleton of base class, specified by static::$isSingleton value
			if ( !class_exists( static::$isSingleton ) ) {
				SdvException::throwFatal( 'static::$isSingleton base class does not exists',
					__METHOD__, $className );
			}
			if ( array_key_exists( static::$isSingleton, self::$fo_instances ) ) {
				# Dbg\log(__METHOD__.':base class, already exists',static::$isSingleton);
				return self::$fo_instances[static::$isSingleton];
			}
			# create new instance
			$reflect = new \ReflectionClass( $className );
			$instance = $reflect->newInstanceArgs( $args );
			if ( method_exists( $instance, 'singletonHook' ) ) {
				$instance->singletonHook();
			}
			if ( !($instance instanceof static::$isSingleton) ) {
				SdvException::throwFatal( 'static::$isSingleton must be valid base class name',
					__METHOD__, $className );
			}
			# Dbg\log(__METHOD__.':base class, new instance',$className);
			self::$fo_instances[static::$isSingleton] = $instance;
			return $instance;
		}
	}

	public function __clone() {
		if ( isset( static::$isSingleton ) &&
				static::$isSingleton !== false ) {
			SdvException::throwFatal( 'Class is a singleton. Use singleton() method to access an instance.',
				__METHOD__, get_called_class() );
		}
	}

	public function hasProp( $propName ) {
		if ( property_exists( $this, $propName ) ) {
			return true;
		}
		foreach ( $this->_mx as $mixin ) {
			if ( property_exists( $mixin, $propName ) ) {
				return true;
			}
		}
		return false;
	}

	public function getProp( $propName ) {
		if ( !property_exists( $this, $propName ) ) {
			foreach ( $this->_mx as $mixin ) {
				if ( property_exists( $mixin, $propName ) ) {
					return $mixin->getProp( $propName );
				}
			}
			SdvException::throwFatal( 'Unknown property name', __METHOD__, $propName );
		}
		return is_object( $this->{$propName} ) ?
			clone $this->{$propName} : $this->{$propName};
	}

	public function getSubProp( $propName, $subPropName ) {
		if ( !property_exists( $this, $propName ) ) {
			foreach ( $this->_mx as $mixin ) {
				if ( property_exists( $mixin, $propName ) ) {
					return $mixin->getSubProp( $propName, $subPropName );
				}
			}
			SdvException::throwFatal( 'Unknown property name', __METHOD__, $propName );
		}
		if ( is_object( $prop = $this->{$propName} ) ) {
			if ( !property_exists( $prop, $subPropName ) ) {
				return null;
			}
			return is_object( $prop->{$subPropName} ) ?
				clone $prop->{$subPropName} : $prop->{$subPropName};
		} elseif ( is_array( $prop ) ) {
			if ( !array_key_exists( $subPropName, $prop ) ) {
				return null;
			}
			return is_object( $prop[$subPropName] ) ?
				clone $prop[$subPropName] : $prop[$subPropName];
		} else {
			SdvException::throwFatal( 'Property is neither object nor array thus does not have subprops', __METHOD__, $propName );
		}
	}

	/**
	 * List of property names that are allowed to set by generic "setter".
	 * key: string
	 *   property name
	 * value: mixed
	 *   string property type
	 *   non-string: do not check the type
	 */
	protected static $propTypes;
	/**
	 * List of property validator methods (optional).
	 * @note: validator also may optionally modify passed value,
	 * when argument is passed by reference.
	 */
	protected static $propValidators;

	/**
	 * Get all defined "settable" property values.
	 * @param $requireAll boolean
	 *   whether non-initialized properties should raise an exception;
	 */
	public function getAllProp( $requireAll = false ) {
		$fields = new \stdClass();
		foreach ( static::$propTypes as $propName => $propType ) {
			if ( $requireAll && !property_exists( $this, $propName ) ) {
				SdvException::throwError(
					'Required property was not set',
					__METHOD__,
					array(
						'propName' => $propName
					)
				);
			}
			$fields->{$propName} = $this->{$propName};
		}
		return $fields;
	}

	/**
	 * Set all defined "settable" property values.
	 */
	public function setAllProp( $fields ) {
		if ( is_array( $fields ) ) {
			$fields = (object) $fields;
		}
		foreach ( static::$propTypes as $propName => $propType ) {
			if ( !property_exists( $fields, $propName ) ) {
				SdvException::throwError(
					'Required property does not exists',
					__METHOD__,
					array(
						'propName' => $propName,
						'fields' => $fields
					)
				);
			}
			$this->setProp( $propName, $fields->{$propName} );
		}
	}

	/**
	 * Set "settable" property value.
	 * @param $propName string;
	 * @param $propValue mixed;
	 */
	public function setProp( $propName, $propValue ) {
		if ( !is_array( static::$propTypes ) ||
				!array_key_exists( $propName, static::$propTypes ) ) {
			SdvException::throwError( 'Unknown settable property name', __METHOD__, $propName );
		}
		if ( is_string( static::$propTypes[$propName] ) ) {
			if ( ($type = gettype( $propValue )) === 'object' ) {
				$type = get_class( $propValue );
			}
			if ( $type !== static::$propTypes[$propName] ) {
				# @todo: Was changed from ::throwError() to ::throwRecoverable().
				# Watch for possible side-effects.
				SdvException::throwRecoverable( 'Invalid type of settable property', __METHOD__,
					array(
						'name' => $propName,
						'value' => $propValue,
						'reqType' => static::$propTypes[$propName],
						'gotType' => $type
					)
				);
			}
		}
		if ( isset( static::$propValidators ) &&
			array_key_exists( $propName, static::$propValidators ) &&
			!$this->{static::$propValidators[$propName]}( $propValue ) ) {
			# !todo: Was changed from ::throwError() to ::throwRecoverable().
			# Watch for possible side-effects.
			SdvException::throwRecoverable( 'Error validation settable property', __METHOD__,
					array(
						'name' => $propName,
						'value' => $propValue,
						'methodName' => static::$propValidators[$propName],
					)
			);
		}
		$this->{$propName} = $propValue;
	}

	/**
	 * Set multiple allowed property values.
	 * @param $props array
	 *   key: property name
	 *   value: property value
	 */
	public function setProps( array $props ) {
		if ( !isset( static::$propTypes ) ) {
			return;
		}
		foreach ( $props as $propName => $propValue ) {
			$this->setProp( $propName, $propValue );
		}
	}

	/**
	 * Checks, whether all subproperties of property are null values.
	 * @param $propName string
	 *   name of local property
	 * @return boolean
	 *   true, when all subproperties are nulls, false otherwise.
	 */
	public function isAllNullProps( $propName ) {
		$subprops = (array) $this->{$propName};
		foreach ( $subprops as $prop ) {
			if ( $prop !== null ) {
				return false;
			}
		}
		return true;
	}

	/**
	 * Execution of chain of instance methods with possible mixin support.
	 * null value of argument is used as callback separator.
	 * todo: support of last null arguments for callbacks ?
	 */
	public function chain( /* args */ ) {
		$args = func_get_args();
		$argsPos = 0;
		$argsCount = count( $args );
		while ( $argsPos < $argsCount ) {
			if ( ( $cb = $args[$argsPos++] ) !== null ) {
				if ( is_object( $cb ) ) {
					# mixin instance
					$instance = $cb;
					$method = $args[$argsPos++];
				} elseif ( is_string( $cb ) ) {
					# this instance
					$instance = $this;
					$method = $cb;
				} else {
					SdvException::throwError( 'Unsupported type of callback', __METHOD__, $cb );
				}
				$methodArgs = array();
				while ( $argsPos < $argsCount &&
					( $methodArg = $args[$argsPos++] ) !== null ) {
					array_push( $methodArgs, $methodArg );
				}
				/*
				if ( !is_callable( $callback ) ) {
					SdvException::throwError( 'Invalid callback', __METHOD__, $callback );
				}
				*/
				switch ( count( $methodArgs ) ) {
					case 0: $instance->{$method}(); break;
					case 1: $instance->{$method}( $methodArgs[0] ); break;
					case 2: $instance->{$method}( $methodArgs[0], $methodArgs[1] ); break;
					case 3: $instance->{$method}( $methodArgs[0], $methodArgs[1], $methodArgs[2] ); break;
					default :
						call_user_func_array( $callback, $methodArgs );
				}
			}
		}
	}

	/**
	 * Start of mixins, lazy loader and shared classes support.
	 */
	# list of added mixin instances (only instances, no methods);
	protected $_mx = array();
	# last used mixin instance/method callback;
	# used to speed up mixin access a little bit;
	protected $_mxcb = array( null, null );
	# last used mixin instance; reference to ->_mxcb[0];
	protected $_mxcl;
	# last used mixin method; reference to ->_mxcb[1];
	protected $_mxmt;

	/**
	 * List of lazy-loaded properties.
	 *   key: property name;
	 *   value: callback used to initialize property value;
	 * Note that when not explicitely unset, such properties are loaded just once,
	 * thus they are similar to singletons, but save some RAM due to dynamic load.
	 */
	protected $_lazy;

	/**
	 * List of property names, which will be copied by reference from
	 * main class to lazy-loaded shared class.
	 *   value: property name;
	 */
	static protected $sharedPropNames;

	/**
	 * Constructor to instantiate "main" class for shared classes.
	 * Do not use it to instantiate shared classes,
	 * otherwise duplicated ->shared classes might be created,
	 * consuming extra RAM and confusing, preventing proper
	 * intercommunications between shared classes.
	 */
	public static function baseForShared( /* args */ ) {
		$args = func_get_args();
		$reflect = new \ReflectionClass( get_called_class() );
		# Dbg\log(__METHOD__.':this class name, new instance',get_called_class());
		$self = $reflect->newInstance();
		if ( method_exists( $self, 'constructBase' ) ) {
			switch ( count( $args ) ) {
			case 0 : $self->constructBase(); break;
			case 1 : $self->constructBase( $args[0] ); break;
			case 2 : $self->constructBase( $args[0], $args[1] ); break;
			case 3 : $self->constructBase( $args[0], $args[1], $args[2] ); break;
			default :
				call_user_func_array(
					array( $self, 'constructBase' ),
					$args
				);
			}
		}
		if ( !isset( static::$lazySharedClassNames ) ) {
			SdvException::throwError(
				'Cannot instantiate base class because static::$lazySharedClassNames is undefined',
				__METHOD__
			);
		}
		$self->shared = new FeaturesObject();
		# Dbg\log(__METHOD__.':lazySharedClassNames',static::$lazySharedClassNames);
		# Dbg\log(__METHOD__.':sharedClassNamespace',static::$sharedClassNamespace);
		foreach ( static::$lazySharedClassNames as $className => $sharedPropName ) {
			if ( $sharedPropName === null ) {
				$sharedPropName = AutoLoader::getBaseClassName( $className );
			}
			# Dbg\log(__METHOD__.':sharedPropName',$sharedPropName);
			# Dbg\log(__METHOD__.':original sharedClassName',$className);
			/**
			 * static::$sharedClassNamespace will not be available in closure,
			 * because it runs in different namespace.
			 */
			if ( isset( static::$sharedClassNamespace ) ) {
				$className = AutoLoader::getFqnClassName(
					$className, static::$sharedClassNamespace
				);
			}
			# Dbg\log(__METHOD__.':fqn sharedClassName',$className);
			# use( $self ) for php 5.3, which does not have Closure::bindTo().
			$self->shared->lazyLoad( $sharedPropName, function()
					use( $className, $self ) {
				# Dbg\log(__METHOD__.'self className',get_class( $self ));
				# Dbg\log(__METHOD__.':instantiating shared className',$className);
				# @note: Shared classes are always instantiated w/o arguments.
				# This is used in HtmlPageView constructor to distinguish
				# instantiation of main class from shared classes.
				$sharedInstance = new $className();
				$self->copySharedProps( $sharedInstance );
				return $sharedInstance;
			} );
		}
		# Dbg\log(__METHOD__.':this class, new instance',$self);
		return $self;
	}

	/**
	 * Copies shared properties from main instance into newly created
	 * shared instance.
	 * Also sets context to main instance $this, because PHP 5.3 closure
	 * do not support Closure::bindTo().
	 * @param $sharedInstance
	 *   newly created shared instance
	 * @modifies $sharedInstance
	 */
	public function copySharedProps( $sharedInstance ) {
		if ( isset( static::$sharedPropNames ) ) {
			# Dbg\log(__METHOD__.':sharedPropNames',static::$sharedPropNames);
			if ( property_exists( $sharedInstance, 'baseInstance' ) ) {
				SdvException::throwError( '->baseInstance is already defined',
					__METHOD__, $sharedInstance
				);
			}
			$sharedInstance->baseInstance = $this;
			foreach ( static::$sharedPropNames as $propName ) {
				/**
				 * Even shared properties of type object should be copied
				 * by reference, otherwise when base instance object property
				 * will be modified, the change will not propagate into
				 * shared instances.
				 */
				$sharedInstance->{$propName} = &$this->{$propName};
			}
		}
		$sharedInstance->shared = $this->shared;
	}

	/**
	 * Lazy-load property on demand (first read access).
	 * @param $propName string
	 * @param $cb callback
	 *   a callback which return value will be used as value of ->{$propName}
	 */
	public function lazyLoad( $propName, $cb ) {
		if ( property_exists( $this, $propName ) ) {
			SdvException::throwError( 'Cannot lazy load already defined property',
				__METHOD__, $propName );
		}
		if ( !is_array( $this->_lazy ) ) {
			$this->_lazy = array();
		}
		if ( array_key_exists( $propName, $this->_lazy ) ) {
			SdvException::throwError( 'Lazy loader for property is already defined',
				__METHOD__, $propName );
		}
		if ( !is_callable( $cb ) ) {
			SdvException::throwError( 'Specified callback is not callable',
				__METHOD__, $cb );
		}
		$this->_lazy[$propName] = $cb;
	}

	public function hasLazy( $propName ) {
		return isset( $this->_lazy ) && array_key_exists( $propName, $this->_lazy );
	}

	public function getLazyList() {
		return array_keys( $this->_lazy );
	}

	public function deleteLazy( $propName ) {
		if ( !$this->hasLazy( $propName ) ) {
			SdvException::throwError( 'There is no lazy loader for property',
				__METHOD__, $propName );
		}
		if ( isset( $this->{$propName} ) ) {
			unset( $this->{$propName} );
		}
		unset( $this->_lazy[$propName] );
	}

	public function setOwner( $owner ) {
		# use reserved word 'parent' as such class cannot be defined
		$this->_mx['parent'] = $owner;
	}

	/**
	 * Adds mixin instance to current instance.
	 * @param $mixin object
	 */
	public function addMixin( $mixin ) {
		# add mixin's methods to owner
		$this->_mx[get_class( $mixin)] = $mixin;
		# add owner's methods to mixin 
		$mixin->setOwner( $this );
		$this->_mxcl = &$this->_mxcb[0];
		$this->_mxmt = &$this->_mxcb[1];
		$this->_mxcl = $mixin;
	}

	public function __get( $name ) {
		# check last used mixin
		if ( $this->_mxcl !== null && property_exists( $this->_mxcl, $name ) ) {
			return $this->_mxcl->{$name};
		}
		# check all defined mixins
		foreach ( $this->_mx as $mixin ) {
			if ( property_exists( $mixin, $name ) ) {
				$this->_mxcl = $mixin;
				return $mixin->{$name};
			}
		}
		# check lazy loader
		if ( isset( $this->_lazy ) &&
				array_key_exists( $name, $this->_lazy ) ) {
			return $this->{$name} = $this->_lazy[$name]();
		}
		# alas, no luck, bail out
		Dbg\log(__METHOD__,array( get_class( $this ), $name ));
		SdvException::throwError( 'Neither instance nor its mixins have a public property',
			__METHOD__, array( get_class( $this ), $name ) );
	}

	public function __set( $name, $value ) {
		if ( $this->_mxcl !== null && property_exists( $this->_mxcl, $name ) ) {
			return $this->_mxcl->{$name} = $value;
		}
		foreach ( $this->_mx as $mixin ) {
			if ( property_exists( $mixin, $name ) ) {
				$this->_mxcl = $mixin;
				return $mixin->{$name} = $value;
			}
		}
		$this->{$name} = $value;
	}

	public function __isset( $name ) {
		if ( $this->_mxcl !== null && property_exists( $this->_mxcl, $name ) ) {
			return $this->_mxcl->{$name} !== null;
		}
		foreach ( $this->_mx as $mixin ) {
			if ( property_exists( $mixin, $name ) ) {
				$this->_mxcl = $mixin;
				return $mixin->{$name} !== null;
			}
		}
		return false;
	}

	public function __unset( $name ) {
		if ( $this->_mxcl !== null && property_exists( $this->_mxcl, $name ) ) {
			return $this->_mxcl->{$name} = null;
		}
		foreach ( $this->_mx as $mixin ) {
			if ( property_exists( $mixin, $name ) ) {
				$this->_mxcl = $mixin;
				return $mixin->{$name} = null;
			}
		}
		SdvException::throwError( 'Neither instance nor its mixins have a public property',
			__METHOD__, array( get_class( $this ), $name ) );
	}

	public function __call( $name, $parameters ) {
		if ( $this->_mxcl !== null && method_exists( $this->_mxcl, $name ) ) {
			# shortcut calls are faster than call_user_func_array()
			switch ( count( $parameters ) ) {
				case 0: return $this->_mxcl->{$name}();
				case 1: return $this->_mxcl->{$name}( $parameters[0] );
				case 2: return $this->_mxcl->{$name}( $parameters[0], $parameters[1] );
				case 3: return $this->_mxcl->{$name}( $parameters[0], $parameters[1], $parameters[2] );
			}
			$this->_mxmt = $name;
			return call_user_func_array( $this->_mxcb, $parameters );
		}
		foreach ( $this->_mx as $mixin ) {
			if ( method_exists( $mixin, $name ) ) {
				$this->_mxcl = $mixin;
				# shortcut calls are faster than call_user_func_array()
				switch ( count( $parameters ) ) {
					case 0: return $this->_mxcl->{$name}();
					case 1: return $this->_mxcl->{$name}( $parameters[0] );
					case 2: return $this->_mxcl->{$name}( $parameters[0], $parameters[1] );
					case 3: return $this->_mxcl->{$name}( $parameters[0], $parameters[1], $parameters[2] );
				}
				$this->_mxmt = $name;
				return call_user_func_array( $this->_mxcb, $parameters );
			}
		}
		Dbg\log(__METHOD__,array( get_class( $this ), $name ));
		SdvException::throwError( 'Neither instance nor its mixins have a method',
			__METHOD__, array( get_class( $this ), $name ) );
	}

	/**
	 * End of mixins support.
	 */

	/**
	 * Begin of object self-saving stack support.
	 */

	public function pushReplace() {
		if ( !isset( $this->_saveobj ) ) {
			$this->_saveobj = array();
		}
		$this->_curr_saveobj = new \stdClass();
		$this->_saveobj[] = $this->_curr_saveobj;
	}

	public function popReplace() {
		if ( !isset( $this->_curr_saveobj ) ) {
			SdvException::throwError( 'Nothing to restore' );
		}
		foreach ( $this->_curr_saveobj as $propName => $propVal ) {
			$this->{$propName} = $propVal;
		}
		unset( $this->_curr_saveobj );
		$this->_curr_saveobj = array_pop( $this->_saveobj );
		if ( $this->_curr_saveobj === null ) {
			unset( $this->_curr_saveobj );
		}
	}

	public function replaceProp( $propName, $propVal ) {
		if ( property_exists( $this->_curr_saveobj, $propName ) ) {
			SdvException::throwError(
				'To replace property value more than once, call ::pushReplace() again',
				__METHOD__,
				array(
					'this' => $this,
					'propName' => $propName,
					'propVal' => $propVal,
				)
			);
		}
		$this->_curr_saveobj->{$propName} = $this->{$propName};
		$this->{$propName} = $propVal;
	}

	/**
	 * End of object self-saving stack support.
	 */

} /* end of FeaturesObject */
