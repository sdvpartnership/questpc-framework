<?php
namespace QuestPC;

/**
 * Basic php curl extension option setter / unsetter with default value.
 * A base class for all the another kinds of options.
 */
class CurlOpt {

	protected $owner;

	# curl_setopt() constant name
	protected $optName;
	# default value when the option is set by owner
	protected $defVal;

	public static function nameDef( CurlExt $owner, $optName, $defVal ) {
		$instance = new static();
		$instance->owner = $owner;
		$instance->optName = $optName;
		$instance->defVal = $defVal;
		return $instance;
	}

	/**
	 * @params $optVal mixed
	 *   null means that parameter was not set
	 */
	public function setOpt( $optVal = null ) {
		if ( $optVal === null ) {
			$optVal = $this->defVal;
		}
		# Dbg\log(__METHOD__.':optName',$this->optName);
		# Dbg\log(__METHOD__.':optVal',$optVal);
		$this->owner->setCurl( constant( $this->optName ), $optVal );
	}

	public function unsetOpt() {
		/* noop */
	}

} /* end of CurlOpt class */

abstract class CustomCurlOpt extends CurlOpt {

	public static function byOwner( CurlExt $owner ) {
		$instance = new static();
		$instance->owner = $owner;
		return $instance;
	}

	public static function nameDef( CurlExt $owner, $optName, $defVal ) {
		SdvException::throwFatal( 'Curl option name and it\'s default value should be set by custom setOpt implementation', __METHOD__ );
	}

} /* end of CustomCurlOpt class */

class CurlOptHeadOnly extends CustomCurlOpt {

	public function setOpt( $optVal = null ) {
		if ( $optVal === null ) {
			$optVal = false;
		}
		# Dbg\log(__METHOD__.':optVal',$optVal);
		$this->owner->setCurl( CURLOPT_NOBODY, $optVal );
		$this->owner->setCurl( CURLOPT_HEADER, $optVal );
	}

} /* end of CurlOptHeadOnly class */

class CurlOptInsecure extends CustomCurlOpt {

	public function setOpt( $optVal = null ) {
		if ( $optVal === null ) {
			$optVal = false;
		}
		# Dbg\log(__METHOD__.':optVal',$optVal);
		# whether curl should check SSL certificates
		# @note: CURLOPT_SSL_* have negated values
		$this->owner->setCurl( CURLOPT_SSL_VERIFYHOST, !$optVal );
		$this->owner->setCurl( CURLOPT_SSL_VERIFYPEER, !$optVal );
	}

} /* end of CurlOptInsecure class */

class CurlOptDownloadFile extends CustomCurlOpt {

	protected $file;

	public function setOpt( $optVal = null ) {
		# Dbg\log(__METHOD__.':optVal',$optVal);
		$this->file = null;
		if ( $optVal !== null ) {
			# Dbg\log(__METHOD__.':fileName',$optVal);
			$this->file = fopen( $optVal, "wb" );
			# Dbg\log(__METHOD__.':file',$this->file);
			$this->owner->setCurl( CURLOPT_FILE, $this->file );
		}
		# note: next line is commented out because CURLOPT_FILE
		# cannot be reset to null :/
		# $this->owner->setCurl( CURLOPT_FILE, $this->file );
	}

	public function unsetOpt() {
		# Dbg\log(__METHOD__.':file',$this->file);
		if ( $this->file !== null ) {
			fclose( $this->file );
		}
	}

} /* end of CurlOptDownloadFile class */

class CurlOptUploadFile extends CurlOptDownloadFile {

	public function setOpt( $optVal = null ) {
		# Dbg\log(__METHOD__.':optVal',$optVal);
		$this->file = null;
		$fileSize = null;
		if ( $optVal !== null ) {
			$fileSize = filesize( $optVal );
			# Dbg\log(__METHOD__.':fileSize',$fileSize);
			# Dbg\log(__METHOD__.':fileName',$optVal);
			$this->file = fopen( $optVal, "rb" );
			# Dbg\log(__METHOD__.':file',$this->file);
			$this->owner->setCurl( CURLOPT_INFILE, $this->file );
			$this->owner->setCurl( CURLOPT_INFILESIZE, $fileSize );
		}
		# note: next line is commented out because CURLOPT_INFILE
		# cannot be reset to null :/
		# $this->owner->setCurl( CURLOPT_INFILE, $this->file );
		# $this->owner->setCurl( CURLOPT_INFILESIZE, $fileSize );
	}

} /* end of CurlOptUploadFile class */

class CurlOptCustomRequest extends CustomCurlOpt {

	public function setOpt( $optVal = null ) {
		if ( $optVal === null ) {
			$optVal = 'GET';
		}
		# Dbg\log(__METHOD__.':optVal',$optVal);
		$this->owner->setCurl( CURLOPT_CUSTOMREQUEST, $optVal );
		$this->owner->setCurl( CURLOPT_POST, $optVal === 'POST' );
		$this->owner->setCurl( CURLOPT_PUT, $optVal === 'PUT' );
	}

} /* end of CurlOptCustomRequest class */


class CurlOptProxy extends CustomCurlOpt {

	public function setOpt( $optVal = null ) {
		# Dbg\log(__METHOD__.':optVal',$optVal);
		$proxyTunnel = false;
		$proxy = null;
		if ( $optVal !== null ) {
			$proxyTunnel = true;
			$proxy = $optVal;
		}
		$this->owner->setCurl( CURLOPT_HTTPPROXYTUNNEL, $proxyTunnel );
		$this->owner->setCurl( CURLOPT_PROXY, $proxy );
	}

} /* end of CurlOptProxy class */

/**
 * Translates curl cli options into php curl extension options.
 * Executes transfer via php curl extension.
 *
 * Provides transparent layer for common (not all) curl cli options.
 */
class CurlExt {

	# Current curl handle.
	protected $ch;
	# A map of curl cli options to curl extension options with default values.
	protected $optMap;

	public function setCurl( $opt, $val ) {
		$result = curl_setopt( $this->ch, $opt, $val );
		if ( $result === false ) {
			Dbg\log(__METHOD__.':error opt',$opt);
			Dbg\log(__METHOD__.':error val',$val);
		}
	}

	function __construct() {
		$this->ch = curl_init();
		$this->optMap = array(
			'fail' => CurlOpt::nameDef( $this, 'CURLOPT_FAILONERROR', false ),
			'cookie' => CurlOpt::nameDef( $this, 'CURLOPT_COOKIE', null ),
			'connect-timeout' => CurlOpt::nameDef( $this, 'CURLOPT_CONNECTTIMEOUT', 60 ),
			'output' => CurlOptDownloadFile::byOwner( $this ),
			'#' => CurlOpt::nameDef( $this, 'CURLOPT_URL', null ),
			'head' => CurlOptHeadOnly::byOwner( $this ),
			'insecure' => CurlOptInsecure::byOwner( $this ),
			'user' => CurlOpt::nameDef( $this, 'CURLOPT_USERPWD', null ),
			'upload-file' => CurlOptUploadFile::byOwner( $this ),
			'request' => CurlOptCustomRequest::byOwner( $this ),
			'proxy' => CurlOptProxy::byOwner( $this ),
		);
	}

	function execute( array $opts, &$retval ) {
		# Check, whether there are options which we do not know how to translate.
		$unknownOptions = array_diff(
			array_keys( $opts ),
			array_keys( $this->optMap )
		);
		if ( count( $unknownOptions ) > 0 ) {
			SdvException::throwFatal( "Do not know how to translate the following curl option(s): " .
				implode( ', ', $unknownOptions ), __METHOD__, $unknownOptions );
		}
		# Set defaults.
		# Emulate Gl::shellExecArray() stdout capture.
		$this->setCurl( CURLOPT_RETURNTRANSFER, false );
		# Translate curl cli options into curl extension options.
		foreach ( $this->optMap as $cliOpt => $curlOpt ) {
			if ( array_key_exists( $cliOpt, $opts ) ) {
				# curl cli option value null is converted into
				# curl extension option value true.
				$optVal = ($opts[$cliOpt] === null) ? true : $opts[$cliOpt];
			} else {
				$optVal = null;
			}
			$curlOpt->setOpt( $optVal );
		}
		# Do not close the curl handle. We want keep-alive transfers,
		# which run sufficiently faster
		ob_start();
		curl_exec( $this->ch );
		$result = ob_get_contents();
		ob_end_clean();
		# Dbg\log(__METHOD__.':result',$result);
		# Close file handles, if any
		foreach ( $this->optMap as $cliOpt => $curlOpt ) {
			if ( array_key_exists( $cliOpt, $opts ) ) {
				$curlOpt->unsetOpt();
			}
		}
		$retval = curl_errno( $this->ch );
		return $result;
	}

} /* end of CurlExt class */

/**
 * Curl execution that supports both external curl cli (slower but a bit more reliable)
 * and internal php curl extension, when available. For internal php curl extension
 * tries to use keep-alive and multiple connections, when possible.
 */
class CurlExec {

	# map of CurlExt instances to their hosts
	static protected $extHosts = array();

	# server output, if wasn't redirected to file via CURLOPT_FILE
	static protected $output;

	/**
	 * Default curl options.
	 */
	static $curlOptions = array(
		'connect-timeout' => 60
	);

	public static function getOutput() {
		return self::$output;
	}

	/**
	 * Create or re-use instance of CurlExt in self::$extHosts for curl cli options.
	 * Every scheme/user/host triple has it's own instance of CurlExt in self::$extHosts
	 * to utilize php curl extension default keep-alive connectivity.
	 * It's something like singleton but for scheme/user/host.
	 * @param $opts array
	 *   curl cli options
	 */
	protected static function curlExtFromOpts( array $opts ) {
		global $appContext;
		$urlParts = parse_url( $opts['#'] );
		$key = $urlParts['scheme'] . '://';
		if ( array_key_exists( 'user', $opts ) ) {
			$key .= $opts['user'] . '@';
		}
		$key .= $urlParts['host'];
		if ( $appContext->debugCurl ) {
			Dbg\log(__METHOD__.':key',$key);
		}
		# multi-singleton (separate CurlExt instance for each proto/host/user triple)
		if ( !array_key_exists( $key, self::$extHosts ) ) {
			return self::$extHosts[$key] = new CurlExt();
		}
		return self::$extHosts[$key];
	}

	/**
	 * Executes curl with Gl::shellExecArray() cli options.
	 * When php curl extension is found, uses CurlExt ability to translate
	 * cli curl options into php curl extension options.
	 * @param $opts array
	 *   curl cli options
	 * @modifies self::$output string
	 *   curl execution output
	 * @return int
	 *   curl return code (0 for success)
	 */
	public static function main( array $opts ) {
		global $appContext;
		if ( $appContext->debugCurl ) {
			Dbg\log(__METHOD__.':opts',$opts);
		}
		# do not check SSL certificates
		if ( !array_key_exists( 'proxy', $opts ) &&
				property_exists( $appContext, 'proxy' ) ) {
			$opts['proxy'] = $appContext->proxy;
		}
		$opts['insecure'] = null;
		$forceExternalCurl =
			$appContext->forceExternalCurl ||
			!function_exists( 'curl_init' ) ||
			# Do not upload files through curl extension;
			# 1. We will not be able to handle keep-alive properly anyway.
			# 2. curl_setopt( CURLOPT_INFILE, null ) cannot be reset, will fail
			# with php warning for the consequent transfers.
			( array_key_exists( 'request', $opts ) &&
				preg_match( '/^(?:PUT|POST)$/', $opts['request'] )
			) ||
			# CURLOPT_NOBODY is randomly ignored in php 5.3.5 / 5.4.11 Windows
			# and 64-bit php 5.3.10 Linux (Ubuntu 12.04LTS),
			# thus '--head' cannot be properly translated in such case.
			( array_key_exists( 'head', $opts ) );
		$retval = -1;
		$appContext->profiler->start('curl');
		self::$output = ( $forceExternalCurl ) ?
			Gl::shellExecArray( 'curl', $opts, $retval ) :
			self::curlExtFromOpts( $opts )->execute( $opts, $retval );
		$appContext->profiler->stop('curl');
		if ( $retval !== 0 ) {
			Dbg\log(__METHOD__.':error retval',$retval);
			Dbg\log(__METHOD__.':error with opts',$opts);
		}
		if ( $appContext->debugProfiler ) {
			Dbg\log(__METHOD__.':curl exec time',
				round( $appContext->profiler->stop('curl'), 7 )
			);
		}
		# Dbg\log(__METHOD__.':curl output',self::$output);
		return $retval;
	}

	const REGEX_RESPONSE_CODE = '%^HTTP/1\.[01] (\d\d\d)%';
	const REGEX_LAST_MODIFIED = '/^Last-Modified: (.*?)$/m';
	const REGEX_CONTENT_LENGTH = '/^Content-Length: (\d+)/m';

	protected static $headerHandlers = array(
		self::REGEX_RESPONSE_CODE => 'getIntVal',
		self::REGEX_LAST_MODIFIED => 'getTimestamp',
		self::REGEX_CONTENT_LENGTH => 'getIntVal',
	);

	protected static function getIntVal( $matches ) {
		return ctype_digit( $matches[1] ) ? intval( $matches[1] ) : $matches[1];
	}

	protected static function getTimestamp( $matches ) {
		# Dbg\log(__METHOD__.':timeStamp TS_RFC2822',$matches[1]);
		return Gl::timestamp( Gl::TS_UNIX, $matches[1] );
	}

	/**
	 * Extracts info from last curl run self::$output
	 * @params $args variadic
	 *   strings PCRE regexp list to match (only $1 will be returned)
	 *   note: HTTP status regexp is always automatically prepended
	 *         as the very first element of the $args list
	 * @return array
	 *   callback results for each PCRE of $args;
	 *   note: null means there was no match;
	 */
	public static function getStatus( /* args */ ) {
		$args = func_get_args();
		array_unshift( $args, self::REGEX_RESPONSE_CODE );
		$result = array_fill( 0, count( $args ), null );
		foreach ( $args as $key => $regExp ) {
			# Dbg\log(__METHOD__.':key,regExp',array($key,$regExp));
			if ( preg_match( $regExp, self::$output, $matches ) ) {
				$method = self::$headerHandlers[$regExp];
				$result[$key] = self::$method( $matches );
			}
			# Dbg\log(__METHOD__.':matches',$matches);
			# Dbg\log(__METHOD__.':result',$result);
		}
		return $result;
	}

	public static function getLastModified() {
		return self::getStatus( self::REGEX_LAST_MODIFIED );
	}

	public static function getContentLength() {
		return self::getStatus( self::REGEX_CONTENT_LENGTH );
	}

	public static function getLengthAndModified() {
		return self::getStatus(
			self::REGEX_CONTENT_LENGTH,
			self::REGEX_LAST_MODIFIED
		);
	}

	/**
	 * Download uri resource into temporary file.
	 * @param $uri string
	 * @modifies $retval int
	 *   curl return code (0 for success)
	 * @param $extraOpts array
	 *   extra options to merge into default cli curl download options
	 * @return string
	 *   temporary file name
	 */
	public static function download( $uri, &$retval, array $extraOpts = array() ) {
		$td = Gl::tempDir();
		$tmpFile = fopen( $tmpFileName = tempnam( $td, 'download-curl-' ), 'w' );
		# Dbg\log('temp nam',$tmpFileName);
		$opts = array(
			'fail' => null,
			'connect-timeout' => self::$curlOptions['connect-timeout'],
			'output' => $tmpFileName,
			'#' => $uri
		);
		$retval = self::main( array_merge( $opts, $extraOpts ) );
		fclose( $tmpFile );
		return $tmpFileName;
	}

} /* end of CurlExec class */
