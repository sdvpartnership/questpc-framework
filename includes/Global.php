<?php
namespace QuestPC;

# Parts from MediaWiki 1.20a.

class Gl {

	# Autodetect, convert and provide timestamps of various types
	# Unix time - the number of seconds since 1970-01-01 00:00:00 UTC
	const TS_UNIX = 0;
	# MediaWiki concatenated string timestamp (YYYYMMDDHHMMSS)
	const TS_MW = 1;
	# MySQL DATETIME (YYYY-MM-DD HH:MM:SS)
	const TS_DB = 2;
	# RFC 2822 format, for E-mail and HTTP headers
	const TS_RFC2822 = 3;
	# ISO 8601 format with no timezone: 1986-02-09T20:00:00Z
	# This is used by Special:Export
	const TS_ISO_8601 = 4;
	/**
	 * An Exif timestamp (YYYY:MM:DD HH:MM:SS)
	 *
	 * @see http://exif.org/Exif2-2.PDF The Exif 2.2 spec, see page 28 for the
	 *       DateTime tag and page 36 for the DateTimeOriginal and
	 *       DateTimeDigitized tags.
	 */
	const TS_EXIF = 5;
	# Oracle format time.
	const TS_ORACLE = 6;
	# Postgres format time.
	const TS_POSTGRES = 7;
	# DB2 format time
	const TS_DB2 = 8;
	# ISO 8601 basic format with no timezone: 19860209T200000Z.  This is used by ResourceLoader
	const TS_ISO_8601_BASIC = 9;
	# Return only time, omit the date. Compatible to MySQL TIME format.
	const TS_TIME = 10;

	/**
	 * Locale for LC_CTYPE, to work around http://bugs.php.net/bug.php?id=45132
	 * For Unix-like operating systems, set this to to a locale that has a UTF-8
	 * character set. Only the character set is relevant.
	 */
	static $shellLocale = 'en_US.utf8';

	# Default ASCII (NOT UTF-8) characters used to indicate suffix of
	# truncated UTF-8 strings.
	const CUT_SUFFIX = '...';
	# A match for filename extension without dot separator.
	const REGEX_FILE_EXT = '`\.(\w+)$`u';

	/**
	 * Safety wrapper around ini_get() for boolean settings.
	 *
	 * @param $setting String
	 * @return Bool
	 */
	static function iniGetBool( $setting ) {
		$val = ini_get( $setting );
		// 'on' and 'true' can't have whitespace around them, but '1' can.
		return strtolower( $val ) == 'on'
			|| strtolower( $val ) == 'true'
			|| strtolower( $val ) == 'yes'
			|| preg_match( "/^\s*[+-]?0*[1-9]/", $val ); // approx C atoi() function
	}

	/**
	 * Check if the operating system is Windows.
	 *
	 * @return Bool: true if it's Windows, False otherwise.
	 */
	static function isWindows() {
		static $isWindows = null;
		if ( $isWindows === null ) {
			$isWindows = substr( php_uname(), 0, 7 ) == 'Windows';
		}
		return $isWindows;
	}

	static function isNullProp( \stdClass $obj, $propName ) {
		return property_exists( $obj, $propName ) && $obj->{$propName} === null;
	}

	/**
	 * Convert an arbitrarily-long digit string from one numeric base
	 * to another, optionally zero-padding to a minimum column width.
	 *
	 * Supports base 2 through 36; digit values 10-36 are represented
	 * as lowercase letters a-z. Input is case-insensitive.
	 *
	 * @param $input String: of digits
	 * @param $sourceBase Integer: 2-36
	 * @param $destBase Integer: 2-36
	 * @param $pad Integer: 1 or greater
	 * @param $lowercase Boolean
	 * @return String or false on invalid input
	 */
	static function baseConvert( $input, $sourceBase, $destBase, $pad = 1, $lowercase = true ) {
		$input = strval( $input );
		if( $sourceBase < 2 ||
			$sourceBase > 36 ||
			$destBase < 2 ||
			$destBase > 36 ||
			$pad < 1 ||
			$sourceBase != intval( $sourceBase ) ||
			$destBase != intval( $destBase ) ||
			$pad != intval( $pad ) ||
			!is_string( $input ) ||
			$input == '' ) {
			return false;
		}
		$digitChars = ( $lowercase ) ? '0123456789abcdefghijklmnopqrstuvwxyz' : '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$inDigits = array();
		$outChars = '';

		// Decode and validate input string
		$input = strtolower( $input );
		for( $i = 0; $i < strlen( $input ); $i++ ) {
			$n = strpos( $digitChars, $input[$i] );
			if( $n === false || $n > $sourceBase ) {
				return false;
			}
			$inDigits[] = $n;
		}

		// Iterate over the input, modulo-ing out an output digit
		// at a time until input is gone.
		while( count( $inDigits ) ) {
			$work = 0;
			$workDigits = array();

			// Long division...
			foreach( $inDigits as $digit ) {
				$work *= $sourceBase;
				$work += $digit;

				if( $work < $destBase ) {
					// Gonna need to pull another digit.
					if( count( $workDigits ) ) {
						// Avoid zero-padding; this lets us find
						// the end of the input very easily when
						// length drops to zero.
						$workDigits[] = 0;
					}
				} else {
					// Finally! Actual division!
					$workDigits[] = intval( $work / $destBase );

					// Isn't it annoying that most programming languages
					// don't have a single divide-and-remainder operator,
					// even though the CPU implements it that way?
					$work = $work % $destBase;
				}
			}

			// All that division leaves us with a remainder,
			// which is conveniently our next output digit.
			$outChars .= $digitChars[$work];

			// And we continue!
			$inDigits = $workDigits;
		}

		while( strlen( $outChars ) < $pad ) {
			$outChars .= '0';
		}

		return strrev( $outChars );
	}

	/**
	 * Converts _binary_ string to _digit_ string of any base in range 2..36.
	 * @param $bin string
	 *   binary data
	 * @param $destBase int
	 *   output digits base
	 */
	static function binToBaseStr( $bin, $destBase ) {
		$hexStr = unpack( 'H*', $bin );
		$hexStr = array_pop( $hexStr );
		return Gl::baseConvert( $hexStr, 16, $destBase );
	}

	static function baseStrToBin( $digitsStr, $srcBase ) {
		return pack( 'H*', Gl::baseConvert( $digitsStr, $srcBase, 16 ) );
	}

	static protected $intBounds = array(
		'TINYINT_U' => array( 0, 255 ),
		'TINYINT_S' => array( -128, 127 ),
		'SMALLINT_U' => array( 0, 65535 ),
		'SMALLINT_S' => array( -32768, 32767 ),
		'MONTH' => array( 1, 12 ),
		'HOUR' => array( 0, 23 ),
		'MINUTE' => array( 0, 59 ),
		'SECOND' => array( 0, 59 )
	);
	/**
	 * Tries to convert number / string to integer with restriction to specified bounds.
	 * @param $val mixed
	 *   int / string to check
	 * @param $key string
	 *   key of Gl::$intBounds array ('TINYINT_U', 'TINYINT_S')
	 * @return int
	 *   value of $val when fits to specified bounds; false otherwise
	 */
	static function isInt( $val, $key ) {
		if ( !is_numeric( $val ) ) {
			return false;
		}
		$val = intval( $val );
		if ( $val >= Gl::$intBounds[$key][0] && $val <= Gl::$intBounds[$key][1] ) {
			return $val;
		}
		return false;
	}

	/**
	 * @return string
	 * extension of filename, lowercase, without dot separator.
	 */
	static function getFileExt( $fileName ) {
		if ( preg_match( Gl::REGEX_FILE_EXT, $fileName, $ext ) ) {
			return mb_strtolower( $ext[1], 'UTF-8' );
		}
		return null;
	}

	static function fileNameFromTitle( $title ) {
		$s = implode(
			preg_split( '/[^A-ZА-Я\d\.,_]/iu',
				str_replace( ' ', '_', $title ),
				-1,
				PREG_SPLIT_NO_EMPTY
			)
		);
	}

	/**
	 * @param $uri string
	 *   resource locator
	 * @param $reqExt string
	 *   required target filename extension (w/o dot separator)
	 * @param $title string
	 *   a name that will be used when $uri path component is not safe filename
	 * @return string
	 *   filename that "matches" supplied $uri
	 */
	static function getFileNameFromUri( $uri, $reqExt, $title ) {
		$components = parse_url( $uri );
		$targetFileName = basename( $components['path'] );
		if ( preg_match( '`^[\w\.]+\.\w+$`u', $targetFileName ) ) {
			if ( Gl::getFileExt( $targetFileName ) !== $reqExt ) {
				$targetFileName .= ".{$reqExt}";
			}
			return $targetFileName;
		}
		$defaultName = Gl::fileNameFromTitle( $title );
		return ( Gl::getFileExt( $defaultName ) === $reqExt ) ?
			$defaultName : "{$defaultName}.{$reqExt}";
	}

	/**
	 * @param $url string
	 * @return string
	 *   host part w/o path and fragment
	 * @throws SdvException
	 *   in case url is invalid
	 */
	static function getHostPart( $url ) {
		$urlParts = parse_url( $url );
		if ( $urlParts === false ) {
			SdvException::throwRecoverable( 'Invalid URL', __METHOD__, $url );
		}
		$hostPart = '';
		if ( isset( $urlParts['scheme'] ) ) {
			$hostPart .= $urlParts['scheme'] . '://';
		}
		if ( isset( $urlParts['username'] ) ) {
			$hostPart .= $urlParts['username'];
			if ( isset( $urlParts['password'] ) ) {
				$hostPart .= ':' . $urlParts['password'];
			}
			$hostPart .= '@';
		}
		if ( !isset( $urlParts['host'] ) ) {
			SdvException::throwRecoverable(
				'URL has no host name', __METHOD__,
				array(
					'url' => $url,
					'urlParts' => $urlParts,
				)
			);
		}
		$hostPart .= $urlParts['host'];
		return $hostPart;
	}

	static function dieUsage( $extraOptions ) {
		global $appContext;
		if ( php_sapi_name() === 'cli' ) {
			die( "Usage: {$appContext->scriptName} {$extraOptions}\n" );
		}
		throw new \Exception( 'Do not ever use die() / exit() in http context.' );
	}

	/**
	 * The very preferred way of abnormal termination.
	 */
	static function checkShutdown( \Exception $e ) {
		global $appContext;
		# Dbg\except_die(__METHOD__,$e);
		if ( $e instanceof ShutdownException ) {
			$exceptionData = $e->getData();
			switch ( $e->getErrorCode() ) {
			case 'redirect' :
			foreach ( array( 'http_code', 'location' ) as $key ) {
				if ( !array_key_exists( $key, $exceptionData ) ) {
					Dbg\except_die( $e );
					SdvException::throwError( "No {$key} specified", __METHOD__, $exceptionData );
				}
			}
			$appContext->response->redirect( $exceptionData['http_code'], $exceptionData['location'] );
			break;
			case 'termination' :
			$appContext->response->handler404();
			break;
			case 'digest_auth' :
			$appContext->response->digestAuthRealm( $exceptionData['realm'] );
			break;
			default :
				Dbg\except_die( $e );
				SdvException::throwError( 'Unknown ShutdownException error code', __METHOD__, $e->getErrorCode() );
			}
		} else {
			Dbg\except_die( $e );
		}
		Dbg\dump_request_time();
		die();
	}

	/**
	 * Return count of minutes in current day since 00:00.
	 */
	static function getMinutesInDay() {
		$minutesInDay = explode( ':', date( 'G:i' ) );
		# Dbg\log(__METHOD__,$minutesInDay);
		return intval( $minutesInDay[0] ) * 60 + intval( ltrim( $minutesInDay[1], '0' ) );
	}

	/**
	 * Get a timestamp string in one of various formats
	 *
	 * @param $outputtype Mixed: A timestamp in one of the supported formats, the
	 *                    function will autodetect which format is supplied and act
	 *                    accordingly.
	 *   int:    one of predefined Gl::TS* constant values
	 *   string: date() format string
	 * @param $ts Mixed: source timestamp
	 *   int:    0 for current timestamp, != 0 for arbitrary integer timestamp
	 *   string: the timestamp in one of GL::TS* formats
	 * @return Mixed: String / false The same date in the format specified in $outputtype or false
	 */
	static function timestamp( $outputtype = Gl::TS_UNIX, $ts = 0 ) {
		$uts = 0;
		$da = array();
		$strtime = '';

		if ( !$ts ) { // We want to catch 0, '', null... but not date strings starting with a letter.
			$uts = time();
			$strtime = "@$uts";
		} elseif ( is_int( $ts ) ) {
			# integer timestamp
			$uts = $ts;
			$strtime = "@$uts"; // http://php.net/manual/en/datetime.formats.compound.php
		} elseif ( preg_match( '/^(\d{4})\-(\d\d)\-(\d\d) (\d\d):(\d\d):(\d\d)$/D', $ts, $da ) ) {
			# Gl::TS_DB
		} elseif ( preg_match( '/^(\d{4}):(\d\d):(\d\d) (\d\d):(\d\d):(\d\d)$/D', $ts, $da ) ) {
			# Gl::TS_EXIF
		} elseif ( preg_match( '/^(\d{4})(\d\d)(\d\d)(\d\d)(\d\d)(\d\d)$/D', $ts, $da ) ) {
			# Gl::TS_MW
		} elseif ( preg_match( '/^-?\d{1,13}$/D', $ts ) ) {
			# Gl::TS_UNIX
			$uts = $ts;
			$strtime = "@$ts"; // http://php.net/manual/en/datetime.formats.compound.php
		} elseif ( preg_match( '/^\d{2}-\d{2}-\d{4} \d{2}:\d{2}:\d{2}.\d{6}$/', $ts ) ) {
			# Gl::TS_ORACLE // session altered to DD-MM-YYYY HH24:MI:SS.FF6
			$strtime = preg_replace( '/(\d\d)\.(\d\d)\.(\d\d)(\.(\d+))?/', "$1:$2:$3",
					str_replace( '+00:00', 'UTC', $ts ) );
		} elseif ( preg_match( '/^(\d{4})-(\d{2})-(\d{2})T(\d{2}):(\d{2}):(\d{2})(?:\.*\d*)?Z$/', $ts, $da ) ) {
			# Gl::TS_ISO_8601
		} elseif ( preg_match( '/^(\d{4})(\d{2})(\d{2})T(\d{2})(\d{2})(\d{2})(?:\.*\d*)?Z$/', $ts, $da ) ) {
			# Gl::TS_ISO_8601_BASIC
		} elseif ( preg_match( '/^(\d{4})\-(\d\d)\-(\d\d) (\d\d):(\d\d):(\d\d)\.*\d*[\+\- ](\d\d)$/', $ts, $da ) ) {
			# Gl::TS_POSTGRES
		} elseif ( preg_match( '/^(\d{4})\-(\d\d)\-(\d\d) (\d\d):(\d\d):(\d\d)\.*\d* GMT$/', $ts, $da ) ) {
			# Gl::TS_POSTGRES
		} elseif (preg_match( '/^(\d{4})\-(\d\d)\-(\d\d) (\d\d):(\d\d):(\d\d)\.\d\d\d$/', $ts, $da ) ) {
			# Gl::TS_TIME
		} elseif ( preg_match( '/^(\d\d):(\d\d):(\d\d)$/', $ts, $da ) ) {
			# Gl::TS_DB2
		} elseif ( preg_match( '/^[ \t\r\n]*([A-Z][a-z]{2},[ \t\r\n]*)?' . # Day of week
								'\d\d?[ \t\r\n]*[A-Z][a-z]{2}[ \t\r\n]*\d{2}(?:\d{2})?' .  # dd Mon yyyy
								'[ \t\r\n]*\d\d[ \t\r\n]*:[ \t\r\n]*\d\d[ \t\r\n]*:[ \t\r\n]*\d\d/S', $ts ) ) { # hh:mm:ss
			# Gl::TS_RFC2822, accepting a trailing comment. See http://www.squid-cache.org/mail-archive/squid-users/200307/0122.html / r77171
			# The regex is a superset of rfc2822 for readability
			$strtime = strtok( $ts, ';' );
		} elseif ( preg_match( '/^[A-Z][a-z]{5,8}, \d\d-[A-Z][a-z]{2}-\d{2} \d\d:\d\d:\d\d/', $ts ) ) {
			# Gl::TS_RFC850
			$strtime = $ts;
		} elseif ( preg_match( '/^[A-Z][a-z]{2} [A-Z][a-z]{2} +\d{1,2} \d\d:\d\d:\d\d \d{4}/', $ts ) ) {
			# asctime
			$strtime = $ts;
		} else {
			# Bogus value...
			if ( function_exists( 'Dbg\\log' ) ) {
				Dbg\log("Gl::timestamp() fed bogus time value: TYPE=$outputtype; VALUE=$ts\n");
			}
			return false;
		}

		static $formats = array(
			Gl::TS_UNIX => 'U',
			Gl::TS_MW => 'YmdHis',
			Gl::TS_DB => 'Y-m-d H:i:s',
			Gl::TS_ISO_8601 => 'Y-m-d\TH:i:s\Z',
			Gl::TS_ISO_8601_BASIC => 'Ymd\THis\Z',
			Gl::TS_EXIF => 'Y:m:d H:i:s', // This shouldn't ever be used, but is included for completeness
			Gl::TS_RFC2822 => 'D, d M Y H:i:s',
			Gl::TS_ORACLE => 'd-m-Y H:i:s.000000', // Was 'd-M-y h.i.s A' . ' +00:00' before r51500
			Gl::TS_POSTGRES => 'Y-m-d H:i:s',
			Gl::TS_DB2 => 'Y-m-d H:i:s',
			Gl::TS_TIME => 'H:i:s',
		);

		# select predefined output format from $formats array or use plain format string
		$outputFormat = isset( $formats[$outputtype] ) ? $formats[$outputtype] : $outputtype;

		if ( function_exists( "date_create" ) ) {
			if ( count( $da ) ) {
				$ds = sprintf("%04d-%02d-%02dT%02d:%02d:%02d.00+00:00",
					(int)$da[1], (int)$da[2], (int)$da[3],
					(int)$da[4], (int)$da[5], (int)$da[6]);

				$d = date_create( $ds, new \DateTimeZone( 'GMT' ) );
			} elseif ( $strtime ) {
				$d = date_create( $strtime, new \DateTimeZone( 'GMT' ) );
			} else {
				return false;
			}

			if ( !$d ) {
				if ( function_exists( 'Dbg\\log' ) ) {
					Dbg\log("Gl::timestamp() fed bogus time value: $outputtype; $ts\n");
				}
				return false;
			}

			$output = $d->format( $outputFormat );
		} else {
			if ( count( $da ) ) {
				// Warning! gmmktime() acts oddly if the month or day is set to 0
				// We may want to handle that explicitly at some point
				$uts = gmmktime( (int)$da[4], (int)$da[5], (int)$da[6],
					(int)$da[2], (int)$da[3], (int)$da[1] );
			} elseif ( $strtime ) {
				$uts = strtotime( $strtime );
			}

			if ( $uts === false ) {
				if ( function_exists( 'Dbg\\log' ) ) {
					Dbg\log("Gl::timestamp() can't parse the timestamp (non 32-bit time? Update php): $outputtype; $ts\n");
				}
				return false;
			}

			if ( Gl::TS_UNIX === $outputtype ) {
				return $uts;
			}
			$output = gmdate( $outputFormat, $uts );
		}

		if ( ( $outputtype === Gl::TS_RFC2822 ) || ( $outputtype === Gl::TS_POSTGRES ) ) {
			$output .= ' GMT';
		}

		return $output;
	}

	static function timestampNow( $outputtype = Gl::TS_UNIX ) {
		return Gl::timestamp( $outputtype, time() );
	}

	static function getFileTimestamp( $fileName, $outputtype = GL::TS_UNIX ) {
		if ( ( $filemtime = @filemtime( $fileName ) ) === false ) {
			return false;
		}
		return Gl::timestamp( $outputtype, $filemtime );
	}

	static function setFileTimestamp( $fileName, $ts = 0 ) {
		return @touch( $fileName, Gl::timestamp( Gl::TS_UNIX, $ts ) );
	}

	/**
	 * Tries to get the system directory for temporary files. The TMPDIR, TMP, and
	 * TEMP environment variables are then checked in sequence, and if none are set
	 * try sys_get_temp_dir() for PHP >= 5.2.1. All else fails, return /tmp for Unix
	 * or C:\Windows\Temp for Windows and hope for the best.
	 * It is common to call it with tempnam().
	 *
	 * NOTE: When possible, use instead the tmpfile() function to create
	 * temporary files to avoid race conditions on file creation, etc.
	 *
	 * @return String
	 */
	static function tempDir() {
		foreach( array( 'TMPDIR', 'TMP', 'TEMP' ) as $var ) {
			$tmp = getenv( $var );
			if( $tmp && file_exists( $tmp ) && is_dir( $tmp ) && is_writable( $tmp ) ) {
				return $tmp;
			}
		}
		if( function_exists( 'sys_get_temp_dir' ) ) {
			return sys_get_temp_dir();
		}
		# Usual defaults
		return Gl::isWindows() ? 'C:\Windows\Temp' : '/tmp';
	}

	static function chmod( $filename, $chmode = null ) {
		global $appContext;
		if ( Gl::isWindows() ) {
			return true;
		}
		if ( $chmode === null ) {
			$chmode = $appContext->fileMode;
		}
		return @chmod( $filename, $chmode );
	}

	static function fopenChmod( $filename, $mode, $chmode = null ) {
		global $appContext;
		if ( ($f = @fopen( $filename, $mode )) === false ) {
			return false;
		}
		if ( !Gl::isWindows() ) {
			if ( $chmode === null ) {
				$chmode = $appContext->fileMode;
			}
			if ( @chmod( $filename, $chmode ) === false ) {
				fclose( $f );
				return false;
			}
		}
		return $f;
	}

	/**
	 * Read value from file.
	 * @param $validationFn mixed
	 *   string / closure to validate read data
	 * @param $conversionFn mixed
	 *   string / closure to convert the type of data if $validationFn() returned true
	 * @param $defaultValue mixed
	 *   default value to return when read data was not validated successfully
	 * @param $fileName string
	 *   filename to read
	 * @return mixed
	 *   read data or default value
	 */
	static function fileGetVar( $fileName, $defaultValue = 0,
			$validationFn = 'ctype_digit', $conversionFn = 'intval' )
	{
		$result = file_exists( $fileName ) ?
			@file_get_contents( $fileName ) : $defaultValue;
		return $validationFn( $result ) ? $conversionFn( $result ) : $defaultValue;
	}

	static function MkDirParents( $dir, $mode = null ) {
		global $appContext;

		if( strval( $dir ) === '' || file_exists( $dir ) ) {
			return true;
		}

		$dir = str_replace( array( '\\', '/' ), DIRECTORY_SEPARATOR, $dir );

		if ( $mode === null ) {
			$mode = $appContext->directoryMode;
		}

		return @mkdir( $dir, $mode, true ); // PHP5 <3
	}

	/**
	 * tempnam() for directories.
	 */
	static function tempDirNam( $dir, $prefix='', $mode = null ) {
		global $appContext;
		if ( $mode === null ) {
			$mode = $appContext->directoryMode;
		}
		$dir = rtrim( $dir, '/\\' );
		do {
			$path = "{$dir}/{$prefix}" . mt_rand( 0, 9999999 );
		} while ( !@mkdir( $path, $mode ) );
		return $path;
	}

	/**
	 * Windows-compatible version of escapeshellarg()
	 * Windows doesn't recognise single-quotes in the shell, but the escapeshellarg()
	 * function puts single quotes in regardless of OS.
	 *
	 * Also fixes the locale problems on Linux in PHP 5.2.6+ (bug backported to
	 * earlier distro releases of PHP)
	 *
	 * @param varargs
	 * @return String
	 */
	static function escapeShellArg( ) {
		Gl::initShellLocale();

		$args = func_get_args();
		$first = true;
		$retVal = '';
		foreach ( $args as $arg ) {
			if ( !$first ) {
				$retVal .= ' ';
			} else {
				$first = false;
			}

			if ( Gl::isWindows() ) {
				// Escaping for an MSVC-style command line parser and CMD.EXE
				// Refs:
				//  * http://web.archive.org/web/20020708081031/http://mailman.lyra.org/pipermail/scite-interest/2002-March/000436.html
				//  * http://technet.microsoft.com/en-us/library/cc723564.aspx
				//  * Bug #13518
				//  * CR r63214
				// Double the backslashes before any double quotes. Escape the double quotes.
				$tokens = preg_split( '/(\\\\*")/', $arg, -1, PREG_SPLIT_DELIM_CAPTURE );
				$arg = '';
				$iteration = 0;
				foreach ( $tokens as $token ) {
					if ( $iteration % 2 == 1 ) {
						// Delimiter, a double quote preceded by zero or more slashes
						$arg .= str_replace( '\\', '\\\\', substr( $token, 0, -1 ) ) . '\\"';
					} elseif ( $iteration % 4 == 2 ) {
						// ^ in $token will be outside quotes, need to be escaped
						$arg .= str_replace( '^', '^^', $token );
					} else { // $iteration % 4 == 0
						// ^ in $token will appear inside double quotes, so leave as is
						$arg .= $token;
					}
					$iteration++;
				}
				// Double the backslashes before the end of the string, because
				// we will soon add a quote
				$m = array();
				if ( preg_match( '/^(.*?)(\\\\+)$/', $arg, $m ) ) {
					$arg = $m[1] . str_replace( '\\', '\\\\', $m[2] );
				}

				// Add surrounding quotes
				$retVal .= '"' . $arg . '"';
			} else {
				$retVal .= escapeshellarg( $arg );
			}
		}
		return $retVal;
	}

	static function escapeShellParameters( array $parameters ) {
		// Escape each parameter for shell
		return implode( ' ', array_map( array( __CLASS__, 'escapeShellArg' ), $parameters ) );
	}

	/**
	 * @param $log mixed
	 *   null - log output to null-device
	 *   false - log output to terminal (available in Windows only)
	 *   string - log output to file name
	 */
	static function execBackground( $cmd, $log = false ) {
		if ( $log === null ) {
			$log = Gl::isWindows() ? '> nul' : '> /dev/null';
		} elseif ( is_string( $log ) ) {
			$log = '> ' . Gl::escapeShellArg( $log );
		} else { /* false */
			$log = '';
		}
		# http://stackoverflow.com/questions/4879044/wamp-and-pcntl-fork
		if ( Gl::isWindows() ) {
			$com = new \Com( 'WScript.shell' );
			$com->run( "%comspec% /c \"{$cmd} {$log} 2>&1\"", 10, false );
		} else {
			exec( "nohup {$cmd} {$log} 2>&1 &" );
		}
	}

	static $lastCmdLine;
	/**
	 * Execute a shell command, with time and memory limits mirrored from the PHP
	 * configuration if supported.
	 * @param $cmd String Command line, properly escaped for shell.
	 * @param &$retval optional, will receive the program's exit code.
	 *                 (non-zero is usually failure)
	 * @param $environ Array optional environment variables which should be
	 *                 added to the executed command environment.
	 * @return string
	 *   captured stdout (trailing newlines stripped)
	 */
	static function shellExec( $cmd, &$retval = null, $environ = array() ) {
		/* global $IP, $wgMaxShellMemory, $wgMaxShellFileSize, $wgMaxShellTime; */

		static $disabled;
		if ( is_null( $disabled ) ) {
			$disabled = false;
			if( Gl::iniGetBool( 'safe_mode' ) ) {
				if ( function_exists( 'Dbg\\log' ) ) {
					Dbg\log( "Gl::shellExec can't run in safe_mode, PHP's exec functions are too broken.\n" );
				}
				$disabled = 'safemode';
			} else {
				$functions = explode( ',', ini_get( 'disable_functions' ) );
				$functions = array_map( 'trim', $functions );
				$functions = array_map( 'strtolower', $functions );
				if ( in_array( 'passthru', $functions ) ) {
					if ( function_exists( 'Dbg\\log' ) ) {
						Dbg\log( "passthru is in disabled_functions\n" );
					}
					$disabled = 'passthru';
				}
			}
		}
		if ( $disabled ) {
			$retval = 1;
			return $disabled == 'safemode' ?
				'Unable to run external programs in safe mode.' :
				'Unable to run external programs, passthru() is disabled.';
		}

		Gl::initShellLocale();

		$envcmd = '';
		foreach( $environ as $k => $v ) {
			if ( Gl::isWindows() ) {
				/* Surrounding a set in quotes (method used by Gl::escapeShellArg) makes the quotes themselves
				 * appear in the environment variable, so we must use carat escaping as documented in
				 * http://technet.microsoft.com/en-us/library/cc723564.aspx
				 * Note however that the quote isn't listed there, but is needed, and the parentheses
				 * are listed there but doesn't appear to need it.
				 */
				$envcmd .= "set $k=" . preg_replace( '/([&|()<>^"])/', '^\\1', $v ) . '&& ';
			} else {
				/* Assume this is a POSIX shell, thus required to accept variable assignments before the command
				 * http://www.opengroup.org/onlinepubs/009695399/utilities/xcu_chap02.html#tag_02_09_01
				 */
				$envcmd .= "$k=" . escapeshellarg( $v ) . ' ';
			}
		}
		$cmd = $envcmd . $cmd;

		if ( Gl::isWindows() ) {
			if ( version_compare( PHP_VERSION, '5.3.0', '<' ) && /* Fixed in 5.3.0 :) */
				( version_compare( PHP_VERSION, '5.2.1', '>=' ) || php_uname( 's' ) == 'Windows NT' ) )
			{
				# Hack to work around PHP's flawed invocation of cmd.exe
				# http://news.php.net/php.internals/21796
				# Windows 9x doesn't accept any kind of quotes
				$cmd = '"' . $cmd . '"';
			}
		} elseif ( php_uname( 's' ) == 'Linux' ) {
			/*
			# time / ram restrictions are temporarily disabled.
			$time = intval( $wgMaxShellTime );
			$mem = intval( $wgMaxShellMemory );
			$filesize = intval( $wgMaxShellFileSize );

			if ( $time > 0 && $mem > 0 ) {
				$script = "$IP/bin/ulimit4.sh";
				if ( is_executable( $script ) ) {
					$cmd = '/bin/bash ' . escapeshellarg( $script ) . " $time $mem $filesize " . escapeshellarg( $cmd );
				}
			}
			$cmd = escapeshellarg( $cmd );
			*/
		}
		if ( function_exists( 'Dbg\\log' ) ) {
			# Dbg\log( "Gl::shellExec: $cmd\n" );
		}

		$retval = 1; // error by default?
		ob_start();
		Gl::$lastCmdLine = $cmd;
		passthru( $cmd, $retval );
		$output = ob_get_contents();
		ob_end_clean();

		if ( $retval == 127 ) {
			if ( function_exists( 'Dbg\\log' ) ) {
				Dbg\log( 'exec', "Possibly missing executable file: $cmd\n" );
			}
		}
		return $output;
	}

	static function shellExecArray( $cmd, array $opts, &$retval = null, $environ = array() ) {
		foreach ( $opts as $opt => $val ) {
			if ( $opt !== '#' ) {
				$cmd .= " --{$opt} ";
				if ( $val !== null ) {
					$cmd .= Gl::escapeShellArg( $val );
				}
			}
		}
		if ( array_key_exists( '#', $opts ) ) {
			$cmd .= ' ' . Gl::escapeShellArg( $opts['#'] );
		}
		Dbg\log(__METHOD__,$cmd);
		return Gl::shellExec( $cmd, $retval, $environ );
	}

	/**
	 * Convert array of scalar values into cookie string
	 * suitable for CurlExec option.
	 */
	static function encodeCookie( array $cookieArray ) {
		$result = '';
		foreach ( $cookieArray as $key => $val ) {
			if ( !is_scalar( $val ) ) {
				SdvException::throwError( 'Recursive cookie encoding is not supported',
					__METHOD__,
					$cookieArray
				);
			}
			if ( $result !== '' ) {
				$result .= '; ';
			}
			$result .= urlencode( $key ) . '=' . urlencode( $val );
		}
		return $result;
	}

	/**
	 * Workaround for http://bugs.php.net/bug.php?id=45132
	 * escapeshellarg() destroys non-ASCII characters if LANG is not a UTF-8 locale
	 */
	static function initShellLocale() {
		static $done = false;
		if ( $done ) {
			return;
		}
		$done = true;
		if ( !Gl::iniGetBool( 'safe_mode' ) ) {
			putenv( 'LC_CTYPE=' . Gl::$shellLocale );
			setlocale( LC_CTYPE, Gl::$shellLocale );
		}
	}

	/*** begin of gunzip methods ***/

	static function isGzipFile( $fileName ) {
		$f = @fopen( $fileName, 'rb' );
		if ( $f === false ) {
			return false;
		}
		$header = fread( $f, 2 );
		fclose( $f );
		return $header === "\x1F\x8B";
	}

	# http://stackoverflow.com/questions/1229571/unpack-large-files-with-gzip-in-php
	static function uncompressGzip( $srcName, $dstName ) {
		$sfp = @gzopen( $srcName, 'rb' );
		if ( $sfp === false ) {
			return false;
		}
		$fp = @fopen( $dstName, 'w' );
		if ( $fp === false ) {
			fclose( $sfp );
			return false;
		}
		while ( $string = gzread( $sfp, 65536 ) ) {
			$numWritten = fwrite( $fp, $string, strlen( $string ) );
			if ( $numWritten !== strlen( $string ) ) {
				gzclose( $sfp );
				fclose( $fp );
				return false;
			}
		}
		gzclose( $sfp );
		fclose( $fp );
		return true;
	}

	/*** end of gunzip methods ***/

	static function json_encode( $var ) {
		$json_opts = JSON_NUMERIC_CHECK;
		if ( version_compare( PHP_VERSION, '5.4.0', '>=' ) ) {
			$json_opts |= JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE;
		}
		return json_encode( $var, $json_opts );
	}

	static function getSubhashesForLevel( $name, $levels ) {
		$hashes = array();
		if ( $levels == 0 ) {
			return $hashes;
		}
		$hash = md5( $name );
		for ( $i = 1; $i <= $levels; $i++ ) {
			$hashes[] = substr( $hash, 0, $i );
		}
		return $hashes;
	}

	/**
	 * @param $hashes array
	 */
	static function hashesToPath( array $hashes ) {
		if ( count( $hashes ) == 0 ) {
			return '';
		} else {
			return implode( '/', $hashes ) . '/';
		}
	}

	static function getHashPathForLevel( $name, $levels ) {
		return Gl::hashesToPath( Gl::getSubhashesForLevel( $name, $levels ) );
	}

	/*** begin of generic string handling ***/

	/**
	 * Limit UTF-8 string by characters count, appending $cutSuffix when neccesary.
	 * @note: $cutSuttix has to be ASCII only.
	 */
	static function limitUtfStr( $s, $maxlen, $cutSuffix = self::CUT_SUFFIX ) {
		return mb_substr( $s, 0, $maxlen - strlen( $cutSuffix ), 'UTF-8' ) .
				$cutSuffix;
	}

	/**
	 * Limit UTF-8 string by bytes count, appending $cutSuffix when neccesary.
	 * @note: $cutSuttix has to be ASCII only.
	 */
	static function limitUtfBytes( $s, $maxbytes, $cutSuffix = self::CUT_SUFFIX ) {
		return mb_strcut( $s, 0, $maxbytes - strlen( $cutSuffix ), 'UTF-8' ) .
				$cutSuffix;
	}

	static function toUnixEOL( $s ) {
		return str_replace( array( "\r\n", "\r" ), array( "\n", "\n" ), $s );
	}

	static function hsc( $s ) {
		return htmlspecialchars( $s, ENT_COMPAT, 'UTF-8' );
	}

	static function hed( $s ) {
		$hed_opts = ENT_QUOTES;
		if ( defined( 'ENT_HTML401' ) ) {
			# is not defined in PHP 5.3.5
			$hed_opts |= ENT_HTML401;
		}
		return html_entity_decode( $s, $hed_opts, 'UTF-8' );
	}

	static function ucfirst( $s ) {
		if ( strlen( $s ) === 0 ) {
			return $s;
		}
		preg_match( '/^(.)(.*)$/u', $s, $matches );
		return mb_strtoupper( isset( $matches[1] ) ? $matches[1] : '' ) .
			(isset( $matches[2] ) ? $matches[2] : '');
	}

	static function ucFirstOnly( $s ) {
		if ( strlen( $s ) === 0 ) {
			return $s;
		}
		preg_match( '/^(.)(.*)$/u', $s, $matches );
		return mb_strtoupper( isset( $matches[1] ) ? $matches[1] : '' ) .
			mb_strtolower( isset( $matches[2] ) ? $matches[2] : '' );
	}

	/*** end of generic string handling ***/

} /* end of Gl class */
