<?php
namespace QuestPC;

class SdvTimer {

	protected $startedAt;
	protected $lastRun;
	protected $total = 0;

	public function start() {
		$this->startedAt = microtime( true );
	}

	public function restart() {
		$this->total = 0;
		$this->start();
	}

	public function stop() {
		$this->lastRun = microtime( true ) - $this->startedAt;
		$this->total += $this->lastRun;
		return $this->lastRun;
	}

	public function getLastRun() {
		return $this->lastRun;
	}

	public function getTotal() {
		return $this->total;
	}

} /* end of SdvTimer class */

class SdvProfiler {

	protected $timers = array();

	public function create( $key ) {
		if ( array_key_exists( $key, $this->timers ) ) {
			SdvException::throwError( 'Specified timer was already created', $key );
		}
		$this->timers[$key] = new SdvTimer();
	}

	public function start( $key ) {
		if ( !array_key_exists( $key, $this->timers ) ) {
			$this->timers[$key] = new SdvTimer();
		}
		$this->timers[$key]->start();
	}

	public function restart( $key ) {
		if ( !array_key_exists( $key, $this->timers ) ) {
			$this->timers[$key] = new SdvTimer();
		}
		$this->timers[$key]->restart();
	}

	public function stop( $key ) {
		if ( !array_key_exists( $key, $this->timers ) ) {
			SdvException::throwError( 'Cannot stop timer which was not previousely started', $key );
		}
		return $this->timers[$key]->stop();
	}

	public function getLastRun( $key ) {
		return array_key_exists( $key, $this->timers ) ? 
			$this->timers[$key]->getLastRun() : 0;
	}

	public function getTotal( $key ) {
		return array_key_exists( $key, $this->timers ) ?
			$this->timers[$key]->getTotal() : 0;
	}

} /* end of SdvProfiler class */
