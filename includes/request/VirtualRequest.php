<?php
namespace QuestPC;

/**
 * Warning: this class is largely untested yet, please fix when something is broken
 */
class VirtualRequest extends AbstractRequest {

	protected $server = array();
	protected $data = array();
	protected $env = array();
	protected $cookie = array();
	protected $session = array();
	protected $files = array();

	function __construct() {
		// set default cookie preferences, one may change these just after the creation of object
		$this->setCookiePreferences();
	}

	function getServer( $key ) {
		return array_key_exists( $key, $this->server ) ? $this->server[$key] : null;
	}

	function setServer( $key, $data ) {
		$this->server[$key] = $data;
	}

	function getFile( $name ) {
		SdvException::throwFatal( 'Unimplemented', __METHOD__, $name );
	}

	function setFile( $name, array $file ) {
		SdvException::throwFatal( 'Unimplemented', __METHOD__, array( $name, $file ) );
	}

	function move_uploaded_file( $filename , $destination ) {
		SdvException::throwFatal( 'Unimplemented', __METHOD__, func_get_args() );
	}

	function hasGPval( $name ) {
		return array_key_exists( $name, $this->data );
	}

	function setData( $key, $data ) {
		$this->data[strtr( $key, '_', '.' )] = $data;
	}

	/**
	 * Fetch a value from the given array or return $default if it's not set.
	 *
	 * @param $name String
	 * @param $default Mixed
	 * @return mixed
	 */
	function getGPval( $name, $default = null ) {
		# PHP is so nice to not touch input data, except sometimes:
		# http://us2.php.net/variables.external#language.variables.external.dot-in-names
		# Work around PHP *feature* to avoid *bugs* elsewhere.
		$name = strtr( $name, '.', '_' );
		if ( isset( $this->data[$name] ) ) {
			return $this->data[$name];
		}
		# taint( $default );
		return $default;
	}

	/**
	 * Get a cookie from the $_COOKIE jar
	 *
	 * @param $key String: the name of the cookie
	 * @param $prefix String: a prefix to use for the cookie name, if not $appContext->cookiePrefix
	 * @param $default Mixed: what to return if the value isn't found
	 * @return Mixed: cookie value or $default if the cookie not set
	 */
	public function getCookie( $key, $prefix = null, $default = null ) {
		SdvException::throwFatal( 'Unimplemented', __METHOD__, func_get_args() );
	}

	public function setCookie( $key, $value, $prefix = null ) {
		SdvException::throwFatal( 'Unimplemented', __METHOD__, func_get_args() );
	}

	function session_id( /* $id */ ) {
		SdvException::throwFatal( 'Unimplemented', __METHOD__, func_get_args() );
	}

	function session_name( /* $name */ ) {
		SdvException::throwFatal( 'Unimplemented', __METHOD__, func_get_args() );
	}

	function session_start() {
		SdvException::throwFatal( 'Unimplemented', __METHOD__, func_get_args() );
	}

	/**
	 * closes the session even before the script was terminated
	 * often improves the performance and slightly improves the security
	 */
	function session_write_close() {
		SdvException::throwFatal( 'Unimplemented', __METHOD__, func_get_args() );
	}

	function session_set_cookie_params( /* $lifetime [, $path [, $domain [, $secure = false [, $httponly = false ]]]] */ ) {
		SdvException::throwFatal( 'Unimplemented', __METHOD__, func_get_args() );
	}

	function session_cache_limiter( /* [ $cache_limiter ] */ ) {
		SdvException::throwFatal( 'Unimplemented', __METHOD__, func_get_args() );
	}

	/* todo: session_set_save_handler() - will we ever scale to support memcached ? */

	function getSession( $key ) {
		SdvException::throwFatal( 'Unimplemented', __METHOD__, func_get_args() );
	}

	function setSession( $key, $data ) {
		SdvException::throwFatal( 'Unimplemented', __METHOD__, func_get_args() );
	}

	function getEnv( $varname ) {
		return array_key_exists( $varname, $this->env ) ? $this->env[$varname] : false;
	}

	function setEnv( $varname, $data ) {
		$this->env[$varname] = $data;
	}

} /* end of VirtualRequest class */
