<?php
namespace QuestPC;

/**
 * Abstraction of system environment and of http request for the sdv framework
 *
 * Please never use _SERVER, _GET, _POST superglobals and php's getenv()
 * use AbstractRequest descendants instead
 *
 */
abstract class AbstractRequest extends FeaturesObject {

	protected $cookiePath;
	protected $cookieDomain;
	protected $cookieSecure;
	protected $cookieHttpOnly;
	protected $sessionHandler;

	protected static $isSingleton = true;

	/**
	 * Do not create derivative instances directly.
	 * Use ->singleton() instead.
	 */

	abstract function getServer( $key );

	abstract function setServer( $key, $data );

	abstract function getFile( $name );

	abstract function setFile( $name, array $file );

	abstract function move_uploaded_file( $filename, $destination );

	abstract function getCookie( $key, $prefix = null, $default = null );

	abstract function setCookie( $key, $value, $prefix = null );

	abstract function session_id( /* $id */);

	abstract function session_name( /* $name */ );

	abstract function session_start();

	abstract function session_write_close();

	abstract function session_set_cookie_params( /* $lifetime [, $path [, $domain [, $secure = false [, $httponly = false ]]]] */ );

	abstract function session_cache_limiter( /* [ $cache_limiter ] */ );

	abstract function getSession( $key );

	abstract function setSession( $key, $data );

	abstract function getEnv( $varname );

	abstract function setEnv( $varname, $data );

	protected function getGPval( $name, $default ) {
		SdvException::throwFatal( 'Unavailable method', __METHOD__, __CLASS__ );
	}

	/**
	 * Fetch a scalar from the input or return $default if it's not set.
	 * Returns a string. Arrays are discarded. Useful for
	 * non-freeform text inputs (e.g. predefined internal text keys
	 * selected by a drop-down menu). For freeform input, see getText().
	 *
	 * @param $name String
	 * @param $default String: optional default (or NULL)
	 * @return String
	 */
	public function getVal( $name, $default = null ) {
		$val = $this->getGPval( $name, $default );
		if ( is_array( $val ) ) {
			$val = $default;
		}
		if ( is_null( $val ) ) {
			return $val;
		}
		return (string)$val;
	}

	/**
	 * Fetch an array from the input or return $default if it's not set.
	 * If source was scalar, will return an array with a single element.
	 * If no source and no default, returns NULL.
	 *
	 * @param $name String
	 * @param $default Array: optional default (or NULL)
	 * @return Array
	 */
	public function getArray( $name, $default = null ) {
		$val = $this->getGPval( $name, $default );
		if ( is_null( $val ) ) {
			return null;
		};
		return (array)$val;
	}

	/**
	 * Fetch an array of integers, or return $default if it's not set.
	 * If source was scalar, will return an array with a single element.
	 * If no source and no default, returns NULL.
	 * If an array is returned, contents are guaranteed to be integers.
	 *
	 * @param $name String
	 * @param $default Array: option default (or NULL)
	 * @return Array of ints
	 */
	public function getIntArray( $name, $default = null ) {
		$val = $this->getArray( $name, $default );
		if ( is_array( $val ) ) {
			$val = array_map( 'intval', $val );
		}
		return $val;
	}

	/**
	 * Fetch an integer value from the input or return $default if not set.
	 * Guaranteed to return an integer; non-numeric input will typically
	 * return 0.
	 *
	 * @param $name String
	 * @param $default Integer
	 * @return Integer
	 */
	public function getInt( $name, $default = 0 ) {
		return intval( $this->getVal( $name, $default ) );
	}

	/**
	 * Fetch an integer value from the input or return null if empty.
	 * Guaranteed to return an integer or null; non-numeric input will
	 * typically return null.
	 *
	 * @param $name String
	 * @return Integer
	 */
	public function getIntOrNull( $name ) {
		$val = $this->getVal( $name );
		return is_numeric( $val )
			? intval( $val )
			: null;
	}

	/**
	 * Fetch a boolean value from the input or return $default if not set.
	 * Guaranteed to return true or false, with normal PHP semantics for
	 * boolean interpretation of strings.
	 *
	 * @param $name String
	 * @param $default Boolean
	 * @return Boolean
	 */
	public function getBool( $name, $default = false ) {
		return (bool)$this->getVal( $name, $default );
	}
	
	/**
	 * Fetch a boolean value from the input or return $default if not set.
	 * Unlike getBool, the string "false" will result in boolean false, which is
	 * useful when interpreting information sent from JavaScript.
	 *
	 * @param $name String
	 * @param $default Boolean
	 * @return Boolean
	 */
	public function getFuzzyBool( $name, $default = false ) {
		return $this->getBool( $name, $default ) && strcasecmp( $this->getVal( $name ), 'false' ) !== 0;
	}

	/**
	 * Return true if the named value is set in the input, whatever that
	 * value is (even "0"). Return false if the named value is not set.
	 * Example use is checking for the presence of check boxes in forms.
	 *
	 * @param $name String
	 * @return Boolean
	 */
	public function getCheck( $name ) {
		# Checkboxes and buttons are only present when clicked
		# Presence connotes truth, abscense false
		$val = $this->getVal( $name, null );
		return isset( $val );
	}

	/**
	 * Fetch a text string from the given array or return $default if it's not
	 * set. Carriage returns are stripped from the text, and with some language
	 * modules there is an input transliteration applied. This should generally
	 * be used for form <textarea> and <input> fields. Used for user-supplied
	 * freeform text input (for which input transformations may be required - e.g.
	 * Esperanto x-coding).
	 *
	 * @param $name String
	 * @param $default String: optional
	 * @return String
	 */
	public function getText( $name, $default = '' ) {
		$val = $this->getVal( $name, $default );
		return str_replace( "\r\n", "\n", $val );
	}

	/**
	 * Extracts the given named values into an array.
	 * If no arguments are given, returns all input values.
	 * No transformation is performed on the values.
	 */
	public function getValues() {
		$names = func_get_args();
		if ( count( $names ) == 0 ) {
			$names = array_keys( $this->data );
		}

		$retVal = array();
		foreach ( $names as $name ) {
			$value = $this->getVal( $name );
			if ( !is_null( $value ) ) {
				$retVal[$name] = $value;
			}
		}
		return $retVal;
	}

	/**
	 * Returns true if the present request was reached by a POST operation,
	 * false otherwise (GET, HEAD, or command-line).
	 *
	 * Note that values retrieved by the object may come from the
	 * GET URL etc even on a POST request.
	 *
	 * @return Boolean
	 */
	public function wasPosted() {
		return $this->getServer( 'REQUEST_METHOD' ) == 'POST';
	}

	public function setCookiePreferences( $path = '/', $domain = '', $http_only = false, $session_handler = 'files' ) {
		$this->cookiePath = $path;
		$this->cookieDomain = $domain;
		$this->cookieSecure = $this->getServer( 'HTTPS' ) == 'on';
		$this->cookieHttpOnly = $http_only;
		$this->sessionHandler = $session_handler;
	}

	/**
	 * Returns true if there is a session cookie set.
	 * This does not necessarily mean that the user is logged in!
	 *
	 * If you want to check for an open session, use session_id()
	 * instead; that will also tell you if the session was opened
	 * during the current request (in which case the cookie will
	 * be sent back to the client at the end of the script run).
	 *
	 * @return Boolean
	 */
	public function checkSessionCookie() {
		return $this->getCookie( $this->session_name(), '', null ) !== null;
	}

	/**
	 * Initialise php session
	 */
	public function setupSession( $sessionid = false ) {
		if ( $this->sessionHandler != ini_get( 'session.save_handler' ) ) {
			# Only set this if $this->sessionHandler isn't null and session.save_handler
			# hasn't already been set to the desired value (that causes errors)
			ini_set( 'session.save_handler', $this->sessionHandler );
		}
		$this->session_set_cookie_params( 0, $this->cookiePath, $this->cookieDomain, $this->cookieSecure, $this->cookieHttpOnly );
		$this->session_cache_limiter( 'private, must-revalidate' );
		if ( $sessionId ) {
			$this->session_id( $sessionId );
		}
		Gl::suppressWarnings();
		$this->session_start();
		Gl::restoreWarnings();
	}

	/**
	 * Parses the http auth header.
	 * http://php.net/manual/en/features.http-auth.php
	 */
	protected function http_digest_parse() {
		// protect against missing data
		$needed_parts = array(
			'nonce' => 1,
			'nc' => 1,
			'cnonce' => 1,
			'qop' => 1,
			'username' => 1,
			'uri' => 1,
			'response' => 1
		);
		$data = array();
		$keys = implode( '|', array_keys( $needed_parts ) );
		preg_match_all(
			'@(' . $keys . ')=(?:([\'"])([^\2]+?)\2|([^\s,]+))@',
			$this->getServer( 'PHP_AUTH_DIGEST' ),
			$matches,
			PREG_SET_ORDER
		);
		foreach ( $matches as $m ) {
			$data[$m[1]] = $m[3] ? $m[3] : $m[4];
			unset( $needed_parts[$m[1]] );
		}
		return $needed_parts ? array( 'username' => false ) : $data;
	}

	public function checkDigestAuth( $realm, $user, $password ) {
		$data = $this->http_digest_parse();
		if ( $data['username'] === $user ) {
			// generate the valid response
			$valid_response = md5( implode( ':', array(
				md5( "{$user}:{$realm}:{$password}" ),
				$data['nonce'],
				$data['nc'],
				$data['cnonce'],
				$data['qop'],
				md5( $this->getServer( 'REQUEST_METHOD' ) . ':' . $data['uri'] )
			) ) );
			if ( $data['response'] === $valid_response ) {
				return;
			}
		}
		ShutdownException::digestAuth( 'Please authorize first', __METHOD__, array( 'realm' => $realm ) );
	}

} /* end of AbstractRequest class */
