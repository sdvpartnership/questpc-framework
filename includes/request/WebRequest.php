<?php
namespace QuestPC;

/**
 * Warning: this class is largely untested yet, please fix when something is broken
 */
class WebRequest extends VirtualRequest {

	function __construct() {
		parent::__construct();
		$this->checkMagicQuotes();
		// POST overrides GET data
		// POST + GET data glued together, POST overrides GET
		// We don't use $_REQUEST here to avoid interference from cookies...
		$this->data = $_POST + $_GET;
	}

	/**
	 * Recursively strips slashes from the given array;
	 * used for undoing the evil that is magic_quotes_gpc.
	 *
	 * @param $arr array: will be modified
	 * @return array the original array
	 */
	protected function &fix_magic_quotes( &$arr ) {
		foreach( $arr as $key => $val ) {
			if( is_array( $val ) ) {
				$this->fix_magic_quotes( $arr[$key] );
			} else {
				$arr[$key] = stripslashes( $val );
			}
		}
		return $arr;
	}

	/**
	 * If magic_quotes_gpc option is on, run the global arrays
	 * through fix_magic_quotes to strip out the stupid slashes.
	 * WARNING: This should only be done once! Running a second
	 * time could damage the values.
	 */
	protected function checkMagicQuotes() {
		$mustFixQuotes = function_exists( 'get_magic_quotes_gpc' )
			&& get_magic_quotes_gpc();
		if( $mustFixQuotes ) {
			$this->fix_magic_quotes( $_COOKIE );
			$this->fix_magic_quotes( $_ENV );
			$this->fix_magic_quotes( $_GET );
			$this->fix_magic_quotes( $_POST );
			$this->fix_magic_quotes( $_REQUEST );
			$this->fix_magic_quotes( $_SERVER );
		}
	}

	function setData( $key, $data ) {
		SdvException::throwFatal( 'Method cannot be used' , __METHOD__, __CLASS__ );
	}

	function getServer( $key ) {
		return array_key_exists( $key, $_SERVER ) ? $_SERVER[$key] : null;
	}

	function setServer( $key, $data ) {
		SdvException::throwFatal( 'Method cannot be used' , __METHOD__, __CLASS__ );
	}

	function getFile( $name ) {
		return array_key_exists( $name, $_FILES ) ? $_FILES[$name] : null;
	}

	function setFile( $name, array $file ) {
		SdvException::throwFatal( 'Method cannot be used' , __METHOD__, __CLASS__ );
	}

	function move_uploaded_file( $filename, $destination ) {
		return move_uploaded_file( $filename, $destination );
	}

	/**
	 * Get a cookie from the $_COOKIE jar
	 *
	 * @param $key String: the name of the cookie
	 * @param $prefix String: a prefix to use for the cookie name, if not $appContext->cookiePrefix
	 * @param $default Mixed: what to return if the value isn't found
	 * @return Mixed: cookie value or $default if the cookie not set
	 */
	public function getCookie( $key, $prefix = null, $default = null ) {
		global $appContext;
		if( $prefix === null ) {
			$prefix = property_exists( $appContext, 'cookiePrefix' ) ? $appContext->cookiePrefix : '';
		}
		$key = $prefix . $key;
		$key = strtr( $key, '.', '_' );
		if ( isset( $_COOKIE[$key] ) ) {
			return $_COOKIE[$key];
		}
		# taint( $default );
		return $default;
	}

	public function setCookie( $key, $value, $prefix = null ) {
		global $appContext;
		if( $prefix === null ) {
			$prefix = property_exists( $appContext, 'cookiePrefix' ) ? $appContext->cookiePrefix : '';
		}
		$_COOKIE[$prefix . $key] = $value;
	}

	function session_id( /* $id */ ) {
		return call_user_func_array( 'session_id', func_get_args() );
	}

	function session_name( /* $name */ ) {
		return call_user_func_array( 'session_name', func_get_args() );
	}

	function session_start() {
		return session_start();
	}

	/**
	 * closes the session even before the script was terminated
	 * often improves the performance and slightly improves the security
	 */
	function session_write_close() {
		return session_write_close();
	}

	function session_set_cookie_params( /* $lifetime [, $path [, $domain [, $secure = false [, $httponly = false ]]]] */ ) {
		call_user_func_array( 'session_set_cookie_params', func_get_args() );
	}

	function session_cache_limiter( /* [ $cache_limiter ] */ ) {
		return call_user_func_array( 'session_cache_limiter', func_get_args() );
	}

	/* todo: session_set_save_handler() - will we ever scale to support memcached ? */

	function getSession( $key ) {
		if( isset( $_SESSION[$key] ) ) {
			return $_SESSION[$key];
		}
		return null;
	}

	function setSession( $key, $data ) {
		$_SESSION[$key] = $data;
	}

	function getEnv( $varname ) {
		return getenv( $varname );
	}

	function setEnv( $varname, $data ) {
		SdvException::throwFatal( 'Method cannot be used' , __METHOD__, __CLASS__ );
	}

} /* end of WebRequest class */
