<?php
namespace QuestPC;

/**
 * Distinctive Exception which accepts arbitrary exception code (arrays, objects).
 * Can be used to save / pass an instance of class where exception occured,
 * providing extra codes, for example:
 *   info = array( 'instance' => $this, 'code' => 'ERR_MY_ERR' )
 *
 * "SdvException" are supposed to be recoverable. Just "Exception" usually are not.
 * "SdvException" with
 *   info = array( 'code' => 'success' )
 * are supposed to be a kind of recoverable "long jump".
 */
class SdvException extends \Exception {

	protected static $tracableErrorCodes = array(
		'error', 'fatal'
	);

	protected $extendedCode;

	static function throwAny( $message, $method, $code = 'error', $data = null ) {
		$info = array( 'code' => $code, 'method' => $method );
		if ( $data !== null ) {
			$info['data'] = $data;
		}
		throw new static( $message, $info );
	}

	/**
	 * Throws an error which may be either fatal or recoverable, decided by catch(){} handler.
	 */
	static function throwError( $message, $method, $data = null ) {
		static::throwAny( $message, $method, 'error', $data );
	}

	/**
	 * Throws an error which is expected to terminate the application.
	 */
	static function throwFatal( $message, $method, $data = null ) {
		static::throwAny( $message, $method, 'fatal', $data );
	}

	/**
	 * Throws an error which is expected to be recoverable.
	 */
	static function throwRecoverable( $message, $method, $data = null ) {
		static::throwAny( $message, $method, 'recoverable', $data );
	}

	/**
	 * Throw an error which is not an error but like a kind of "long jump" in C.
	 */
	static function throwSuccess( $message, $method, $data = null ) {
		static::throwAny( $message, $method, 'success', $data );
	}

	/**
	 * By default, $info['code'] === 'error' is being thrown.
	 * Such error can be recoverable / fatal / success - anything catch() wishes.
	 * Uses use message / extendedCode from previous exception, when available and
	 * message / info has default values.
	 */
	function __construct( $message = '', $info = 0, \Exception $previous = null ) {
		# Dbg\log(__METHOD__.':info',$info);
		if ( $message === '' && $previous instanceof \Exception ) {
			$message = $previous->getMessage();
		}
		if ( $info === 0 && $previous instanceof SdvException ) {
			$info = $previous->getExtendedCode();
		}
		if ( is_array( $info ) ) {
			if ( !array_key_exists( 'code', $info ) ) {
				# set default error string
				$info['code'] = 'error';
			}
			if ( !array_key_exists( 'data', $info ) ) {
				# set empty data
				$info['data'] = null;
			}
			$this->extendedCode = $info;
		} else {
			$this->extendedCode = array( 'code' => 'error', 'data' => $info );
		}
		$this->setQuickTrace();
		if ( !is_int( $info ) ) {
			$info = 0;
		}
		# Dbg\log(__METHOD__.':extendedCode',$this->extendedCode);
		parent::__construct( $message, $info, $previous );
	}

	function setQuickTrace() {
		if ( !in_array( $this->extendedCode['code'], static::$tracableErrorCodes ) ) {
			$this->extendedCode['trace'] = "Recoverable exception trace is disabled for performance reasons.\nAdd {$this->extendedCode['code']} to static::\$tracableErrorCodes to get trace.";
			return;
		}
		$this->extendedCode['trace'] = $this->getTrace();
		# Compact trace data so it will be more easily browsable.
		foreach ( $this->extendedCode['trace'] as &$traceData ) {
			if ( array_key_exists( 'type', $traceData ) ) {
				$traceData['function'] = $traceData['class'] . $traceData['type'] . $traceData['function'];
				unset( $traceData['type'] );
				unset( $traceData['class'] );
			}
			if ( array_key_exists( 'args', $traceData ) ) {
				foreach ( $traceData['args'] as &$arg ) {
					if ( is_object( $arg ) ) {
						$arg = 'instanceof ' . get_class( $arg );
					}
				}
				try {
					$traceData['args'] = serialize( $traceData['args'] );
				} catch ( \Exception $e ) {
					# Serialization of Closure is unsupported.
				}
			}
		}
	}

	function getErrorCode() {
		return $this->extendedCode['code'];
	}

	function getData() {
		return $this->extendedCode['data'];
	}

	function setData( $data ) {
		$this->extendedCode['data'] = $data;
	}

	function getTraceJsonString() {
		return Gl::json_encode( $this->extendedCode['trace'] );
	}

	function getExtendedCode() {
		return $this->extendedCode;
	}

} /* end of SdvException class */
