<?php
namespace QuestPC;

/**
 * Represents matching route controllers with PCRE matches and query arguments
 * according to current request uri.
 */
class Route {

	public $matches;
	public $queryArgs;
	public $ctrl;
	public $fallback;

} /* end of Route class */

/**
 * PCRE request uri / path and query args router.
 */
class Router {

	protected $requestUri;
	protected $requestPath;
	protected $queryArgs;

	function setRequestUri( $requestUri = null ) {
		global $appContext;
		$this->requestUri = ($requestUri === null) ?
			$appContext->request->getServer( 'REQUEST_URI' ) : $requestUri;
		$urlParts = parse_url( $this->requestUri );
		$this->requestPath = $urlParts['path'];
		$this->queryArgs = array();
		if ( array_key_exists( 'query', $urlParts ) ) {
			parse_str( $urlParts['query'], $this->queryArgs );
		}
	}

	function getRequestUri() {
		return $this->requestUri;
	}

	/**
	 * @param $routeRules array
	 * example:
		array(
			array(
				'requestPath' => '%^/(backend_market|market)(/.*?|)/page(\d+?).html$%', // or 'requestUri';
				'ctrl' => 'MktCategoryBrowserPage',
				'queryArgs' => array( 'uid', 'pid' ), // optional GET keys to match;
				'fallback' => 'ShutdownPage', // optional;
			),
		)
	 * Get a route rule that matches current request uri.
	 * @return mixed
	 *   instanceof Route: matching route;
	 *   false: no matches;
	 */
	function getRoute( array $routeRules ) {
		# Dbg\log("requestUri",$this->requestUri,true);
		# Dbg\log("routeRules",$routeRules,true);
		foreach( $routeRules as $rule ) {
			$result = new Route();
			$ruleMatch = null;
			# Request path regexp has predescence over request uri regexp.
			if ( isset( $rule['requestPath'] ) ) {
				$ruleMatch = (bool) preg_match( $rule['requestPath'], $this->requestPath, $result->matches );
			} elseif ( isset( $rule['requestUri'] ) ) {
				$ruleMatch = (bool) preg_match( $rule['requestUri'], $this->requestUri, $result->matches );
			}
			if ( $ruleMatch !== false && isset( $rule['queryKeys'] ) ) {
				$result->queryArgs = array_intersect_key( $this->queryArgs, array_flip( $rule['queryKeys'] ) );
				Dbg\log(__METHOD__.'router queryArgs',$this->queryArgs);
				Dbg\log(__METHOD__.'rule queryKeys',$rule['queryKeys']);
				Dbg\log(__METHOD__.'matching queryArgs',$result->queryArgs);
				$ruleMatch = ( count( $result->queryArgs ) === count( $rule['queryKeys'] ) );
			}
			if ( $ruleMatch ) {
				$result->ctrl = AutoLoader::getFqnClassName( $rule['ctrl'] );
				$result->fallback = isset( $rule['fallback'] ) ?
					AutoLoader::getFqnClassName( $rule['fallback'] ) : null;
				return $result;
			}
		}
		// default path
		return false;
	}

	/**
	 * Executes a route (usually obtained via ->getRoute() call).
	 */
	function doRoute( Route $route ) {
		global $appContext;
		# PHP 5.4 only
		# $ctrl = (new \ReflectionClass( $route->ctrl ))->newInstance( $route->matches );
		# PHP 5.3
		$reflect = new \ReflectionClass( $route->ctrl );
		# check for non-fatal termination of "primary" controller
		try {
			$ctrl = $reflect->newInstance();
			$ctrl->main( $route );
		} catch ( ShutdownException $e ) {
			if ( $route->fallback === null ) {
				# there is no fallback, re-throw ShutdownException to caller
				throw $e;
			} else {
				$route->exception = $e;
				$route->pagePath = $ctrl->getPagePath();
				# use fallback controller
				$reflect = new \ReflectionClass( $route->fallback );
				$ctrl = $reflect->newInstance();
				$ctrl->main( $route );
			}
		}
		return $ctrl;
	}

} /* end of Router class */
