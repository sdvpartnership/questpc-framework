<?php
namespace QuestPC;

class ShutdownException extends SdvException {

	/**
	 * Throws an error which should be treated as application termination by catch(){} handler.
	 */
	static function terminate( $message, $method, $data = null ) {
		Dbg\log(__METHOD__.":message",$message);
		Dbg\log(__METHOD__.":method",$method);
		Dbg\log(__METHOD__.":data",$data);
		static::throwAny( $message, $method, 'termination', $data );
	}

	/**
	 * Throws an error which should be treated as redirect in http mode or
	 * application termination in cli mode by catch(){} handler.
	 */
	static function redirect( $message, $method, $data = null ) {
		static::throwAny( $message, $method, 'redirect', $data );
	}

	/**
	 * Throws an error which should be treated as digest authorization request
	 * in http mode or application termination in cli mode by carch(){} handler.
	 */
	static function digestAuth( $message, $method, $data = null ) {
		static::throwAny( $message, $method, 'digest_auth', $data );
	}

} /* end of ShutdownException class */
