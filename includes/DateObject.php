<?php
namespace QuestPC;

/**
 * Provides date storage and validation.
 */
class DateObject extends FeaturesObject {

	# list of "settable" properties
	protected static $propTypes = array(
		'year' => null,
		'month' => null,
		'day' => null,
		'hour' => null,
		'minute' => null,
		'second' => null,
	);
	protected static $propValidators = array(
		'year' => 'toIntVal',
		'month' => 'toIntVal',
		'day' => 'toIntVal',
		'hour' => 'toIntVal',
		'minute' => 'toIntVal',
		'second' => 'toIntVal',
	);
	protected static $ymdhmsPropNames;

	protected $year;
	protected $month;
	protected $day;
	protected $hour;
	protected $minute;
	protected $second;

	/**
	 * Create date from arguments supplied (ymdhms).
	 * Every next argument is optional.
	 * @param variadic
	 *   y,m,d,h,m,s
	 */
	function __construct( /* args */ ) {
		if ( !isset( static::$ymdhmsPropNames ) ) {
			static::$ymdhmsPropNames = array_keys( static::$propTypes );
		}
		$args = func_get_args();
		# Dbg\log(__METHOD__,$args);
		foreach ( $args as $key => $val ) {
			$propName = static::$ymdhmsPropNames[$key];
			$this->{$propName} = intval( $val );
		}
	}

	/**
	 * @note: Does not actually validate date properties.
	 * We use separate method to validate the date, because incomplete
	 * dates are supported.
	 */
	protected function toIntVal( &$propVal ) {
		$propVal = intval( $propVal );
		return true;
	}

	public static function fromTimestamp( $ts ) {
		return static::fromSqlDate( Gl::timestamp( Gl::TS_DB, $ts ) );
	}

	public static function fromNow() {
		return static::fromTimestamp( time() );
	}

	public static function getLocalOffset() {
		$now = new \DateTime();
		$now->setTimestamp( time() );
		return $now->getOffset();
	}

	public function offsetInstance( $tsDelta ) {
		return static::fromTimestamp( $this->getTimestamp() + $tsDelta );
	}

	public function getTimestamp( $type = Gl::TS_UNIX ) {
		return Gl::timestamp( $type, $this->getSqlDate() );
	}

	public function getSqlDate() {
		if ( !$this->isValid() ) {
			SdvException::throwError( 'Cannot convert incomplete or wrong DateObject to SQL date',
				__METHOD__, $this );
		}
		return sprintf( "%04d-%02d-%02d %02d:%02d:%02d",
			$this->year, $this->month, $this->day,
			$this->hour, $this->minute, $this->second );
	}

	/**
	 * Create date from SQL date provided
	 * @param $val string
	 *   MySQL date string
	 * @return mixed
	 *   new DateObject instance - valid SQL date
	 *   false - invalid SQL date
	 */
	public static function fromSqlDate( $val ) {
		# '2012-06-19 22:04:18'
		$matches = array();
		if ( !preg_match( '/^(\d{4})-(\d\d)-(\d\d) (\d\d):(\d\d):(\d\d)$/', $val, $matches ) ) {
			return false;
		}
		# remove "all" match
		array_shift( $matches );
		# Dbg\log(__METHOD__,$matches);
		# create instance
		$reflect = new \ReflectionClass( get_called_class() );
		return $reflect->newInstanceArgs( $matches );
	}

	/**
	 * Validate full timestamp (date+time).
	 * @note: requires all ymdhms properties to be set.
	 * @return bool
	 *   true, when the date is valid, false otherwise
	 */
	public function isValid() {
		return
			Gl::isInt( $this->hour, 'HOUR' ) !== false &&
			Gl::isInt( $this->minute, 'MINUTE' ) !== false &&
			Gl::isInt( $this->second, 'SECOND' ) !== false &&
			checkdate( $this->month, $this->day, $this->year );
	}

	public function isSameDay( DateObject $another ) {
		if ( !checkdate(
				$another->month, $another->day, $another->year
			) ) {
			SdvException::throwError( 'Supplied another date is not valid YYYYMMDD',
				__METHOD__, $another );
		}
		return
			$this->day === $another->day &&
			$this->month === $another->month &&
			$this->year === $another->year;
	}

	protected static $monthNames = array(
		1 =>  'january',
		2 =>  'february',
		3 =>  'march',
		4 =>  'april',
		5 =>  'may',
		6 =>  'june',
		7 =>  'july',
		8 =>  'august',
		9 =>  'september',
		10 => 'october',
		11 => 'november',
		12 => 'december'
	);
	public function getMonthName() {
		if ( !array_key_exists( $this->month, static::$monthNames ) ) {
			SdvException::throwError( 'Invalid month number', __METHOD__, $this );
		}
		return static::$monthNames[$this->month];
	}

	public static function fromYearMonth( $YYYYMM ) {
		if ( !ctype_digit( strval( $YYYYMM ) ) ) {
			return false;
		}
		preg_match( '%^(\d{4})(\d{2})$%', $YYYYMM, $matches );
		list( $all, $year, $month ) = $matches;
		if ( !checkdate( $month, 1, $year ) ) {
			return false;
		}
		# @note: warning - partial date
		return new static( $year, $month );
	}

	/**
	 * Generate HMS hash for incomplete YMD date to make complete unique YMDHMS timestamp.
	 */
	public function setTimeHash() {
		# Calculate fixed time hash in seconds for current date.
		$timeHash = ($this->year * 366 + $this->month * 31 + $this->day) % 86400;
		$this->hour = intval( $timeHash / 3600 );
		$this->minute = intval( ( $timeHash - $this->hour * 3600 ) / 60 );
		$this->second = intval( $timeHash - $this->hour * 3600 - $this->minute * 60 );
	}

} /* end of DateObject class */
