<?php
namespace QuestPC;

abstract class AbstractResponse extends FeaturesObject {

	protected static $isSingleton = true;

	/**
	 * Do not create the instances of AbstractRequest or it's derivatives directly.
	 * Use ->singleton() instead.
	 */
	function __construct() {
	}

	function flush( $s ) {
		$this->out( $s );
		$this->output();
	}

} /* end of AbstractResponse class */
