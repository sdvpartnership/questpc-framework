<?php
namespace QuestPC;

class VirtualResponse extends AbstractResponse {

	// output buffer
	protected $out = '';
	protected $finalOutput = '';
	protected $headers = '';

	protected static $redirectCodeValue = array(
		301 => 'Moved Permanently',
		302 => 'Found',
		303 => 'See Other',
		304 => 'Not Modified',
		305 => 'Use Proxy',
		307 => 'Temporary Redirect',
		
	);

	function setHeader( $data ) {
		$this->headers .= "$data\n";
	}

	function out( $s ) {
		if ( is_null( $this->out ) ) {
			SdvException::throwError( 'Output was already sent', __METHOD__ );
		}
		$this->out = $s;
	}

	function add( $s ) {
		if ( is_null( $this->out ) ) {
			SdvException::throwError( 'Output was already sent', __METHOD__ );
		}
		$this->out .= $s;
	}

	function output() {
		if ( is_null( $this->out ) ) {
			SdvException::throwError( 'Output was already sent', __METHOD__ );
		}
		$this->finalOutput = $this->out;
		$this->out = null;
	}

	function getFinalOutput() {
		return $this->finalOutput;
	}

	function redirect( $code, $location ) {
		$code = intval( $code );
		if ( !array_key_exists( $code, static::$redirectCodeValue ) ) {
			SdvException::throwError( 'Unknown redirect code', __METHOD__, $code );
		}
		$this->setHeader( "HTTP/1.1 {$code} " . static::$redirectCodeValue[$code] );
		$this->setHeader( "Location: {$location}" );
	}

	function digestAuthRealm( $realm ) {
		$this->setHeader( 'HTTP/1.1 401 Unauthorized' );
		$this->setHeader(
			'WWW-Authenticate: Digest realm="' . $realm .
			'",qop="auth",nonce="' . uniqid() .
			'",opaque="' . md5( $realm ) . '"'
		);
	}

	function handler404() {
		global $appContext;
		$this->setHeader( 'HTTP/1.1 404 Not Found' );
		$fileExt = Gl::getFileExt( $appContext->request->getServer( 'REQUEST_URI' ) );
		if ( !in_array( $fileExt, array( null, 'htm', 'html' ) ) ) {
			# display meaningful html page only for html-type path "file extensions"
			return;
		}
		$this->setHeader( 'Content-Type: text/html; charset=utf-8' );
		if ( ( $server_signature = $appContext->request->getServer( 'SERVER_SIGNATURE' ) ) === null ) {
			$server_signature = '';
		}
		$this->flush(
'<!DOCTYPE HTML PUBLIC "-//IETF//DTD HTML 2.0//EN">
<html><head>
<title>404 Not Found</title>
</head><body>
<h1>Not Found</h1>
<p>The requested URL ' . Gl::hsc( $appContext->request->getServer( 'REQUEST_URI' ) ) . ' was not found on this server.</p>
<hr>
' . $server_signature . '
</body></html>' );
	}

} /* end of VirtualResponse class */
