<?php
namespace QuestPC;

class WebResponse extends VirtualResponse {

	function setHeader( $data ) {
		# Dbg\log(__METHOD__,$data);
		header( $data );
	}

	function output() {
		if ( is_null( $this->out ) ) {
			SdvException::throwError( 'Output was already sent', __METHOD__ );
		}
		echo $this->out;
		$this->finalOutput = $this->out;
		$this->out = null;
	}

} /* end of WebResponse class */
