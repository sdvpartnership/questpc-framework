<?php
namespace QuestPC;

class WebDav {

	# default curl options
	protected $defaultOpts = array( 'fail' => null );

	/**
	 * @param $host string
	 *   WebDav server host
	 * @param $login string
	 *   WebWebDav server login
	 * @param $pass string
	 *   WebDav server password
	 */
	public function setHost( $host, $login, $pass ) {
		global $appContext;
		$this->defaultOpts = array(
			'connect-timeout' => CurlExec::$curlOptions['connect-timeout'],
			'user' => "{$login}:{$pass}",
			'#' => $host
		);
		if ( property_exists( $appContext, 'proxy' ) ) {
			$this->defaultOpts['proxy'] = $appContext->proxy;
		}
	}

	public function getBasicAuthHeader() {
		return 'Basic ' . base64_encode( $this->defaultOpts['user'] );
	}

	/**
	 * Executes curl with ->defaultOptions and optionally adds
	 *   replacement options and append options to these default options.
	 * @param $replaceOpts mixed
	 *   null - no replacement options
	 *   array
	 *     associative array to be merged into ->defaultOptions
	 * @param $appendOpts mixed
	 *   null - no append options
	 *   array
	 *     associative array each value has to be appended to already existing
	 *     value of ->defaultOptions
	 *     eg. '#' will append resource path to ->defaultOptions['#'] WebDav server path
	 * @throws SdvException
	 *   in case of fatal curl error
	 */
	protected function curl( $replaceOpts = null, $appendOpts = null ) {
		$opts = $this->defaultOpts;
		if ( is_array( $replaceOpts ) ) {
			$opts = array_merge( $opts, $replaceOpts );
		}
		if ( is_array( $appendOpts ) ) {
			foreach ( $appendOpts as $key => $opt ) {
				if ( !array_key_exists( $key, $opts ) ) {
					SdvException::throwFatal( 'Specified option key is not pre-defined, thus cannot be appended',
						__METHOD__,
						array( 'key' => $key, 'opt' => $opt )
					);
				}
				$opts[$key] .= $opt;
			}
		}
		# Dbg\log(__METHOD__,$opts);
		if ( CurlExec::main( $opts ) !== 0 ) {
			SdvException::throwFatal( 'Error executing webdav command', __METHOD__, $opts );
		}
	}

	public function mkCol( $path ) {
		$this->curl( array( 'request' => 'MKCOL' ), array( '#' => $path ) );
	}

	public function getFileSize( $path ) {
		$this->curl( array( 'head' => null ), array( '#' => $path ) );
		list( $status, $contentLength ) = CurlExec::getContentLength();
		Dbg\log(__METHOD__.':status',$status);
		if ( $status !== 200 ) {
			return false;
		}
		Dbg\log(__METHOD__.':contentLength',$contentLength);
		return $contentLength;
	}

	public function getFileTimestamp( $path ) {
		$this->curl( array( 'head' => null ), array( '#' => $path ) );
		list( $status, $timeStamp ) = CurlExec::getLastModified();
		Dbg\log(__METHOD__.':status',$status);
		if ( $status !== 200 ) {
			return false;
		}
		Dbg\log(__METHOD__.':timeStamp unix_ts',$timeStamp);
		return $timeStamp;
	}

	public function uploadFile( $path, $localFilePath ) {
		Dbg\log(__METHOD__.':path',$path);
		Dbg\log(__METHOD__.':localFilePath',$localFilePath );
		$this->curl( array( 'request' => 'PUT', 'upload-file' => $localFilePath ), array( '#' => $path ) );
		list( $status ) = CurlExec::getStatus();
		Dbg\log(__METHOD__.':status',$status);
		# MS Exchange returns 201 Created;
		# MochiWeb/1.0 violates by not returning response code (null)
		if ( $status === 201 || $status === null ) {
			return;
		}
		SdvException::throwError( 'WebDav PUT code is not 201 Created', __METHOD__, array( 'status' => $status, 'path' => $path ) );
	}

	/**
	 * @note: Places the whole content of resource into the memory, thus
	 * is not suitable for large files.
	 * @note: Was developed for CopyRepoJob, which is unused now;
	 * was used to remotely copy small person jpegs.
	 * 
	 */
	public function getFileContent( $path ) {
		$this->curl( null, array( '#' => $path ) );
		$output = CurlExec::getOutput();
		return (strlen( $output ) !== 0) ? $output : false;
	}

	/**
	 * @note: Should be used to download large files.
	 * @throw SdvException on error
	 * @param $path
	 *   url
	 * @return string
	 *   temporary file name
	 */
	public function download( $path ) {
		$td = Gl::tempDir();
		$tmpFile = fopen( $tmpFileName = tempnam( $td, 'download-curl-' ), 'w' );
		# Dbg\log('temp nam',$tmpFileName);
		$this->curl( array( 'output' => $tmpFileName ), array( '#' => $path ) );
		fclose( $tmpFile );
		return $tmpFileName;
	}

	public function delete( $path ) {
		$this->curl( array( 'request' => 'DELETE' ), array( '#' => $path ) );
		list( $status ) = CurlExec::getStatus();
		Dbg\log(__METHOD__.':status',$status);
		# MS Exchange returns 200 Ok;
		# MochiWeb/1.0 violates by not returning response code (null)
		return  $status === 200 || $status === null;
	}

} /* end of WebDav class */
