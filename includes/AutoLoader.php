<?php
namespace QuestPC;

# Locations of core classes
# This array is a global instead of a static member of AutoLoader to work around a bug in APC

$appContext->autoloadClasses = array(
	# ctrl / page
	'AbstractPage' => 'ctrl/AbstractPage.php',
	# ctrl / field
	'AbstractField' => 'ctrl/FieldControllers.php',
	'CheckboxField' => 'ctrl/FieldControllers.php',
	'FileField' => 'ctrl/FieldControllers.php',
	'HiddenField' => 'ctrl/FieldControllers.php',
	'RadioField' => 'ctrl/FieldControllers.php',
	'RangeField' => 'ctrl/FieldControllers.php',
	'ScalarField' => 'ctrl/FieldControllers.php',
	'SingleField' => 'ctrl/FieldControllers.php',
	# database
	'AbstractPager' => 'db/AbstractPager.php',
	'CategoryPager' => 'db/CategoryPager.php',
	'Dbw' => 'db/Dbw.php',
	'DbPager' => 'db/DbPager.php',
	'DbTableAlias' => 'db/DbwParams.php',
	'Dbw2dFieldSet' => 'db/DbwParams.php',
	'DbwFieldSet' => 'db/DbwParams.php',
	'DbwParams' => 'db/DbwParams.php',
	'Schema' => 'db/Schema.php',
	'SchemaCreator' => 'db/SchemaCreator.php',
	'UpdateFieldSet' => 'db/DbwParams.php',
	# includes
	'CurlExec' => 'includes/CurlExec.php',
	'DateObject' => 'includes/DateObject.php',
	'FeaturesObject' => 'includes/FeaturesObject.php',
	'Gl' => 'includes/Global.php',
	'ResourceStreamer' => 'includes/ResourceStreamer.php',
	'Route' => 'includes/Router.php',
	'Router' => 'includes/Router.php',
	'SdvTimer' => 'includes/SdvProfiler.php',
	'SdvProfiler' => 'includes/SdvProfiler.php',
	'WebDav' => 'includes/WebDav.php',
	# includes / exception handling
	'SdvException' => 'includes/SdvException.php',
	'ShutdownException' => 'includes/ShutdownException.php',
	# jobs / cron class
	'ArchiveExpiredModelsJob' => 'jobs/ArchiveExpiredModelsJob.php',
	'BaseJob' => 'jobs/BaseJob.php',
	'CronTimeParts' => 'jobs/CronPeriod.php',
	'CronPeriod' => 'jobs/CronPeriod.php',
	'CronExec' => 'jobs/CronExec.php',
	'DeletePendingModelsJob' => 'jobs/DeletePendingModelsJob.php',
	'VirtualJob' => 'jobs/BaseJob.php',
	# language
	'Inflections' => 'language/Inflections.php',
	'InflectionsCollection' => 'language/InflectionsCollection.php',
	# model
	'AbstractModel' => 'model/AbstractModel.php',
	'AbstractModelCollection' => 'model/AbstractModelCollection.php',
	'AbstractUrlDescription' => 'model/AbstractUrlDescription.php',
	'AbstractUrlSource' => 'model/AbstractUrlSource.php',
	# model / category trees
	'AbstractTreeMap' => 'model/AbstractTreeMap.php',
	'CategoryTree' => 'model/CategoryTree.php',
	'RtIndexCategoryTree' => 'model/RtIndexCategoryTree.php',
	# model / field
	'AbstractFieldDataSource' => 'model/FieldDataSources.php',
	'CheckboxFieldDataSource' => 'model/FieldDataSources.php',
	'FileFieldDataSource' => 'model/FieldDataSources.php',
	'SingleFieldDataSource' => 'model/FieldDataSources.php',
	'ScalarFieldDataSource' => 'model/FieldDataSources.php',
	'RangeFieldDataSource' => 'model/FieldDataSources.php',
	# model / form
	'AbstractForm' => 'model/AbstractForm.php',
	'AbstractSearchForm' => 'model/AbstractSearchForm.php',
	'ModelEditForm' => 'model/ModelEditForm.php',
	# parser
	'WikiText' => 'parser/wp-mediawiki.php',
	# request
	'AbstractRequest' => 'includes/request/AbstractRequest.php',
	'VirtualRequest' => 'includes/request/VirtualRequest.php',
	'WebRequest' => 'includes/request/WebRequest.php',
	# response
	'AbstractResponse' => 'includes/response/AbstractResponse.php',
	'VirtualResponse' => 'includes/response/VirtualResponse.php',
	'WebResponse' => 'includes/response/WebResponse.php',
	# includes / xml
	'XmlTree' => 'includes/Xml/XmlTree.php',
	'GenericFeedGenerator' => 'includes/Xml/GenericFeedGenerator.php',
	# includes / xml / reader
	'XmlNode' => 'includes/Xml/Reader/XmlNode.php',
	'XmlOneLevelParser' => 'includes/Xml/Reader/XmlOneLevelParser.php',
	'XmlReaderContext' => 'includes/Xml/Reader/XmlReaderContext.php',
	'XmlReaderParser' => 'includes/Xml/Reader/XmlReaderParser.php',
	# includes / xml / writer
	'GenericXmlWriter' => 'includes/Xml/Writer/GenericXmlWriter.php',
	'HtmlCollectionWriter' => 'includes/Xml/Writer/HtmlCollectionWriter.php',
	'RssCollectionWriter' => 'includes/Xml/Writer/RssCollectionWriter.php',
	'XmlCollectionWriter' => 'includes/Xml/Writer/XmlCollectionWriter.php',
	# view
	'CategoryLinksView' => 'view/CategoryLinksView.php',
	'PagerPageView' => 'view/PagerPageView.php',
	'HtmlPageView' => 'view/HtmlPageView.php',
	# view / mixin
	'TabularItemsViewMix' => 'view/mixin/TabularItemsViewMix.php',
	# view / field
	'AbstractChoiceEdit' => 'view/FieldEditGenerators.php',
	'CheckboxChoiceEdit' => 'view/FieldEditGenerators.php',
	'CompoundFieldEdit' => 'view/FieldEditGenerators.php',
	'HiddenChoiceEdit' => 'view/FieldEditGenerators.php',
	'RadioChoiceEdit' => 'view/FieldEditGenerators.php',
	'RangeChoiceEdit' => 'view/FieldEditGenerators.php',
	'ScalarChoiceEdit' => 'view/FieldEditGenerators.php',
	# view / form
	'AccordeonFormView' => 'view/AccordeonFormView.php',
	'FormView' => 'view/FormView.php',
);

/**
 * Used to indicate no value whenever null is valid value (eg. for SQL).
 * The class is really tiny so it is always loaded without using autoloader.
 */
class NoValue {
} /* end of NoValue class */

/**
 * Defines autoloading handler for whole QuestPC framework.
 */
class AutoLoader {

	/**
	 * @return string
	 *   class name without namespace prefix;
	 */
	static public function getBaseClassName( $className ) {
		return basename( str_replace( '\\', DIRECTORY_SEPARATOR, $className ) );
	}

	/**
	 * Get namespace-prefixed fully-qualified class name.
	 * @note: Only non-prefixed class names will be made prefixed.
	 *
	 * @param $className string
	 * @param $namespace mixed
	 *   string: namespace;
	 *   null: use $appContext->localNamespace;
	 * @return string
	 *   fully-qualified class name;
	 */
	static public function getFqnClassName( $className, $namespace = null ) {
		global $appContext;
		if ( $namespace === null ) {
			$namespace = $appContext->localNamespace;
		}
		$classPath = explode( '\\', $className );
		# Dbg\log(__METHOD__,$classPath);
		return ( count( $classPath ) === 1 ) ?
			"{$namespace}\\{$className}" :
			$className;
	}

	/**
	 * autoload - take a class name and attempt to load it
	 *
	 * @param $classNameName string
	 *   fully-qualified name of class we're looking for;
	 * @return bool
	 *   Returning false is important on failure as it allows Zend to
	 *   try and look in other registered autoloaders as well.
	 */
	static public function autoload( $fullClassName ) {
		global $appContext;
		$classPath = explode( '\\', $fullClassName );
		# Dbg\log(__METHOD__.':classPath',$classPath);
		# Dbg\backtrace(true);
		if ( count( $classPath ) === 2 ) {
			list( $namespace, $className ) = $classPath;
		} else {
			# Dbg\log(__METHOD__,'QuestPC framework assumes 2-level namespaces, got:' . $fullClassName);
			return false;
		}
		# Will make an absolute path, this improves performance by avoiding some stat calls.
		$fileName = null;
		if ( $namespace === 'QuestPC' ) {
			if ( isset( $appContext->autoloadLocalClasses[$fullClassName] ) ) {
				# Load local override of framework class.
				$fileName = "{$appContext->IP}/" . $appContext->autoloadLocalClasses[$fullClassName];
			} elseif ( isset( $appContext->autoloadClasses[$className] ) ) {
				# Load framework class.
				$fileName = "{$appContext->frmDir}/" . $appContext->autoloadClasses[$className];
			}
		} elseif ( $namespace === $appContext->localNamespace ) {
			if ( isset( $appContext->autoloadLocalClasses[$className] ) ) {
				# Load CMS class.
				$fileName = "{$appContext->IP}/" . $appContext->autoloadLocalClasses[$className];
			}
		}
		# Dbg\log(__METHOD__.':fileName',$fileName);
		if ( $fileName === null ) {
			if( function_exists( 'Dbg\\log' ) ) {
				Dbg\log( "Class {$fullClassName} not found; skipped loading\n", 1 );
			}
			# Give up
			return false;
		}
		if ( !file_exists( $fileName ) ) {
			throw new \Exception( "Cannot load class {$fullClassName} from {$fileName}" );
		}
		require( $fileName );
		return true;
	}

}

spl_autoload_register( __NAMESPACE__ . '\AutoLoader::autoload' );
