<?php
namespace QuestPC;

/**
 * @todo: Support merging of CSS files.
 */
class ResourceStreamer {

	# How many seconds should pass to purged re-merge of source scripts.
	const expirationTime = 86400;

	public static $jQueryUrl;

	protected $resourceGroups = array();

	public function addResourceGroup( $groupName, array &$groupDef ) {
		# validate $groupDef
		foreach ( $groupDef as $target => $source ) {
			if ( is_int( $target ) ) {
				if ( !is_string( $source ) ) {
					SdvException::throwError( 'Integer keys of groupDef must have string value',
						__METHOD__,
						array( 'key' => $target, 'source' => $source )
					);
				}
			} elseif ( !is_array( $source ) ) {
				SdvException::throwError( 'String keys of groupDef must have array value',
					__METHOD__,
					array( 'key' => $target, 'source' => $source )
				);
			} else {
				foreach ( $source as $script ) {
					if ( !is_string( $script ) ) {
						SdvException::throwError( 'Source definition scripts must be strings',
							__METHOD__,
							array( 'key' => $target, 'source' => $source )
						);
					}
				}
			}
		}
		$this->resourceGroups[$groupName] = $groupDef;
	}

	protected function isHasToMerge( array &$source, $target ) {
		global $appContext;
		$targetFSpath = "{$appContext->IP}{$target}";
		$targetStat = @stat( $targetFSpath );
		if ( $targetStat === false ) {
			# Target does not exists, merging of source scripts is required.
			return true;
		} else {
			$targetAge = time() - $targetStat['mtime'];
			if ( abs( $targetAge ) > self::expirationTime ) {
				# Purged re-merge for safety once per day,
				# because comparing timestamps and sizes does not provide
				# integrity and comparsion of sha1 is too slow.
				return true;
			} else {
				$sourceTotalSize = 0;
				# Merged target exists, compare source timestamps and target timestamp.
				foreach ( $source as $script ) {
					$sourceFSpath = "{$appContext->IP}{$script}";
					$sourceStat = @stat( $sourceFSpath );
					if ( $sourceStat === false ) {
						SdvException::throwError( 'Source script file does not exists',
							__METHOD__,
							$sourceFSpath
						);
					}
					if ( $sourceStat['mtime'] > $targetStat['mtime'] ) {
						# Source script is newer than current merged targed.
						# Has to re-merge.
						return true;
					}
					$sourceTotalSize += $sourceStat['size'];
				}
				if ( $sourceTotalSize !== $targetStat['size'] ) {
					# Total size of sources is not equal to size of
					# current merged targed. Has to re-merge.
					return true;
				}
			}
		}
		return false;
	}

	protected function mergeSources( array &$source, $target ) {
		global $appContext;
		$targetFSpath = "{$appContext->IP}{$target}";
		$td = Gl::tempDir();
		$tmpFile = fopen( $tmpTargetName = tempnam( $td, 'resource-streamer-' ), 'w' );
		foreach ( $source as $script ) {
			$sourceFSpath = "{$appContext->IP}{$script}";
			$sourceScript = @file_get_contents( $sourceFSpath );
			if ( $sourceScript === false ) {
				fclose( $tmpFile );
				SdvException::throwError( 'Cannot read source script. Check permissions / errors.',
					__METHOD__,
					$sourceFSpath
				);
			}
			$writeResult = @fwrite( $tmpFile, $sourceScript );
			if ( $writeResult === false || $writeResult !== strlen( $sourceScript ) ) {
				fclose( $tmpFile );
				SdvException::throwError( 'Error writing temporary target script file.',
					__METHOD__,
					array(
						'target' => $target,
						'tmpTargetName' => $tmpTargetName,
						'writeResult' => $writeResult,
					)
				);
			}
		}
		fclose( $tmpFile );
		$targetMoved = true;
		if ( !@rename( $tmpTargetName, $targetFSpath ) ) {
			# Make sure target directory exists.
			@mkdir( dirname( $targetFSpath ) );
			/**
			 * There are reports that rename() in Windows may fail when target exists.
			 * In older versions of PHP (maybe also depends on user rights?).
			 */
			@unlink( $targetFSpath );
			$targetMoved = @rename( $tmpTargetName, $targetFSpath );
		}
		if ( !$targetMoved ) {
			SdvException::throwError( 'Error moving temporary target script file.',
				__METHOD__,
				array(
					'targetFSpath' => $targetFSpath,
					'tmpTargetName' => $tmpTargetName,
				)
			);
		}
	}

	protected function updateTarget( array &$source, $target ) {
		if ( $this->isHasToMerge( $source, $target ) ) {
			$this->mergeSources( $source, $target );
		}
	}

	public function merge( $groupName ) {
		global $appContext;
		$result = array();
		if ( !array_key_exists( $groupName, $this->resourceGroups ) ) {
			SdvException::throwError( 'Unknown resougce group name', __METHOD__, $groupName );
		}
		foreach ( $this->resourceGroups[$groupName] as $target => $source ) {
			if ( is_int( $target ) ) {
				# Add single $source w/o merging.
				$result[] = $source;
			} else {
				if ( $appContext->debugResourceStreamer ) {
					# Debug mode.
					# Do not merge scripts, just append source scripts to html/head.
					foreach ( $source as $script ) {
						$result[] = $script;
					}
				} else {
					$this->updateTarget( $source, $target );
					$result[] = $target;
				}
			}
		}
		return $result;
	}

	function __construct( array $resources ) {
		foreach ( $resources as $groupName => $groupDef ) {
			$this->addResourceGroup( $groupName, $groupDef );
		}
	}

} /* end of ResourceStreamer class */
