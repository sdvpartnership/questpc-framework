<?php
namespace QuestPC;
/**
 * Uses PHP 5.3:
 *   Namespaces, closures, late static binding, built-in json_encode(), SPL.
 * Todo for PHP 5.5:
 *   Convert __get() mixins to Closure::bindTo() mixins;
 *   Convert FeaturesObject to trait.
 */

if ( version_compare( PHP_VERSION, '5.3.3', '<' ) ) {
	# PHP 5.3.0 to 5.3.2 has some nasty bugs.
	die( 'Minimal supported PHP version is 5.3.3' );
}

if ( !isset( $appContext ) ) {
	die( 'Undefined application context.' );
}

/**
 * Check required core classes and functions.
 */
call_user_func( function () {
	$check_cb = array(
		# type => check_fn
		'class' => 'class_exists',
		'definition' => 'defined',
		'extension' => 'extension_loaded',
		'function' => 'function_exists'
	);
	$entries = array(
		# id => type
		'DOMDocument' => 'class',
		'SplDoublyLinkedList' => 'class',
		'spl_autoload_register' => 'function',
		'mb_internal_encoding' => 'function',
		'mb_strlen' => 'function',
		'mb_strtolower' => 'function',
		'gzopen' => 'function',
#		'istainted' => 'function'
	);
	foreach ( $entries as $entry => $type ) {
		if ( !call_user_func( $check_cb[$type], $entry ) ) {
			die( "Required {$type} is not found: {$entry}" );
		}
	}
} );

mb_internal_encoding( 'UTF-8' );

require_once( "{$appContext->frmDir}/includes/SdvProfiler.php" );
$appContext->profiler = new SdvProfiler();
$appContext->profiler->start('request');
$appContext->debugging = true;
$appContext->debugCurl = true;
$appContext->debugProfiler = true;
$appContext->debugSQL = true;
$appContext->debugResourceStreamer = true;
$appContext->forceExternalCurl = false;

if ( !property_exists( $appContext, 'IP' ) ) {
	# Installation path. NIX slashes only. Does not have trailig slash.
	$appContext->IP = rtrim( str_replace( '\\', '/', call_user_func( function () {
		# Full path to working directory.
		# Makes it possible to have effective exclude path in apc.
		# Also doesn't break installations using symlinked includes, like
		# __DIR__ would do.
		$IP = false;
		# getEnv is bad, may break virtual hosts, thus commented out:
		# $IP = getenv( 'SDV_INSTALL_PATH' );

		if ( isset( $_SERVER ) && array_key_exists( 'DOCUMENT_ROOT', $_SERVER ) ) {
			$IP = $_SERVER['DOCUMENT_ROOT'];
			if ( file_exists( "$IP/sdv_engine.txt" ) ) {
				# echo 'from document root:' . $IP;
				return $IP;
			}
		}

		$IP = realpath( '.' );
		do {
			if ( file_exists( "$IP/sdv_engine.txt" ) ) {
				# echo 'from realpath:' . $IP;
				return $IP;
			}
			$prevIP = $IP;
			$IP = dirname( $IP );
		} while ( $prevIP !== $IP );

		$IP = __DIR__;

		if ( !file_exists( "$IP/sdv_engine.txt" ) ) {
			# note: cannot throw SdvException here, no autoloader yet
			throw new \Exception( 'Cannot find $IP (installation path) in ' . __METHOD__ );
		}
		# echo 'from dirname:' . $IP;
		return $IP;
	} ) ), '/' );
}

$appContext->validUrlSchemes = array(
	'http' => null, 'https' => null
);

# sdv_*() default log directory
$appContext->sdvDebugLog = "{$appContext->IP}/sdv.out";

# Default value for chmoding of new directories.
$appContext->directoryMode = 0770;
# Default value for chmoding of new files.
$appContext->fileMode = 0640;

$appContext->autoloadLocalClasses = array();

if ( php_sapi_name() === 'cli' ) {
	$appContext->scriptName = array_shift( $argv );
	$appContext->argv = $argv;
} else {
	$appContext->scriptName = basename( __FILE__ );
	$appContext->argv = array();
}

if ( gethostname() === 'cnit19' ) {
	$appContext->proxy = 'http://10.0.0.78:3128';
}

require_once( "{$appContext->frmDir}/sdv_debug.php" );
require_once( "{$appContext->frmDir}/includes/AutoLoader.php" );

// Convert stdClass $appContext to FeaturesObject $appContext.
// Requires for $appContext->lazyLoad() to work.
$appContext = FeaturesObject::fromStdClass( $appContext );

require_once( "{$appContext->IP}/LocalContext.php" );
